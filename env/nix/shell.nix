let
  rev = "f1682a7f126d4d56dfbb96bb8c8c5582abb22828";
  nixpkgs = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs-channels/archive/${rev}.tar.gz";
    sha256 = "0drjfmkh83i8acf68xifa49hkybw7cv1wxyw1wifsm557b21p0jg";
  };
in
with (import nixpkgs {});

let
  sandbox-exec = callPackage pkgs/sandbox-exec.nix {};
  util-linux   = callPackage pkgs/util-linux.nix   {};
  g_plus_plus  = callPackage pkgs/g_plus_plus.nix  {};
  open         = callPackage pkgs/open.nix         {};
in

mkShell rec {
  name = "smartpy";

  buildInputs = [
    sandbox-exec  # Needed for 'opam'.
    util-linux    # Needed for 'setsid'.
    g_plus_plus   # Some opam packages have hardwired references to g++.
    open          # Needed for 'open' in Makefile's 'www' target

    asciidoctor
    bash
    binutils
    coreutils
    curl
    gettext
    git
    gmp
    hidapi
    libev
    m4
    ncurses
    nix
    nixpkgs
    nodejs
    ocaml
    opam
    perl
    pkgconfig
    python3
    rsync
    unixtools.netstat
    which
    xdg_utils
  ];

  OCAMLPARAM="_,ccopt=-Wno-unused-command-line-argument";

  shellHook = ''
    # Allow 'opam' to run 'curl':
    export SSL_CERT_FILE="$NIX_SSL_CERT_FILE"

    # Source opam environment, if any:
    [ -d _opam ] && eval $(opam env)
  '';
}
