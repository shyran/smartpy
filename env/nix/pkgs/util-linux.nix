{ stdenv, fetchurl, lib }:

stdenv.mkDerivation rec {
  pname = "util-linux";
  version = "2.33.2";
  src = fetchurl {
    url = "mirror://kernel/linux/utils/util-linux/v${lib.versions.majorMinor version}/${pname}-${version}.tar.xz";
    sha256 = "15yf2dh4jd1kg6066hydlgdhhs2j3na13qld8yx30qngqvmfh6v3";
  };
  configureFlags = [
    "--disable-ipcrm"
    "--disable-ipcs"
    "--disable-use-tty-group"
    "--disable-libfdisk"
    "--disable-libblkid"
    "--disable-libuuid"
    "--disable-libsmartcols"
  ];
}
