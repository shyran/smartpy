{ stdenv }:

stdenv.mkDerivation {
  name = "sandbox-exec";
  src = /usr/bin/sandbox-exec;
  unpackPhase = "true";
  buildPhase = "true";
  installPhase = "mkdir -p $out/bin && ln -s $src $out/bin/sandbox-exec";
}
