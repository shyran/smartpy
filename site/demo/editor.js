// Copyright 2019-2020 Smart Chain Arena LLC.
$(document).ready(() => {
    $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
  })

if (document.getElementById("editorDiv") != undefined) {
    editor = ace.edit("editorDiv");
    setTheme(editorMode());
    editor.session.setMode("ace/mode/python");
    editor.session.setUseWrapMode(true);
    editor.session.setUseSoftTabs(true);
    editor.session.setTabSize(4);
    editor.setAutoScrollEditorIntoView(true);
    editor.setShowPrintMargin(false);

    editor.commands.addCommand({
        name: 'showShortcuts',
        bindKey: {"win": "Shift-Ctrl-h"},
        exec: function(editor) {
            try{showShortcuts("win");}
            catch(error){showErrorPre(error);}
        }
    });

    editor.commands.addCommand({
        name: 'showShortcuts',
        bindKey: {"mac": "Shift-Cmd-h"},
        exec: function(editor) {
            try{showShortcuts("mac");}
            catch(error){showErrorPre(error);}
        }
    });

    var langTools = ace.require("ace/ext/language_tools");
    editor.setOptions({enableBasicAutocompletion : true});
}

function editorMode() {
    return window.localStorage.getItem('theme', 'light')
}

function setTheme(theme) {
    editor.setTheme(theme === 'dark' ? "ace/theme/monokai" : "ace/theme/chrome");
    window.localStorage.setItem('theme', theme);
    setThemeClass(theme)
}

function cleanOutputPanel() {
    if (document.getElementById("outputPanel") != undefined) {
        outputPanel.classList.add("empty-output")
        outputPanel.innerHTML = "<span>Output Panel</span>"
    }
}

function setOutput(s) {
    outputPanel.classList.remove("empty-output")
    outputPanel.innerHTML = s;
    initDefaultButtons();
}

function addOutput(s){
    outputPanel.innerHTML += s;
    initDefaultButtons();
}

function cleanAll(){
    $("#dynamicButtons button").prop('disabled', true);
    $('#dynamicButtons ul').empty();
    cleanOutputPanel();
}

function addButton(name, f){

    $("#dynamicButtons ul").append("<li><button class='dropdown-item' onclick=\'evalTestClick(\"" + name + "\")\'>" + name + "</button></li>");
    $("#dynamicButtons button").removeAttr('disabled');
}

function evalTestClick(test){
    try{
        window.evalTest(test);
    }
    catch(error){
        showTraceback(error, "" + error);
    }
}

function evalButtonClick(autoTests){
    try{
        localStorage.setItem('saved_last', editor.getValue());
        if (window.evalRun == undefined) {
            showErrorPre("Editor not fully initialized yet.\nPlease wait for a few seconds and start again.");
        }
        else {
            window.evalRun(autoTests);
        }
    }
    catch(error){
        try{
            showTraceback(error, "" + error);
        }
        catch(_error){
            showErrorPre("" + error);
        }
    }
}

function changeLayout(layout) {
    const currentClass = $('main').attr('class');
    $('main').removeClass(currentClass).addClass(layout);

    editor.resize();
}

function switchView(){
    const classes = ['layout-side-by-side', 'layout-output-only', 'layout-editor-only', 'layout-stacked']
    const currentClass = $('main').attr('class')
    const newClassName = classes.indexOf(currentClass) === classes.length - 1
            ? classes[0]
            : classes[classes.indexOf(currentClass) + 1];

    changeLayout(newClassName)
}


function getEditorFromLocalStorage(name) {
    editor.setValue(localStorage.getItem('saved_' + name) || '');
    editor.selection.moveCursorToPosition({row: 0, column: 0});
    editor.setAutoScrollEditorIntoView(true);
}

function loadTemplate(name) {
    setTemplateAjax(name, false);
}

function toggleHamburger() {
    document.getElementById("hamburger").classList.toggle("show");
}

function selectNextTemplate(test) {
    var lastTemplate = localStorage.getItem('lastTemplate') || '';
    var i;
    for (i = 0; i < nextTemplateList.length - 2; i++) {
        if (nextTemplateList[i] == lastTemplate) {
            setTemplateAjax(nextTemplateList[i + 1], test);
        }
    }
}

function selectCurrentTemplate(test) {
    var lastTemplate = localStorage.getItem('lastTemplate') || '';
    if (lastTemplate != '') {
        setTemplateAjax(lastTemplate, test);
    }
}

function parseParameters(url) {
  parseParameters_(document.URL)
}

function parseParameters_(url) {
    var parser = document.createElement('a'),
        searchObject = {},
        split, i;
    parser.href = url;
    var queries = parser.search.replace(/^\?/, '').split('&');
    for( i = 0; i < queries.length; i++ ) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
    }
    var template = searchObject['template'];
    var code = searchObject['code'];
    var test = searchObject['test'] == "true";
    if (template) {
        setTemplateAjax(template, test);
    }
    if (code) {
        var decoded = pako.inflate(atob(code.replace(/@/g, '+').replace(/_/g, '/').replace(/-/g, '=')), { to: 'string' });
        editor.setValue(decoded);
        editor.selection.moveCursorToPosition({row: 0, column: 0});
        editor.setAutoScrollEditorIntoView(true);
    }
}

function hashCode(str) {
  return str.split('').reduce((prevHash, currVal) =>
    (((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
}

function gotoOrigination(storageCode, contractCode, storageCodeJson, contractCodeJson, storageCodeHtml, contractCodeHtml, storageCodeJsonHtml, contractCodeJsonHtml) {
    var href = window.location.href;
    var dir = href.substring(0, href.lastIndexOf('/')) + "/";
    var target = {'storage': storageCode, 'contract': contractCode, 'storageJson': storageCodeJson, 'contractJson': contractCodeJson,
                  'storageHtml': storageCodeHtml, 'contractHtml': contractCodeHtml, 'storageJsonHtml': storageCodeJsonHtml, 'contractJsonHtml': contractCodeJsonHtml};
    console.log(target);
    var target = JSON.stringify(target);
    var key = hashCode(target);
    sessionStorage.setItem(key.toString(), pako.deflate(target, { to: 'string' }));
    window.open(dir + "origination.html?key="+key);
}

function showLine(line) {
    editor.resize(true);
    editor.scrollToLine(line, true, true, function () {});
    editor.gotoLine(line, 0, true);
    editor.setAutoScrollEditorIntoView(true);
    editor.focus();
}

contractNextId = 0;
function nextId() {
    var result = contractNextId;
    contractNextId += 1;
    return result;
}
