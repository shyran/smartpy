# Copyright 2019-2020 Smart Chain Arena LLC.

SOURCES:=$(shell find  ./smartML/ \( ! -name ".\#*" \) -name "*.ml" -o -iname "*.mli")

GIT_HOOKS=pre-commit pre-push

all: .phony smartML/build_ok

init:
	./env/current/init

distclean:
	rm -rf _build _opam env/current env/{naked,nix}/_opam package{,_lock}.json

full: .phony
	$(MAKE)
	$(MAKE) test-quick
	$(MAKE) doc
	$(MAKE) referenceManual
	@echo -e '\n\n\n'
	@echo -e 'To launch full tests:\n ./with_env make test'
	@echo -e 'To launch a webserver:\n  ./with_env make www'

clean: .phony
	dune clean

SMARTML_EXE=_build/default/smartML/app/smartml.exe

smartML/build_ok: $(SOURCES)
	@rm -f smartML/build_ok
	dune build smartML/smart-ml-js/smartmljs.bc.js smartML/app/smartml.exe smartML/test/main.exe
	@$(MAKE) fmt-check
	@touch smartML/build_ok

smartML.loop: .phony $(SMARTML_EXE)
	dune build smartML/smart-ml-js/smartmljs.bc.js smartML/app/smartml.exe -w

PORT=8000
www: .phony
	(sleep 1; open http://localhost:$(PORT)/demo) &
	cd site && python3 -m http.server $(PORT)

fmt: .phony
	@ocamlformat --inplace $(SOURCES)

fmt-check: .phony
	ocamlformat --check $(SOURCES)

build: .phony smartML/build_ok

-include python/test/compile.mk
-include python/test/scenario.mk

python/test/compile.mk: smartML/test/basic-app-test.sh
	smartML/test/basic-app-test.sh prepare compile

python/test/scenario.mk: smartML/test/basic-app-test.sh
	smartML/test/basic-app-test.sh prepare scenario

python/test/scenario/%.ok: smartML/build_ok python/templates/%.py
	@./test_wrapper python/test/scenario/$* python/SmartPy.sh test python/templates/$*.py python/test/scenario/$*

smartML/test/value-pack_ok: smartML/build_ok smartML/test/value-pack.sh
	@./test_wrapper smartML/test/value-pack smartML/test/value-pack.sh smartML/test/value-pack
	@touch smartML/test/value-pack_ok

test-clean: .phony smartML/build_ok
	smartML/test/basic-app-test.sh clean
	@$(MAKE) test-compile-purge
	@$(MAKE) test-scenario-purge
	rm -f smartML/build_ok
	rm -f smartML/test/value-pack_ok
	rm -f doc/all_ok

test-build: .phony smartML/build_ok
	@dune build smartML/test/main.exe

test-unit: .phony smartML/build_ok
	@dune build @smartML/test/runtest

test-app: .phony smartML/build_ok
	smartML/test/basic-app-test.sh run

michelsons: .phony smartML/build_ok
	smartML/test/basic-app-test.sh michelsons

test-prepare:
	smartML/test/basic-app-test.sh prepare

test-compile-purge: .phony
	rm -rf python/test/compile

test-scenario-purge: .phony
	rm -rf python/test/scenario

python/test/compile/all_ok: smartML/build_ok $(COMPILE_OK)
	@touch python/test/compile/all_ok

python/test/scenario/all_ok: smartML/build_ok $(SCENARIO_OK)
	@touch python/test/scenario/all_ok

test-compile-incremental: .phony
	@$(MAKE) python/test/compile/all_ok

test-scenario-incremental: .phony
	@$(MAKE) python/test/scenario/all_ok

test-compile: .phony
	$(MAKE) test-compile-purge
	$(MAKE) test-compile-incremental

test-scenario: .phony
	$(MAKE) test-scenario-purge
	$(MAKE) test-scenario-incremental

test-value-pack: .phony
	@$(MAKE) smartML/test/value-pack_ok

test: .phony test-quick test-app

test-quick: .phony
	@$(MAKE) smartML/build_ok
	@$(MAKE) test-scenario
	@$(MAKE) test-compile
	@$(MAKE) test-unit
	@$(MAKE) test-value-pack
	git status

test-quick-incremental: .phony
	@$(MAKE) smartML/build_ok
	@$(MAKE) test-scenario-incremental
	@$(MAKE) test-compile-incremental
	@$(MAKE) test-unit
	git status

incremental: .phony
	@$(MAKE) fmt
	@$(MAKE) doc/all_ok
	@$(MAKE) test-quick-incremental

git-hooks-install:
	for h in $(GIT_HOOKS); do ln -s ../../.githooks/$$h .git/hooks || :; done

.phony:

.PHONY: .phony

doc/all_ok: doc/*.md
	asciidoctor doc/reference.md      -a toc -a linkcss --destination-dir site/demo/
	asciidoctor doc/faq.md            -a toc -a linkcss --destination-dir site/demo/
	asciidoctor doc/editor_help.md    -a toc -a linkcss --destination-dir site/demo/
	touch doc/all_ok

referenceManual: .phony
	@rm -f doc/all_ok
	$(MAKE) doc/all_ok

doc: .phony
	dune build @doc
	@echo -e '\n\n\n'
	@echo -e 'SmartML doc:\n  open _build/default/_doc/_html/smart-ml/index.html'
	@echo -e 'SmartPy doc:\n  open site/demo/reference.html'

latest_release:
	git log -n1 release


ifeq (,$(wildcard env/current))
$(error Please run './env/nix/init' or './env/naked/init' before running make)
endif

ifndef SMARTPY_ENV
$(error Please run 'make' under './envsh' or directly as './with_env make')
endif
