(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Smart_ml
open Basics
open Html

type 'jsString exportToJs =
  { exportToJs : 'a 'b. string -> ('a -> 'b) -> unit
  ; exportToJsString : 'a. string -> ('a -> string) -> unit
  ; js_to_string : 'jsString -> string
  ; string_to_js : string -> 'jsString
  ; getText : string -> string
  ; setText : string -> string -> unit
  ; setValue : string -> string -> unit
  ; isChecked : string -> bool }

let importValueString ~primitives ~scenario_state s : tvalue =
  Interpreter.interpret_expr_external
    ~primitives
    ~no_env:[]
    ~scenario_state
    (Import.import_expr
       (Typing.init_env ~contract_data_types:(Hashtbl.create 5))
       (Import.early_env ())
       (Parsexp.Single.parse_string_exn s))

let importContractString ~scenario_state primitives s =
  let contract =
    Import.import_contract
      ~primitives
      ~scenario_state
      (Typing.init_env ~contract_data_types:(Hashtbl.create 5))
      (Parsexp.Single.parse_string_exn s)
  in
  { contract with
    storage =
      Interpreter.interpret_expr_external
        ~primitives
        ~no_env:[`Text "Compute storage"]
        ~scenario_state
        contract.storage }

let dbgf fmt =
  Format.kasprintf
    (fun s ->
      Js_of_ocaml.Firebug.console##log (Js_of_ocaml.Js.string s) |> ignore)
    fmt

let make_js_primitives () =
  let open Browser_sodium in
  let open Utils.Hex in
  let ojs_error_protect msg f =
    let m = Printf.sprintf "Crypto-primitives.%s" msg in
    try f () with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" m (Ojs_exn.to_string e)
    | other -> Printf.ksprintf failwith "%s: %s" m (Base.Exn.to_string other)
  in
  let call_sodium_hash ~name ~hash s =
    ojs_error_protect "sodium_hash" (fun () ->
        let hexcaped = hexcape s in
        let hex = hash (Crypto_bytes.of_hex hexcaped) |> Crypto_bytes.to_hex in
        if false then dbgf "hash %s: 0x%s → 0x%s" name hexcaped hex;
        unhex hex)
  in
  let hash_sha512 = call_sodium_hash ~name:"SHA512" ~hash:crypto_hash_sha512 in
  let hash_sha256 = call_sodium_hash ~name:"SHA256" ~hash:crypto_hash_sha256 in
  let hash_blake2b =
    call_sodium_hash ~name:"BLAKE2B" ~hash:(fun bytes ->
        crypto_hash_blake_2b bytes ~size:32)
  in
  let check_signature ~public_key ~signature msg =
    ojs_error_protect "check_signature" (fun () ->
        let hex_msg = hexcape msg in
        if false
        then
          dbgf
            "check_signature.js:\n\
            \   public_key: %S,\n\
            \   signature: %S,\n\
            \   msg: %S\n\
            \   hex-msg: %s\n"
            public_key
            signature
            msg
            hex_msg;
        Ed25519.verify_signature
          ~message:
            (Crypto_bytes.of_hex hex_msg |> crypto_hash_blake_2b ~size:32)
          ~public_key:
            ( public_key
            |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edpk
            )
          ( signature
          |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig ))
  in
  let sign ~secret_key message =
    ojs_error_protect "sign" (fun () ->
        let message =
          Crypto_bytes.of_hex (hexcape message)
          |> crypto_hash_blake_2b ~size:32
        in
        let secret_key =
          secret_key
          |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsk64
        in
        Ed25519.sign ~message ~secret_key
        |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig)
  in
  let account_of_seed seed =
    let real_seed =
      (* The seed is expected to be a 32-byte “C-string” (no '\x00'
         characters alloweed). *)
      let hashed = (hash_sha256 seed |> hexcape) ^ String.make 32 'B' in
      String.sub hashed 0 32
    in
    let kp =
      ojs_error_protect "keypair_of_seed" (fun () ->
          Ed25519.keypair_of_seed real_seed)
    in
    let public_key =
      kp.publicKey
      |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.edpk
    in
    let public_key_hash =
      kp.publicKey
      |> crypto_hash_blake_2b ~size:20
      |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.tz1
    in
    let full_secret_key =
      kp.privateKey
      |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.edsk64
    in
    if false
    then
      dbgf
        "real_seed: %S\npkh: %S\npk: %S\nsk: %s"
        real_seed
        public_key_hash
        public_key
        full_secret_key;
    Interpreter.Primitive_implementations.
      {pkh = public_key_hash; pk = public_key; sk = full_secret_key}
  in
  let hash_key s =
    let pkh =
      s
      |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edpk
      |> crypto_hash_blake_2b ~size:20
      |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.tz1
    in
    dbgf "pk: %s → hashed: %s" s pkh;
    pkh
  in
  try
    let _ = hash_blake2b "hello" in
    Interpreter.Primitive_implementations.
      { account_of_seed
      ; sign
      ; check_signature
      ; hash_key
      ; hash_blake2b
      ; hash_sha256
      ; hash_sha512 }
  with
  | _ ->
      dbgf "WARNING: js_primitives falling back to `faked_in_ocaml`";
      Interpreter.Primitive_implementations.faked_in_ocaml

let js_primitives = lazy (make_js_primitives ())

(* In the console:
       smartmlCtx.call_exn_handler('runSelfTest', '')
   , or in the python IDE:
       window.smartmlCtx.call('runSelfTest', '')
*)
let test_js_primitives () : unit =
  let public_key = "edpku5ZuqYibUQrAhove2B1bZDYCQyDRTGGa1ZwtZYiJ6nvJh4GXcE" in
  let secret_key = "edsk3ZAU1vr8z3Laj6kGqjsgL9a1kQA9Zs7QeEk2vBsVR5RGbb6UQX" in
  let message_hex = "050a00000003424146" in
  let signature =
    "edsigtbqJKYNhTVAYaY1sjFKTxS7awXwFrXv2fPBV1BzcYv4PvKzzjBYZNJmZeBpd1ec7J2VVAdqPCnMXoS7qu3KrdZZC5faCUZ"
  in
  let wrong_signature =
    "edsigtjoEiJAEdbeWqmhwc1RJE1AgdnBStaECLykBDvzWQ6bbVsMMEdz21iJqBrgXwYmmWKLCFp7tMuY7sfAubY75ThLxNwSVuG"
  in
  let open Browser_sodium in
  dbgf "-------------------- SELF TEST ------------------";
  let should_true b = if b then "Ok" else "#### ERROR ####" in
  let should_false b = should_true (not b) in
  let b58_involution ~prefix v =
    let i =
      v
      |> Crypto_bytes.of_b58_check ~prefix
      |> Crypto_bytes.to_b58_check ~prefix
    in
    dbgf
      "B58-involution: %S -> %S -> %S -> %s"
      v
      (v |> Crypto_bytes.of_b58_check ~prefix |> Crypto_bytes.to_hex)
      i
      (String.equal v i |> should_true)
  in
  b58_involution ~prefix:Crypto_bytes.B58_prefix.edpk public_key;
  b58_involution ~prefix:Crypto_bytes.B58_prefix.edsk32 secret_key;
  b58_involution ~prefix:Crypto_bytes.B58_prefix.edsig wrong_signature;
  let message =
    Crypto_bytes.of_hex message_hex |> crypto_hash_blake_2b ~size:32
  in
  let public_key =
    public_key
    |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edpk
  in
  dbgf
    "Ed25519.verify GOOD signature: %s"
    ( Ed25519.verify_signature
        ~public_key
        ~message
        ( signature
        |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig )
    |> should_true );
  dbgf
    "Ed25519.verify WRONG signature: %s"
    ( Ed25519.verify_signature
        ~public_key
        ~message
        ( wrong_signature
        |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig )
    |> should_false );
  let full_secret_key =
    Crypto_bytes.append
      ( secret_key
      |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsk32 )
      public_key
  in
  let new_signature =
    Ed25519.sign ~message ~secret_key:full_secret_key
    |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig
  in
  dbgf
    "Re-sign with Ed25519 (full sk: %s): new-sig: %s -> %s"
    (Crypto_bytes.to_b58_check
       ~prefix:Crypto_bytes.B58_prefix.edsk64
       full_secret_key)
    new_signature
    (String.equal new_signature signature |> should_true);
  dbgf "account_of_seed";
  let kp = Ed25519.keypair_of_seed "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" in
  dbgf
    "pk: %s ; sk: %s ; keyType: %s"
    (Crypto_bytes.to_b58_check
       ~prefix:Crypto_bytes.B58_prefix.edpk
       kp.publicKey)
    (Crypto_bytes.to_b58_check
       ~prefix:Crypto_bytes.B58_prefix.edsk64
       kp.privateKey)
    kp.keyType;
  dbgf "Contract.Primitive_implementations.self_test";
  ( try
      List.iteri
        (fun i -> function
          | Ok s -> dbgf "self-test-%d-OK: %s" i s
          | Error s -> dbgf "self-test-%d-ERROR: %s" i s)
        (Interpreter.Primitive_implementations.self_test
           (Lazy.force js_primitives))
    with
  | e ->
      dbgf
        "Contract.Primitive_implementations.self_test: error: %s"
        (Printexc.to_string e) );
  dbgf "Contract.Primitive_implementations.self_test done";
  ()

let execMessage
    (type jsString)
    (ctx : jsString exportToJs)
    ~scenario_state
    ~line_no
    title
    execMessageClass
    sender
    time
    amount
    initContract
    channel
    (params : tvalue)
    debug =
  let execMessageClass = ctx.js_to_string execMessageClass in
  let execMessageClass =
    if execMessageClass <> "" then " " ^ execMessageClass else execMessageClass
  in
  let title = ctx.js_to_string title in
  let channel = ctx.js_to_string channel in
  let sender = ctx.js_to_string sender in
  let amount =
    Value.unMutez
      (importValueString
         ~primitives:(Lazy.force js_primitives)
         ~scenario_state
         (ctx.js_to_string amount))
  in
  let context = Interpreter.context ~sender ~time ~amount ~line_no ~debug () in
  let result =
    Contract.execMessageInner
      ~primitives:(Lazy.force js_primitives)
      ~scenario_state
      ~title
      ~execMessageClass
      ~context
      ~initContract
      ~channel
      ~params
  in
  {result with html = ctx.string_to_js result.html}

let callGui id output t ~line_no : unit =
  let nextId = Value.nextId id in
  let input = Value_gui.inputGuiR ~nextId t in
  let v = input.get true in
  let contextInput = Value_gui.inputGuiR ~nextId contextSimulationType in
  let contextV = contextInput.get true in
  match (v.v, contextV.v, !Html.simulatedContract) with
  | ( Variant (channel, params)
    , Record
        [ ( _
          , { v =
                Record
                  [ ("sender", sender)
                  ; ("source", source)
                  ; ("timestamp", timestamp)
                  ; ("amount", amount) ] } )
        ; (_, {v = Record [("debug", debug); ("full_output", full_output)]}) ]
    , Some initContract ) ->
      let time =
        match Value.unString timestamp with
        | "" -> 0
        | x -> int_of_string x
      in
      let amount =
        match amount.v with
        | Variant (_, tez) when Value.unString tez = "" -> Big_int.zero_big_int
        | Variant ("Tez", tez) ->
            Big_int.mult_int_big_int
              1000000
              (Big_int.big_int_of_string (Value.unString tez))
        | Variant ("Mutez", mutez) ->
            Big_int.big_int_of_string (Value.unString mutez)
        | _ -> assert false
      in
      let context =
        Interpreter.context
          ~sender:(Value.unString sender)
          ~source:(Value.unString source)
          ~time
          ~amount
          ~line_no
          ~debug:(Value.unBool debug)
          ()
      in
      let message =
        Contract.execMessageInner
          ~title:""
          ~primitives:(Lazy.force js_primitives)
          ~scenario_state:(scenario_state ())
          ~execMessageClass:""
          ~context
          ~initContract
          ~channel
          ~params
      in
      let outputHtml =
        let michelson =
          match (message.contract, Value.unBool full_output) with
          | _, false | None, _ -> ""
          | Some contract, _ ->
              Html.full_html
                ~contract
                ~def:"Michelson"
                ~onlyDefault:false
                ~line_no
        in
        Printf.sprintf "<br>%s%s" message.html michelson
      in
      SmartDom.setText output outputHtml
      (* ^ delayedInputGui t*)
  | _ -> assert false

let buildTransfer id output kt account t : unit =
  try
    let input = Value_gui.inputGuiR ~nextId:(Value.nextId id) t in
    let initialValue = input.get true in
    let value =
      let value = Compiler.compile_value initialValue in
      let as_string = Michelson.Literal.to_michelson_string value in
      let as_json_string =
        let mich = Michelson.To_micheline.literal value in
        Micheline.to_json_string mich
      in
      SmartDom.setText
        "michClient"
        (Printf.sprintf
           "# ./tezos-client transfer 0 from %s to %s -arg '%s'"
           account
           kt
           as_string);
      SmartDom.setValue "messageSent" as_string;
      SmartDom.setValue "messageSentJSON" as_json_string;
      Printf.sprintf
        "Message OK.<br>  Value: %s<br>  Michelson: %s"
        (Printer.value_to_string initialValue)
        as_string
    in
    SmartDom.setText output value
  with
  | _ as e ->
      SmartDom.setText
        output
        (Printf.sprintf
           "Error during execution: %s"
           (Printer.exception_to_string true e))

let string_of_contract c =
  Printer.contract_to_string ~options:Printer.Options.string c

let string_of_value c =
  Printer.value_to_string ~options:Printer.Options.string c

let html_of_value c stripStrings =
  let options =
    if stripStrings
    then Printer.Options.htmlStripStrings
    else Printer.Options.html
  in
  Printer.value_to_string ~options c

let html_of_contract c =
  Printer.contract_to_string ~options:Printer.Options.html c

let interface (type jsString) (ctx : jsString exportToJs) =
  SmartDom.getTextRef := ctx.getText;
  SmartDom.setTextRef := ctx.setText;
  SmartDom.setValueRef := ctx.setValue;
  SmartDom.isCheckedRef := ctx.isChecked;
  ctx.exportToJs "importType" (fun s ->
      ( Import.import_type
          (Typing.init_env ~contract_data_types:(Hashtbl.create 5))
          (Import.early_env ())
          (Parsexp.Single.parse_string_exn (ctx.js_to_string s))
        : Type.t ));
  ctx.exportToJs "importContract" (fun s ->
      ( importContractString
          (Lazy.force js_primitives)
          ~scenario_state:(scenario_state ())
          (ctx.js_to_string s)
        : Contract.t ));
  ctx.exportToJs "compileContractStorage" (fun contract ->
      ctx.string_to_js (Compiler.michelson_storage contract));
  ctx.exportToJs "compileContract" Compiler.michelson_contract;
  ctx.exportToJs "compiledContract_to_michelson" (fun michelson_contract ->
      ctx.string_to_js
        (Compiler.michelson_contract_michelson michelson_contract));
  ctx.exportToJs "compiledContract_to_micheline" (fun michelson_contract ->
      ctx.string_to_js
        (Compiler.michelson_contract_micheline michelson_contract));
  ctx.exportToJs "buildTransfer" (fun s o kt account t ->
      buildTransfer
        (ctx.js_to_string s)
        (ctx.js_to_string o)
        (ctx.js_to_string kt)
        (ctx.js_to_string account)
        t);
  ctx.exportToJs "ppContractTypes" (fun contract ->
      ctx.string_to_js
        (Printf.sprintf
           "Storage: %s\nParams: %s"
           (Printer.type_to_string contract.storage.t)
           (Printer.type_to_string
              (Type.variant
                 (List.map
                    (fun {channel; paramsType} -> (channel, paramsType))
                    contract.entry_points)))));
  ctx.exportToJs "stringOfException" (fun html exc ->
      ctx.string_to_js (Printer.exception_to_string html exc));
  ctx.exportToJsString "js_string" (fun s -> s);
  ctx.exportToJs "callGui" (fun s o t ->
      callGui (ctx.js_to_string s) (ctx.js_to_string o) t);
  ctx.exportToJs "runSelfTest" (fun _ -> test_js_primitives ());
  ctx.exportToJs "explore" (fun address json operations ->
      Explorer.explore
        ~address:(ctx.js_to_string address)
        ~json:(ctx.js_to_string json)
        ~operations:(ctx.js_to_string operations));
  ctx.exportToJs "runScenario" (fun filename output_dir ->
      ctx.string_to_js
        (Scenario.run_scenario_filename
           ~primitives:(Lazy.force js_primitives)
           ~filename:(ctx.js_to_string filename)
           ~output_dir:(ctx.js_to_string output_dir)));

  ctx.exportToJs "runScenarioInBrowser" (fun scenario ->
      Scenario.run_scenario_browser
        ~primitives:(Lazy.force js_primitives)
        ~scenario:(ctx.js_to_string scenario));

  ()
