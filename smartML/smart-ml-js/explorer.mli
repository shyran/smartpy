(* Copyright 2019-2020 Smart Chain Arena LLC. *)

val explore : address:string -> json:string -> operations:string -> unit
