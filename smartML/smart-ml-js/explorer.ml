(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Smart_ml
open Utils
open Html

let messageBuilder storage storageType parameterType code operations =
  let _, getStorage, storageType = Micheline.typeOfStorageType storageType in
  let storage = getStorage storage in
  let _, getParameter, parameterType =
    Micheline.typeOfStorageType parameterType
  in
  let editor =
    let t = parameterType in
    let name = "contractId" in
    let accountName = "accountId" in
    let buttonText = "Build Message" in
    let output = nextOutputGuiId () in
    let id = nextInputGuiId () ^ "." in
    let nextId = Value.nextId id in
    let input = Value_gui.inputGuiR ~nextId t in
    Printf.sprintf
      "<div class='simulationBuilder'><form>%s\n\
       <br><button type='button' onClick=\"cleanMessages();t = \
       smartmlCtx.call_exn_handler('importType', '%s'); if (t) \
       smartmlCtx.call_exn_handler('buildTransfer', '%s', '%s', %s.value, \
       %s.value, t)\">%s</button></form>\n\
       <div id='%s'></div></div>"
      input.gui
      (Export.export_type t)
      id
      output
      name
      accountName
      buttonText
      output
  in
  SmartDom.setText
    "storageDiv"
    ( "<h3>Storage</h3>"
    ^ Printer.value_to_string ~options:Printer.Options.html storage );
  SmartDom.setText "messageBuilder" editor;
  let types =
    tabs
      "Contract:"
      [ tab
          ~active:()
          "Storage Type"
          (Printer.type_to_string ~options:Printer.Options.html storageType)
      ; tab
          "Parameter Type"
          (Printer.type_to_string ~options:Printer.Options.html parameterType)
      ; tab
          "Contract Code JSON"
          (Printf.sprintf
             "<div class='white-space-pre'>%s</div>"
             (Micheline.to_json_string code)) ]
  in
  SmartDom.setText "types" types;
  let ppOperation (operation, details, params, storage, errors, json) =
    let params =
      match params with
      | `Error error -> error
      | `OK (branch, parameters) ->
          let params =
            Base.String.fold
              ~init:(Micheline.parse parameters)
              ~f:(fun params c ->
                if c = 'L'
                then Micheline.left params
                else Micheline.right params)
              (Base.String.rev branch)
          in
          ( try
              let params = getParameter params in
              Printer.value_to_string ~options:Printer.Options.html params
            with
          | exn -> Printer.exception_to_string true exn )
    in
    let storage =
      match storage with
      | `Error error -> error
      | `OK storage ->
        ( try
            Printer.value_to_string
              ~options:Printer.Options.html
              (getStorage (Micheline.parse storage))
          with
        | exn -> Printer.exception_to_string true exn )
    in
    let errors =
      match errors with
      | None -> ""
      | Some errors -> Printf.sprintf "<h4>Errors</h4>%s" errors
    in
    let full_data =
      Printf.sprintf
        "<button class='centertextbutton' onClick='popupJson(\"Operation \
         Details\", %s)'>View Full Data</button>"
        (Base.String.substr_replace_all
           (Yojson.Basic.to_string json)
           ~pattern:"'"
           ~with_:"&apos")
    in
    Printf.sprintf
      "<h4>Operation</h4>%s%s<h4>Details</h4>%s<h4>Parameters</h4>%s<h4>Storage</h4>%s%s"
      operation
      full_data
      details
      params
      storage
      errors
  in
  let operations =
    String.concat "<hr>" (List.rev_map ppOperation operations)
  in
  SmartDom.setText "operationsList" operations

let parseTZStatAPI operation_ =
  let operation = json_getter operation_ in
  let module M = (val operation : JsonGetter) in
  let string x = Value.string (Yojson.Basic.to_string (M.get x)) in
  let bool x = Value.bool (M.bool x) in
  let mainlines =
    let fields =
      [ ("date and time", fun _ -> Value.string (M.string "time"))
      ; ("sender", fun x -> Value.address (M.string x))
      ; ("block", fun x -> Value.address (M.string x))
      ; ("hash", fun x -> Value.address (M.string x)) ]
    in
    List.map
      (fun (field, f) -> (String.capitalize_ascii field, f field))
      fields
  in
  let sublines =
    let fields =
      [ ("status", fun x -> Value.string (M.string x))
      ; ("is_success", bool)
      ; ("height", string)
      ; ("cycle", string)
      ; ("counter", string)
      ; ("gas_limit", string)
      ; ("gas_used", string)
      ; ("gas_price", string)
      ; ("storage_limit", string)
      ; ("storage_size", string)
      ; ("storage_paid", string)
      ; ("volume", string)
      ; ("fee", string)
      ; ("reward", string)
      ; ("deposit", string)
      ; ("burned", string) ]
    in
    List.map
      (fun (field, f) -> (String.capitalize_ascii field, f field))
      fields
  in
  let metaData =
    Printer.value_to_string
      ~options:Printer.Options.htmlStripStrings
      (Value.record mainlines)
  in
  let details =
    Printer.value_to_string
      ~options:Printer.Options.htmlStripStrings
      (Value.record sublines)
  in
  let parameters =
    let parameters = M.get "parameters" in
    match parameters with
    | `Null -> `Error "No parameters"
    | _ ->
        let module M = (val json_getter parameters : JsonGetter) in
        `OK (M.string "branch", M.get "prim")
  in
  let storage =
    match M.get "storage" with
    | `Null -> `Error "No storage"
    | storage ->
        let module M = (val json_getter storage : JsonGetter) in
        ( try `OK (M.get "prim") with
        | exn -> `Error (Printer.exception_to_string true exn) )
  in
  let errors =
    match M.get "errors" with
    | `Null -> None
    | errors -> Some (Yojson.Basic.to_string errors)
  in
  (metaData, details, parameters, storage, errors, operation_)

let explorerShowContractInfos address balance =
  let lines =
    [ ("Address", Value.address address)
    ; ("Balance", Value.mutez (Big_int.big_int_of_string balance)) ]
  in
  SmartDom.setText
    "contractDataDiv"
    (Printer.value_to_string ~options:Printer.Options.html (Value.record lines))

let explore ~address ~json ~operations =
  let json = json_getter (Yojson.Basic.from_string json) in
  let module M = (val json : JsonGetter) in
  let open M in
  explorerShowContractInfos address (string "balance");
  let script = json_sub json "script" in
  let module Script = (val script : JsonGetter) in
  if Script.null
  then
    SmartDom.setText
      "messageBuilder"
      "<span style='color:grey;'>&mdash; no contract code &mdash;</span>"
  else
    let code = Script.get "code" in
    let storage = Script.get "storage" in
    let parameterType, storageType, code =
      match code with
      | `List
          [ `Assoc [("prim", `String "parameter"); ("args", `List [parameter])]
          ; `Assoc [("prim", `String "storage"); ("args", `List [storage])]
          ; `Assoc [("prim", `String "code"); ("args", `List [code])] ] ->
          (parameter, storage, code)
      | `Assoc _ -> assert false
      | _ -> assert false
    in
    let operations =
      match operations with
      | "" -> []
      | _ ->
        ( match Yojson.Basic.from_string operations with
        | `List operations -> List.map parseTZStatAPI operations
        | _ -> assert false )
    in
    let parse = Micheline.parse in
    messageBuilder
      (parse storage)
      (parse storageType)
      (parse parameterType)
      (parse code)
      operations
