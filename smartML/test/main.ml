(* Copyright 2019-2020 Smart Chain Arena LLC. *)

let pp = print_endline

module Hashtbl_ = Hashtbl
open Base
open Smart_ml

let dbg f : unit =
  Fmt.(
    epr
      "%a@.@?"
      (fun ppf () ->
        box
          (fun ppf () ->
            string ppf "|DBG-test>";
            sp ppf ();
            f ppf)
          ppf
          ())
      ())

(*
   A first experiment of OCaml EDSL.

   - Very simple wrapping.
   - No types/GADTs.
   - Some very incomplete type inference.

   Cf, https://gitlab.com/SmartPy/smartpy-private/issues/19
*)
module EDSL_exp0 = struct
  open Basics

  module Ty = struct
    let bool = Type.bool

    let string = Type.string

    let unit = Type.unit

    let int = Type.int ()

    let record = Type.record

    let address = Type.address

    let token = Type.token
  end

  let get_type_or_try_to_infer infer_fun t v =
    match t with
    | Some s -> s
    | None -> infer_fun v

  let infer_from_expression storage_type = function
    | ENot _ -> Ty.bool
    | EStorage -> storage_type
    | other ->
        Fmt.kstrf
          failwith
          "expresssion: cannot infer type from expr: %a"
          pp_uexpr
          other

  let expression ?t storage_type e =
    { e
    ; el = -1
    ; et = get_type_or_try_to_infer (infer_from_expression storage_type) t e }

  let infer_from_value = function
    | Literal {l = Unit; _} -> Ty.unit
    | Literal {l = String _; _} -> Ty.string
    | Literal {l = Int _; _} -> Ty.int
    | Literal {l = Address _; _} -> Ty.address
    | Literal {l = Mutez _; _} -> Ty.token
    | other ->
        Fmt.kstrf failwith "value: cannot infer type from %a" pp_uvalue other

  let value ?t v = {v; t = get_type_or_try_to_infer infer_from_value t v}

  let big_int = Big_int.big_int_of_int

  let expr_unit = Expr.unit

  module Expr = struct
    let get_params params_type storage_type =
      expression ~t:params_type storage_type (EParams params_type)

    let field ?t storageType ?record_type name e =
      let t =
        match t with
        | Some s -> s
        | None ->
          ( match
              Option.bind record_type ~f:(function
                  | Type.{u = TRecord l} ->
                      List.find_map l ~f:(fun (k, t) ->
                          if String.equal k name then Some t else None)
                  | _ -> None)
            with
          | Some s -> s
          | None -> Fmt.kstrf failwith "Expr.field: cannot find type of field"
          )
      in
      expression ~t storageType (EAttr (name, e))

    let unit = expr_unit
  end

  module Val = struct
    let unit = Value.unit

    let string s = Value.string s

    let record ~t l = value ~t (Record l)

    let address s = Value.address s

    let int i = Value.int (big_int i)

    let token t = Value.mutez (big_int t)
  end

  module Var = struct
    let string v = (v, Ty.string)
  end

  let message channel params = {channel; params}

  let channel channel ?(t = Ty.unit) body = {channel; paramsType = t; body}

  let command c ?(line = 0) () = {line_no = line; c; has_operations = false}

  module Cmd = struct
    let make = command

    let seq ?line l = make ?line (CSeq l) ()

    let set_var_gen ?line e1 e2 = make ?line (CSetVar (e1, e2)) ()

    let set_var ?t storage_type var e =
      set_var_gen
        (expression ?t storage_type (ELocal var))
        (expression ?t storage_type e)

    let transfer ~destination ~amount =
      make @@ CTransfer {arg = Expr.unit; amount; destination}
  end

  let contract
      ?(partial_contract = false)
      ?(balance = 0)
      ?(storage = Val.unit)
      ?(flags = [])
      entry_points =
    let tparameter =
      Type.variant
        (List.map
           ~f:(fun channel -> (channel.channel, channel.paramsType))
           entry_points)
    in
    { balance = big_int balance
    ; storage
    ; tparameter
    ; entry_points
    ; partialContract = partial_contract
    ; flags }

  module Example = struct
    module Send_channel = struct
      let params_type = Ty.(record [("address", address); ("amount", int)])

      let channel storage_type =
        let params_field name =
          Expr.field
            storage_type
            ~record_type:params_type
            name
            (Expr.get_params params_type storage_type)
        in
        channel
          "send"
          ~t:params_type
          (Cmd.seq
             [ Cmd.transfer
                 ~destination:(params_field "address")
                 ~amount:(params_field "amount")
                 () ])
    end
  end
end

(** Additional constructs for {!Alcotest}. *)
module Testing_helpers = struct
  (** Alcotest.testable for {!Basics.Execution.error}. *)
  let testable_exec_error =
    Alcotest.of_pp
      (Fmt.option ~none:Fmt.(const string "No-error") Basics.Execution.pp_error)

  (** Alcotest.testable for {!Basics.teffect}. *)
  let testable_teffect = Alcotest.of_pp Basics.Execution.pp_effect

  (** [does_raise "interesting thing to say"
        ~p:predicate_on_exn ~f:function_supposed_to_raise]. *)
  let does_raise : string -> p:(exn -> bool) -> f:(unit -> unit) -> unit =
   fun msg ~p ~f ->
    try
      f ();
      Alcotest.failf "%s: did not raise an exception" msg
    with
    | e when p e -> ()
    | other ->
        Alcotest.failf
          "%s: did not raise the right exception: %a"
          msg
          Exn.pp
          other

  (** Match any exception in the [~p] argument of {!does_raise}. *)
  let any_failure = function
    | Failure _ | Basics.SmartExcept _ -> true
    | _ -> false

  exception Found
  (** Do not use this directly. *)

  (** [check_find m f] calls f with a function [found : unit -> 'a]
      that has to be called for the test to be considered successful. *)
  let check_find msg f =
    Alcotest.check_raises msg Found (fun () -> f (fun () -> raise Found))
end

(** Tests for {!Smart_ml.Contract.eval}. *)
module Contract_eval_test = struct
  let do_test_of_contract_eval ?context contract message =
    let ctx =
      Option.value
        context
        ~default:
          (Interpreter.context
             ~sender:"tz1sender"
             ~time:42
             ~amount:(Big_int.big_int_of_int 10)
             ~line_no:10
             ()
             ~debug:false)
    in
    let contract_cmd_option, effects, error_opt, _ =
      Interpreter.interpret_message
        ~primitives:Interpreter.Primitive_implementations.faked_in_ocaml
        ~scenario_state:(Smart_ml.Basics.scenario_state ())
        ctx
        contract
        message
    in
    dbg
      Fmt.(
        fun ppf ->
          vbox
            ~indent:2
            (fun ppf () ->
              string ppf "Testing Contract.eval:";
              sp ppf ();
              pf
                ppf
                "Contract-commands:@, %a"
                (box (option (pair Basics.pp_tcontract Basics.pp_tcommand)))
                contract_cmd_option;
              cut ppf ();
              pf
                ppf
                "Effects: %d:@, %a."
                (List.length effects)
                (list Basics.Execution.pp_effect)
                effects;
              cut ppf ();
              pf
                ppf
                "Error: %a."
                (option ~none:(const string "none") Basics.Execution.pp_error)
                error_opt)
            ppf
            ());
    (contract_cmd_option, effects, error_opt)

  let run () =
    let open EDSL_exp0 in
    let open Testing_helpers in
    let () =
      (* Completely empty contract called on an inexistent entry-point *)
      let _, _, error_opt =
        do_test_of_contract_eval (contract []) (message "hello" Val.unit)
      in
      Alcotest.check
        testable_exec_error
        "entry-point not found"
        (Some (Basics.Execution.Exec_channel_not_found "hello"))
        error_opt
    in
    let hello_channel_sets_storage =
      let storage_type = Type.unit in
      channel
        "hello"
        ~t:Ty.string
        (Cmd.seq
           [ Cmd.set_var_gen
               (expression storage_type EStorage)
               (expression storage_type (EParams Ty.string) ~t:Ty.string) ])
    in
    Alcotest.check_raises
      "Duplicate channels"
      (Failure "Too many channels")
      (fun () ->
        let contract =
          contract
            ~balance:10
            ~storage:(Val.string "world")
            [hello_channel_sets_storage; hello_channel_sets_storage]
        in
        let message = message "hello" (Val.string "foo") in
        do_test_of_contract_eval contract message |> ignore);
    does_raise
      "wrong parameter type"
      ~p:(function
        | Basics.SmartExcept (`Text s :: _) -> String.equal s "Type Error"
        | Failure s -> String.is_prefix s ~prefix:"Type error"
        | _ -> false)
      ~f:(fun () ->
        do_test_of_contract_eval
          (contract
             ~balance:10
             ~storage:(Val.string "world")
             [hello_channel_sets_storage])
          (message "hello" Val.unit)
        |> ignore);
    let new_value = Val.(string "foo") in
    let new_contract, effects, error_opt =
      do_test_of_contract_eval
        (contract
           ~balance:10
           ~storage:(Val.string "world")
           [hello_channel_sets_storage])
        (message "hello" new_value)
    in
    Alcotest.check testable_exec_error "no error" None error_opt;
    Alcotest.(check (list testable_teffect)) "no effect" [] effects;
    check_find "storage got changed" (fun found ->
        match new_contract with
        | None -> failwith "not here"
        | Some ({storage; _}, _) ->
            if Poly.equal storage new_value then found () else ());
    let new_value =
      Val.(
        record
          ~t:Example.Send_channel.params_type
          [("address", address "KT1blabla"); ("amount", token 42)])
    in
    let _new_contract, effects, error_opt =
      let storage_type = Type.unit in
      do_test_of_contract_eval
        (contract
           ~balance:10
           ~storage:(Val.string "world")
           [ Example.Send_channel.channel storage_type
           ; hello_channel_sets_storage ])
        (message "send" new_value)
    in
    Alcotest.check testable_exec_error "no error" None error_opt;
    check_find "one effect" (fun found ->
        List.iter effects ~f:(function
            | Sending
                { arg = _
                ; destination = _
                ; amount = {v = Smart_ml.Basics.Literal {l = Mutez tok; _}; _}
                }
              when Big_int.eq_big_int tok (big_int 42) ->
                (* TODO: also check left side *)
                found ()
            | _ -> ()));

    (* Alcotest.(check bool) "JUST MAKING TEST FAIL" true false ; *)
    ()
end

(** Tests for {!Michelson_compiler.michelson_contract}. *)
module Michelson_compiler_test = struct
  let run () =
    let open EDSL_exp0 in
    let open Testing_helpers in
    let run_compiler msg contract =
      let res =
        Result.try_with (fun () -> Compiler.michelson_contract contract)
      in
      dbg
        Fmt.(
          fun ppf ->
            vbox
              (fun ppf () ->
                pf ppf "Test: %s" msg;
                cut ppf ();
                box
                  ~indent:2
                  (fun ppf () ->
                    pf ppf "# Compiler on:";
                    sp ppf ();
                    Basics.pp_tcontract ppf contract)
                  ppf
                  ();
                cut ppf ();
                box
                  ~indent:2
                  (fun ppf () ->
                    pf ppf "# Result:";
                    sp ppf ();
                    match res with
                    | Ok o ->
                        pf ppf "OK:@ %a" Michelson.Michelson_contract.pp o
                    | Error e -> pf ppf "ERROR:@ %a" Exn.pp e)
                  ppf
                  ())
              ppf
              ());
      res
    in
    let some_string = "Some-string" in
    let res =
      contract ~storage:(Val.string some_string) []
      |> run_compiler "first test"
    in
    check_find "empty contract" (fun found ->
        match res with
        | Ok {storage = {mt = MTstring}; parameter = {mt = MTunit}; code = _}
          ->
            found ()
        | _ -> ());
    let contract =
      let storage_type = Type.unit in
      contract
        ~storage:(Val.string some_string)
        [Example.Send_channel.channel storage_type]
      |> run_compiler "with send channel"
    in
    check_find "send-channel-basic" (fun found ->
        match contract with
        | Ok
            { storage = {mt = MTstring}
            ; parameter = {mt = MTannot ("send", {mt = MTpair _}, false)}
            ; code = _ } ->
            found ()
        | _ -> ());
    ()
end

module Command_dot_fix = struct
  let run () =
    let open Testing_helpers in
    let open EDSL_exp0 in
    let homogenize_command c =
      Smart_ml.Basics.{(c : tcommand) with line_no = -1}
    in
    let try_fix cmd =
      let fix_env =
        Fixer.
          { partial = false
          ; check_variables = false
          ; storage = Ty.unit
          ; variable_names = []
          ; substContractData = Hashtbl_.create 10
          ; reducer = (fun ~line_no:_ x -> x)
          ; lambda_param = None }
      in
      let typing_env =
        Typing.init_env ~contract_data_types:(Hashtbl_.create 5)
      in
      try
        Smart_ml.Fixer.fix_command fix_env typing_env cmd |> homogenize_command
      with
      | exn -> failwith (Smart_ml.Printer.exception_to_string false exn)
    in
    does_raise "Duplicate local variables" ~p:any_failure ~f:(fun () ->
        let cmd =
          try_fix
            (Cmd.seq
               [ Smart_ml.Command.defineLocal
                   ~line_no:(-1)
                   "aa"
                   Smart_ml.Expr.now
                   Smart_ml.Type.timestamp
               ; Smart_ml.Command.defineLocal
                   ~line_no:(-1)
                   "aa"
                   Smart_ml.Expr.now
                   Smart_ml.Type.timestamp ])
        in
        dbg
          Fmt.(
            fun ppf ->
              pf ppf "command result %a" Smart_ml.Basics.pp_tcommand cmd));
    let testable_command = Alcotest.testable Basics.pp_tcommand Poly.equal in
    let with_or_without_set_type ~with_set_type =
      let open Smart_ml in
      [Command.defineLocal ~line_no:(-1) "aa" Expr.now Type.timestamp]
      @ ( if with_set_type
        then
          [ Command.setType
              ~line_no:(-1)
              (Expr.local ~line_no:(-1) "aa" Type.timestamp)
              Type.timestamp ]
        else [] )
      @ [ Cmd.set_var_gen
            (Expr.local ~line_no:(-1) "aa" Type.timestamp)
            Expr.now ]
      |> Cmd.seq
      |> homogenize_command
    in
    Alcotest.check
      testable_command
      "Use twice local variables, clean-up settype"
      (try_fix (with_or_without_set_type ~with_set_type:true))
      (with_or_without_set_type ~with_set_type:false);
    let while_loop ~ok ~in_seq =
      let while_thing =
        if ok
        then
          Command.whileLoop
            ~line_no:1
            (Smart_ml.Expr.cst ~line_no:(-1) (Literal.bool false))
            (Command.seq ~line_no:(-1) [])
        else
          Command.whileLoop
            ~line_no:1
            (Smart_ml.Expr.cst ~line_no:(-1) (Literal.nat (big_int 2)))
            (Command.seq ~line_no:(-1) [])
      in
      if in_seq then Cmd.(seq [while_thing]) else while_thing
    in
    Alcotest.check
      testable_command
      "OK while loop"
      (try_fix (while_loop ~ok:true ~in_seq:true))
      (while_loop ~ok:true ~in_seq:false |> homogenize_command);
    does_raise "Wrong while" ~p:any_failure ~f:(fun () ->
        try_fix (while_loop ~ok:false ~in_seq:true) |> ignore);
    ()

  let alcotest_test =
    ("command-dot-fix", [Alcotest.test_case "Command.fix" `Quick run])
end

module Interpreter_on_expressions = struct
  let test_interpret_expr_early () =
    let do_check name expr expected =
      let testable_value = Alcotest.testable Value.pp Value.equal in
      Alcotest.(check testable_value)
        (Fmt.str "interpret_expr_early:%s" name)
        (Interpreter.interpret_expr_external
           ~primitives:
             Smart_ml.Interpreter.Primitive_implementations.faked_in_ocaml
           ~no_env:[`Text "Computing expression"; `Expr expr; `Line expr.el]
           ~scenario_state:(Smart_ml.Basics.scenario_state ())
           expr)
        expected
    in
    do_check
      "zero"
      Expr.(cst ~line_no:0 (Literal.bool false))
      Value.(bool false);
    let typing_env =
      Typing.init_env ~contract_data_types:(Hashtbl_.create 5)
    in
    let of_which which =
      match which with
      | `SB_string -> ("String", Literal.string, Value.string, Type.string)
      | `SB_bytes -> ("Bytes", Literal.bytes, Value.bytes, Type.bytes)
    in
    let check_slice ~which ~ofs ~len s =
      let ks, lit, value, t = of_which which in
      let res =
        match String.sub s ~pos:ofs ~len with
        | s -> Value.some (value s)
        | exception _ -> Value.none t
      in
      do_check
        (Fmt.str "slice%s:%S:%d--%d" ks s ofs len)
        Expr.(
          slice
            ~line_no:0
            typing_env
            ~offset:(cst ~line_no:0 @@ Literal.nat (Big_int.big_int_of_int ofs))
            ~length:(cst ~line_no:0 @@ Literal.nat (Big_int.big_int_of_int len))
            ~buffer:(cst ~line_no:0 @@ lit s))
        res
    in
    let both f =
      f ~which:`SB_string;
      f ~which:`SB_bytes
    in
    both @@ check_slice ~ofs:0 ~len:0 "";
    both @@ check_slice "" ~ofs:0 ~len:0;
    both @@ check_slice "a" ~ofs:0 ~len:1;
    both @@ check_slice "a" ~ofs:1 ~len:0;
    both @@ check_slice "abcdef" ~ofs:1 ~len:3;
    both @@ check_slice "abcdef" ~ofs:1 ~len:5;
    both @@ check_slice "abcdef" ~ofs:(-1) ~len:5;
    both @@ check_slice "abcdef" ~ofs:0 ~len:50;
    let check_concat ~which l =
      let ks, lit, value, t = of_which which in
      let res = String.concat ~sep:"" l in
      do_check
        (Fmt.str "concat%s:%S" ks res)
        Expr.(
          concat_list
            ~line_no:0
            typing_env
            (build_list
               ~line_no:0
               ~tvalue:t
               ~elems:(List.map l ~f:(fun s -> cst ~line_no:0 @@ lit s))))
        (value res)
    in
    both @@ check_concat [];
    both @@ check_concat ["a"];
    both @@ check_concat ["a"; "b"];
    both @@ check_concat (List.init 42 ~f:(Fmt.str "%d"));
    let check_size ~which s =
      let ks, lit, _, _ = of_which which in
      let res = String.length s in
      do_check
        (Fmt.str "size%s:%S" ks s)
        Expr.(size ~line_no:0 typing_env (cst ~line_no:0 @@ lit s))
        (Value.nat (Big_int.big_int_of_int res))
    in
    both @@ check_size "";
    both @@ check_size "a";
    both @@ check_size "aaaaaaaaaa";

    (* Alcotest.(check bool) "JUST MAKING TEST FAIL" true false ; *)
    ()

  let alcotest_test =
    ( "interpreter-on-expressions"
    , [ Alcotest.test_case
          "interpret_expr_early"
          `Quick
          test_interpret_expr_early ] )
end

let () =
  Utils.Dbg.on := true;
  Alcotest.run
    ~argv:Sys.argv
    "smart-ml-library"
    [ ( "contract-eval"
      , [Alcotest.test_case "Contract.eval" `Quick Contract_eval_test.run] )
    ; ( "michompilation"
      , [ Alcotest.test_case
            "Michelson_compiler.michelson_contract"
            `Quick
            Michelson_compiler_test.run ] )
    ; Command_dot_fix.alcotest_test
    ; Interpreter_on_expressions.alcotest_test
    ; ( "fake-primitive-implementations"
      , [ Alcotest.test_case
            "fake-primitive-implementations-self-test"
            `Quick
            (fun () ->
              List.iter
                ~f:(function
                  | (Ok s | Error s) as res ->
                      Alcotest.(check bool)
                        (Fmt.str "fake-prim: %s" s)
                        ( match res with
                        | Ok _ -> true
                        | _ -> false )
                        true)
                Interpreter.Primitive_implementations.(
                  self_test faked_in_ocaml)) ] ) ]
