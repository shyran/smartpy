#! /bin/sh

set -e

usage () {
    cat >&2 <<EOF
usage: $0 {run}
EOF
}

say () {
    echo "$*" >&2
}

smartml='dune exec -- smartML/app/smartml.exe'
export smpypy="./python/"
# This path is also used in `.gitlab-ci.yml`:
export test_path="/tmp/smartpy-test-root"
bench_date () {
    date +"%H-%M-%S.%N"
}
babble () {
    if [ "$VERBOSE" = "true" ] ; then
        say "Babbling[$(bench_date)]: $*"
    fi
}
export sbx_root="$PWD/_build/sbx-root"
sandbox_deps () {
    if which tezos-node > /dev/null 2>&1 ; then
        export tzexes=""
    else
        src=ext/tezos/mainnet-staging/src
        dune build \
             $src/bin_node/main.exe \
             $src/bin_client/main_client.exe \
             $src/bin_client/main_admin.exe
        export tzexes="\
              --tezos-node $PWD/_build/default/$src/bin_node/main.exe \
              --tezos-clie $PWD/_build/default/$src/bin_client/main_client.exe \
              --tezos-admi $PWD/_build/default/$src/bin_client/main_admin.exe"
    fi
}
try_sandbox () {
    sandbox_deps
    $smartml smartbox --root "$sbx_root" $tzexes "$@"
}

custom_full_tests () {
    local output="$1"
    local contract_path="$2"
    case $(basename $output) in
        "testHashFunctions-full-test.json" )
            cat > "$output" <<EOF
[
  {
    "scenario": "Example-one",
    "run-with-contract": {
      "source": "$contract_path",
      "balance-mutez": "10000000",
      "parameters-raw": "Pair (Pair (Pair (Pair 0x6f 0x61) 0x62) \"tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J\") 0x63",
      "do": [
        {
          "action": "call",
          "parameters-raw": "Right 0x414246"
        },
        {"action": "storage-greps-to", "posix-re": "\"bytes\": *\"0a2e9ef9530100a67cecf5490"},
        {"action": "storage-greps-to", "posix-re": "\"bytes\": *\"97cfbd1bcadcd9e8ccd24"},
        {"action": "storage-greps-to", "posix-re": "\"bytes\": *\"73d5a41891300f77410c107024f80"},
        {
          "action": "call",
          "amount-mutez": "0",
          "parameters-raw": "Right 0x414246"
        }
      ]
    }
  }
]
EOF
            ;;
        "testCheckSignature-full-test.json" )
            # The keys and signatures come from the commands in the comments of the .py:
            pubkey=edpku5ZuqYibUQrAhove2B1bZDYCQyDRTGGa1ZwtZYiJ6nvJh4GXcE
            good_signature=edsigtpBv2DqWrwmWQw8ykTwKnKc5gZoYF3Suu1Mv1rtRFdCoyNWw2XUfL8n9NbxfqRFQt4VVfMgWFhmjCfsWFs4jCv82Crfpve
            wrong_signtaure=edsigtgfbUT8NV3tzUmusZevyLD1LSXyVnucN597b732kfCmnsmzNKtU4WeYuMj243BnUTJNvZtmAks6hRu4m6kvJMnup34Cpz9
            cat > "$output" <<EOF
[
  {
    "scenario": "Check-signatures-template",
    "run-with-contract": {
      "source": "$contract_path",
      "balance-mutez": "10000000",
      "parameters-raw": "(Pair (Pair \"$pubkey\" 0) \"Hello World\" )",
      "do": [
        {
          "action": "call",
          "parameters-raw": "Pair \"should work\" \"$good_signature\""
        },
        {"action": "storage-greps-to", "posix-re": "\"string\": *\"should work\""},
        {
          "action": "call",
          "parameters-raw": "(Pair \"should not be able to put this in storage\" \"$wrong_signature\")",
          "should": "fail"
        },
        {"action": "storage-greps-to", "posix-re": "\"string\": *\"should work\""}
      ]
    }
  }
]
EOF
            ;;
        "stringManipulations-full-test.json" )
            # parameter
            #(or
            #   (pair %concatenating
            #         (pair (bytes %b0) (bytes %b1))
            #         (list %s string))
            #   (pair %slicing (bytes %b) (string %s)));
            cat > "$output" <<EOF
[
  {
    "scenario": "String-manipulations-template",
    "run-with-contract": {
      "source": "$contract_path",
      "balance-mutez": "10000000",
      "parameters-raw": "(Pair (Pair (Pair (Some 0xDEADBEEF) 0) 0) (Some \"hello\"))",
      "do": [
        {
          "action": "call",
          "parameters-raw": "Left (Left (Pair (Pair (Pair 0x41 0x4243) { \"hello\"; \"world\"}) { 0x1234; 0x56789abc}))"
        },
        {"action": "storage-greps-to", "posix-re": "414243"},
        {"action": "storage-greps-to", "posix-re": "helloworld"},
        {
          "action": "call",
          "parameters-raw": "Left (Left (Pair (Pair (Pair 0x41 0x4243) { \"justhello\"}) { 0x1234; 0x56789abc}))"
        },
        {"action": "storage-greps-to", "posix-re": "\"justhello\""},
        {
          "action": "call",
          "parameters-raw":
            "Left (Left (Pair (Pair (Pair 0xdead 0xbeef) { \"a\"; \"bb\"; \"ccc\"}) {}))"
        },
        {"action": "storage-greps-to", "posix-re": "\"abbccc\""},
        {"action": "storage-greps-to", "posix-re": "\"deadbeef\""},
        {
          "action": "call",
          "parameters-raw": "Right (Pair 0xDEADBEEF \"0123456789\")"
        },
        {"action": "storage-greps-to", "posix-re": "\"adbe\""},
        {"action": "storage-greps-to", "posix-re": "\"23456\""},
        {
          "action": "call",
          "parameters-raw": "Right (Pair 0xDEAD \"012345\")"
        },
        {"action": "storage-greps-to", "posix-re": "\"None\".*\"None\""}
      ]
    }
  }
]
EOF
            ;;
        * )
            say "Unknown custom full test: '$1'"
            exit 4
    esac
}

with_python () {
    local template="$1"
    local class_call="$2"
    local origination=""
    local full_test=false
    local testname="$(echo $template | sed 's:.*/\([^/]*\)\.py:\1:')"
    local testdir="$test_path/$testname"
    local dune="$test_path/$testname/dune"
    mkdir -p "$testdir"
    local      pysrcfile="$testname.py"
    local  pyadaptedfile="$testname-adapted.py"
    local      sexprfile="$testname.smlse"
    local      michelson="$testname.tz"
    local      michelcmt="$testname-with-comments.tz"
    local      micheinit="$testname-init.tz"
    local      micheinij="$testname-init.json"
    local      michesize="$testname-size.txt"
    local     micheljson="$testname.json"
    local          pylog="$testname-python-log.txt"
    local          mllog="$testname-ocaml-log.txt"
    local         sbxlog="$testname-sandbox-log.txt"
    local         report="$testname-report.md"
    local full_test_json="$testname-full-test.json"
    local smartbox_scenarios_option="--originate \\\"$michelson:\$(cat $micheinit)\\\""
    local smartbox_extra_deps=""

    SMARTPY_SH=python/SmartPy.sh
    TEMPLATES=python/templates

    OUTDIR=python/test/scenario/$testname
    [ -n "$makefile_scenario" ] &&
        (echo >> $makefile_scenario
         echo "SCENARIO_OK+=$OUTDIR.ok" >> $makefile_scenario)

    if [ "$class_call" = "None" ]; then
        say "|| $1 $2 → Skip"
        rm -f $report
        return
    fi
    OUTDIR=python/test/compile/$testname
    [ -n "$makefile_compile" ] &&
        (echo >> $makefile_compile
         echo "COMPILE_OK+=$OUTDIR.ok" >> $makefile_compile
         echo "$OUTDIR.ok: smartML/build_ok $TEMPLATES/$testname.py" | sed 's:\\":":g' >> $makefile_compile
         echo "	@./test_wrapper $OUTDIR $SMARTPY_SH compile $TEMPLATES/$testname.py '$class_call' $OUTDIR" | sed 's:\\":":g' >> $makefile_compile)
    case "$3" in
        "origination:works" )
            smartbox_post=""
            smartbox_expected_msg="SHOULD SUCCEED"
            ;;
        "testing:full" )
            full_test=true
            # We use absolute paths to be able to reuse the JSON for
            # interactive exploration:
            custom_full_tests \
                "$testdir/$full_test_json" \
                "$test_path/_build/default/$testname/$michelson"
            smartbox_scenarios_option="--run-scenarios $full_test_json"
            smartbox_extra_deps="$full_test_json"
            ;;
        * )
            smartbox_post=" || echo '$1 succeeding-anyway'"
            smartbox_expected_msg="SHOULD FAIL"
            ;;
    esac
    cp $smpypy/$template $testdir/$pysrcfile
    export result_path="$test_path/_build/default/$testname"
    export smartml="$PWD/_build/default/smartML/app/smartml.exe"
    export extra_deps="$PWD/_build/default/smartML/app/smartml.exe"
    dune format-dune-file > "$dune" <<EOF
(rule (targets $pyadaptedfile $sexprfile)
      (deps $pysrcfile $extra_deps $PWD/python/smartpybasic.py)
      (action (run sh -c "PYTHONPATH=$PWD/python/ python3 $PWD/python/smartpybasic.py $pysrcfile --class_call '$class_call' --sexprfile '$sexprfile' --pyadaptedfile '$pyadaptedfile'")))
(rule (targets $michelson $michesize)
      (deps $sexprfile $extra_deps)
      (action
        (run sh -c "$smartml compile --no-push-drop --write-binary-size $michesize \
              $sexprfile $michelson > \"$mllog\" 2>&1")))
(rule (targets $michelcmt )
      (deps $sexprfile $extra_deps)
      (action
        (run sh -c "$smartml compile \
              $sexprfile $michelcmt > \"$mllog\" 2>&1")))
(rule (targets $micheljson )
      (deps $sexprfile $extra_deps)
      (action (run sh -c "$smartml compile $sexprfile $micheljson > \"$mllog\" 2>&1")))
(rule (targets $micheinit )
      (deps $sexprfile $extra_deps)
      (action (run sh -c "$smartml init $sexprfile $micheinit > \"$mllog\" 2>&1")))
(rule (targets $micheinij )
      (deps $sexprfile $extra_deps)
      (action (run sh -c "$smartml init $sexprfile $micheinij > \"$mllog\" 2>&1")))
(rule (targets $sbxlog )
      (locks /tcp-port/46000)
      (deps $micheinit $michelson $extra_deps $smartbox_extra_deps)
      (action (run sh -c "$smartml smartbox \
       --root \"$sbx_root\" $tzexes \
       --output-results \"$sbxlog\" $smartbox_scenarios_option \
       > \"$mllog\" 2>&1 $smartbox_post")))
(rule (targets $report)
      (deps $pyadaptedfile $michelson $micheljson $micheinit $sbxlog)
   (action
      (with-stdout-to $report
        (progn
           (echo "## Testing $testname
* Adapted python: $pyadaptedfile
* SmartPy log: $pylog
* SmartML S-Expression: $sexprfile
* Michelson:
    * $result_path/$michelson
    * With push-drop-comments: $result_path/$michelcmt
    * JSON: $result_path/$micheljson
    * Init-mich: $result_path/$micheinit
    * Init-json: $result_path/$micheinij
    * Size: $result_path/$michesize
* Smartbox:
    * Test-result: $result_path/$sbxlog
    * Log: $result_path/$mllog
")
          (run sh -c "echo \"    * $smartbox_expected_msg: \$(grep -E 'Test-summary:' $mllog).\"")
          (run sh -c "echo \"* Michelson: \$(wc -l $michelson | cut -d' ' -f 1) lines.\"")
          (run sh -c "echo \"* Michelson binary: \$(cat $michesize) bytes (~ \$(( \$(cat $michesize) / 160 )) % of maximum operation size).\"")
        )
      )))
(alias (name runtest)
       (deps $report $michesize $pyadaptedfile $michelcmt
             $michelson $micheljson $micheinit $micheinij $sbxlog))
(alias (name michelsons)
       (deps $michesize $michelcmt $michelson $micheinit))
EOF
    say "|| $1 $2 → $dune"
}

prepare_all_defaults () {
    [ -n "$makefile_compile" ] && echo "# This file hase been auto-generated by basic-app-test.sh" > $makefile_compile
    [ -n "$makefile_scenario" ] && echo "# This file hase been auto-generated by basic-app-test.sh" > $makefile_scenario
    with_python templates/welcome.py "MyContract(42, 42)" "origination:works"
    with_python templates/jingleBells.py "JingleBells()" "origination:works"
    with_python templates/calculator.py "Calculator()" origination:works
    with_python templates/testLists.py "TestLists()"
    with_python templates/tictactoe.py "TicTacToe()" "origination:works"
    with_python templates/tictactoeFactory.py \
                'TicTacToe(admin = sp.address(\"tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr\"))' \
                origination:works
    with_python templates/nim.py "NimGame(size = 5, bound = 2)" origination:works

    with_python templates/nimLift.py "NimMultipleGame()"
    # Error-partial-type: No conversion for expression range(1, params.size + 1, 1)

    with_python templates/chess.py "Chess()"
    #| invalid primitive ITER, only [...], SELF or LAMBDA can be used here.

    with_python templates/voting.py "SimpleVote()"
    #| At line 15 characters 15 to 19, wrong stack type for instruction CONS:
    #|   [ address @parameter.left : map int address @storage : ... ].

    with_python templates/multisig.py "MultiSigFactoryWithPayment()"
    #| At line 41 characters 15 to 19, wrong stack type for instruction CONS:
    #

    with_python \
        templates/FA1.2.py \
        'FA12(admin = sp.address(\"tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr\"))' \
        origination:works

    with_python templates/decompilation.py "WelcomeDecompilation()" origination:works

    with_python templates/atomicSwap.py \
                'AtomicSwap(sp.mutez(12),sp.timestamp(50),sp.bytes(\"0x307839304336453830343039324344304138324531443132413043324538324238384544\"), sp.address(\"tz1c3GoMtLJgd3pyodoG7BpHdXgbKYVfnK3N\"), sp.address(\"tz1c3GoMtLJgd3pyodoG7BpHdXgbKYVfnK3N\"))' \
                origination:works

    with_python \
        templates/syntax.py \
        'SyntaxDemo(True, \"abc\", sp.bytes(\"0xaabb\"), 7, toto = \"ABC\", acb = \"toto\", f = False)' \
        origination:works

    ## with_python templates/bls12381.py         "Bls12381()"
    #    TypeError: int.__new__(Fq): Fq is not a subtype of int

    with_python templates/fifo.py "Fifo()"
    # "Cannot compile missing type: Unknown Type"

    with_python templates/storeValue.py "StoreValue(12)" origination:works

    with_python templates/minikitties.py \
                'MiniKitties(creator = sp.address(\"tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr\"), newAuctionDuration = 10, hatchingDuration = 100)' \
                origination:works

    ## with_python templates/escrow.py \
    #    'Escrow("AAA", sp.tez(50), "BBB", sp.tez(4), sp.timestamp(123), "hashSecret")'
    #    (Failure "Type error, (string) is not (hash)<br>comparison

    with_python templates/stateChannels.py 'None' ### Only in custom tests

    with_python templates/checkLanguage.py \
                "CheckOperator(0, (lambda x, y: x + y), 42, 51)" \
                origination:works

    with_python templates/testHashFunctions.py \
                'TestHashes()' \
                testing:full

    with_python templates/testEmpty.py "C()" origination:works

    with_python templates/testVoid1.py 'C1()' origination:works
    with_python templates/testVoid2.py 'C2()' origination:works

    with_python templates/testSend1.py 'C1()' origination:works
    with_python templates/testSend2.py 'C2()' origination:works
    with_python templates/testSend3.py 'C3()' origination:works
    with_python templates/testSend4.py 'C4()' origination:works
    with_python templates/testSend5.py 'C5()' origination:works
    with_python templates/testSend6.py 'C6()' origination:works

    with_python templates/testFor.py 'C()' origination:works

    with_python templates/testMin.py 'C()' origination:works

    with_python templates/testTimestamp.py "C()" origination:works

    with_python templates/testCheckSignature.py  \
                'TestCheckSignature(sp.key(\"edpku5ZuqYibUQrAhove2B1bZDYCQyDRTGGa1ZwtZYiJ6nvJh4GXcE\"))' \
                testing:full

    with_python templates/testLocalRecord.py "C()" origination:works

    with_python templates/collatz.py 'Collatz(sp.address(\"KT1QEaMVhcGvnf31cmWN4YWcujfzwhEQqX8c\"),sp.address(\"KT1CbRd63JiSrZsVEJzmXAmA5ACNvpqYTGbM\"))' origination:works

    with_python templates/stringManipulations.py "MyContract()" testing:full

    with_python templates/testVariant.py "TestOptionsAndVariants()" origination:works

    with_python templates/inlineMichelson.py "MyContract()"

    with_python templates/lambdas.py "MyContract()"

    cp dune-project $test_path
}

case "$1" in
    "prepare" )
        sandbox_deps
        case "$2" in
            compile)
                makefile_compile=python/test/compile.mk
                ;;
            scenario)
                makefile_scenario=python/test/scenario.mk
                ;;
            *)
                usage
                ;;
        esac
        prepare_all_defaults
        ;;
    "run" )
        sandbox_deps
        prepare_all_defaults
        (
            cd $test_path
            dune build @runtest
            echo ""
            find _build -name '*-report.md' -exec printf "* \`$test_path/%s\`\\n" {} \;
        )
        ;;
    "one" )
        sandbox_deps
        (
            cd $test_path
            dune build @$2/runtest || ret=$?
            rep="$PWD/_build/default/$2/$2-report.md"
            say "Report: $(if ! [ -f "$rep" ] ; then echo "Not created (failure)" ; else echo "$rep" ; fi)"
            say "SmartML Log: $PWD/_build/default/$2/$2-ocaml-log.txt"
            exit $ret
        ) ;;
    "michelsons" )
        sandbox_deps
        prepare_all_defaults
        (
            cd $test_path
            dune build @michelsons
            echo ""
            find _build -name '*.tz' -exec printf "* \`$test_path/%s\`\\n" {} \;
        )
        ;;
    "clean" )
        rm -fr "$test_path"
        ;;
    "reports" )
        find $test_path/_build -name '*.md' -type f -exec cat {} \;
        ;;
    "env" )
        echo "export test_path=$test_path"
        ;;
    "do" )
        sandbox_deps
        shift
        "$@"
        ;;
    "interactive" )
        shift
        try_sandbox --pause-at-end true "$@"
        ;;
    "balances" )
        for f in $test_path/_build/default/*/*-ocaml-log.txt ; do
            echo "* Test $(basename $f)"
            grep -E "(-> Origination:|-> Contract-call|Balance-update)" "$f"
        done
        ;;
    * )
        usage
        exit 4 ;;
esac
