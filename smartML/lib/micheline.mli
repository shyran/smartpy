(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t =
  | Int       of string
  | String    of string
  | Bytes     of string
  | Primitive of
      { name : string
      ; annotations : string list
      ; arguments : t list }
  | Sequence  of t list

val unAnnot : string list -> string

val pp : string -> t -> string

val to_json_string : t -> string

val left : t -> t

val right : t -> t

val annotName : string -> string

val extractAnnot : string -> string list -> string

val identity : t -> 'a

val error : string -> t -> tvalue

val unfoldRecord : ?deep:bool -> t -> (t -> t list) * t option list

val unfoldVariant :
  ?deep:bool -> t -> ((Type.t -> t -> tvalue) * (string * t) list) option

val typeOfStorageType : t -> string * (t -> Value.t) * Type.t

val unString : [> `String of 'a ] -> 'a

val parse : Yojson.Basic.t -> t

val int : string -> t

val string : string -> t

val bytes : string -> t

val primitive : string -> ?annotations:string list -> t list -> t

val sequence : t list -> t
