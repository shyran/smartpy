(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Utils
open Result
module Option = Base.Option

let full_types_and_tags = false

type 'm mtype_f =
  | MTunit
  | MTbool
  | MTnat
  | MTint
  | MTmutez
  | MTstring
  | MTbytes
  | MTtimestamp
  | MTaddress
  | MTkey
  | MTkey_hash
  | MTsignature
  | MToperation
  | MToption    of 'm
  | MTlist      of 'm
  | MTset       of 'm
  | MTcontract  of 'm
  | MTpair      of 'm * 'm
  | MTor        of 'm * 'm
  | MTlambda    of 'm * 'm
  | MTmap       of 'm * 'm
  | MTbig_map   of 'm * 'm
  | MTannot     of string * 'm * bool (* explicit ? *)
  | MTmissing   of string
[@@deriving eq, ord, fold, map, show {with_path = false}]

type mtype = {mt : mtype mtype_f}
[@@deriving eq, ord, show {with_path = false}]

let rec cata_mtype f {mt} = f (map_mtype_f (cata_mtype f) mt)

let has_missing_type =
  cata_mtype (function
      | MTmissing _ -> true
      | x -> fold_mtype_f ( || ) false x)

let mt_unit = {mt = MTunit}

let mt_bool = {mt = MTbool}

let mt_nat = {mt = MTnat}

let mt_int = {mt = MTint}

let mt_mutez = {mt = MTmutez}

let mt_string = {mt = MTstring}

let mt_bytes = {mt = MTbytes}

let mt_timestamp = {mt = MTtimestamp}

let mt_address = {mt = MTaddress}

let mt_key = {mt = MTkey}

let mt_key_hash = {mt = MTkey_hash}

let mt_signature = {mt = MTsignature}

let mt_operation = {mt = MToperation}

let mt_option t = {mt = MToption t}

let mt_list t = {mt = MTlist t}

let mt_set t = {mt = MTset t}

let mt_contract t = {mt = MTcontract t}

let mt_pair t1 t2 = {mt = MTpair (t1, t2)}

let mt_or t1 t2 = {mt = MTor (t1, t2)}

let mt_lambda t1 t2 = {mt = MTlambda (t1, t2)}

let mt_map t1 t2 = {mt = MTmap (t1, t2)}

let mt_big_map t1 t2 = {mt = MTbig_map (t1, t2)}

let mt_missing s = {mt = MTmissing s}

let mt_annot s t real = {mt = MTannot (s, t, real)}

let rec remove_annots {mt} = {mt = remove_annots_f mt}

and remove_annots_f = function
  | MTannot (_, t, _) -> (remove_annots t).mt
  | MToption t -> MToption (remove_annots t)
  | MTlist t -> MTlist (remove_annots t)
  | MTcontract t -> MTcontract (remove_annots t)
  | MTpair (t1, t2) -> MTpair (remove_annots t1, remove_annots t2)
  | MTor (t1, t2) -> MTor (remove_annots t1, remove_annots t2)
  | MTlambda (t1, t2) -> MTlambda (remove_annots t1, remove_annots t2)
  | MTmap (t1, t2) -> MTmap (remove_annots t1, remove_annots t2)
  | MTbig_map (t1, t2) -> MTbig_map (remove_annots t1, remove_annots t2)
  | ( MTint | MTnat | MTstring | MTbytes | MTmutez | MTbool | MTkey
    | MTkey_hash | MTtimestamp | MTaddress | MTunit | MTsignature | MTset _
    | MToperation | MTmissing _ ) as x ->
      x

type ad_step =
  | A
  | D
[@@deriving eq, show {with_path = false}]

module Literal = struct
  type tezos_int = Utils.Big_int.big_int [@@deriving show {with_path = false}]

  let compare_tezos_int = Utils.Big_int.compare_big_int

  let equal_tezos_int x y = Utils.Big_int.eq_big_int x y

  type t =
    | Nat    of tezos_int
    | Int    of tezos_int
    | Bool   of bool
    | String of string
    | Bytes  of string
    | Unit
    | Mutez  of tezos_int
    | Pair   of t * t
    | None   of mtype
    | Left   of t
    | Right  of t
    | Some   of t
    | Set    of {elements : t list}
    | Map    of {elements : (t * t) list}
    | List   of
        { t : mtype
        ; elements : t list }
  [@@deriving eq, ord, show {with_path = false}]

  let nat i = Nat (Utils.Big_int.big_int_of_int i)

  let int i = Int (Utils.Big_int.big_int_of_int i)

  let rec has_missing = function
    | Pair (t, u) -> has_missing t || has_missing u
    | None t -> has_missing_type t
    | Set {elements} | List {elements} -> List.exists has_missing elements
    | Map {elements} ->
        List.exists (fun (t, u) -> has_missing t || has_missing u) elements
    | _ -> false

  let rec to_michelson_string ~protect =
    let continue ~protect = to_michelson_string ~protect in
    let open Printf in
    let prot ~protect s = if protect then Printf.sprintf "(%s)" s else s in
    function
    | Nat i -> Big_int.string_of_big_int i
    | Int i -> Big_int.string_of_big_int i
    | Unit -> "Unit"
    | Mutez i -> Big_int.string_of_big_int i
    | String s -> Printf.sprintf "%S" s
    (* | Hash s -> Some (Printf.sprintf "\"%s\"" (String.escaped s))
     * | Address s -> Some (Printf.sprintf "\"%s\"" (String.escaped s)) *)
    | Bool true -> "True"
    | Bool false -> "False"
    | Pair (l, r) ->
        prot
          ~protect
          (sprintf
             "Pair %s %s"
             (continue ~protect:true l)
             (continue ~protect:true r))
    | None _ -> "None"
    | Left l -> prot ~protect (sprintf "Left %s" (continue ~protect:true l))
    | Right l -> prot ~protect (sprintf "Right %s" (continue ~protect:true l))
    | Some l -> prot ~protect (sprintf "Some %s" (continue ~protect:true l))
    | Bytes string_bytes -> "0x" ^ Utils.Hex.hexcape string_bytes
    | Map {elements} ->
        sprintf
          "{%s}"
          (String.concat
             "; "
             (List.map
                (fun (k, v) ->
                  sprintf
                    "Elt %s %s"
                    (continue ~protect:true k)
                    (continue ~protect:true v))
                (List.sort (fun (k1, _) (k2, _) -> compare k1 k2) elements)))
    | Set {elements} ->
        sprintf
          "{%s}"
          (String.concat
             "; "
             (List.map (continue ~protect:false) (List.sort compare elements)))
    | List {elements} ->
        sprintf
          "{%s}"
          (String.concat "; " (List.map (continue ~protect:false) elements))

  let to_michelson_string = to_michelson_string ~protect:true
end

type target =
  | T_params
  | T_storage
  | T_operations
  | T_lambda_parameter
  | T_local            of string
  | T_iter             of string
  | T_variant_arg      of string
[@@deriving eq, show {with_path = false}]

type stack_tag =
  | ST_none
  | ST_target of target
  | ST_pair   of stack_tag * stack_tag
[@@deriving eq, show {with_path = false}]

type stack_element =
  { se_type : mtype
  ; se_tag : stack_tag }
[@@deriving eq, show {with_path = false}]

type stack =
  | Stack_ok     of stack_element list
  | Stack_failed
[@@deriving eq, show {with_path = false}]

type 'i instr_f =
  | MIerror            of string
  | MIcomment          of string
  | MImich             of mtype Basics.inline_michelson
  | MIdip              of 'i
  | MIdipn             of int * 'i
  | MIloop             of 'i
  | MIiter             of 'i
  | MImap              of 'i
  | MIdrop
  | MIdup
  | MIdig              of int
  | MIdug              of int
  | MIfailwith
  | MIif               of 'i * 'i
  | MIif_left          of 'i * 'i
  | MIif_some          of 'i * 'i
  | MInil              of mtype
  | MIcons
  | MInone             of mtype
  | MIsome
  | MIpair
  | MIleft             of mtype
  | MIright            of mtype
  | MIpush             of mtype * Literal.t
  | MIseq              of 'i list
  | MIswap
  | MIunpair
  | MIfield            of ad_step list
  | MIsetField         of ad_step list
  | MIcontract         of string option * mtype
  | MIexec
  | MIlambda           of mtype * mtype * 'i
  | MIself
  | MIaddress
  | MIimplicit_account
  | MItransfer_tokens
  | MIcheck_signature
  | MIset_delegate
  | MIeq
  | MIneq
  | MIle
  | MIlt
  | MIge
  | MIgt
  | MIcompare
  | MImul
  | MIadd
  | MIsub
  | MIediv
  | MInot
  | MIand
  | MIor
  | MIconcat
  | MIslice
  | MIsize
  | MIget
  | MIupdate
  | MIsender
  | MIsource
  | MIamount
  | MIbalance
  | MInow
  | MImem
  | MIhash_key
  | MIblake2b
  | MIsha256
  | MIsha512
  | MIabs
  | MIneg
  | MIint
  | MIisnat
  | MIpack
  | MIunpack           of mtype
[@@deriving eq, show {with_path = false}, map, fold]

type instr = {instr : instr instr_f} [@@deriving eq, show {with_path = false}]

type tinstr =
  { tinstr : tinstr instr_f
  ; stack : stack Result.t }
[@@deriving eq, show {with_path = false}]

let rec instr_size {instr} =
  fold_instr_f ( + ) 1 (map_instr_f instr_size instr)

let rec forget_types : tinstr -> instr =
 fun {tinstr} -> {instr = map_instr_f forget_types tinstr}

(** Print instr as if we had it defined as [type instr = instr
   instr_f] (using [-rectypes]), i.e. without [{instr=...}] at each
   level. *)
let rec pp_instr_rectypes pp {instr} = pp_instr_f pp_instr_rectypes pp instr

let show_instr_rectypes {instr} = show_instr_f pp_instr_rectypes instr

let string_of_ad_path p =
  String.concat
    ""
    (List.map
       (function
         | A -> "A"
         | D -> "D")
       p)

let rec s_expression_of_mtype ?full ?annot {mt} =
  let is_full = full = Some () in
  let open Base.Sexp in
  let atom s = Atom s in
  let call s l = List (atom s :: l) in
  let with_annot s =
    match annot with
    | None -> atom s
    | Some x -> call s [atom x]
  in
  let call_with_annot s l =
    match annot with
    | None -> List (atom s :: l)
    | Some x -> List (atom s :: atom x :: l)
  in
  let continue = s_expression_of_mtype ?full in
  match mt with
  | MTannot (a, t, true) -> continue ~annot:("%" ^ a) t
  | MTannot (a, t, false) when is_full -> continue ~annot:("@" ^ a) t
  | MTannot (_, t, false) -> continue ?annot t
  | MTkey -> with_annot "key"
  | MTunit -> with_annot "unit"
  | MTsignature -> with_annot "signature"
  | MToption t -> call_with_annot "option" [continue t]
  | MTlist t -> call_with_annot "list" [continue t]
  | MTset t -> call_with_annot "set" [continue t]
  | MToperation -> with_annot "operation"
  | MTcontract t -> call_with_annot "contract" [continue t]
  | MTpair (t1, t2) -> call_with_annot "pair" [continue t1; continue t2]
  | MTor (t1, t2) -> call_with_annot "or" [continue t1; continue t2]
  | MTlambda (t1, t2) -> call_with_annot "lambda" [continue t1; continue t2]
  | MTmap (t1, t2) -> call_with_annot "map" [continue t1; continue t2]
  | MTbig_map (t1, t2) -> call_with_annot "big_map" [continue t1; continue t2]
  | MTmissing s -> call_with_annot "missing_type_conversion" [Atom s]
  | MTint -> with_annot "int"
  | MTnat -> with_annot "nat"
  | MTstring -> with_annot "string"
  | MTbytes -> with_annot "bytes"
  | MTmutez -> with_annot "mutez"
  | MTbool -> with_annot "bool"
  | MTkey_hash -> with_annot "key_hash"
  | MTtimestamp -> with_annot "timestamp"
  | MTaddress -> with_annot "address"

let string_of_mtype ?full ?protect ?annot t =
  let s_expr = s_expression_of_mtype ?full ?annot t in
  let maybe_escape_string =
    (* See how this is used in `_opam/lib/sexplib0/sexp.ml` *)
    Base.Sexp.Private.mach_maybe_esc_str
  in
  let sexp_to_string_flat sexp =
    let open Base.Sexp in
    let buf = Buffer.create 512 in
    let rec go = function
      | Atom s -> Buffer.add_string buf (maybe_escape_string s)
      | List [] -> Buffer.add_string buf "()"
      | List (h :: t) ->
          Buffer.add_char buf '(';
          go h;
          Base.List.iter t ~f:(fun elt ->
              Buffer.add_char buf ' ';
              go elt);
          Buffer.add_char buf ')'
    in
    go sexp;
    Buffer.contents buf
  in
  match (protect, s_expr) with
  | None, List l -> List.map sexp_to_string_flat l |> String.concat " "
  | None, Atom a -> maybe_escape_string a
  | Some (), any -> sexp_to_string_flat any

let string_of_target = function
  | T_params -> "params"
  | T_storage -> "storage"
  | T_operations -> "operations"
  | T_lambda_parameter -> Printf.sprintf "lambda_parameter"
  | T_local n -> Printf.sprintf "local(%s)" n
  | T_iter n -> Printf.sprintf "iter(%s)" n
  | T_variant_arg arg_name -> Printf.sprintf "variant_arg(%s)" arg_name

let rec string_of_stack_tag ?protect =
  let prot x = if protect = Some () then Printf.sprintf "(%s)" x else x in
  function
  | ST_none -> "_"
  | ST_target t -> string_of_target t
  | ST_pair (x, y) ->
      prot
        (Printf.sprintf
           "pair(%s, %s)"
           (string_of_stack_tag x)
           (string_of_stack_tag y))

let string_of_stack_element ?full = function
  | {se_type; se_tag = ST_none} -> string_of_mtype ?full se_type
  | {se_type; se_tag} ->
      if full = Some ()
      then
        Printf.sprintf
          "%s{%s}"
          (string_of_mtype ?full se_type)
          (string_of_stack_tag se_tag)
      else string_of_stack_tag se_tag

let string_of_ok_stack ?full stack =
  String.concat "." (List.map (string_of_stack_element ?full) stack)

let string_of_stack ?full = function
  | Stack_ok stack -> string_of_ok_stack ?full stack
  | Stack_failed -> "FAILED"

let strip_one_field_annot = function
  | {mt = MTannot (_, t, true)} -> t
  | t -> t

let rec strip_annots = function
  | {mt = MTannot (_, t, _)} -> strip_annots t
  | t -> t

let strip_annots_se {se_type; se_tag} = {se_type = strip_annots se_type; se_tag}

(** {1 Stack helpers} *)
type rule_result =
  | Rule_ok     of int * stack_element list
  | Rule_failed

type instr_spec =
  { name : string
  ; rule : tparams:mtype -> stack_element list -> rule_result option
  ; commutative : bool }

let stripped f x = f (List.map strip_annots_se x)

let mk_spec_tagged name ?commutative ?no_strip r =
  { name
  ; rule =
      (fun ~tparams:_ stack ->
        Base.Option.map
          ~f:(fun (i, x) -> Rule_ok (i, x))
          ((if no_strip = Some () then r else stripped r) stack))
  ; commutative = commutative = Some () }

let untag {se_type = {mt}} = mt

let untagged se_type = {se_type; se_tag = ST_none}

let mk_spec name ?commutative ?no_strip r =
  mk_spec_tagged name ?commutative ?no_strip (fun stack ->
      Base.Option.map
        ~f:(fun (i, x) -> (i, List.map untagged x))
        (r (List.map untag stack)))

let mk_spec_const name t = mk_spec name (fun _ -> Some (0, [t]))

let spec_on_stack ~tparams {name; rule} stack =
  match rule ~tparams stack with
  | None -> Error (name ^ " on " ^ string_of_stack ~full:() (Stack_ok stack))
  | Some Rule_failed -> Ok Stack_failed
  | Some (Rule_ok (n, xs)) -> Ok (Stack_ok (xs @ Base.List.drop stack n))

let mi_error msg _ = Error msg

let unstack_se err ok = function
  | se :: tail -> (ok se) tail
  | [] -> Error err

let unstack_strip err ok =
  unstack_se err (fun {se_type} -> ok (strip_annots se_type))

let unstack_x x err ok =
  unstack_strip err (fun x' -> if x = x' then ok else mi_error err)

let unstack_bool = unstack_x mt_bool "not a bool"

let unstack_option ok =
  unstack_strip "empty stack" (function
      | {mt = MToption t} -> ok t
      | _ -> mi_error "not an option")

let unstack_any = unstack_se "empty stack"

let cons_stack x = function
  | Stack_ok stack -> Stack_ok (x :: stack)
  | Stack_failed -> Stack_failed

let prepend_stack xs = function
  | Stack_ok stack -> Stack_ok (xs @ stack)
  | Stack_failed -> Stack_failed

(** {1 Unification}  *)

let unifiable_types t u = remove_annots t = remove_annots u

let unify_stack_tags t u =
  match (t, u) with
  | _ when t = u -> t
  | ST_none, _ -> u
  | _, ST_none -> t
  | _ -> ST_none

let unify_stack_elements
    {se_type = t1; se_tag = tag1} {se_type = t2; se_tag = tag2} =
  if remove_annots t1 = remove_annots t2
  then Some {se_type = t1; se_tag = unify_stack_tags tag1 tag2}
  else None

let rec unify_ok_stacks s1 s2 =
  match (s1, s2) with
  | se1 :: s1, se2 :: s2 ->
      Option.map2
        ~f:(fun x xs -> x :: xs)
        (unify_stack_elements se1 se2)
        (unify_ok_stacks s1 s2)
  | [], [] -> Some []
  | _ -> None

let unifiable_ok_stacks t u = Option.is_some (unify_ok_stacks t u)

(* Types_only removes annotations and tags. *)
let unify_stacks s1 s2 =
  match (s1, s2) with
  | Stack_ok s1, Stack_ok s2 ->
      Option.map ~f:(fun x -> Stack_ok x) (unify_ok_stacks s1 s2)
  | Stack_failed, s2 -> Some s2
  | s1, Stack_failed -> Some s1

(** {1 Michelson instructions} *)

let rec ty_seq_in stack = function
  | [] -> []
  | x :: xs ->
      let x = x stack in
      x :: ty_seq_in x.stack xs

let rec ty_seq_out xs stack =
  match xs with
  | [] -> stack
  | x :: xs -> ty_seq_out xs (Ok x)

let mi_failwith =
  { name = "FAILWITH"
  ; rule = (fun ~tparams:_ _ -> Some Rule_failed)
  ; commutative = false }

let ty_if_some_in1 =
  unstack_option (fun se_type tail ->
      Ok (Stack_ok ({se_type; se_tag = ST_none} :: tail)))

let ty_if_some_in2 = unstack_option (fun _ tail -> Ok (Stack_ok tail))

let ty_if_left_in1, ty_if_left_in2 =
  let aux f =
    unstack_se "empty stack" (fun {se_tag; se_type} tail ->
        match strip_annots se_type with
        | {mt = MTor (a, b)} ->
            let se_tag =
              if se_tag = ST_target T_params
              then ST_target T_params
              else ST_none
            in
            Ok (Stack_ok ({se_type = f a b; se_tag} :: tail))
        | _ -> Error "not an or")
  in
  (aux (fun l _ -> l), aux (fun _ r -> r))

let ty_if_in = unstack_bool (fun tail -> Ok (Stack_ok tail))

let ty_if_out l r _stack =
  match unify_stacks l r with
  | Some s -> Ok s
  | None -> Error "cannot unify branches"

let ty_dip_in = unstack_any (fun _ tail -> Ok (Stack_ok tail))

let ty_dip_out body = unstack_any (fun se _ -> Ok (cons_stack se body))

let ty_dipn_in n stack =
  if n < 0
  then Error "negative index"
  else if n > List.length stack
  then Error "stack too short"
  else Ok (Stack_ok (Base.List.drop stack n))

let ty_dipn_out n body stack =
  if n < 0
  then Error "negative index"
  else if n > List.length stack
  then Error "stack too short"
  else Ok (prepend_stack (Base.List.take stack n) body)

let mi_dup =
  mk_spec_tagged "DUP" ~no_strip:() (function
      | [] -> None
      | x :: _ -> Some (1, [x; x]))

let mi_dig n stack =
  let hi, lo = Base.List.split_n stack n in
  match lo with
  | [] -> Error (Printf.sprintf "DIG %i: stack too short" n)
  | x :: lo -> Ok (Stack_ok ((x :: hi) @ lo))

let mi_dug n = function
  | x :: tail ->
      if n > List.length tail
      then Error (Printf.sprintf "DUG %i: stack too short" n)
      else
        let hi, lo = Base.List.split_n tail n in
        Ok (Stack_ok (hi @ (x :: lo)))
  | [] -> Error (Printf.sprintf "DUG %i: stack not long enough" n)

let mi_swap =
  mk_spec_tagged "SWAP" ~no_strip:() (function
      | a :: b :: _ -> Some (2, [b; a])
      | _ -> None)

let mi_drop =
  mk_spec_tagged "DROP" (function
      | _ :: _ -> Some (1, [])
      | [] -> None)

let mi_unpair =
  mk_spec_tagged "UNPAIR" ~no_strip:() (function
      | {se_type = {mt = MTpair (a, b)}; se_tag} :: _ ->
          let ta, tb =
            match se_tag with
            | ST_pair (ta, tb) -> (ta, tb)
            | _ -> (ST_none, ST_none)
          in
          Some
            ( 1
            , [ {se_type = strip_one_field_annot a; se_tag = ta}
              ; {se_type = strip_one_field_annot b; se_tag = tb} ] )
      | _ -> None)

let ty_iter_in =
  let aux se_type tail = Ok (Stack_ok ({se_type; se_tag = ST_none} :: tail)) in
  unstack_strip "empty stack" (function
      | {mt = MTmap (k, v)} -> aux {mt = MTpair (k, v)}
      | {mt = MTlist t} -> aux t
      | {mt = MTset k} -> aux k
      | _ -> fun _ -> Error "not a container")

let ty_iter_out body =
  unstack_strip "empty stack" (function
      | {mt = MTmap _ | MTlist _ | MTset _} ->
          fun tail ->
            ( match body with
            | Stack_ok stack' when not (unifiable_ok_stacks tail stack') ->
                Error "stack mismatch"
            | _ -> Ok (Stack_ok tail) )
      | _ -> fun _ -> Error "not a container")

let ty_map_in = ty_iter_in

let ty_lambda_in t_in =
  Ok (Stack_ok [{se_type = t_in; se_tag = ST_target T_lambda_parameter}])

let ty_lambda_out t1 t2 stack s =
  match stack with
  | Stack_ok [{se_type}] when unifiable_types t2 se_type ->
      Ok (Stack_ok (untagged (mt_lambda t1 t2) :: s))
  | _ -> Error "lambda: incompatible output stack"

let ty_map_out body =
  let aux f tail =
    match body with
    | Stack_ok (_ :: tail') when not (unifiable_ok_stacks tail tail') ->
        Error "stack mismatch"
    | Stack_ok ({se_type = v'} :: _) ->
        f (strip_annots v')
        >>= fun se_type ->
        return (Stack_ok ({se_type; se_tag = ST_none} :: tail))
    | Stack_ok [] -> Error "empty stack"
    | Stack_failed -> Error "body fails"
  in
  unstack_strip "empty stack" (function
      | {mt = MTmap (k, _)} -> aux (fun t -> Ok {mt = MTmap (k, t)})
      | {mt = MTlist _} -> aux (fun t -> Ok {mt = MTlist t})
      | {mt = MTset _} -> aux (fun v -> Ok {mt = MTset v})
      | _ -> fun _ -> Error "not a container")

let ty_loop_in = unstack_bool (fun tail -> Ok (Stack_ok tail))

let ty_loop_out body =
  unstack_bool (fun stack ->
      match body with
      | Stack_ok ({se_type} :: _) when strip_annots se_type != mt_bool ->
          Error "not a bool"
      | Stack_ok (_ :: tail) when not (unifiable_ok_stacks stack tail) ->
          Error "stack mismatch"
      | Stack_ok _ -> Ok (Stack_ok stack)
      | Stack_failed -> Error "body fails")

let mi_pair =
  mk_spec_tagged "PAIR" ~no_strip:() (function
      | {se_type = a; se_tag = ta} :: {se_type = b; se_tag = tb} :: _ ->
          Some
            ( 2
            , [ { se_type = {mt = MTpair (a, b)}
                ; se_tag =
                    ( match (ta, tb) with
                    | ST_none, ST_none -> ST_none
                    | _ -> ST_pair (ta, tb) ) } ] )
      | _ -> None)

let mi_cons =
  mk_spec_tagged "CONS" (function
      | {se_type = a} :: {se_type = {mt = MTlist a'}} :: _
        when unifiable_types (strip_annots a) (strip_annots a') ->
          Some (2, [untagged (mt_list a)])
      | _ -> None)

let mi_get =
  mk_spec_tagged "GET" ~no_strip:() (function
      | {se_type = key} :: {se_type = {mt = MTmap (key', value)}} :: _
        when unifiable_types key key' ->
          Some (2, [untagged (mt_option value)])
      | {se_type = key} :: {se_type = {mt = MTbig_map (key', value)}} :: _
        when unifiable_types key key' ->
          Some (2, [untagged (mt_option value)])
      | _ -> None)

let mi_eq =
  mk_spec "EQ" ~commutative:() (function
      | MTint :: _ -> Some (1, [mt_bool])
      | _ -> None)

let mi_neq = {mi_eq with name = "NEQ"}

let mi_lt = {mi_neq with name = "LT"; commutative = false}

let mi_le = {mi_lt with name = "LE"}

let mi_gt = {mi_lt with name = "GT"}

let mi_ge = {mi_lt with name = "GE"}

let mi_neg =
  mk_spec "NEG" (function
      | MTnat :: _ -> Some (1, [mt_int])
      | MTint :: _ -> Some (1, [mt_int])
      | _ -> None)

let mi_abs =
  mk_spec "ABS" (function
      | MTint :: _ -> Some (1, [mt_nat])
      | _ -> None)

let mi_isnat =
  mk_spec "ISNAT" (function
      | MTint :: _ -> Some (1, [mt_option mt_nat])
      | _ -> None)

let mi_int =
  mk_spec "INT" (function
      | MTnat :: _ -> Some (1, [mt_int])
      | _ -> None)

let rec is_simple_comparable {mt} =
  match mt with
  | MTint | MTnat | MTstring | MTbytes | MTmutez | MTbool | MTkey_hash
   |MTtimestamp | MTaddress ->
      true
  | MTannot (_, t, _) -> is_simple_comparable t
  | _ -> false

let rec is_comparable {mt} =
  match mt with
  | MTpair (t1, t2) -> is_simple_comparable t1 && is_comparable t2
  | MTannot (_, t, _) -> is_comparable t
  | mt -> is_simple_comparable {mt}

let mi_compare =
  mk_spec "COMPARE" (function
      | a :: b :: _ when is_comparable {mt = a} && is_comparable {mt = b} ->
          Some (2, [mt_int])
      | _ -> None)

let mi_add =
  mk_spec "ADD" ~commutative:() (function
      | MTint :: MTint :: _ -> Some (2, [mt_int])
      | MTint :: MTnat :: _ -> Some (2, [mt_int])
      | MTnat :: MTint :: _ -> Some (2, [mt_int])
      | MTnat :: MTnat :: _ -> Some (2, [mt_nat])
      | MTmutez :: MTmutez :: _ -> Some (2, [mt_mutez])
      | MTtimestamp :: MTint :: _ -> Some (2, [mt_timestamp])
      | MTint :: MTtimestamp :: _ -> Some (2, [mt_timestamp])
      | _ -> None)

let mi_sub =
  mk_spec "SUB" (function
      | MTint :: MTint :: _ -> Some (2, [mt_int])
      | MTint :: MTnat :: _ -> Some (2, [mt_int])
      | MTnat :: MTint :: _ -> Some (2, [mt_int])
      | MTnat :: MTnat :: _ -> Some (2, [mt_nat])
      | MTmutez :: MTmutez :: _ -> Some (2, [mt_mutez])
      | MTtimestamp :: MTint :: _ -> Some (2, [mt_timestamp])
      | MTtimestamp :: MTtimestamp :: _ -> Some (2, [mt_int])
      | _ -> None)

let mi_mul =
  mk_spec "MUL" ~commutative:() (function
      | MTint :: MTint :: _ -> Some (2, [mt_int])
      | MTint :: MTnat :: _ -> Some (2, [mt_int])
      | MTnat :: MTint :: _ -> Some (2, [mt_int])
      | MTnat :: MTnat :: _ -> Some (2, [mt_nat])
      | MTmutez :: MTnat :: _ -> Some (2, [mt_mutez])
      | MTnat :: MTmutez :: _ -> Some (2, [mt_mutez])
      | _ -> None)

let mi_ediv =
  mk_spec "EDIV" (function
      | MTint :: MTint :: _ -> Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | MTint :: MTnat :: _ -> Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | MTnat :: MTint :: _ -> Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | MTnat :: MTnat :: _ -> Some (2, [mt_option (mt_pair mt_nat mt_nat)])
      | MTmutez :: MTnat :: _ ->
          Some (2, [mt_option (mt_pair mt_mutez mt_mutez)])
      | MTmutez :: MTmutez :: _ ->
          Some (2, [mt_option (mt_pair mt_nat mt_mutez)])
      | _ -> None)

let mi_not =
  mk_spec "NOT" (function
      | MTbool :: _ -> Some (1, [mt_bool])
      | _ -> None)

let mi_and =
  mk_spec "AND" ~commutative:() (function
      | MTbool :: MTbool :: _ -> Some (2, [mt_bool])
      | _ -> None)

let mi_or = {mi_and with name = "OR"}

let mi_nil t = mk_spec_const "NIL" (mt_list t)

let mi_none t = mk_spec_const "NONE" (mt_option t)

let mi_push t = mk_spec_const "PUSH" t

let mi_some =
  mk_spec "SOME" ~no_strip:() (function
      | t :: _ -> Some (1, [mt_option {mt = t}])
      | [] -> None)

let mi_left b =
  mk_spec "LEFT" ~no_strip:() (function
      | a :: _ -> Some (1, [mt_or {mt = a} b])
      | [] -> None)

let mi_right a =
  mk_spec "RIGHT" ~no_strip:() (function
      | b :: _ -> Some (1, [mt_or a {mt = b}])
      | [] -> None)

(** Select the part of the type designated by the ad_path. *)
let rec ad_path_in_type ops {mt} =
  match (ops, mt) with
  | [], mt -> Some {mt}
  | ops, MTannot (_, t, _) -> ad_path_in_type ops t
  | A :: p, MTpair (a, _) -> ad_path_in_type p a
  | D :: p, MTpair (_, d) -> ad_path_in_type p d
  | _ :: _, _ -> None

let rec ad_path_in_stack_tag ops t =
  match (ops, t) with
  | [], t -> t
  | A :: p, ST_pair (a, _) -> ad_path_in_stack_tag p a
  | D :: p, ST_pair (_, d) -> ad_path_in_stack_tag p d
  | _ :: _, _ -> ST_none

let mi_field steps =
  mk_spec_tagged
    (Printf.sprintf "C%sR" (string_of_ad_path steps))
    (function
      | {se_type; se_tag} :: _ ->
        ( match ad_path_in_type steps se_type with
        | None -> None
        | Some t ->
            Some
              ( 1
              , let se_tag = ad_path_in_stack_tag steps se_tag in
                [{se_type = strip_one_field_annot t; se_tag}] ) )
      | [] -> None)

let mi_set_field steps =
  mk_spec
    (Printf.sprintf "SET_C%sR" (string_of_ad_path steps))
    (function
      | t :: x :: _ ->
        ( match ad_path_in_type steps {mt = t} with
        | Some x' when unifiable_types {mt = x} x' -> Some (2, [{mt = t}])
        | _ -> None )
      | _ -> None)

let mi_update =
  mk_spec "UPDATE" (function
      | k :: MTbool :: MTset k' :: _ when unifiable_types {mt = k} k' ->
          Some (2, [])
      | k :: MToption v :: MTmap (k', v') :: _
        when unifiable_types {mt = k} k' && unifiable_types v v' ->
          Some (2, [])
      | k :: MToption v :: MTbig_map (k', v') :: _
        when unifiable_types {mt = k} k' && unifiable_types v v' ->
          Some (2, [])
      | _ -> None)

let mi_mem =
  mk_spec "MEM" (function
      | k :: MTset k' :: _ when unifiable_types {mt = k} k' ->
          Some (2, [mt_bool])
      | k :: MTmap (k', _) :: _ when unifiable_types {mt = k} k' ->
          Some (2, [mt_bool])
      | k :: MTbig_map (k', _) :: _ when unifiable_types {mt = k} k' ->
          Some (2, [mt_bool])
      | _ -> None)

let mi_exec =
  mk_spec "EXEC" (function
      | k :: MTlambda (k', v) :: _ when unifiable_types {mt = k} k' ->
          Some (2, [v])
      | _ -> None)

let mi_contract t =
  mk_spec "CONTRACT" (function
      | MTaddress :: _ -> Some (1, [mt_option (mt_contract t)])
      | _ -> None)

let mi_transfer_tokens =
  mk_spec "TRANSFER_TOKENS" (function
      | p :: MTmutez :: MTcontract p' :: _ when unifiable_types {mt = p} p' ->
          Some (3, [mt_operation])
      | _ -> None)

let mi_set_delegate =
  mk_spec "SET_DELEGATE" (function
      | MToption {mt = MTkey_hash} :: _ -> Some (1, [mt_operation])
      | _ -> None)

let mi_hash_key =
  mk_spec "HASH_KEY" (function
      | MTkey :: _ -> Some (1, [mt_key_hash])
      | _ -> None)

let mi_blake2b =
  mk_spec "BLAKE2B" (function
      | MTbytes :: _ -> Some (1, [mt_bytes])
      | _ -> None)

let mi_sha256 = {mi_blake2b with name = "SHA256"}

let mi_sha512 = {mi_blake2b with name = "SHA512"}

let mi_check_signature =
  mk_spec "CHECK_SIGNATURE" (function
      | MTkey :: MTsignature :: MTbytes :: _ -> Some (3, [mt_bool])
      | _ -> None)

let mi_sender = mk_spec_const "SENDER" mt_address

let mi_source = mk_spec_const "SOURCE" mt_address

let mi_amount = mk_spec_const "AMOUNT" mt_mutez

let mi_balance = mk_spec_const "BALANCE" mt_mutez

let mi_now = mk_spec_const "NOW" mt_timestamp

let mi_concat =
  mk_spec "CONCAT" (function
      | MTlist {mt = MTstring} :: _ -> Some (1, [mt_string])
      | MTlist {mt = MTbytes} :: _ -> Some (1, [mt_bytes])
      | MTstring :: MTstring :: _ -> Some (2, [mt_string])
      | MTbytes :: MTbytes :: _ -> Some (2, [mt_bytes])
      | _ -> None)

let mi_pack =
  mk_spec "PACK" (function
      | _ :: _ -> Some (1, [mt_bytes])
      | [] -> None)

let mi_unpack t =
  mk_spec "UNPACK" (function
      | MTbytes :: _ -> Some (1, [mt_option t])
      | _ -> None)

let mi_slice =
  mk_spec "SLICE" (function
      | MTnat :: MTnat :: MTstring :: _ -> Some (3, [mt_option mt_string])
      | MTnat :: MTnat :: MTbytes :: _ -> Some (3, [mt_option mt_bytes])
      | _ -> None)

let mi_size =
  mk_spec "SIZE" (function
      | (MTstring | MTbytes | MTset _ | MTmap _ | MTbig_map _ | MTlist _) :: _
        ->
          Some (1, [mt_nat])
      | _ -> None)

let mi_mich ~name ~types_in ~types_out =
  mk_spec name (fun stack ->
      if Base.List.is_prefix
           ~prefix:types_in
           (List.map (fun mt -> {mt}) stack)
           ~equal:equal_mtype
      then Some (List.length types_in, types_out)
      else None)

let mi_self =
  { name = "SELF"
  ; rule =
      (fun ~tparams _ -> Some (Rule_ok (0, [untagged (mt_contract tparams)])))
  ; commutative = false }

let mi_address =
  mk_spec "ADDRESS" (function
      | MTcontract _ :: _ -> Some (1, [mt_address])
      | _ -> None)

let mi_implicit_account =
  mk_spec "IMPLICIT_ACCOUNT" (function
      | MTkey_hash :: _ -> Some (1, [mt_contract mt_unit])
      | _ -> None)

let spec_of_instr = function
  | MIadd -> Some mi_add
  | MIsub -> Some mi_sub
  | MImul -> Some mi_mul
  | MIediv -> Some mi_ediv
  | MInil t -> Some (mi_nil t)
  | MInone t -> Some (mi_none t)
  | MIpush (t, _l) -> Some (mi_push t)
  | MIcontract (_, t) -> Some (mi_contract t)
  | MIconcat -> Some mi_concat
  | MIslice -> Some mi_slice
  | MIset_delegate -> Some mi_set_delegate
  | MIcheck_signature -> Some mi_check_signature
  | MIhash_key -> Some mi_hash_key
  | MIblake2b -> Some mi_blake2b
  | MIsha256 -> Some mi_sha256
  | MIsha512 -> Some mi_sha512
  | MIeq -> Some mi_eq
  | MIneq -> Some mi_neq
  | MIlt -> Some mi_lt
  | MIle -> Some mi_le
  | MIgt -> Some mi_gt
  | MIge -> Some mi_ge
  | MIneg -> Some mi_neg
  | MIabs -> Some mi_abs
  | MIisnat -> Some mi_isnat
  | MIint -> Some mi_int
  | MIcompare -> Some mi_compare
  | MIsender -> Some mi_sender
  | MIsource -> Some mi_source
  | MIamount -> Some mi_amount
  | MIbalance -> Some mi_balance
  | MInow -> Some mi_now
  | MIpair -> Some mi_pair
  | MIswap -> Some mi_swap
  | MIdrop -> Some mi_drop
  | MIunpair -> Some mi_unpair
  | MIfailwith -> Some mi_failwith
  | MIsome -> Some mi_some
  | MIleft t -> Some (mi_left t)
  | MIright t -> Some (mi_right t)
  | MItransfer_tokens -> Some mi_transfer_tokens
  | MIcons -> Some mi_cons
  | MInot -> Some mi_not
  | MIand -> Some mi_and
  | MIor -> Some mi_or
  | MIfield steps -> Some (mi_field steps)
  | MIsetField steps -> Some (mi_set_field steps)
  | MIpack -> Some mi_pack
  | MIunpack t -> Some (mi_unpack t)
  | MIdup -> Some mi_dup
  | MIget -> Some mi_get
  | MIsize -> Some mi_size
  | MIupdate -> Some mi_update
  | MImem -> Some mi_mem
  | MIself -> Some mi_self
  | MIexec -> Some mi_exec
  | MIaddress -> Some mi_address
  | MIimplicit_account -> Some mi_implicit_account
  | MImich {name; typesIn; typesOut} ->
      Some (mi_mich ~name ~types_in:typesIn ~types_out:typesOut)
  | MIerror _ | MIcomment _ | MIdip _
   |MIdipn (_, _)
   |MIloop _ | MIiter _ | MImap _ | MIdig _ | MIdug _
   |MIif (_, _)
   |MIif_left (_, _)
   |MIif_some (_, _)
   |MIseq _ | MIlambda _ ->
      None

let is_commutative instr =
  match spec_of_instr instr with
  | Some {commutative} -> commutative
  | None -> false

let name_of_instr instr =
  match spec_of_instr instr with
  | Some {name} -> name
  | None -> failwith "name_of_instr"

let instr_on_stack ~tparams instr stack =
  match spec_of_instr instr with
  | Some spec -> spec_on_stack ~tparams spec stack
  | None -> failwith "instr_on_stack"

(** {1 Type checking} *)

module Traversable (A : Base.Applicative.S) :
  TRAVERSABLE with type 'a f = 'a A.t and type 'a t = 'a instr_f = struct
  open A

  type 'a f = 'a A.t

  type 'a t = 'a instr_f

  let traverse f = function
    | MIdip x -> A.map ~f:(fun x -> MIdip x) (f x)
    | MIdipn (n, x) -> A.map ~f:(fun x -> MIdipn (n, x)) (f x)
    | MIloop x -> A.map ~f:(fun x -> MIloop x) (f x)
    | MIiter x -> A.map ~f:(fun x -> MIiter x) (f x)
    | MImap x -> A.map ~f:(fun x -> MImap x) (f x)
    | MIif (i1, i2) -> A.map2 ~f:(fun i1 i2 -> MIif (i1, i2)) (f i1) (f i2)
    | MIif_left (i1, i2) ->
        A.map2 ~f:(fun i1 i2 -> MIif_left (i1, i2)) (f i1) (f i2)
    | MIif_some (i1, i2) ->
        A.map2 ~f:(fun i1 i2 -> MIif_some (i1, i2)) (f i1) (f i2)
    | MIlambda (t1, t2, i) -> A.map ~f:(fun x -> MIlambda (t1, t2, x)) (f i)
    | MIseq is -> A.map ~f:(fun is -> MIseq is) (A.all (List.map f is))
    | ( MIdrop | MIdup | MIfailwith | MIcons | MIsome | MIpair | MIswap
      | MIunpair | MIself | MIexec | MIaddress | MIimplicit_account
      | MItransfer_tokens | MIcheck_signature | MIset_delegate | MIeq | MIneq
      | MIle | MIlt | MIge | MIgt | MIcompare | MImul | MIadd | MIsub | MIediv
      | MInot | MIand | MIor | MIconcat | MIslice | MIsize | MIget | MIupdate
      | MIsender | MIsource | MIamount | MIbalance | MInow | MImem | MIhash_key
      | MIblake2b | MIsha256 | MIsha512 | MIabs | MIneg | MIint | MIisnat
      | MIpack | MIerror _ | MIcomment _ | MImich _ | MIdig _ | MIdug _
      | MInil _ | MInone _ | MIleft _ | MIright _ | MIpush _ | MIfield _
      | MIsetField _ | MIcontract _ | MIunpack _ ) as instr ->
        return instr
end

module TR = Traversable (Result)

(** Catamorphisms on `instr`. *)
let rec cata_instr f {instr} = f (map_instr_f (cata_instr f) instr)

(** Feeds the appropriate stack into any sub-instructions. *)
let feed stack = function
  | MIdip i -> MIdip (i (ty_dip_in stack))
  | MIdipn (n, i) -> MIdipn (n, i (ty_dipn_in n stack))
  | MIloop i -> MIloop (i (ty_loop_in stack))
  | MIiter i -> MIiter (i (ty_iter_in stack))
  | MImap i -> MImap (i (ty_map_in stack))
  | MIlambda (t1, t2, i) -> MIlambda (t1, t2, i (ty_lambda_in t1))
  | MIif (i1, i2) ->
      let s = ty_if_in stack in
      MIif (i1 s, i2 s)
  | MIif_some (i1, i2) ->
      MIif_some (i1 (ty_if_some_in1 stack), i2 (ty_if_some_in2 stack))
  | MIif_left (i1, i2) ->
      MIif_left (i1 (ty_if_left_in1 stack), i2 (ty_if_left_in2 stack))
  | MIseq xs -> MIseq (ty_seq_in (Ok (Stack_ok stack)) xs)
  | ( MImich _ | MIswap | MIunpair | MIfield _ | MIsetField _ | MIcontract _
    | MIself | MIexec | MIaddress | MIimplicit_account | MItransfer_tokens
    | MIcheck_signature | MIset_delegate | MIeq | MIneq | MIle | MIlt | MIge
    | MIgt | MIcompare | MImul | MIadd | MIsub | MIediv | MInot | MIand | MIor
    | MIconcat | MIslice | MIsize | MIget | MIupdate | MIsource | MIsender
    | MIamount | MIbalance | MInow | MImem | MIhash_key | MIblake2b | MIsha256
    | MIsha512 | MIabs | MIneg | MIint | MIisnat | MIpack | MIunpack _
    | MIerror _ | MIcomment _ | MIdrop | MIdup | MIdig _ | MIdug _ | MIfailwith
    | MInil _ | MIcons | MInone _ | MIsome | MIpair | MIleft _ | MIright _
    | MIpush _ ) as s ->
      s

let combine ~tparams = function
  | ( MIadd | MIsub | MImul | MIediv | MInil _ | MInone _ | MIpush _
    | MIcontract _ | MIconcat | MIslice | MIcheck_signature | MIset_delegate
    | MIeq | MIle | MIge | MIlt | MIgt | MIneq | MIcompare | MIsender
    | MIsource | MIamount | MIbalance | MInow | MIhash_key | MIblake2b
    | MIsha256 | MIsha512 | MIabs | MIneg | MIint | MIisnat | MIpair | MIswap
    | MIdrop | MIunpair | MIfailwith | MIsome | MIleft _ | MIright _ | MIcons
    | MItransfer_tokens | MInot | MIand | MIor | MIfield _ | MIsetField _
    | MIpack | MIunpack _ | MIdup | MIget | MIsize | MIupdate | MImem
    | MImich _ | MIself | MIexec | MIaddress | MIimplicit_account ) as instr ->
      instr_on_stack ~tparams instr
  | MIdip i -> ty_dip_out i
  | MIdipn (n, i) -> ty_dipn_out n i
  | MIloop i -> ty_loop_out i
  | MIiter i -> ty_iter_out i
  | MImap i -> ty_map_out i
  | MIif (l, r) -> ty_if_out l r
  | MIif_left (l, r) -> ty_if_out l r
  | MIif_some (l, r) -> ty_if_out l r
  | MIseq xs -> fun stack -> ty_seq_out xs (Ok (Stack_ok stack))
  | MIerror _ -> fun s -> Ok (Stack_ok s)
  | MIcomment _ -> fun s -> Ok (Stack_ok s)
  | MIdig i -> mi_dig i
  | MIdug i -> mi_dug i
  | MIlambda (t1, t2, i) -> ty_lambda_out t1 t2 i

let combine tparams i stack =
  match TR.traverse (fun x -> x) i with
  | Ok x -> combine ~tparams x stack
  | Error e -> Error e

let typecheck_f tparams i = function
  | Ok (Stack_ok stack) ->
      let tinstr = feed stack i in
      let stack =
        combine tparams (map_instr_f (fun x -> x.stack) tinstr) stack
      in
      {tinstr; stack}
  | Ok Stack_failed ->
      let err = Error "instruction after FAILWITH" in
      {tinstr = map_instr_f (fun x -> x err) i; stack = err}
  | Error _ ->
      let err = Error "previous type error" in
      {tinstr = map_instr_f (fun x -> x err) i; stack = err}

let typecheck tparams stack i = cata_instr (typecheck_f tparams) i (Ok stack)

let has_error =
  cata_instr (function
      | MIerror _ -> true
      | MInil t
       |MInone t
       |MIleft t
       |MIright t
       |MIcontract (_, t)
       |MIunpack t ->
          has_missing_type t
      | MIpush (t, l) -> has_missing_type t || Literal.has_missing l
      | MImich {typesIn; typesOut} ->
          List.exists has_missing_type typesIn
          || List.exists has_missing_type typesOut
      | x -> fold_instr_f ( || ) false x)

(** {1 Printing} *)

let name_of_instr_exn = function
  | ( MIadd | MIsub | MImul | MIediv | MInil _ | MInone _ | MIpush _
    | MIcontract _ | MIconcat | MIslice | MIcheck_signature | MIset_delegate
    | MIeq | MIle | MIge | MIlt | MIgt | MIneq | MIcompare | MIsender
    | MIsource | MIamount | MIbalance | MInow | MIhash_key | MIblake2b
    | MIsha256 | MIsha512 | MIabs | MIneg | MIint | MIisnat | MIpair | MIswap
    | MIdrop | MIunpair | MIfailwith | MIsome | MIleft _ | MIright _ | MIcons
    | MItransfer_tokens | MInot | MIor | MIand | MIfield _ | MIsetField _
    | MIpack | MIunpack _ | MIdup | MIget | MIsize | MIupdate | MImem
    | MImich _ | MIself | MIexec | MIaddress | MIimplicit_account ) as instr ->
      name_of_instr instr
  | MIdip _ -> "DIP"
  | MIdipn _ -> "DIPN"
  | MIloop _ -> "LOOP"
  | MIiter _ -> "ITER"
  | MImap _ -> "MAP"
  | MIif_left _ -> "IF_LEFT"
  | MIif_some _ -> "IF_SOME"
  | MIif _ -> "IF"
  | MIdig _ -> "DIG"
  | MIdug _ -> "DUG"
  | MIlambda _ -> "LAMBDA"
  | (MIerror _ | MIcomment _ | MIseq _) as instr ->
      Format.kasprintf
        failwith
        "Instruction %a has no name"
        (pp_instr_f pp_tinstr)
        instr

let rec string_of_michCode ~html ?sub_sequence indent inst =
  let spaces =
    if html
    then
      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
    else "                        "
  in
  let ppAlign ?app s =
    let app =
      match app with
      | None -> ""
      | Some s -> Printf.sprintf " %s" s
    in
    Printf.sprintf
      "%s%s%s;%s"
      indent
      s
      app
      ( if html
      then
        String.sub
          spaces
          0
          (6 * max 0 (10 - String.length s - String.length app))
      else
        String.sub spaces 0 (max 0 (10 - String.length s - String.length app))
      )
  in
  let span className text =
    if html
    then Printf.sprintf "<span class='%s'>%s</span>" className text
    else text
  in
  let twoSpaces = if html then "&nbsp;&nbsp;" else "  " in
  let s =
    match inst.tinstr with
    | MIseq [] -> Printf.sprintf "%s{}" indent
    | MIseq l ->
        Printf.sprintf
          "%s{\n%s\n%s}"
          indent
          (String.concat
             "\n"
             (List.map (string_of_michCode ~html (indent ^ twoSpaces)) l))
          indent
    | MIdip code ->
        Printf.sprintf
          "%sDIP\n%s;"
          indent
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) code)
    | MIdipn (n, code) ->
        Printf.sprintf
          "%sD%sP\n%s;"
          indent
          (String.make n 'I')
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) code)
    | MIloop code ->
        Printf.sprintf
          "%sLOOP\n%s;"
          indent
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) code)
    | MIiter code ->
        Printf.sprintf
          "%sITER\n%s;"
          indent
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) code)
    | MImap code ->
        Printf.sprintf
          "%sMAP\n%s;"
          indent
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) code)
    | MIif_left (l, r) ->
        Printf.sprintf
          "%sIF_LEFT\n%s\n%s;"
          indent
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) l)
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) r)
    | MIif_some (l, r) ->
        Printf.sprintf
          "%sIF_SOME\n%s\n%s;"
          indent
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) l)
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) r)
    | MIif (l, r) ->
        Printf.sprintf
          "%sIF\n%s\n%s;"
          indent
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) l)
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) r)
    | MIcomment comment ->
        Printf.sprintf
          "%s%s"
          indent
          (span "comment" (Printf.sprintf "# %s" comment))
    | MIdig n -> ppAlign (Printf.sprintf "DIG %d" n)
    | MIdug n -> ppAlign (Printf.sprintf "DUG %d" n)
    | MIerror error ->
        Printf.sprintf
          "%s%s"
          indent
          (span "partialType" (Printf.sprintf "Internal error: %s" error))
    | MInil t
     |MInone t
     |MIcontract (None, t)
     |MIunpack t
     |MIleft t
     |MIright t ->
        ppAlign
          (name_of_instr_exn inst.tinstr)
          ~app:(string_of_mtype ~protect:() t)
    | MIcontract (Some ep, t) ->
        ppAlign
          (Printf.sprintf "CONTRACT %%%s %s" ep (string_of_mtype ~protect:() t))
    | MIpush (t, l) ->
        ppAlign
          (Printf.sprintf
             "PUSH %s %s"
             (string_of_mtype ~protect:() t)
             (Literal.to_michelson_string l))
    | MIlambda (t1, t2, l) ->
        Printf.sprintf
          "%sLAMBDA %s %s %s"
          indent
          (string_of_mtype ~protect:() t1)
          (string_of_mtype ~protect:() t2)
          (string_of_michCode ~html ~sub_sequence:() (indent ^ twoSpaces) l)
    | ( MIdrop | MIdup | MIfailwith | MIcons | MIsome | MIpair | MIswap
      | MIunpair | MIself | MIexec | MIaddress | MIimplicit_account
      | MItransfer_tokens | MIcheck_signature | MIset_delegate | MIeq | MIneq
      | MIle | MIlt | MIge | MIgt | MIcompare | MImul | MIadd | MIsub | MIediv
      | MInot | MIand | MIor | MIconcat | MIslice | MIsize | MIget | MIupdate
      | MIsender | MIsource | MIamount | MIbalance | MInow | MImem | MIhash_key
      | MIblake2b | MIsha256 | MIsha512 | MIabs | MIneg | MIint | MIisnat
      | MIpack | MImich _ | MIfield _ | MIsetField _ ) as simple ->
        ppAlign (name_of_instr_exn simple)
  in
  if sub_sequence = Some ()
  then
    match inst.tinstr with
    | MIseq _ -> s
    | _ ->
        string_of_michCode
          ~html
          ~sub_sequence:()
          indent
          {tinstr = MIseq [inst]; stack = inst.stack}
  else
    let full =
      match inst.tinstr with
      | MIerror _ -> Some ()
      | _ when full_types_and_tags -> Some ()
      | _ -> None
    in
    match inst.stack with
    | Ok inst ->
        Printf.sprintf
          "%s %s %s"
          s
          (span "comment" "#")
          (span "stack" (Printf.sprintf "%s" (string_of_stack ?full inst)))
    | Error msg ->
        Printf.sprintf
          "%s  %s"
          s
          (span "partialType" (Printf.sprintf "Internal error: %s" msg))

module To_micheline = struct
  let rec mtype ?(annotations = []) {mt} =
    let open Micheline in
    match mt with
    | MTint -> primitive ~annotations "int" []
    | MTbool -> primitive ~annotations "bool" []
    | MTstring -> primitive ~annotations "string" []
    | MTnat -> primitive ~annotations "nat" []
    | MTaddress -> primitive ~annotations "address" []
    | MTbytes -> primitive ~annotations "bytes" []
    | MTmutez -> primitive ~annotations "mutez" []
    | MTkey_hash -> primitive ~annotations "key_hash" []
    | MTkey -> primitive ~annotations "key" []
    | MTtimestamp -> primitive ~annotations "timestamp" []
    | MTannot (annot, t, true) -> mtype ~annotations:["%" ^ annot] t
    | MTannot (_, t, false) -> mtype ~annotations t
    | MTpair (a, b) -> primitive ~annotations "pair" [mtype a; mtype b]
    | MToperation -> primitive ~annotations "operation" []
    | MTmap (ct, ft) -> primitive ~annotations "map" [mtype ct; mtype ft]
    | MTor (left, right) ->
        primitive ~annotations "or" [mtype left; mtype right]
    | MTunit -> primitive ~annotations "unit" []
    | MTsignature -> primitive ~annotations "signature" []
    | MToption o -> primitive ~annotations "option" [mtype o]
    | MTlist l -> primitive ~annotations "list" [mtype l]
    | MTset e -> primitive ~annotations "set" [mtype e]
    | MTcontract x -> primitive ~annotations "contract" [mtype x]
    | MTlambda (x, f) -> primitive ~annotations "lambda" [mtype x; mtype f]
    | MTbig_map (k, v) -> primitive ~annotations "big_map" [mtype k; mtype v]
    | MTmissing msg ->
        primitive
          "ERROR"
          [Format.kasprintf string "Cannot compile missing type: %s" msg]

  let rec literal lit =
    let open Literal in
    let open Micheline in
    let z_big bi = int (Big_int.string_of_big_int bi) in
    match lit with
    | Nat bi -> z_big bi
    | Int bi -> z_big bi
    | Bool false -> primitive "False" []
    | Bool true -> primitive "True" []
    | String s -> string s
    | Unit -> primitive "Unit" []
    | Mutez bi -> z_big bi
    | Bytes b -> bytes b
    | Pair (left, right) -> primitive "Pair" [literal left; literal right]
    | None (_ : mtype) ->
        (* We are in the literal None case, not the NONE instruction *)
        primitive "None" []
    | Some l -> primitive "Some" [literal l]
    | Left e -> primitive "Left" [literal e]
    | Right e -> primitive "Right" [literal e]
    | Map {elements} ->
        elements
        |> Base.List.sort ~compare:(fun (k1, _) (k2, _) -> compare k1 k2)
        |> Base.List.map ~f:(fun (k, v) ->
               primitive "Elt" [literal k; literal v])
        |> sequence
    | Set {elements} ->
        elements
        |> Base.List.sort ~compare
        |> Base.List.map ~f:literal
        |> sequence
    | List {elements} -> elements |> Base.List.map ~f:literal |> sequence

  module Macro = struct
    open Micheline

    let if_some t e =
      (* src/proto_alpha/lib_client/michelson_v1_macros.ml
           let expand_if_some = function
             | Prim (loc, "IF_SOME", [right; left], annot) ->
                 ok @@ Some (Seq (loc, [Prim (loc, "IF_NONE", [left; right], annot)]))
        *)
      [primitive "IF_NONE" [sequence e; sequence t]]

    let dip_seq i = primitive "DIP" [sequence i]

    let rec diiip n i =
      (*
        > DII(\rest=I* )P code / S  =>  DIP (DI(\rest)P code) / S
      *)
      match n with
      | 1 -> dip_seq [i]
      | n when n >= 2 ->
          dip_seq [diiip (n - 1) i] (* ~annot:[Fmt.strf "%%di%dp" n] *)
      | other -> Format.kasprintf failwith "Macro.diiip: %d" other

    let rec c_ad_r s =
      (*
         See http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#syntactic-conveniences
         > CA(\rest=[AD]+)R / S  =>  CAR ; C(\rest)R / S
         > CD(\rest=[AD]+)R / S  =>  CDR ; C(\rest)R / S
      *)
      match s.[0] with
      | 'A' -> primitive "CAR" [] :: c_ad_r (Base.String.drop_prefix s 1)
      | 'D' -> primitive "CDR" [] :: c_ad_r (Base.String.drop_prefix s 1)
      | exception _ -> []
      | other ->
          Format.kasprintf
            failwith
            "c_ad_r macro: wrong char: '%c' (of %S)"
            other
            s

    let rec set_c_ad_r s =
      (*
         See http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#syntactic-conveniences
         > SET_CA(\rest=[AD]+)R / S   =>
             { DUP ; DIP { CAR ; SET_C(\rest)R } ; CDR ; SWAP ; PAIR } / S
         > SET_CD(\rest=[AD]+)R / S   =>
             { DUP ; DIP { CDR ; SET_C(\rest)R } ; CAR ; PAIR } / S
         Then,
         > SET_CAR  =>  CDR ; SWAP ; PAIR
         > SET_CDR  =>  CAR ; PAIR
      *)
      match s.[0] with
      | 'A' when String.length s > 1 ->
          [ primitive "DUP" []
          ; dip_seq
              (primitive "CAR" [] :: set_c_ad_r (Base.String.drop_prefix s 1))
          ; primitive "CDR" []
          ; primitive "SWAP" []
          ; primitive "PAIR" [] ]
      | 'A' -> [primitive "CDR" []; primitive "SWAP" []; primitive "PAIR" []]
      | 'D' when String.length s > 1 ->
          [ primitive "DUP" []
          ; dip_seq
              (primitive "CDR" [] :: set_c_ad_r (Base.String.drop_prefix s 1))
          ; primitive "CAR" []
          ; primitive "PAIR" [] ]
      | 'D' -> [primitive "CAR" []; primitive "PAIR" []]
      | exception _ ->
          Format.kasprintf failwith "set_c_r_macro: called with no chars: S" s
      | other ->
          Format.kasprintf
            failwith
            "set_c_r_macro: wrong char: '%c' (of %S)"
            other
            s
  end

  let rec instruction (the_instruction : tinstr) =
    let open Micheline in
    let prim0 ?annotations n = [primitive ?annotations n []] in
    let primn ?annotations n l = [primitive ?annotations n l] in
    let one_of_seq = function
      | [one] -> one
      | l -> sequence l
    in
    match the_instruction.tinstr with
    | MIerror s -> primn "ERROR" [string s]
    | MIcomment _comment ->
        []
        (*
          [ primitive "PUSH" [primitive "string" []; string comment]
          ; primitive "DROP" [] ] *)
    | MIdip instr -> primn "DIP" [sequence (instruction instr)]
    | MIdipn (n, instr) -> [Macro.diiip n (one_of_seq (instruction instr))]
    (* primn "DIPn" [int (Base.Int.to_string n); sequence (instruction instr)] *)
    | MIdig n -> primn "DIG" [int (Base.Int.to_string n)]
    | MIdug n -> primn "DUG" [int (Base.Int.to_string n)]
    (* primn "DUPn" [int (Base.Int.to_string n)] *)
    | MIloop instr -> primn "LOOP" (instruction instr)
    | MIiter ({tinstr = MIseq _} as instr) -> primn "ITER" (instruction instr)
    | MIiter i ->
        primn "ITER" (instruction {tinstr = MIseq [i]; stack = i.stack})
    | MImap instr -> primn "MAP" (instruction instr)
    | MIseq ils -> [sequence (Base.List.concat_map ils ~f:instruction)]
    | MIif (t, e) ->
        primn "IF" [sequence (instruction t); sequence (instruction e)]
    | MIif_left (t, e) ->
        primn "IF_LEFT" [sequence (instruction t); sequence (instruction e)]
    | MIif_some (t, e) -> Macro.if_some (instruction t) (instruction e)
    | MIpush (mt, lit) -> primn "PUSH" [mtype mt; literal lit]
    | MIright mty -> primn "RIGHT" [mtype mty]
    | MIleft mty -> primn "LEFT" [mtype mty]
    | MInone mty -> primn "NONE" [mtype mty]
    | MInil mty -> primn "NIL" [mtype mty]
    | MIcontract (None, mty) -> primn "CONTRACT" [mtype mty]
    | MIcontract (Some entry_point, mty) ->
        primn ~annotations:["%" ^ entry_point] "CONTRACT" [mtype mty]
    | MIlambda (t1, t2, b) ->
        primn "LAMBDA" [mtype t1; mtype t2; sequence (instruction b)]
    | MIunpack mty -> primn "UNPACK" [mtype mty]
    | MIfield op -> Macro.c_ad_r (string_of_ad_path op)
    | MIsetField op -> Macro.set_c_ad_r (string_of_ad_path op)
    | ( MIself | MIexec | MIaddress | MIimplicit_account | MIcons | MIsome
      | MIpair | MItransfer_tokens | MIcheck_signature | MIset_delegate | MIeq
      | MIfailwith | MIdrop | MIdup | MIneq | MIle | MIlt | MIge | MIgt
      | MIswap | MIunpair | MIcompare | MImul | MIadd | MIsub | MIediv | MInot
      | MIand | MIor | MIconcat | MIslice | MIsize | MIget | MIupdate
      | MIsource | MIsender | MIamount | MIbalance | MInow | MImem | MIpack
      | MIhash_key | MIblake2b | MIsha256 | MIsha512 | MIabs | MIneg | MIint
      | MIisnat | MImich _ ) as simple ->
      ( try prim0 (name_of_instr_exn simple) with
      | _ -> [sequence [primitive "ERROR-NOT-SIMPLE" []]] )
end

module Michelson_contract = struct
  type t =
    { storage : mtype
    ; parameter : mtype
    ; code : tinstr }
  [@@deriving show {with_path = false}]

  let make ?(storage = mt_unit) ?(parameter = mt_unit) code =
    {storage; parameter; code}

  let has_error {storage; parameter; code} =
    has_missing_type storage
    || has_missing_type parameter
    || has_error (forget_types code)

  let to_micheline {storage; parameter; code} =
    let open Micheline in
    sequence
      [ primitive "storage" [To_micheline.mtype storage]
      ; primitive "parameter" [To_micheline.mtype parameter]
      ; primitive "code" (To_micheline.instruction code) ]
end
