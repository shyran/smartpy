(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Michelson
module Option = Base.Option

let do_simplification = true

(** {1 Actions for code construction} *)

(** A monad that can reader the (immutable) parameter type and modify
   state: (1) the instruction emitted within the current block and (2)
   the current stack. Monadic actions are composed via the bind (>>=)
   operator. *)
module type ACTION = sig
  type 'a t

  type constant =
    { tparams : mtype
    ; tstorage : mtype
    ; flags : flag list }

  type state =
    { block : instr list
    ; stack : stack }

  (** State *)

  val get_state : state t

  val get_stack : stack t

  val set_state : state -> unit t

  val set_stack : stack -> unit t

  val modify_state : (state -> state) -> unit t

  val _run : constant -> state -> 'a t -> 'a * state

  val exec : constant -> state -> unit t -> state

  (** Reader *)

  val ask : constant t

  val ask_tparams : mtype t

  val _ask_tstorage : mtype t

  (** Applicative *)

  val apply : ('a -> 'b) t -> 'a t -> 'b t

  (** Functor *)

  val map : [> `Custom of 'a t -> f:('a -> 'b) -> 'b t ]

  (** Monad *)

  val return : 'a -> 'a t

  val bind : 'a t -> f:('a -> 'b t) -> 'b t
end

module Action : ACTION = struct
  type constant =
    { tparams : mtype
    ; tstorage : mtype
    ; flags : flag list }

  type state =
    { block : instr list
    ; stack : stack }

  type 'a t = {action : constant -> state -> 'a * state}

  let ask = {action = (fun constant state -> (constant, state))}

  let ask_tparams = {action = (fun {tparams} state -> (tparams, state))}

  let _ask_tstorage = {action = (fun {tstorage} state -> (tstorage, state))}

  let map =
    `Custom
      (fun {action} ~f ->
        { action =
            (fun constant state ->
              let x, state = action constant state in
              (f x, state)) })

  let return x = {action = (fun _ state -> (x, state))}

  let apply {action = f} {action = x} =
    { action =
        (fun constant state ->
          let f, state = f constant state in
          let x, state = x constant state in
          (f x, state)) }

  let bind {action} ~f =
    { action =
        (fun constant state ->
          let x, state = action constant state in
          (f x).action constant state) }

  let get_state = {action = (fun _ state -> (state, state))}

  let get_stack = {action = (fun _ state -> (state.stack, state))}

  let set_state state = {action = (fun _ _ -> ((), state))}

  let set_stack stack = {action = (fun _ state -> ((), {state with stack}))}

  let modify_state f = {action = (fun _ state -> ((), f state))}

  let _run constant state {action} = action constant state

  let exec constant state action = snd (_run constant state action)
end

module A = Base.Applicative.Make (Action)
module M = Base.Monad.Make (Action)
open M

let mi_seq xs = M.all xs >>= fun _ -> return ()

let string_take i xs = String.sub xs 0 (min i (String.length xs))

let instr instr =
  Action.ask_tparams
  >>= fun tparams ->
  Action.modify_state (fun state ->
      match typecheck tparams state.stack {instr} with
      | {stack = Ok stack} -> {block = state.block @ [{instr}]; stack}
      | {stack = Error msg} ->
          let err =
            Printf.sprintf
              "Compiler error:\n\
              \  Message      : %s\n\
              \  Input stack  : %s\n\
              \  Instruction  : %s\n"
              msg
              (string_of_stack ~full:() state.stack)
              (string_take 100 (show_instr_rectypes {instr}))
          in
          { block = state.block @ [{instr = MIerror err}; {instr}]
          ; stack = state.stack })

let instr_of_state Action.{block} =
  match block with
  | [i] -> i
  | _ -> {instr = MIseq block}

let debug_stack msg =
  Action.get_stack
  >>= fun stack ->
  instr (MIcomment (msg ^ " [ " ^ string_of_stack ~full:() stack ^ " ]"))

let with_ok_stack lbl k =
  Action.get_stack
  >>= function
  | Stack_ok stack -> k stack
  | Stack_failed ->
      instr (MIerror (Printf.sprintf "instruction after FAIL: %s" lbl))

(** {1 Michelson helpers} *)

let mi_car = instr (MIfield [A])

let mi_cdr = instr (MIfield [D])

let mi_dupn_cheap = function
  | 0 -> instr MIdup
  | n -> mi_seq [instr (MIdig n); instr MIdup; instr (MIdug (n + 1))]

let mi_fail = mi_seq [instr (MIpush (mt_unit, Literal.Unit)); instr MIfailwith]

let mi_failwith_str s =
  Action.ask
  >>= fun constant ->
  if List.mem Unit_failwith constant.flags
  then mi_seq [instr (MIpush (mt_unit, Literal.Unit)); instr MIfailwith]
  else mi_seq [instr (MIpush (mt_string, Literal.String s)); instr MIfailwith]

(** This function implements UNPAIR as [DUP; CDR; SWAP; CAR]. This is
   cheaper, as it avoids DIP. *)
let mi_unpair_cheap = mi_seq [instr MIdup; mi_cdr; instr MIswap; mi_car]

(** {1 Stack tags and targets} *)

let ( <|> ) x y = if Option.is_some x then x else y ()

let invariant_top action =
  Action.ask
  >>= fun constant ->
  Action.get_state
  >>= fun state ->
  let state' = Action.exec constant state action in
  match (state.stack, state'.stack) with
  | Stack_ok ({se_type = t1; se_tag} :: _), Stack_ok ({se_type = t2} :: rest)
    when remove_annots t1 = remove_annots t2 ->
      Action.set_state
        { block = state'.block
        ; stack = Stack_ok ({se_type = t1; se_tag} :: rest) }
  | _ ->
      mi_seq
        [ debug_stack "invariant_top begin"
        ; action
        ; debug_stack "invariant_top end" ]

let tag_top se_tag =
  with_ok_stack "tag_top" (function
      | {se_type} :: tail ->
          Action.set_stack (Stack_ok ({se_type; se_tag} :: tail))
      | [] -> instr (MIerror "tag_top"))

let tag_top2 tag1 tag2 =
  with_ok_stack "tag_top" (function
      | {se_type = type1} :: {se_type = type2} :: tail ->
          Action.set_stack
            (Stack_ok
               ( {se_type = type1; se_tag = tag1}
               :: {se_type = type2; se_tag = tag2}
               :: tail ))
      | _ -> instr (MIerror "tag_top"))

(** Finds the offset of the first occurence of the target. *)
let find_target target =
  let f offset = function
    | {se_tag = ST_none} -> None
    | {se_tag = ST_pair _} -> None (* We do *not* recurse into pairs. *)
    | {se_tag = ST_target t} -> if target = t then Some offset else None
  in
  Base.List.find_mapi ~f

(** Does the stack have the given target? *)
let has_target target stack = Option.is_some (find_target target stack)

(** Move the top of the stack to the position indicated by the tag. *)
let set_target target =
  with_ok_stack "set_target" (fun stack ->
      match stack with
      | [] -> instr (MIerror "set_target: empty stack")
      | _ :: stack ->
        ( match find_target target stack with
        | None -> instr (MIerror ("set_target " ^ show_target target))
        (* | Some offset -> mi_seq [instr MIdig (offset + 1); instr MIdrop; instr MIdug offset] *)
        | Some offset ->
            mi_seq
              [ tag_top (ST_target target)
              ; instr (MIdug (offset + 1))
              ; instr (MIdig offset)
              ; instr MIdrop ] ))

let dup_target target =
  with_ok_stack "dup_target" (fun stack ->
      match find_target target stack with
      | None -> instr (MIerror ("dup_target " ^ show_target target))
      | Some offset -> mi_dupn_cheap offset)

let with_target target op =
  with_ok_stack "with_target" (fun stack ->
      match find_target target stack with
      | None -> instr (MIerror ("with_target " ^ show_target target))
      | Some offset ->
          mi_seq
            [ instr (MIdig offset)
            ; op
            ; tag_top (ST_target target)
            ; instr (MIdug offset) ])

let drop_target target =
  with_ok_stack "drop_target" (fun stack ->
      match find_target target stack with
      | None -> instr (MIerror ("drop_target " ^ show_target target))
      | Some offset -> mi_seq [instr (MIdig offset); instr MIdrop])

(** {1 Stack unification with operations} *)

let type_top se_type =
  with_ok_stack "type_top" (function
      | {se_tag} :: tail ->
          Action.set_stack (Stack_ok ({se_type; se_tag} :: tail))
      | [] -> instr (MIerror "type_top"))

let ops_init =
  instr (MInil mt_operation) >>= fun _ -> tag_top (ST_target T_operations)

let stack_cons x = function
  | Stack_ok stack -> Stack_ok (x :: stack)
  | Stack_failed -> Stack_failed

let prepare stack body =
  Action.ask
  >>= fun constant -> return (Action.exec constant {block = []; stack} body)

let cond_with_ops cond sin_l l sin_r r =
  prepare sin_l l
  >>= fun l ->
  prepare sin_r r
  >>= fun r ->
  let se_ops =
    {se_type = mt_list mt_operation; se_tag = ST_target T_operations}
  in
  let has_ops = has_target T_operations in
  let tl, tr =
    match (l.stack, r.stack) with
    | Stack_failed, _ -> (l, r)
    | _, Stack_failed -> (l, r)
    | Stack_ok sl, Stack_ok sr when has_ops sl && has_ops sr -> (l, r)
    | Stack_ok sl, _ when has_ops sl ->
        ( l
        , { block = r.block @ [{instr = MInil mt_operation}]
          ; stack = stack_cons se_ops r.stack } )
    | _, Stack_ok sr when has_ops sr ->
        ( { block = l.block @ [{instr = MInil mt_operation}]
          ; stack = stack_cons se_ops l.stack }
        , r )
    | Stack_ok _, Stack_ok _ -> (l, r)
  in
  instr (cond (instr_of_state tl) (instr_of_state tr))
  >>= fun _ ->
  match unify_stacks tl.stack tr.stack with
  | Some s -> Action.set_stack s
  | None ->
      instr
        (MIerror
           (Printf.sprintf
              "Cannot unify branches [%s] [%s]"
              (string_of_stack ~full:() tl.stack)
              (string_of_stack ~full:() tr.stack)))

let mi_if l r =
  with_ok_stack "mi_if" (fun stack ->
      match (ty_if_in stack, ty_if_in stack) with
      | Ok sin_l, Ok sin_r ->
          cond_with_ops (fun l r -> MIif (l, r)) sin_l l sin_r r
      | _ -> instr (MIerror "mi_if"))

let mi_if_some l r =
  with_ok_stack "mi_if_some" (fun stack ->
      match (ty_if_some_in1 stack, ty_if_some_in2 stack) with
      | Ok sin_l, Ok sin_r ->
          cond_with_ops (fun l r -> MIif_some (l, r)) sin_l l sin_r r
      | _ -> instr (MIerror "mi_if_some"))

let mi_if_left l r =
  with_ok_stack "mi_if_left" (fun stack ->
      match (ty_if_left_in1 stack, ty_if_left_in2 stack) with
      | Ok sin_l, Ok sin_r ->
          cond_with_ops (fun l r -> MIif_left (l, r)) sin_l l sin_r r
      | _ -> instr (MIerror "mi_if_left"))

let mi_map body =
  with_ok_stack "mi_map" (fun stack ->
      match ty_map_in stack with
      | Ok stack_in ->
          prepare stack_in body >>= fun x -> instr (MImap (instr_of_state x))
      | Error msg -> instr (MIerror ("mi_map: " ^ msg)))

let mi_iter body =
  with_ok_stack "mi_iter" (fun stack ->
      match ty_iter_in stack with
      | Ok stack_in ->
          prepare stack_in body >>= fun x -> instr (MIiter (instr_of_state x))
      | Error msg -> instr (MIerror ("mi_iter: " ^ msg)))

let mi_loop body =
  with_ok_stack "mi_loop" (fun stack ->
      match ty_loop_in stack with
      | Ok stack_in ->
          prepare stack_in body >>= fun x -> instr (MIloop (instr_of_state x))
      | Error msg -> instr (MIerror ("mi_loop: " ^ msg)))

let mi_lambda tparameter ttarget body =
  with_ok_stack "mi_lambda" (fun _stack ->
      let stack =
        Stack_ok [{se_type = tparameter; se_tag = ST_target T_lambda_parameter}]
      in
      Action.ask
      >>= fun {flags} ->
      let x =
        Action.exec
          {tparams = mt_unit; tstorage = mt_unit; flags}
          {block = []; stack}
          (mi_seq [body; drop_target T_lambda_parameter])
      in
      instr (MIlambda (tparameter, ttarget, instr_of_state x)))

(** {1 Types} *)

let isBigMap t =
  match Type.getRepr t with
  | TMap {big} ->
    ( match !(Type.getRef big) with
    | UnValue true -> true
    | _ -> false )
  | _ -> false

let rec split acc1 acc2 = function
  | [] -> (List.rev acc1, List.rev acc2)
  | a :: b :: l -> split (a :: acc1) (b :: acc2) l
  | a :: l -> split (a :: acc1) acc2 l

let mtype_of_record mtype_of_type l =
  let rec mtype_of_record ~real l =
    match split [] [] l with
    | [], [] -> mt_unit
    | [(name, t)], [] -> mt_annot name (mtype_of_type t) real
    | acc1, acc2 ->
        mt_pair
          (mtype_of_record ~real:true acc1)
          (mtype_of_record ~real:true acc2)
  in
  mtype_of_record ~real:false l

let rec mtype_of_type t =
  match Type.getRepr t with
  | TUnit -> mt_unit
  | TBool -> mt_bool
  | TInt {isNat} ->
    ( match Typing.intType isNat with
    | `Unknown -> mt_int
    | `Nat -> mt_nat
    | `Int -> mt_int )
  | TTimestamp -> mt_timestamp
  | TBytes -> mt_bytes
  | TString -> mt_string
  | TRecord fields ->
      if true
      then
        match fields with
        | [] -> mt_unit
        | [(name1, t1)] -> mt_annot name1 (mtype_of_type t1) false
        | (name1, t1) :: l ->
            List.fold_left
              (fun t (name2, t2) ->
                mt_pair t (mt_annot name2 (mtype_of_type t2) true))
              (mt_annot name1 (mtype_of_type t1) true)
              l
      else mtype_of_record mtype_of_type fields
  | TVariant [("None", u); ("Some", t)] when u = Type.unit ->
      mt_option (mtype_of_type t)
  | TVariant l ->
    ( match l with
    | [(name1, t1)] -> mt_annot name1 (mtype_of_type t1) false
    | (name1, t1) :: l ->
        List.fold_left
          (fun t (name2, t2) ->
            mt_or t (mt_annot name2 (mtype_of_type t2) true))
          (mt_annot name1 (mtype_of_type t1) true)
          l
    | [] -> mt_unit )
  | TSet element -> mt_set (mtype_of_type element)
  | TMap {key; item} ->
      let key = mtype_of_type key in
      if isBigMap t
      then mt_big_map key (mtype_of_type item)
      else mt_map key (mtype_of_type item)
  | TAddress -> mt_address
  | TContract t -> mt_contract (mtype_of_type t)
  | TKeyHash -> mt_key_hash
  | TKey -> mt_key
  | TSecretKey -> mt_missing "Secret keys are forbidden in contracts"
  | TToken -> mt_mutez
  | TSignature -> mt_signature
  | TUnknown _ -> mt_missing "Unknown Type"
  | TPair (t1, t2) -> mt_pair (mtype_of_type t1) (mtype_of_type t2)
  | TAnnots ([a], t) -> mt_annot a (mtype_of_type t) true
  | TAnnots _ -> failwith "several annots not handled"
  | TList t -> mt_list (mtype_of_type t)
  | TLambda (t1, t2) -> mt_lambda (mtype_of_type t1) (mtype_of_type t2)

(** {1 Values} *)

let compile_literal (l : Basics.Literal.t) e =
  let module L = Literal in
  match l.l with
  | Unit -> L.Unit
  | Int i -> L.Int i
  | Timestamp i -> L.String (Big_int.string_of_big_int i)
  | Mutez i -> L.Mutez i
  | String s -> L.String s
  | Bytes s -> L.Bytes s
  | Key_hash s -> L.String s
  | Signature s -> L.String s
  | Key s | Address s -> L.String s
  | Bool b -> L.Bool b
  | Secret_key _ ->
      raise
        (SmartExcept
           [`Text "Secret keys are forbidden in contracts"; `Expr e; `Line e.el])

let op_of_attr name =
  let rec get acc {mt} =
    match mt with
    | MTannot (annot, _, _) when annot = name -> Some (List.rev acc)
    | MTpair (t1, t2) ->
      ( match get (A :: acc) t1 with
      | Some _ as result -> result
      | None -> get (D :: acc) t2 )
    | _ -> None
  in
  get []

let of_Some msg =
  let comment = instr (MIcomment (Printf.sprintf "of_some: %s" msg)) in
  mi_if_some (mi_seq [comment]) (mi_failwith_str msg)

(** {1 Expressions} *)

let mich_of_binOpInfix t = function
  | BNeq -> MIneq
  | BEq -> MIeq
  | BAnd -> MIand
  | BOr -> MIor
  | BAdd when t = mt_string -> MIconcat
  | BAdd when t = mt_bytes -> MIconcat
  | BAdd -> MIadd
  | BSub -> MIsub
  | BEDiv -> MIediv
  | BDiv -> assert false
  | BMul -> MImul
  | BMod -> assert false
  | BLt -> MIlt
  | BLe -> MIle
  | BGt -> MIgt
  | BGe -> MIge

let mi_rev_list t =
  mi_seq [instr (MInil (mtype_of_type t)); instr MIswap; mi_iter (instr MIcons)]

(** Execute a branch according to the sign of the [int] on the top of
   the stack. Optimized assuming positivity is the common case. *)
let case_sign x pos neg zero =
  mi_seq
    [ x
    ; instr MIgt
    ; mi_if
        (* 0 < x *) pos
        (mi_seq [x; instr MIlt; mi_if (* 0 > x *) neg zero]) ]

(** Ensure an operations element is on the stack if the given command
   may output any. *)
let ensure_ops_if_out has_operations =
  if has_operations
  then
    with_ok_stack "ensure_ops_if_out" (fun stack ->
        if has_target T_operations stack then mi_seq [] else ops_init)
  else mi_seq []

let compile_command_f = ref (fun _ -> (assert false : unit Action.t))

let rec compile_expr e =
  match e.e with
  | EMichelson (michelson, exprs) ->
      let apply mich =
        instr
          (MImich
             { mich with
               typesIn = List.map mtype_of_type mich.typesIn
             ; typesOut = List.map mtype_of_type mich.typesOut })
      in
      mi_seq
        [ mi_seq (List.rev_map compile_expr exprs)
        ; mi_seq (List.map (fun mich -> apply mich) michelson) ]
  | ENot x -> mi_seq [compile_expr x; instr MInot]
  | EAbs x -> mi_seq [compile_expr x; instr MIabs]
  | EToInt x -> mi_seq [compile_expr x; instr MIint]
  | EIsNat x -> mi_seq [compile_expr x; instr MIisnat]
  | ENeg x -> mi_seq [compile_expr x; instr MIneg]
  | ESign x ->
      let code = compile_expr x in
      let zero = instr (MIpush (mt_int, Literal.int 0)) in
      mi_seq [code; zero; instr MIcompare]
  | EParams _ -> mi_seq [dup_target T_params]
  | EStorage -> mi_seq [dup_target T_storage]
  | ELocal (name, _) -> dup_target (T_local name)
  | EIter (name, _) -> dup_target (T_iter name)
  | EOpenVariant (constructor, scrutinee) ->
      compile_match
        scrutinee
        constructor
        "dummy"
        (mi_seq [instr MIdup; tag_top ST_none])
        mi_fail
  | EVariant_arg arg_name -> dup_target (T_variant_arg arg_name)
  | EItem (map, key, _) ->
      mi_seq
        [ compile_expr map
        ; compile_expr key
        ; instr MIget
        ; Format.kasprintf of_Some "Get-item:%d" key.el ]
  | EAttr (name, x) ->
    ( match op_of_attr name (mtype_of_type x.et) with
    | None -> instr (MIerror ("EAttr '" ^ name ^ "', " ^ Type.show x.et))
    | Some [] -> mi_seq [compile_expr x]
    | Some op -> mi_seq [compile_expr x; instr (MIfield op)] )
  | ECst l -> instr (MIpush (mtype_of_type l.t, compile_literal l e))
  | EBinOpInf (op, x, y) when List.mem op [BNeq; BEq; BLt; BLe; BGt; BGe] ->
      mi_seq
        [ compile_expr y
        ; compile_expr x
        ; instr MIcompare
        ; instr (mich_of_binOpInfix (mtype_of_type x.et) op) ]
  | EBinOpInf (BDiv, x, y) ->
      mi_seq
        [ compile_expr y
        ; compile_expr x
        ; instr MIediv
        ; mi_if_some mi_car (mi_failwith_str "division by zero") ]
  | EBinOpInf (BMod, x, y) ->
      mi_seq
        [ compile_expr y
        ; compile_expr x
        ; instr MIediv
        ; mi_if_some mi_cdr (mi_failwith_str "division by zero") ]
  | EBinOpInf (BOr, x, y) ->
      mi_seq
        [ compile_expr x
        ; mi_if (instr (MIpush (mt_bool, Literal.Bool true))) (compile_expr y)
        ]
  | EBinOpInf (BAnd, x, y) ->
      mi_seq
        [ compile_expr x
        ; mi_if
            (compile_expr y)
            (instr (MIpush (mtype_of_type Type.bool, Literal.Bool false))) ]
  | EBinOpInf (op, x, y) ->
      mi_seq
        [ compile_expr y
        ; compile_expr x
        ; instr (mich_of_binOpInfix (mtype_of_type y.et) op) ]
  | EVariant (name, x) ->
    ( match Type.getRepr e.et with
    | TVariant [("None", _); ("Some", t)] ->
        let t = mtype_of_type t in
        ( match name with
        | "None" -> instr (MIpush (mt_option t, Literal.None t))
        | "Some" -> mi_seq [compile_expr x; instr MIsome]
        | _ -> assert false )
    | TVariant [("Left", tl); ("Right", tr)] ->
      ( match name with
      | "Left" -> mi_seq [compile_expr x; instr (MIleft (mtype_of_type tr))]
      | "Right" -> mi_seq [compile_expr x; instr (MIright (mtype_of_type tl))]
      | _ -> assert false )
    | TVariant _ ->
        let rec compile_variant acc {mt} =
          match mt with
          | MTannot (annot, _, _) when annot = name -> Some acc
          | MTor (t1, t2) ->
              compile_variant (instr (MIleft t2) :: acc) t1
              <|> fun () -> compile_variant (instr (MIright t1) :: acc) t2
          | _ -> None
        in
        let mt = mtype_of_type e.et in
        ( match compile_variant [] mt with
        | Some rl -> mi_seq (compile_expr x :: rl)
        | None ->
            instr
              (MIerror
                 (Printf.sprintf
                    "EVariant: annotation '%s' not found in '%s'"
                    name
                    (string_of_mtype mt))) )
    | t ->
        instr
          (MIerror
             (Printf.sprintf
                "EVariant: not a variant: '%s' '%s'"
                name
                (Type.show_u t))) )
  | EIsVariant (constructor, scrutinee) ->
      compile_match
        scrutinee
        constructor
        "dummy"
        (mi_seq [instr (MIpush (mt_bool, Literal.Bool true))])
        (mi_seq [instr (MIpush (mt_bool, Literal.Bool false))])
  | ESum {e = EListValues (l, _)} ->
      let zero = instr (MIpush (mt_int, Literal.int 0)) in
      let code = compile_expr l in
      let body = mi_seq [mi_cdr; instr MIadd] in
      mi_seq [zero; code; mi_iter body]
  | ESum l ->
      let zero = instr (MIpush (mt_int, Literal.int 0)) in
      let code = compile_expr l in
      let body = mi_seq [instr MIadd] in
      mi_seq [zero; code; mi_iter body]
  | ERecord entries ->
      let rec compile_fields {mt} =
        match mt with
        | MTannot (annot, _, _) ->
          ( match List.assoc_opt annot entries with
          | Some x -> compile_expr x
          | None -> instr (MIerror "record") )
        | MTpair (t1, t2) ->
            mi_seq [compile_fields t2; compile_fields t1; instr MIpair]
        | _ -> instr (MIerror "record")
      in
      compile_fields (mtype_of_type e.et)
  | EPair (e1, e2) -> mi_seq [compile_expr e2; compile_expr e1; instr MIpair]
  | EFirst e -> mi_seq [compile_expr e; mi_car]
  | ESecond e -> mi_seq [compile_expr e; mi_cdr]
  | ESender -> instr MIsender
  | ESource -> instr MIsource
  | ENow -> instr MInow
  | ECons (x, l) ->
      let x = compile_expr x in
      let l = compile_expr l in
      mi_seq [l; x; instr MIcons]
  | EBinOpPre (op, x, y) ->
      mi_seq
        [ compile_expr x (* x *)
        ; instr MIdup (* x x *)
        ; compile_expr y (* y x x *)
        ; instr MIdup (* y y x x *)
        ; instr (MIdug 2) (* y x y x *)
        ; instr MIcompare
        ; instr MIle (* (y<=x) y x *)
        ; (let x = instr MIdrop in
           let y = mi_seq [instr MIswap; instr MIdrop] in
           match op with
           | BMin ->
               mi_if
                 (mi_seq [y; tag_top ST_none])
                 (mi_seq [x; tag_top ST_none])
           | BMax ->
               mi_if
                 (mi_seq [x; tag_top ST_none])
                 (mi_seq [y; tag_top ST_none])) ]
  | EAdd_seconds (t, s) -> mi_seq [compile_expr t; compile_expr s; instr MIadd]
  | EAmount -> instr MIamount
  | EBalance -> instr MIbalance
  | EPack e -> mi_seq [compile_expr e; instr MIpack]
  | EUnpack (e, t) ->
      mi_seq [compile_expr e; instr (MIunpack (mtype_of_type t))]
  | ECheck_signature (pub_key, signature, bytes) ->
      mi_seq
        [ compile_expr bytes
        ; compile_expr signature
        ; compile_expr pub_key
        ; instr MIcheck_signature ]
  | ESlice {offset; length; buffer} ->
      mi_seq
        [ compile_expr buffer
        ; compile_expr length
        ; compile_expr offset
        ; instr MIslice ]
  | EConcat_list {e = EList [a]} -> compile_expr a
  | EConcat_list {e = EList [a; b]} ->
      mi_seq [compile_expr b; compile_expr a; instr MIconcat]
  | EConcat_list l -> mi_seq [compile_expr l; instr MIconcat]
  | ESize s -> mi_seq [compile_expr s; instr MIsize]
  | EContains (items, member) ->
    ( match Type.getRepr items.et with
    | TMap _ -> mi_seq [compile_expr items; compile_expr member; instr MImem]
    | TSet _ -> mi_seq [compile_expr items; compile_expr member; instr MImem]
    | _ -> instr (MIerror "EContains") )
  | EHash_key e -> mi_seq [compile_expr e; instr MIhash_key]
  | EHash (algo, e) ->
      let algo =
        match algo with
        | BLAKE2B -> MIblake2b
        | SHA256 -> MIsha256
        | SHA512 -> MIsha512
      in
      mi_seq [compile_expr e; instr algo]
  | ESplit_tokens (e, quantity, {e = ECst {l = Int v}})
    when Big_int.eq_big_int v (Big_int.big_int_of_int 1) ->
      mi_seq [compile_expr e; compile_expr quantity; instr MImul]
  | ESplit_tokens (e, quantity, total) ->
      mi_seq
        [ compile_expr total
        ; compile_expr e
        ; compile_expr quantity
        ; instr MImul
        ; instr MIediv
        ; of_Some "split_tokens"
        ; mi_car ]
  | EList elems ->
    ( match mtype_of_type e.et with
    | {mt = MTlist t} ->
        mi_seq
          [ instr (MIpush (mt_list t, Literal.List {t; elements = []}))
          ; mi_seq
              (List.rev_map
                 (fun value -> mi_seq [compile_expr value; instr MIcons])
                 elems) ]
    | _ -> failwith "elems" )
  | EMap entries ->
      mi_seq
        [ instr (MIpush (mtype_of_type e.et, Literal.Map {elements = []}))
        ; mi_seq
            (List.map
               (fun (key, value) ->
                 mi_seq
                   [ compile_expr value
                   ; instr MIsome
                   ; compile_expr key
                   ; instr MIupdate ])
               entries) ]
  | ESet entries ->
      mi_seq
        [ instr (MIpush (mtype_of_type e.et, Literal.Set {elements = []}))
        ; mi_seq
            (List.map
               (fun key ->
                 mi_seq
                   [ instr (MIpush (mt_bool, Literal.Bool true))
                   ; compile_expr key
                   ; instr MIupdate ])
               entries) ]
  | EContract {entry_point; arg_type; address} ->
      mi_seq
        [ compile_expr address
        ; instr (MIcontract (entry_point, mtype_of_type arg_type)) ]
  | ESelf -> instr MIself
  | ESelf_entry_point (entry_point, t) ->
      mi_seq
        [ instr MIself
        ; instr MIaddress
        ; instr (MIcontract (Some entry_point, mtype_of_type t))
        ; mi_if_some (mi_seq []) mi_fail ]
  | EContract_address e -> mi_seq [compile_expr e; instr MIaddress]
  | EImplicit_account e -> mi_seq [compile_expr e; instr MIimplicit_account]
  | EListRev e ->
    ( match Type.getRepr e.et with
    | TList t -> mi_seq [compile_expr e; mi_rev_list t]
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "map.items error: %s"
                (Printer.type_to_string e.et))) )
  | EListItems (map, rev) ->
    ( match Type.getRepr map.et with
    | TMap {key; item} ->
        compile_container_access_list
          map
          (mi_seq [])
          (Typing.key_value key item)
          rev
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "map.items error: %s"
                (Printer.type_to_string map.et))) )
  | EListValues (map, rev) ->
    ( match Type.getRepr map.et with
    | TMap {item} -> compile_container_access_list map mi_cdr item rev
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "map.values error: %s"
                (Printer.type_to_string map.et))) )
  | EListKeys (map, rev) ->
    ( match Type.getRepr map.et with
    | TMap {key} -> compile_container_access_list map mi_car key rev
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "map.keys error: %s"
                (Printer.type_to_string map.et))) )
  | EListElements (set, rev) ->
    ( match Type.getRepr set.et with
    | TSet element -> compile_container_access_list set (mi_seq []) element rev
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "set.elements error: %s"
                (Printer.type_to_string set.et))) )
  | EReduce _ | EAccount_of_seed _ | EMake_signature _ ->
      instr
        (MIerror
           (Printf.sprintf
              "Expression %s cannot be converted to Michelson (missing \
               pre-evaluation)"
              (String.escaped (Printer.expr_to_string e))))
  | ERange (a, b, step) ->
      mi_seq
        [ instr (MIcomment (Printer.expr_to_string e))
        ; instr (MInil (mtype_of_type a.et))
        ; compile_range
            ~a
            ~b
            ~step
            ~name:"RANGE"
            ~body:
              (mi_seq
                 [ instr MIdup
                 ; instr (MIdig 2)
                 ; instr MIswap
                 ; instr MIcons
                 ; instr MIswap ])
            ~has_operations:false
        ; mi_rev_list a.et ]
  | ECallLambda (lambda, parameter) ->
      mi_seq [compile_expr lambda; compile_expr parameter; instr MIexec]
  | ELambda {id = _; name = _; tParams; command; tResult} ->
      mi_lambda
        (mtype_of_type tParams)
        (mtype_of_type tResult)
        (!compile_command_f command)
  | ELambdaParams _ -> mi_seq [dup_target T_lambda_parameter]
  | EInt_x_or _ | EUpdate_map _ | EContract_data _ | EScenario_var _ ->
      instr
        (MIerror
           (Printf.sprintf
              "No conversion for expression %s of type %s"
              (String.escaped (Printer.expr_to_string e))
              (String.escaped (Printer.type_to_string e.et))))

and compile_container_access_list container projection t rev =
  mi_seq
    [ instr (MInil (mtype_of_type t))
    ; compile_expr container
    ; mi_iter (mi_seq [projection; instr MIcons])
    ; (if rev then mi_seq [] else mi_rev_list t) ]

and compile_match scrutinee constructor arg_name body body_else =
  let tag = T_variant_arg arg_name in
  let body_tagged = mi_seq [tag_top (ST_target tag); body; drop_target tag] in
  let drop_else = mi_seq [instr MIdrop; body_else] in
  let rec compile_match {mt} =
    match mt with
    | MTannot (annot, _, _) when annot = constructor -> (body_tagged, true)
    | MTor (t1, t2) ->
        let c1, done1 = compile_match t1 in
        let c2, done2 = compile_match t2 in
        (mi_if_left c1 c2, done1 || done2)
    | _ -> (drop_else, false)
  in
  mi_seq
    [ compile_expr scrutinee
    ; ( match constructor with
      | "None" -> mi_if_some drop_else body
      | "Some" -> mi_if_some body_tagged body_else
      | "Left" -> mi_if_left body_tagged drop_else
      | "Right" -> mi_if_left drop_else body_tagged
      | _ ->
          let c, d = compile_match (mtype_of_type scrutinee.et) in
          if d then c else instr (MIerror "wrong type") ) ]

(** Provide the continuation [k] with an instruction that makes the
   value of [e] available. The instruction is either [dup_tag] or
   [PUSH], depending on whether [e] is a constant or not. Currently
   only implemented for int and nat. *)
and with_target_or_cst e tag k =
  let put_tag = tag_top (ST_target tag) in
  match (e.e, mtype_of_type e.et) with
  | ECst {l = Int i}, {mt = MTint} ->
      k (mi_seq [instr (MIpush (mt_int, Literal.Int i)); put_tag])
  | ECst {l = Int i}, {mt = MTnat} ->
      k (mi_seq [instr (MIpush (mt_nat, Literal.Nat i)); put_tag])
  | _ -> mi_seq [compile_expr e; put_tag; k (dup_target tag); instr MIdrop]

and compile_range ~a ~b ~step ~name ~body ~has_operations =
  let i = T_iter name in
  mi_seq
    [ with_target_or_cst
        b
        (T_iter (name ^ "#b"))
        (fun get_b ->
          with_target_or_cst
            step
            (T_iter (name ^ "#step"))
            (fun get_step ->
              let cond cmp =
                mi_seq [dup_target i; get_b; instr MIcompare; cmp]
              in
              let loop cmp =
                mi_seq
                  [ cond cmp
                  ; mi_loop
                      (mi_seq
                         [ body
                         ; instr (MIcomment "loop step")
                         ; get_step
                         ; instr MIadd
                         ; tag_top (ST_target i)
                         ; cond cmp ]) ]
              in
              mi_seq
                [ ensure_ops_if_out has_operations
                ; compile_expr a
                ; tag_top (ST_target i) (* i := a *)
                ; (let err = mi_failwith_str "step size is 0" in
                   match mtype_of_type step.et with
                   | {mt = MTint} ->
                       case_sign
                         get_step
                         (loop (instr MIgt))
                         (loop (instr MIlt))
                         err
                   | {mt = MTnat} ->
                       mi_seq
                         [ get_step
                         ; instr (MIpush (mt_nat, Literal.nat 0))
                         ; instr MIcompare
                         ; instr MIeq
                         ; mi_if err (loop (instr MIgt)) ]
                   | _ -> assert false)
                ; drop_target i ])) ]

(** {1 Commands} *)

let mi_strip_var_annot target =
  Action.get_stack
  >>= function
  | Stack_ok
      ({se_type = {mt = MTannot (name, se_type, false)}; se_tag} :: tail)
    when name = target ->
      Action.set_stack (Stack_ok ({se_type; se_tag} :: tail))
  | _ -> instr (MIerror (Printf.sprintf "mi_strip_one_var_annot '%s'" target))

type lstep =
  | LItem of texpr
  | LAttr of string

type lroot =
  | LStorage
  | LLocal   of string
  | LIter    of string

type lexpr =
  { lroot : lroot
  ; lsteps : lstep list }

let extend_lexpr {lroot; lsteps} s = {lroot; lsteps = lsteps @ [s]}

let lexpr_of_expr =
  let rec of_expr acc e =
    match e.e with
    | EStorage -> Some {lroot = LStorage; lsteps = acc}
    | ELocal (name, _) -> Some {lroot = LLocal name; lsteps = acc}
    | EIter (name, _) -> Some {lroot = LIter name; lsteps = acc}
    | EItem (items, key, None) -> of_expr (LItem key :: acc) items
    | EAttr (name, expr) -> of_expr (LAttr name :: acc) expr
    | _ -> None
  in
  of_expr []

(* There are two ways to modify an iterator variable: (1) assign to it
   directly; (2) iterate over it or one of its substructures (nested
   loop). *)
let rec modifies_iter i c =
  match c.c with
  | CDelItem (lhs, _) | CSetVar (lhs, _) ->
    ( match lexpr_of_expr lhs with
    | None -> assert false
    | Some {lroot = LIter name} when name = i -> true
    | Some _ -> false )
  | CFor ((name, _), container, body) ->
      modifies_iter i body
      ||
      ( match lexpr_of_expr container with
      | Some {lroot = LIter cname} when i = cname -> modifies_iter name body
      | _ -> false )
  | CIf (_, c1, c2) -> modifies_iter i c1 || modifies_iter i c2
  | CSeq cs -> List.exists (modifies_iter i) cs
  | CWhile (_, c) -> modifies_iter i c
  | _ -> false

(** Brings the target into focus, leaves a zipper on the stack. *)
let rec mi_unzip = function
  | A :: rest ->
      mi_seq [instr MIdup; mi_cdr; instr MIswap; mi_car; mi_unzip rest]
  | D :: rest ->
      mi_seq [instr MIdup; mi_car; instr MIswap; mi_cdr; mi_unzip rest]
  | [] -> return ()

let rec mi_zip = function
  | A :: rest -> mi_seq [mi_zip rest; instr MIpair]
  | D :: rest -> mi_seq [mi_zip rest; instr MIswap; instr MIpair]
  | [] -> return ()

(** Sets the path inside the top stack element to the given expr. *)
let rec set_in_top isUpdate path rhs =
  match path with
  | LItem key :: rest ->
    ( match (rest, rhs) with
    | [], None ->
        with_ok_stack "set_in_top" (function
            | {se_type} :: _ ->
              ( match strip_annots se_type with
              | {mt = MTmap (_, tval) | MTbig_map (_, tval)} ->
                  let _tval' = strip_annots tval in
                  mi_seq
                    [ instr (MIpush (mt_option _tval', Literal.None _tval'))
                    ; compile_expr key
                    ; instr MIupdate ]
              | _ -> assert false )
            | _ -> assert false)
    | [], Some rhs ->
        if isUpdate
        then
          mi_seq
            [ instr MIdup
            ; compile_expr key
            ; instr MIdup
            ; instr (MIdug 2)
            ; instr MIget
            ; of_Some "set_in_top-nil-some"
            ; rhs
            ; instr MIswap
            ; instr MIsome
            ; instr MIupdate ]
        else mi_seq [rhs; instr MIsome; compile_expr key; instr MIupdate]
    | _ :: _, _ ->
        mi_seq
          [ instr MIdup (* x x *)
          ; compile_expr key (* k x x *)
          ; instr MIdup (* k k x x *)
          ; instr (MIdug 2) (* k x k x *)
          ; instr MIget
          ; of_Some "set_in_top-any"
          ; set_in_top isUpdate rest rhs
          ; instr MIsome
          ; instr MIswap
          ; instr MIupdate ] )
  | LAttr name :: rest ->
      with_ok_stack "set_in_top" (fun stack ->
          match stack with
          | [] -> instr (MIerror "set_in_top: LAttr")
          | {se_type} :: _ ->
            ( match op_of_attr name se_type with
            | None ->
                instr
                  (MIerror
                     (Printf.sprintf
                        "set_in_top: cannot find '%s' in '%s' "
                        name
                        (string_of_mtype ~protect:() se_type)))
            | Some [] ->
                mi_seq [mi_strip_var_annot name; set_in_top isUpdate rest rhs]
            | Some op ->
                mi_seq [mi_unzip op; set_in_top isUpdate rest rhs; mi_zip op]
            ))
  | [] ->
    ( match rhs with
    | None -> assert false
    | Some rhs -> mi_seq [(if isUpdate then mi_seq [] else instr MIdrop); rhs]
    )

let target_of_lexpr = function
  | {lroot = LStorage; lsteps} -> (T_storage, lsteps)
  | {lroot = LLocal name; lsteps} -> (T_local name, lsteps)
  | {lroot = LIter name; lsteps} -> (T_iter name, lsteps)

let compile_assignment lexpr rhs =
  let t, lsteps = target_of_lexpr lexpr in
  match (lsteps, rhs) with
  | [], Some rhs -> with_target t (mi_seq [rhs; instr MIswap; instr MIdrop])
  | _ ->
      mi_seq
        [ dup_target t
        ; invariant_top (set_in_top false lsteps rhs)
        ; set_target t ]

let compile_update_set lexpr element add =
  let rhs =
    mi_seq
      [ instr (MIpush (mt_bool, Literal.Bool add))
      ; compile_expr element
      ; instr MIupdate ]
  in
  let t, lsteps = target_of_lexpr lexpr in
  mi_seq
    [ dup_target t
    ; invariant_top (set_in_top true lsteps (Some rhs))
    ; set_target t ]

(** Given an instruction that pushes a new operation on the stack,
   records it for later. *)
let make_operation i =
  with_ok_stack "make_operation" (fun stack ->
      if has_target T_operations stack
      then
        mi_seq
          [dup_target T_operations; i; instr MIcons; set_target T_operations]
      else mi_seq [ops_init; i; instr MIcons; tag_top (ST_target T_operations)])

let rec compile_command x =
  match x.c with
  | CSeq l ->
      List.fold_left
        (fun code command -> mi_seq [code; compile_command command])
        (mi_seq [])
        l
  | CIf (c, t, e) ->
      mi_seq
        [ instr (MIcomment (Printf.sprintf "if %s:" (Printer.expr_to_string c)))
        ; compile_expr c
        ; mi_if (compile_command t) (compile_command e) ]
  | CMatch (scrutinee, (constructor, arg_name, body)) ->
      mi_seq
        [ instr
            (MIcomment
               (Printf.sprintf
                  "with %s.match('%s') as %s:"
                  (Printer.expr_to_string scrutinee)
                  constructor
                  arg_name))
        ; compile_match
            scrutinee
            constructor
            arg_name
            (compile_command body)
            (mi_seq []) ]
  | CFailwith message -> mi_seq [compile_expr message; instr MIfailwith]
  | CVerify (_, true, _) (* ghost *) -> mi_seq []
  | CVerify (e, false, message) ->
      let error =
        match message with
        | None ->
            mi_failwith_str
              (Printf.sprintf "WrongCondition: %s" (Printer.expr_to_string e))
        | Some message -> mi_seq [compile_expr message; instr MIfailwith]
      in
      mi_seq
        [ instr (MIcomment (Printer.command_to_string x))
        ; compile_expr e
        ; mi_if (mi_seq []) error ]
  | CSetVar (lhs, e) ->
      mi_seq
        [ instr (MIcomment (Printer.command_to_string x))
        ; ( match lexpr_of_expr lhs with
          | None ->
              instr
                (MIerror
                   ("Invalid lexpr (CSetVar): " ^ Printer.expr_to_string lhs))
          | Some lhs -> compile_assignment lhs (Some (compile_expr e)) ) ]
  | CUpdateSet (lhs, e, add) ->
      mi_seq
        [ instr (MIcomment (Printer.command_to_string x))
        ; ( match lexpr_of_expr lhs with
          | None ->
              instr
                (MIerror
                   ("Invalid lexpr (CSetVar): " ^ Printer.expr_to_string lhs))
          | Some lhs -> compile_update_set lhs e add ) ]
  | CFor ((name, _), ({e = ERange (a, b, step)} as range), body)
    when List.mem (mtype_of_type a.et) [mt_nat; mt_int] ->
      mi_seq
        [ instr
            (MIcomment
               (Printf.sprintf
                  "for %s in %s: ... (%s)"
                  name
                  (Printer.expr_to_string range)
                  (Printer.type_to_string step.et)))
        ; compile_range
            ~a
            ~b
            ~step
            ~name
            ~body:(compile_command body)
            ~has_operations:body.has_operations ]
  | CFor ((name, _), container, body) ->
      let projection, map, onMap =
        match container.e with
        | EListItems (map, false) ->
          ( match Type.getRepr map.et with
          | TMap {key; item} ->
              ( type_top (mtype_of_type (Typing.key_value key item))
              , map
              , mi_cdr )
          | _ ->
              ( instr
                  (MIerror
                     (Printf.sprintf
                        "map.items error: %s"
                        (Printer.type_to_string map.et)))
              , map
              , mi_seq [] ) )
        | EListValues (map, false) -> (mi_cdr, map, mi_seq [])
        | EListKeys (map, false) -> (mi_car, map, mi_seq [])
        | EListElements (set, false) -> (mi_seq [], set, mi_seq [])
        | _ -> (mi_seq [], container, mi_seq [])
      in
      let lcontainer = lexpr_of_expr map in
      let iterName = T_iter name in
      let writes = modifies_iter name body in
      mi_seq
        [ instr
            (MIcomment
               (Printf.sprintf
                  "for %s in %s: ..."
                  name
                  (Printer.expr_to_string container)))
        ; ensure_ops_if_out body.has_operations
        ; compile_expr map
        ; (if writes then mi_map else mi_iter)
            (mi_seq
               [ projection
               ; tag_top (ST_target iterName)
               ; mi_seq
                   [compile_command body; (if writes then onMap else mi_seq [])]
               ; (if writes then mi_seq [] else drop_target iterName) ])
        ; ( match lcontainer with
          | None when writes ->
              instr
                (MIerror ("Invalid lexpr (CFor): " ^ Printer.expr_to_string map))
          | Some lhs when writes ->
              let t = T_local (name ^ "#iteratee") in
              mi_seq
                [ tag_top (ST_target t)
                ; instr MIswap
                ; compile_assignment lhs (Some (dup_target t))
                ; instr MIswap
                ; instr MIdrop ]
          | _ -> mi_seq [] ) ]
  | CDefineLocal ((name, _), expr) ->
      mi_seq
        [ instr (MIcomment (Printer.command_to_string x))
        ; compile_expr expr
        ; tag_top (ST_target (T_local name))
        ; type_top (mtype_of_type expr.et) ]
  | CDropLocal name -> drop_target (T_local name)
  | CDelItem (map, key) ->
    ( match lexpr_of_expr map with
    | Some lexpr ->
        mi_seq
          [ instr (MIcomment (Printer.command_to_string x))
          ; compile_assignment (extend_lexpr lexpr (LItem key)) None ]
    | None -> instr (MIerror "invalid lexpr") )
  | CWhile (e, c) ->
      mi_seq
        [ instr
            (MIcomment
               (Printf.sprintf "while %s : ..." (Printer.expr_to_string e)))
        ; compile_expr e
        ; mi_loop
            (mi_seq
               [ compile_command c
               ; instr
                   (MIcomment
                      (Printf.sprintf
                         "check for next loop: %s"
                         (Printer.expr_to_string e)))
               ; compile_expr e ]) ]
  | CTransfer {arg; amount; destination} ->
      mi_seq
        [ instr (MIcomment (Printer.command_to_string x))
        ; make_operation
          @@ mi_seq
               [ compile_expr destination
               ; compile_expr amount
               ; compile_expr arg
               ; instr MItransfer_tokens ] ]
  | CSetDelegate e ->
      mi_seq
        [ instr (MIcomment (Printer.command_to_string x))
        ; make_operation (mi_seq [compile_expr e; instr MIset_delegate]) ]
  | CLambdaResult e ->
      mi_seq [instr (MIcomment (Printer.command_to_string x)); compile_expr e]

let () = compile_command_f := compile_command

let compile_entry_point ep =
  mi_seq
    [ instr (MIcomment (Printf.sprintf "Entry point: %s" ep.channel))
    ; tag_top2 (ST_target T_params) (ST_target T_storage)
    ; compile_command ep.body
    ; drop_target T_params ]

(** Tidies up the stack at the end of the contract. *)
let finalize_contract =
  with_ok_stack "finalize_contract" (fun stack ->
      match stack with
      | [{se_tag = ST_target T_operations}; {se_tag = ST_target T_storage}] ->
          instr MIpair
      | [{se_tag = ST_target T_storage}] -> mi_seq [ops_init; instr MIpair]
      | _ -> instr (MIerror "finalize"))

let compile_contract eps =
  mi_seq
    [ ( match eps with
      | [] -> mi_seq [mi_cdr; tag_top (ST_target T_storage)]
      | x :: l ->
          mi_seq
            [ mi_unpair_cheap
            ; List.fold_left
                (fun mich x -> mi_if_left mich (compile_entry_point x))
                (compile_entry_point x)
                l ] )
    ; finalize_contract ]

let simplify code =
  let code, _ =
    Michelson_rewriter.normalize_instr Michelson_rewriter.unfold_macros code
  in
  let code, _ =
    Michelson_rewriter.normalize_instr Michelson_rewriter.simplify code
  in
  let code, _ =
    Michelson_rewriter.normalize_instr Michelson_rewriter.fold_macros_etc code
  in
  code

let michelson_contract Basics.{storage; entry_points; tparameter; flags} =
  let tparams = mtype_of_type tparameter in
  let tstorage = mtype_of_type storage.t in
  let initial_stack =
    Stack_ok
      [ { se_type = mt_pair tparams (mtype_of_type storage.t)
        ; se_tag = ST_pair (ST_target T_params, ST_target T_storage) } ]
  in
  let code =
    instr_of_state
      (Action.exec
         {tparams; tstorage; flags}
         {block = []; stack = initial_stack}
         (compile_contract entry_points))
  in
  let code = if do_simplification then simplify code else code in
  Michelson_contract.make
    ~storage:tstorage
    ~parameter:tparams
    (typecheck tparams initial_stack code)

(* To translate SmartPy values to Michelson literals, we take a detour
   through expressions. This ensures consistent translation of values
   and expressions. *)
let compile_value v =
  let stack = Stack_ok [] in
  let tparams = mt_unit in
  let tstorage = mt_unit in
  let fixEnv =
    Fixer.
      { partial = false
      ; check_variables = false
      ; storage = Type.unit
      ; variable_names = []
      ; substContractData = Hashtbl.create 10
      ; reducer = (fun ~line_no:_ x -> x)
      ; lambda_param = None }
  in
  let typing_env = Typing.init_env ~contract_data_types:(Hashtbl.create 5) in
  let e = Fixer.fix_expr fixEnv typing_env (Expr.of_value v) in
  let code =
    instr_of_state
      (Action.exec
         {tparams; tstorage; flags = []}
         {block = []; stack}
         (compile_expr e))
  in
  match (simplify code).instr with
  | MIpush (_, l) -> l
  | MInone t -> Michelson.Literal.None t
  | i ->
      failwith
        (Printf.sprintf
           "compile_value: %s %s"
           (Printer.value_to_string v)
           (show_instr_rectypes {instr = i}))

let michelson_storage smart_contract =
  Literal.to_michelson_string (compile_value smart_contract.storage)

let michelson_contract_michelson (contract : Michelson_contract.t) =
  Printf.sprintf
    "parameter %s;\nstorage   %s;\ncode\n%s"
    (string_of_mtype ~protect:() contract.parameter)
    (string_of_mtype ~protect:() contract.storage)
    (string_of_michCode ~html:false "  " contract.code)

let michelson_contract_micheline (contract : Michelson_contract.t) =
  let mich = Michelson_contract.to_micheline contract in
  Micheline.to_json_string mich
