(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = texpr [@@deriving show]

val build : line_no:int -> uexpr -> Type.t -> t

val add : line_no:int -> Typing.env -> t -> t -> t

val mul : line_no:int -> Typing.env -> t -> t -> t

val e_mod : line_no:int -> t -> t -> t

val ediv : line_no:int -> t -> t -> t

val div : line_no:int -> t -> t -> t

val sub : line_no:int -> Typing.env -> t -> t -> t

val le : line_no:int -> Typing.env -> t -> t -> t

val lt : line_no:int -> Typing.env -> t -> t -> t

val ge : line_no:int -> Typing.env -> t -> t -> t

val gt : line_no:int -> Typing.env -> t -> t -> t

val eq : line_no:int -> Typing.env -> t -> t -> t

val neq : line_no:int -> Typing.env -> t -> t -> t

val b_or : line_no:int -> t -> t -> t

val b_and : line_no:int -> t -> t -> t

val e_max : line_no:int -> t -> t -> t

val e_min : line_no:int -> t -> t -> t

val storage : Type.t -> line_no:int -> t

val attr : line_no:int -> t -> string -> Type.t -> t

val variant : string -> t -> Type.t -> line_no:int -> t

val uvariant : Typing.env -> string -> t -> line_no:int -> t

val isVariant : line_no:int -> Typing.env -> string -> t -> t

val variant_arg : line_no:int -> Typing.env -> string -> t

val openVariant : line_no:int -> Typing.env -> t -> string -> t

val updateMap : t -> t -> t -> line_no:int -> t

val params : Type.t -> line_no:int -> t

val local : string -> Type.t -> line_no:int -> t

val item : line_no:int -> Typing.env -> t -> t -> t option -> t

val contains : line_no:int -> Typing.env option -> t -> t -> t

val sum : line_no:int -> t -> t

val range : line_no:int -> t -> t -> t -> t

val cons : line_no:int -> t -> t -> t

val cst : Literal.t -> line_no:int -> t

val unit : t

val record : line_no:int -> ((string * Type.t) * t) list -> t

val build_list : line_no:int -> tvalue:Type.t -> elems:t list -> t

val build_map :
     line_no:int
  -> big:bool Type.unknown ref
  -> tkey:Type.t
  -> tvalue:Type.t
  -> entries:(t * t) list
  -> t

val build_set : line_no:int -> telement:Type.t -> entries:t list -> t

val intXor : line_no:int -> t -> t -> t

val hash_key : line_no:int -> t -> t

val hashCrypto : line_no:int -> hash_algo -> t -> t

val pack : t -> line_no:int -> t

val unpack : line_no:int -> t -> Type.t -> t

val check_signature : line_no:int -> t -> t -> t -> t

val account_of_seed : seed:string -> line_no:int -> t

val make_signature :
     secret_key:t
  -> message:t
  -> message_format:[ `Hex | `Raw ]
  -> line_no:int
  -> t

val scenario_var : line_no:int -> t -> Type.t -> t

val reduce : line_no:int -> t -> t

val split_tokens : line_no:int -> t -> t -> t -> t

val now : t

val add_seconds : line_no:int -> t -> t -> t

val notE : line_no:int -> t -> t

val absE : line_no:int -> t -> t

val to_int : line_no:int -> t -> t

val is_nat : line_no:int -> t -> t

val negE : line_no:int -> t -> t

val signE : line_no:int -> t -> t

val slice : line_no:int -> Typing.env -> offset:t -> length:t -> buffer:t -> t

val concat_list : line_no:int -> Typing.env -> t -> t

val size : line_no:int -> Typing.env -> t -> t

val iterator : line_no:int -> string -> Type.t -> t

val balance : t

val sender : t

val source : t

val amount : t

val self : Type.t -> t

val self_entry_point : string -> Type.t -> t

val contract_address : line_no:int -> Typing.env -> t -> t

val implicit_account : line_no:int -> t -> t

val listRev : line_no:int -> Typing.env -> t -> t

val listItems : line_no:int -> Typing.env -> t -> bool -> t

val listKeys : line_no:int -> Typing.env -> t -> bool -> t

val listValues : line_no:int -> Typing.env -> t -> bool -> t

val listElements : line_no:int -> Typing.env -> t -> bool -> t

val contract : line_no:int -> string option -> Type.t -> t -> t

val pair : line_no:int -> t -> t -> t

val first : line_no:int -> Typing.env -> t -> t

val second : line_no:int -> Typing.env -> t -> t

val inline_michelson :
  line_no:int -> Type.t inline_michelson list -> t list -> t

val call_lambda : line_no:int -> Typing.env -> t -> t -> t

val lambda : line_no:int -> int -> string -> Type.t -> Type.t -> tcommand -> t

val lambdaParams : line_no:int -> int -> string -> Type.t -> t

val of_value : tvalue -> t
