(* Copyright 2019-2020 Smart Chain Arena LLC. *)

module Big_int : sig
  include module type of Big_int with type big_int = Big_int.big_int

  val pp_big_int : Format.formatter -> big_int -> unit

  val equal_big_int : big_int -> big_int -> bool

  val compare_big_int : big_int -> big_int -> int
end

(** Debug printing functions. *)
module Dbg : sig
  val on : bool ref
  (** Off by default, use [Dbg.on := true] to activate. *)

  val p : (Format.formatter -> unit -> unit) -> unit

  val f : ('a, Format.formatter, unit, unit) format4 -> 'a

  val summary : ?len:int -> string -> string
end

module type JsonGetter = sig
  val null : bool

  val get : string -> Yojson.Basic.t

  val string : string -> string

  val string_option : string -> string option

  val int : string -> int

  val bool : string -> bool
end

val json_getter : Yojson.Basic.t -> (module JsonGetter)

val json_sub : (module JsonGetter) -> string -> (module JsonGetter)

module Result : sig
  type 'a t = ('a, string) result [@@deriving eq, show]

  include Base.Applicative.S with type 'a t := 'a t

  include Base.Monad.S with type 'a t := 'a t
end

module type TRAVERSABLE = sig
  type 'a t

  type 'a f

  val traverse : ('a -> 'b f) -> 'a t -> 'b t f
end

(** Shared Utilities on hexadecimal encodings. *)
module Hex : sig
  val hexcape : string -> string

  val unhex : string -> string
end
