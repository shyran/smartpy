(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = tvalue [@@deriving eq, show {with_path = false}]

let build v t = {v; t}

let literal l t = build (Literal l) t

let int x = literal (Literal.int x) (Type.int ())

let nat x = literal (Literal.nat x) (Type.nat ())

let intOrNat t x = literal (Literal.intOrNat t x) t

let mutez i = literal (Literal.mutez i) Type.token

let int_of_value v =
  match v.v with
  | Literal {l = Int x} -> Big_int.int_of_big_int x
  | _ ->
      failwith
        (Printf.sprintf
           "Cannot convert value %s into int."
           (Printer.value_to_string v))

let bool_of_value v =
  match v.v with
  | Literal {l = Bool x} -> x
  | _ ->
      failwith
        (Printf.sprintf
           "Cannot convert value %s into bool."
           (Printer.value_to_string v))

(** Comparison (igoring [tvalue.t]). *)
let rec compare {v = v1} {v = v2} = compare_value_f compare v1 v2

let lt v1 v2 = compare v1 v2 < 0

let le v1 v2 = compare v1 v2 <= 0

let getType {t} = t

let openV x = x.v

let string s = literal (Literal.string s) Type.string

let bytes s = literal (Literal.bytes s) Type.bytes

let unString = function
  | {v = Literal {l = String s}} -> s
  | _ -> failwith "unString"

let list (l : t list) (t : Type.t) =
  List.iter
    (fun x ->
      Typing.assertEqual x.t t ~pp:(fun () ->
          [ `Text
              (Printf.sprintf
                 "building array [%s]"
                 (String.concat
                    ", "
                    (List.map (fun _ -> Printer.value_to_string x) l)))
          ; `Text "of type"
          ; `Type t ]))
    l;
  build (List l) (Type.list t)

let buildSet l telement =
  List.iter
    (fun x ->
      Typing.assertEqual x.t telement ~pp:(fun () ->
          [ `Text "bad type for set element"
          ; `Value x
          ; `Text "is not of type"
          ; `Type telement ]))
    l;
  let l = List.sort compare l in
  let rec aux acc = function
    | a :: (b :: _ as rest) ->
        if equal a b then aux acc rest else aux (a :: acc) rest
    | [a] -> List.rev (a :: acc)
    | [] -> List.rev acc
  in
  build (Set (aux [] l)) (Type.set telement)

let buildMap l t =
  let cmp (k1, v1) (k2, v2) =
    match compare k1 k2 with
    | 0 -> compare v1 v2
    | c -> c
  in
  match Type.getRepr t with
  | TMap {key = tkey; item = tvalue} ->
      let item (k, v) =
        Typing.assertEqual k.t tkey ~pp:(fun () ->
            [ `Text "bad type for map key"
            ; `Value k
            ; `Text "is not of type"
            ; `Type tkey ]);
        Typing.assertEqual v.t tvalue ~pp:(fun () ->
            [ `Text "bad type for map value"
            ; `Value v
            ; `Text "is not of type"
            ; `Type tvalue ]);
        (k, v)
      in
      build (Map (List.sort cmp (List.map item l))) t
  | _ -> assert false

let unit = literal Literal.unit Type.unit

let bool x = literal (Literal.bool x) Type.bool

let unBool = function
  | {v = Literal {l = Bool b}} -> b
  | _ -> failwith "Not a bool"

let unList = function
  | {v = List l} -> l
  | _ -> failwith "Not a list"

let unMap = function
  | {v = Map l} -> l
  | _ -> failwith "Not a map"

let unSet = function
  | {v = Set l} -> l
  | _ -> failwith "Not a set"

let unOption v =
  match v.v with
  | Variant ("Some", arg) -> Some arg
  | Variant ("None", _) -> None
  | _ -> failwith "Not an option"

let getItem ~pp items key defaultValue =
  match (items.v, key.v) with
  | Map map, _ ->
    ( match (List.find_opt (fun (x, _) -> equal key x) map, defaultValue) with
    | Some (_, v), _ -> v
    | None, Some v -> v
    | _ ->
        failwith
          (Printf.sprintf
             "Missing item in map: (%s) is not in (%s), while evaluating %s"
             (Printer.value_to_string key)
             (Printer.value_to_string items)
             (pp ())) )
  | _ ->
      failwith
        (Printf.sprintf
           "Bad getItem %s[%s]"
           (Printer.value_to_string items)
           (Printer.value_to_string key))

let unInt = function
  | {v = Literal {l = Int b}} -> b
  | _ -> failwith "Not a int"

let unMutez = function
  | {v = Literal {l = Mutez b}} -> b
  | _ -> failwith "Not a mutez"

let unAddress = function
  | {v = Literal {l = Address b}} -> b
  | _ -> failwith "Not a address"

let plus_inner x y =
  match (x.v, y.v) with
  | Literal {l = Int i}, Literal {l = Int j} ->
      intOrNat x.t (Big_int.add_big_int i j) (* TODO type *)
  | Literal {l = Mutez x}, Literal {l = Mutez y} ->
      mutez (Big_int.add_big_int x y)
  | Literal {l = String x}, Literal {l = String y} -> string (x ^ y)
  | Literal {l = Bytes x}, Literal {l = Bytes y} -> bytes (x ^ y)
  | _ -> failwith "Invalid + operation with different types"

let plus x y = plus_inner x y

let mul_inner x y =
  match (x.v, y.v) with
  | Literal {l = Int i}, Literal {l = Int j} ->
      intOrNat x.t (Big_int.mult_big_int i j) (* TODO type *)
  | _ -> failwith "Invalid * operation with different types"

let mul x y = mul_inner x y

let e_mod x y =
  match (x.v, y.v) with
  | Literal {l = Int i}, Literal {l = Int j} ->
      intOrNat x.t (Big_int.mod_big_int i j) (* TODO type *)
  | _ -> failwith "Invalid * operation with different types"

let div_inner x y =
  match (x.v, y.v) with
  | Literal {l = Int x}, Literal {l = Int y} ->
      nat (Big_int.div_big_int x y) (* TODO type *)
  | _ -> failwith "Invalid / operation with different types"

let div x y = div_inner x y

let minus x y =
  match (openV x, openV y) with
  | Literal {l = Int x}, Literal {l = Int y} ->
      int (Big_int.sub_big_int x y) (* TODO type *)
  | Literal {l = Mutez x}, Literal {l = Mutez y} ->
      mutez (Big_int.sub_big_int x y)
  | _ -> failwith "Invalid - operation"

let key_hash s = literal (Literal.key_hash s) Type.key_hash

let key s = literal (Literal.key s) Type.key

let secret_key s = literal (Literal.secret_key s) Type.secret_key

let signature s = literal (Literal.signature s) Type.signature

let record = function
  | [] -> unit
  | l ->
      build
        (Record l)
        (Type.record_or_unit (List.map (fun (s, v) -> (s, v.t)) l))

let pair v1 v2 = build (Pair (v1, v2)) (Type.pair v1.t v2.t)

let unpair = function
  | {v = Pair (x1, x2)} -> (x1, x2)
  | _ -> assert false

let variant name x t = build (Variant (name, x)) t

let none t = build (Variant ("None", unit)) (Type.option t)

let some x = build (Variant ("Some", x)) (Type.option x.t)

let ediv x y =
  let pp () = [`Text "sp.ediv("; `Value x; `Text ","; `Value y; `Text ")"] in
  Typing.assertEqual x.t y.t ~pp;
  match (x.v, y.v) with
  | Literal {l = Int x_}, Literal {l = Int y_} ->
      let t = Type.record [("a", x.t); ("b", Type.nat ())] in
      if Big_int.eq_big_int Big_int.zero_big_int y_
      then none t
      else
        some
          (record
             [ ("a", nat (Big_int.div_big_int x_ y_))
             ; ("b", nat (Big_int.mod_big_int x_ y_)) ])
  | _ -> failwith "Invalid / operation with different types"

let timestamp i = literal (Literal.timestamp i) Type.timestamp

let intXor a b =
  match (openV a, openV b) with
  | Literal {l = Int _a}, Literal {l = Int _b} ->
      assert false (*int (a lxor b)*)
  | _ -> failwith "Invalid intXor operation"

let address s = literal (Literal.address s) Type.address

let cons x l =
  match l with
  | {v = List l; t} -> build (List (x :: l)) t
  | _ -> failwith "Type error list"

(** Access the elements of a list. *)
let lens_list =
  Lens.make (fun x ->
      match (x.v, x.t.u) with
      | List focus, TList item -> {focus; zip = (fun focus -> list focus item)}
      | _ -> failwith "lens_list")

let lens_list_nth n = Lens.(lens_list @. Lens.nth n)

(** Access the entries of a map. *)
let lens_map =
  Lens.make (fun x ->
      match (x.v, x.t.u) with
      | Map focus, TMap {big; key; item} ->
          {focus; zip = (fun focus -> buildMap focus (Type.map big key item))}
      | _ -> failwith "lens_map")

let lens_map_at ~key = Lens.(lens_map @. Lens.assoc ~equal ~key)

(** Access the elements of a set. *)
let lens_set =
  Lens.make (fun x ->
      match (x.v, x.t.u) with
      | Set focus, TSet t -> {focus; zip = (fun focus -> buildSet focus t)}
      | _ -> failwith "lens_set")

let lens_set_at ~elem = Lens.(lens_set @. sorted_list ~equal ~elem)

(** Access the entries of a record. *)
let lens_record =
  Lens.make (fun x ->
      match x.v with
      | Record focus -> {focus; zip = record}
      | _ -> failwith "lens_map")

let lens_record_at ~attr =
  Lens.(lens_record @. Lens.assoc ~equal:( = ) ~key:attr)

let checkType _t _v =
  (* TODO *)
  None

let hash l =
  Digest.to_hex
    (Digest.string
       (String.concat
          " "
          (List.map
             (fun v ->
               Digest.to_hex (Digest.string (Printer.value_to_string v)))
             l)))

let zero_of_type t =
  match Type.getRepr t with
  | TBool -> bool false
  | TString -> string ""
  | TInt _ -> int Big_int.zero_big_int
  | TUnit -> unit
  | _ ->
      failwith
        (Printf.sprintf
           "zero_of_type not implemented on type [%s]"
           (Printer.type_to_string t))

let nextId prefix =
  let ids = ref 0 in
  fun () ->
    incr ids;
    Printf.sprintf "%s%i" prefix !ids
