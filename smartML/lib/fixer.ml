(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Utils

type fix_env =
  { mutable partial : bool
  ; storage : Type.t
  ; check_variables : bool
  ; mutable variable_names : string list
  ; substContractData : (int, texpr) Hashtbl.t
  ; reducer : line_no:int -> texpr -> texpr
  ; lambda_param : Type.t option }

let fix_type env =
  let open Type in
  let rec fix_type t =
    match getRepr t with
    | TBool -> bool
    | TString -> string
    | TInt {isNat} ->
      ( match !(getRef isNat) with
      | UnUnknown _ ->
          (*setEqualUnknownOption isNat (ref {value = UnValue(Some false); refs = []});*)
          t
      | UnValue true -> t
      | UnValue false -> t
      | UnRef _ -> assert false )
    | TTimestamp -> timestamp
    | TBytes -> bytes
    | TRecord l ->
        record_or_unit (List.map (fun (name, t) -> (name, fix_type t)) l)
    | TVariant l -> variant (List.map (fun (name, t) -> (name, fix_type t)) l)
    | TSet element -> set (fix_type element)
    | TMap {big; key; item} ->
        let pp () =
          [`Text "Type sp.TMap / sp.TBigMap mismatch"; `Br; `Type t]
        in
        ( match !(getRef big) with
        | UnUnknown _ -> setEqualUnknownOption ~pp big (ref (UnValue false))
        | _ -> () );
        map big (fix_type key) (fix_type item)
    | TToken -> token
    | TUnit -> unit
    | TAddress -> address
    | TContract t -> contract (fix_type t)
    | TKeyHash -> key_hash
    | TKey -> key
    | TSecretKey -> key
    | TSignature -> signature
    | TUnknown ({contents = UExact t} as r) ->
        let t = fix_type t in
        r := UExact t;
        t
    | TUnknown {contents = UUnknown _i} ->
        env.partial <- true;
        t
    | TUnknown ({contents = URecord l} as r) ->
        let t =
          record_or_unit (List.map (fun (name, t) -> (name, fix_type t)) l)
        in
        r := UExact t;
        t
    | TUnknown ({contents = UVariant l} as r) ->
        let t = variant (List.map (fun (name, t) -> (name, fix_type t)) l) in
        r := UExact t;
        t
    | TUnknown ({contents = UItems (tk, tv)} as r) ->
        r := UItems (fix_type tk, fix_type tv);
        env.partial <- true;
        t
    | TPair (t1, t2) -> pair (fix_type t1) (fix_type t2)
    | TAnnots (a, t) -> annots a (fix_type t)
    | TList t -> list (fix_type t)
    | TLambda (t1, t2) -> lambda (fix_type t1) (fix_type t2)
  in
  fix_type

let fix_literal env = Literal.map_type (fix_type env)

let fix_value env =
  let open Value in
  let rec fix_value v =
    match v.v with
    | Literal l -> literal (fix_literal env l) (fix_type env l.t)
    | Record _ -> assert false
    | Variant (_, _v) -> v (*build v.v (fix_type env v.t)*)
    | List l ->
      ( match v.t.u with
      | TList item -> list (List.map fix_value l) (fix_type env item)
      | _ -> assert false )
    | Set _ -> assert false
    | Map l ->
      ( match v.t.u with
      | TMap {big; key; item} ->
          buildMap
            (List.map (fun (k, v) -> (fix_value k, fix_value v)) l)
            (Type.map big (fix_type env key) (fix_type env item))
      | _ -> assert false )
    | Pair (_, _) -> assert false
  in
  fix_value

let add_variable_name env name ~line_no =
  match Base.List.find env.variable_names ~f:(String.equal name) with
  | None -> env.variable_names <- name :: env.variable_names
  | Some _ ->
      raise
        (SmartExcept
           [ `Text "Declaration Error"
           ; `Br
           ; `Text (Printf.sprintf "Variable name %S already in use." name)
           ; `Line line_no ])

let remove_variable_name env name ~line_no =
  match Base.List.find env.variable_names ~f:(String.equal name) with
  | None ->
      raise
        (SmartExcept
           [ `Text "Declaration Error"
           ; `Br
           ; `Text (Printf.sprintf "Missing variable %S." name)
           ; `Line line_no ])
  | Some _ ->
      env.variable_names <-
        Base.List.filter env.variable_names ~f:(fun x ->
            not (String.equal name x))

let rec fix_expr env typing_env =
  let open Expr in
  let rec fix_expr e =
    match e.e with
    | EReduce inner -> env.reducer ~line_no:e.el (fix_expr inner)
    | EStorage -> storage ~line_no:e.el (fix_type env env.storage)
    | EParams t -> params ~line_no:e.el (fix_type env t)
    | ELocal (name, t) ->
        if env.check_variables
           && not (Base.List.exists env.variable_names ~f:(String.equal name))
        then
          raise
            (SmartExcept
               [ `Text "Declaration Error"
               ; `Br
               ; `Text
                   (Printf.sprintf
                      "Local variable name %S escapes its scope."
                      name)
               ; `Line e.el ]);

        local ~line_no:e.el name (fix_type env t)
    | EPack inner -> pack ~line_no:e.el (fix_expr inner)
    | ECallLambda (lambda, parameter) ->
        call_lambda
          ~line_no:e.el
          typing_env
          (fix_expr lambda)
          (fix_expr parameter)
    | ELambda {id; name; tParams; command; tResult} ->
        lambda
          ~line_no:e.el
          id
          name
          (fix_type env tParams)
          (fix_type env tResult)
          (fix_command
             {env with lambda_param = Some tResult}
             typing_env
             command)
    | ELambdaParams {id; name; tParams} ->
        lambdaParams ~line_no:e.el id name (fix_type env tParams)
    | EUnpack (inner, t) ->
        unpack ~line_no:e.el (fix_expr inner) (fix_type env t)
    | ECst v -> cst ~line_no:e.el (fix_literal env v)
    | EBinOpPre (op, e1, e2) ->
        build ~line_no:e.el (EBinOpPre (op, fix_expr e1, fix_expr e2)) e.et
    | EBinOpInf (op, e1, e2) ->
        let e1 = fix_expr e1 in
        let e2 = fix_expr e2 in
        build ~line_no:e.el (EBinOpInf (op, e1, e2)) e.et
    | EAbs e -> absE ~line_no:e.el (fix_expr e)
    | EToInt e -> to_int ~line_no:e.el (fix_expr e)
    | EIsNat e -> is_nat ~line_no:e.el (fix_expr e)
    | ENeg e -> negE ~line_no:e.el (fix_expr e)
    | ESign e -> signE ~line_no:e.el (fix_expr e)
    | ENot e ->
      ( match e.e with
      | ENot x -> fix_expr x
      | EBinOpInf (BNeq, x, y) ->
          eq ~line_no:e.el typing_env (fix_expr x) (fix_expr y)
      | EBinOpInf (BEq, x, y) ->
          neq ~line_no:e.el typing_env (fix_expr x) (fix_expr y)
      | _ -> notE ~line_no:e.el (fix_expr e) )
    | EContract_data ({e = ECst {l = Int i}} as c) ->
        let subst =
          Hashtbl.find_opt env.substContractData (Big_int.int_of_big_int i)
        in
        ( match subst with
        | Some x ->
            Typing.assertEqual e.et x.et ~pp:(fun () ->
                [`Text "Substitution"; `Exprs [e; x]]);
            x
        | None ->
            build
              ~line_no:e.el
              (EContract_data (fix_expr c))
              (fix_type env e.et) )
    | ESplit_tokens (mutez, quantity, total) ->
        split_tokens
          ~line_no:e.el
          (fix_expr mutez)
          (fix_expr quantity)
          (fix_expr total)
    | EUpdate_map (map, key, value) ->
        updateMap ~line_no:e.el (fix_expr map) (fix_expr key) (fix_expr value)
    | ECons (x, xs) -> cons ~line_no:e.el (fix_expr x) (fix_expr xs)
    | EInt_x_or (e1, e2) -> intXor ~line_no:e.el (fix_expr e1) (fix_expr e2)
    | ESum xs -> sum ~line_no:e.el (fix_expr xs)
    | ERange (a, b, step) ->
        range ~line_no:e.el (fix_expr a) (fix_expr b) (fix_expr step)
    | EAdd_seconds (t, s) ->
        add_seconds ~line_no:e.el (fix_expr t) (fix_expr s)
    | ECheck_signature (pk, signature, message) ->
        check_signature
          ~line_no:e.el
          (fix_expr pk)
          (fix_expr signature)
          (fix_expr message)
    | EContract_data e1 ->
        build ~line_no:e.el (EContract_data (fix_expr e1)) (fix_type env e.et)
    | EScenario_var id ->
        scenario_var ~line_no:e.el (fix_expr id) (fix_type env e.et)
    | EContract_address e ->
        contract_address ~line_no:e.el typing_env (fix_expr e)
    | EImplicit_account e -> implicit_account ~line_no:e.el (fix_expr e)
    | ESelf_entry_point (name, t) -> self_entry_point name (fix_type env t)
    | ENow | ESender | ESource | EAmount | EBalance | ESelf ->
        build ~line_no:e.el e.e e.et
    | ERecord l ->
        record
          ~line_no:e.el
          (List.map
             (fun (name, e) ->
               let e = fix_expr e in
               ((name, e.et), e))
             l)
    | EList elems ->
      ( match e.et.u with
      | TList item ->
          build_list
            ~line_no:e.el
            ~tvalue:(fix_type env item)
            ~elems:(List.map fix_expr elems)
      | _ -> assert false )
    | ESet entries ->
      ( match e.et.u with
      | TSet element ->
          build_set
            ~line_no:e.el
            ~telement:(fix_type env element)
            ~entries:(List.map (fun k -> fix_expr k) entries)
      | _ -> assert false )
    | EMap entries ->
      ( match e.et.u with
      | TMap {big; key; item} ->
          build_map
            ~line_no:e.el
            ~big
            ~tkey:(fix_type env key)
            ~tvalue:(fix_type env item)
            ~entries:
              (List.map (fun (k, v) -> (fix_expr k, fix_expr v)) entries)
      | _ -> assert false )
    | EIter (name, t) ->
        if env.check_variables
           && not (Base.List.exists env.variable_names ~f:(String.equal name))
        then
          raise
            (SmartExcept
               [ `Text "Declaration Error"
               ; `Br
               ; `Text
                   (Printf.sprintf
                      "Iterator variable name %S escapes its scope."
                      name)
               ; `Line e.el ]);
        iterator ~line_no:e.el name (fix_type env t)
    | EAttr (name, inner) ->
        attr ~line_no:e.el (fix_expr inner) name (fix_type env e.et)
    | EOpenVariant (name, inner) ->
        openVariant ~line_no:e.el typing_env (fix_expr inner) name
    | EVariant_arg arg_name ->
        if env.check_variables
           && not
                (Base.List.exists env.variable_names ~f:(String.equal arg_name))
        then
          raise
            (SmartExcept
               [ `Text "Declaration Error"
               ; `Br
               ; `Text
                   (Printf.sprintf
                      "Pattern variable name %S escapes its scope."
                      arg_name)
               ; `Line e.el ]);
        variant_arg ~line_no:e.el typing_env arg_name
    | EIsVariant (name, inner) ->
        isVariant ~line_no:e.el typing_env name (fix_expr inner)
    | EVariant (name, inner) ->
        variant ~line_no:e.el name (fix_expr inner) (fix_type env e.et)
    | EItem (l, x, None) ->
        item ~line_no:e.el typing_env (fix_expr l) (fix_expr x) None
    | EItem (l, x, Some d) ->
        item
          ~line_no:e.el
          typing_env
          (fix_expr l)
          (fix_expr x)
          (Some (fix_expr d))
    | EContains (l, x) ->
        contains ~line_no:e.el (Some typing_env) (fix_expr l) (fix_expr x)
    | EHash (algo, x) -> hashCrypto ~line_no:e.el algo (fix_expr x)
    | EHash_key key -> hash_key ~line_no:e.el (fix_expr key)
    | EListRev e -> listRev ~line_no:e.el typing_env e
    | EListItems (e, rev) -> listItems ~line_no:e.el typing_env e rev
    | EListKeys (e, rev) -> listKeys ~line_no:e.el typing_env e rev
    | EListValues (e, rev) -> listValues ~line_no:e.el typing_env e rev
    | EListElements (e, rev) -> listElements ~line_no:e.el typing_env e rev
    | EContract {entry_point; arg_type; address} ->
        contract
          ~line_no:e.el
          entry_point
          (fix_type env arg_type)
          (fix_expr address)
    | EPair (e1, e2) -> pair ~line_no:e.el (fix_expr e1) (fix_expr e2)
    | EFirst e -> first ~line_no:e.el typing_env (fix_expr e)
    | ESecond e -> second ~line_no:e.el typing_env (fix_expr e)
    | ESlice {offset; length; buffer} ->
        slice ~line_no:e.el typing_env ~offset ~length ~buffer
    | EConcat_list l -> concat_list ~line_no:e.el typing_env l
    | ESize s -> size ~line_no:e.el typing_env s
    | EMichelson (mich, exprs) ->
        inline_michelson ~line_no:e.el mich (List.map fix_expr exprs)
    | EAccount_of_seed _ -> build ~line_no:e.el e.e e.et
    | EMake_signature {secret_key; message; message_format} ->
        make_signature
          ~secret_key:(fix_expr secret_key)
          ~message:(fix_expr message)
          ~message_format
          ~line_no:e.el
  in
  fix_expr

and fix_command env typing_env =
  let open Command in
  let rec fix_command {c; line_no} =
    match c with
    | CFailwith e -> sp_failwith ~line_no (fix_expr env typing_env e)
    | CIf (c, t, e) ->
        ifte
          ~line_no
          (fix_expr env typing_env c)
          (fix_command t)
          (fix_command e)
    | CMatch (scrutinee, (constructor, arg_name, body)) ->
        add_variable_name env arg_name ~line_no;
        let r =
          mk_match
            ~line_no
            typing_env
            (fix_expr env typing_env scrutinee)
            constructor
            arg_name
            (fun () -> fix_command body)
        in
        remove_variable_name env arg_name ~line_no;
        r
    | CSeq l -> seq ~line_no (List.map fix_command l)
    | CTransfer {arg; amount; destination} ->
        transfer
          ~line_no
          ~destination:(fix_expr env typing_env destination)
          ~arg:(fix_expr env typing_env arg)
          ~amount:(fix_expr env typing_env amount)
    | CDefineLocal ((name, t), e) ->
        Dbg.f
          "CDefineLocal: %S ([ %s ])"
          name
          (Base.String.concat ~sep:";" env.variable_names);
        add_variable_name env name ~line_no;
        defineLocal ~line_no name (fix_expr env typing_env e) (fix_type env t)
    | CDropLocal name ->
        remove_variable_name env name ~line_no;
        dropLocal ~line_no name
    | CSetVar (v, e) ->
        set
          ~line_no
          typing_env
          (fix_expr env typing_env v)
          (fix_expr env typing_env e)
    | CDelItem (expr, item) ->
        delItem
          ~line_no
          typing_env
          (fix_expr env typing_env expr)
          (fix_expr env typing_env item)
    | CUpdateSet (expr, element, add) ->
        updateSet
          ~line_no
          (fix_expr env typing_env expr)
          (fix_expr env typing_env element)
          add
    | CFor ((name, t), e, c) ->
        add_variable_name env name ~line_no;
        let r =
          forGroup
            ~line_no
            (name, fix_type env t)
            (fix_expr env typing_env e)
            (fix_command c)
        in
        remove_variable_name env name ~line_no;
        r
    | CWhile (e, c) ->
        whileLoop ~line_no (fix_expr env typing_env e) (fix_command c)
    | CVerify (e, ghost, message) ->
        verify
          ~line_no
          (fix_expr env typing_env e)
          ghost
          (Base.Option.map ~f:(fix_expr env typing_env) message)
    | CSetDelegate e -> set_delegate ~line_no (fix_expr env typing_env e)
    | CLambdaResult e ->
      ( match env.lambda_param with
      | None -> assert false
      | Some t ->
          Typing.assertEqual t e.et ~pp:(fun () ->
              [`Text "Typing lambda"; `Type t; `Type e.et]);
          lambda_result ~line_no (fix_expr env typing_env e) )
  in
  fix_command

let checkDiff fix_env typing_env e e1 e2 ~line_no =
  let e1 = fix_expr fix_env typing_env e1 in
  let e2 = fix_expr fix_env typing_env e2 in
  let pp () =
    [ `Text "Type Error"
    ; `Exprs [e1; e2]
    ; `Text "cannot be subtracted in "
    ; `Expr e
    ; `Line line_no ]
  in
  match Type.getRepr e1.et with
  | TToken -> Typing.assertEqual ~pp e1.et e.et
  | TInt _ -> Typing.assertEqual ~pp e.et (Type.int ())
  | TTimestamp -> Typing.assertEqual ~pp e.et (Type.int ())
  | TString | TBytes | TUnit | TBool | TRecord _ | TVariant _ | TSet _
   |TMap _ | TAddress | TContract _ | TKeyHash | TKey | TSecretKey
   |TSignature | TUnknown _ | TPair _ | TAnnots _ | TList _ | TLambda _ ->
      raise (SmartExcept (pp ()))

let rec checkComparable fixEnv t =
  match Type.getRepr t with
  | TInt _ | TTimestamp | TBool | TString | TKeyHash | TKey | TAddress
   |TBytes | TToken ->
      true
  | TRecord l ->
      List.fold_left (fun t (_, e) -> t && checkComparable fixEnv e) true l
  | TMap {item} -> checkComparable fixEnv item (* ??? TODO check *)
  | TUnknown {contents = UUnknown _} ->
      fixEnv.partial <- true;
      true
  | _ -> false

let checkConstraint fix_env typing_env = function
  | HasSub (e, e1, e2) ->
      let e1 = fix_expr fix_env typing_env e1 in
      let e2 = fix_expr fix_env typing_env e2 in
      checkDiff fix_env typing_env e e1 e2 ~line_no:e.el
  | IsComparable e ->
      let e = fix_expr fix_env typing_env e in
      if not (checkComparable fix_env e.et)
      then
        failwith
          (Printf.sprintf
             "Type error, [%s : %s] doesn't have a comparable type."
             (Printer.expr_to_string e)
             (Printer.type_to_string e.et))
  | HasGetItem (l, pos) ->
      let l = fix_expr fix_env typing_env l in
      let pos = fix_expr fix_env typing_env pos in
      let pp () =
        [`Text "Type Error"; `Expr l; `Text "cannot get item"; `Expr pos]
      in
      if not (Typing.checkGetItem pp l.et pos.et)
      then raise (SmartExcept (pp ()))
  | HasContains (items, member) ->
      let items = fix_expr fix_env typing_env items in
      let member = fix_expr fix_env typing_env member in
      let pp () =
        [`Text "Type Error"; `Expr items; `Text "cannot contains"; `Expr member]
      in
      if not (Typing.checkContains pp items.et member.et)
      then raise (SmartExcept (pp ()))
  | HasSize e ->
      let e = fix_expr fix_env typing_env e in
      let pp () =
        [`Text "Type Error"; `Expr e; `Text "has no length or size"; `Line e.el]
      in
      ( match Type.getRepr e.et with
      | TList _ | TString | TBytes | TSet _ | TMap _ -> ()
      | _ -> raise (SmartExcept (pp ())) )
  | HasSlice e ->
      let e = fix_expr fix_env typing_env e in
      let pp () =
        [`Text "Type Error"; `Expr e; `Text "cannot be sliced"; `Line e.el]
      in
      ( match Type.getRepr e.et with
      | TString | TBytes -> ()
      | _ -> raise (SmartExcept (pp ())) )
  | HasAdd (e, e1, e2) ->
      let e1 = fix_expr fix_env typing_env e1 in
      let e2 = fix_expr fix_env typing_env e2 in
      let pp () =
        [ `Text "Type Error"
        ; `Exprs [e1; e2]
        ; `Text "cannot be added in "
        ; `Expr e
        ; `Line e.el ]
      in
      ( match Type.getRepr e1.et with
      | TToken | TString | TBytes | TInt _ -> ()
      | TUnit | TBool | TTimestamp | TRecord _ | TVariant _ | TSet _ | TMap _
       |TAddress | TContract _ | TKeyHash | TKey | TSecretKey | TSignature
       |TUnknown _ | TPair _ | TAnnots _ | TList _ | TLambda _ ->
          raise (SmartExcept (pp ())) )

let fix_contract fix_env typing_env ~storage ~entry_points ~tparameter ~flags =
  let fixParamsType (t : Type.t) =
    match t.u with
    | TUnknown {contents = UUnknown _} ->
        Typing.assertEqual t Type.unit ~pp:(fun () -> [`Text "fix_contract"]);
        t
    | _ -> fix_type fix_env t
  in
  List.iter (checkConstraint fix_env typing_env) !(typing_env.constraints);
  let entry_points =
    List.map
      (fun (typing_env, ev) ->
        fix_env.variable_names <- [];
        { ev with
          paramsType = fixParamsType ev.paramsType
        ; body =
            fix_command
              {fix_env with check_variables = true}
              typing_env
              ev.body })
      entry_points
  in
  let tparameter = fixParamsType tparameter in
  List.iter (checkConstraint fix_env typing_env) !(typing_env.constraints);
  { balance = Big_int.zero_big_int
  ; storage
  ; tparameter
  ; entry_points
  ; partialContract = fix_env.partial
  ; flags }
