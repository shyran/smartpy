(* Copyright 2019-2020 Smart Chain Arena LLC. *)

module Big_int = struct
  include Big_int

  let pp_big_int ppf p = Format.fprintf ppf "%s" (string_of_big_int p)

  let equal_big_int = Big_int.eq_big_int

  let compare_big_int = Big_int.compare_big_int
end

module Dbg = struct
  open Format

  let on = ref false

  let zero = Sys.time ()

  let f fmt =
    let now = Sys.time () in
    if !on
    then eprintf ("\n@[|DBG-lib:%f> " ^^ fmt ^^ "@]\n%!") (now -. zero)
    else ikfprintf ignore err_formatter fmt

  let p g = f "%a" g ()

  let summary ?(len = 200) s =
    try Base.String.sub ~pos:0 ~len s with
    | _ -> s
end

module type JsonGetter = sig
  val null : bool

  val get : string -> Yojson.Basic.t

  val string : string -> string

  val string_option : string -> string option

  val int : string -> int

  val bool : string -> bool
end

let json_getter x =
  ( module struct
    open Yojson.Basic.Util

    let null = x = `Null

    let get attr = member attr x

    let string attr =
      try to_string (get attr) with
      | _ -> failwith ("Cannot parse " ^ attr)

    let string_option attr = to_string_option (get attr)

    let int attr = to_int (get attr)

    let bool attr = to_bool (get attr)
  end : JsonGetter )

let json_sub x attr =
  let module M = (val x : JsonGetter) in
  let open M in
  json_getter (get attr)

module Result = struct
  module Basic = struct
    type 'a t = ('a, string) result [@@deriving eq, show {with_path = false}]

    let return x = Ok x

    let apply f x =
      match (f, x) with
      | Ok f, Ok x -> Ok (f x)
      | (Error _ as e), _ -> e
      | _, (Error _ as e) -> e

    let map =
      `Custom
        (fun x ~f ->
          match x with
          | Ok x -> Ok (f x)
          | Error _ as x -> x)

    let bind x ~f =
      match x with
      | Ok x -> f x
      | Error _ as e -> e
  end

  include Basic
  include Base.Applicative.Make (Basic)
  include Base.Monad.Make (Basic)
end

module type TRAVERSABLE = sig
  type 'a t

  type 'a f

  val traverse : ('a -> 'b f) -> 'a t -> 'b t f
end

module Hex = struct
  let hexcape s =
    let b = Buffer.create (String.length s * 2) in
    let addf fmt = Printf.ksprintf (Buffer.add_string b) fmt in
    Base.String.iter s ~f:(fun c -> addf "%02x" (Char.code c));
    Buffer.contents b

  let unhex s =
    let b = Buffer.create ((String.length s / 2) + 2) in
    for i = 0 to String.length s - 2 do
      if i mod 2 = 0
      then
        Scanf.sscanf (String.sub s i 2) "%02x" (fun c ->
            Buffer.add_char b (Char.chr c))
    done;
    Buffer.contents b
end
