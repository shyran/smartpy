(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Type

type env =
  { unknowns : (string, Type.t) Hashtbl.t
  ; constraints : importConstraint list ref
  ; variant_args : (string, Type.t) Hashtbl.t
  ; contract_data_types : (int, Type.t) Hashtbl.t }

let init_env ~contract_data_types =
  { unknowns = Hashtbl.create 10
  ; constraints = ref []
  ; variant_args = Hashtbl.create 5
  ; contract_data_types }

let unknown importEnv i =
  match Hashtbl.find_opt importEnv.unknowns i with
  | Some t -> t
  | None ->
      let r = unknown_raw (ref (UUnknown i)) in
      if i <> "" then Hashtbl.add importEnv.unknowns i r;
      r

let intType isNat =
  match !(getRef isNat) with
  | UnUnknown _ -> `Unknown
  | UnValue true -> `Nat
  | UnValue false -> `Int
  | UnRef _ -> assert false

let for_variant importEnv name t =
  match name with
  | "None" -> option (unknown importEnv "")
  | "Some" -> option t
  | "Left" -> tor t (unknown importEnv "")
  | "Right" -> tor (unknown importEnv "") t
  | _ -> uvariant name t

let key_value tkey tvalue = record [("key", tkey); ("value", tvalue)]

let rec assertEqual ~pp t1 t2 =
  match (getRepr t1, getRepr t2) with
  | TBool, TBool
   |TString, TString
   |TBytes, TBytes
   |TToken, TToken
   |TUnit, TUnit
   |TAddress, TAddress
   |TKey, TKey
   |TSecretKey, TSecretKey
   |TKeyHash, TKeyHash
   |TSignature, TSignature
   |TTimestamp, TTimestamp ->
      ()
  | TContract t1, TContract t2 -> assertEqual ~pp t1 t2
  | TInt {isNat = isNat1}, TInt {isNat = isNat2} when isNat1 == isNat2 -> ()
  | TInt {isNat = isNat1}, TInt {isNat = isNat2} ->
      setEqualUnknownOption
        ~pp:(fun () ->
          [`Text "Type sp.TInt / sp.TNat mismatch"; `Br; `Rec (pp ())])
        isNat1
        isNat2
  | TUnknown t1, TUnknown t2 when t1 == t2 -> ()
  | TUnknown ({contents = UItems (k1, v1)} as r1), TMap {key = k2; item = v2}
    ->
      assertEqual ~pp k1 k2;
      assertEqual ~pp v1 v2;
      r1 := UExact t2
  | TUnknown ({contents = UVariant l1} as r1), TVariant l2
   |TUnknown ({contents = URecord l1} as r1), TRecord l2 ->
      let checkInside (name, t) =
        match List.assoc_opt name l2 with
        | None ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text (Printf.sprintf "Missing field [%s] in type" name)
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | Some t2 -> assertEqual t t2 ~pp
      in
      List.iter checkInside l1;
      r1 := UExact t2
  | TMap _, TUnknown _
   |TSet _, TUnknown _
   |TRecord _, TUnknown _
   |TPair _, TUnknown _
   |TVariant _, TUnknown _ ->
      assertEqual t2 t1 ~pp
  | TUnknown ({contents = UUnknown _} as r), _ -> r := UExact t2
  | _, TUnknown ({contents = UUnknown _} as r) -> r := UExact t1
  | ( TUnknown {contents = UItems (tk1, tv1)}
    , TUnknown ({contents = UItems (tk2, tv2)} as r2) ) ->
      assertEqual tk1 tk2 ~pp;
      assertEqual tv1 tv2 ~pp;
      r2 := UItems (tk1, tv1) (* TODO better sharing *)
  | ( TUnknown ({contents = URecord l1_} as r1)
    , TUnknown ({contents = URecord l2_} as r2) ) ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> []
        | [], x | x, [] -> x
        | a :: l1, (b :: _ as l2) when fst a < fst b -> a :: acc l1 l2
        | (a :: _ as l1), b :: l2 when fst b < fst a -> b :: acc l1 l2
        | (a, t1_) :: l1, (_b, t2_) :: l2 ->
            assertEqual ~pp t1_ t2_;
            (a, t1_) :: acc l1 l2
      in
      let l = acc (List.sort compare l1_) (List.sort compare l2_) in
      r1 := URecord l;
      r2 := UExact t1
  | ( TUnknown ({contents = UVariant l1_} as r1)
    , TUnknown ({contents = UVariant l2_} as r2) ) ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> []
        | [], x | x, [] -> x
        | a :: l1, (b :: _ as l2) when fst a < fst b -> a :: acc l1 l2
        | (a :: _ as l1), b :: l2 when fst b < fst a -> b :: acc l1 l2
        | (a, t1_) :: l1, (b, t2_) :: l2 ->
            assert (a = b);
            assertEqual ~pp t1_ t2_;
            (a, t1_) :: acc l1 l2
      in
      let l = acc (List.sort compare l1_) (List.sort compare l2_) in
      r1 := UVariant l;
      r2 := UExact t1
  | TLambda (t11, t12), TLambda (t21, t22) | TPair (t11, t12), TPair (t21, t22)
    ->
      assertEqual t11 t21 ~pp;
      assertEqual t12 t22 ~pp
  | TList t1, TList t2 -> assertEqual t1 t2 ~pp
  | TRecord l1_, TRecord l2_ ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> ()
        | [], (a, _) :: _ | (a, _) :: _, [] ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text (Printf.sprintf "missing field %s in record" a)
                 ; `Type t1
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | (a, t1_) :: l1, (b, t2_) :: l2 ->
            if a <> b
            then
              raise
                (SmartExcept
                   [ `Text "Type Error"
                   ; `Br
                   ; `Text
                       (Printf.sprintf
                          "non matching names [%s] and [%s] in records"
                          a
                          b)
                   ; `Type t1
                   ; `Type t2
                   ; `Br
                   ; `Rec (pp ()) ]);
            assertEqual ~pp t1_ t2_;
            acc l1 l2
      in
      acc (List.sort compare l1_) (List.sort compare l2_)
  | TSet t1, TSet t2 -> assertEqual t1 t2 ~pp
  | TVariant l1, TVariant l2 ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> ()
        | [], _ | _, [] ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text "Incompatible variant types:"
                 ; `Br
                 ; `Type t1
                 ; `Br
                 ; `Text "and"
                 ; `Br
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | (a, t1) :: l1, (b, t2) :: l2 ->
            if a <> b
            then
              raise
                (SmartExcept
                   [ `Text "Type Error"
                   ; `Br
                   ; `Text
                       (Printf.sprintf
                          "non matching names [%s] and [%s] in variant \
                           constructors"
                          a
                          b)
                   ; `Type t1
                   ; `Text "and"
                   ; `Type t2
                   ; `Rec (pp ()) ]);
            assertEqual ~pp t1 t2;
            acc l1 l2
      in
      acc (List.sort compare l1) (List.sort compare l2)
  | ( TMap {big = big1; key = k1; item = i1}
    , TMap {big = big2; key = k2; item = i2} ) ->
      assertEqual ~pp i1 i2;
      assertEqual ~pp k1 k2;
      setEqualUnknownOption
        ~pp:(fun () ->
          [`Text "Type sp.TMap / sp.TBigMap mismatch"; `Br; `Rec (pp ())])
        big1
        big2
  | _ ->
      raise
        (SmartExcept
           [ `Text "Type Error"
           ; `Br
           ; `Type t1
           ; `Text "is not"
           ; `Type t2
           ; `Br
           ; `Rec (pp ()) ])

let checkGetItem pp items pos =
  match getRepr items with
  | TMap {key} | TUnknown {contents = UItems (key, _)} ->
      assertEqual ~pp key pos;
      true
  | _ -> false

let checkContains pp items member =
  match getRepr items with
  (* | TItems {item; kind = TArray _} ->
   *     assertEqual ~pp item member;
   *     true *)
  | TSet element ->
      assertEqual ~pp element member;
      true
  | TMap {key} ->
      assertEqual ~pp key member;
      true
  | _ -> false
