(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Typing

type t = tcommand

let build ~line_no has_operations c = {c; line_no; has_operations}

let ifte ~line_no c t e =
  assertEqual c.et Type.bool ~pp:(fun () ->
      [`Text "sp.if"; `Expr c; `Text ":"; `Line line_no]);
  match c.e with
  | ECst v -> if Literal.unBool v then t else e
  | _ -> build ~line_no (t.has_operations || e.has_operations) (CIf (c, t, e))

let ifteSome ~line_no env c t e =
  ifte ~line_no (Expr.isVariant ~line_no env "Some" c) t e

let mk_match ~line_no env scrutinee constructor arg_name body =
  let at = unknown env "" in
  let vt = for_variant env constructor at in
  assertEqual scrutinee.et vt ~pp:(fun () ->
      [ `Text "match scrutinee has incompatible type:"
      ; `Type scrutinee.et
      ; `Br
      ; `Text "Expected type:"
      ; `Type vt
      ; `Line line_no ]);
  Hashtbl.replace env.variant_args arg_name at;
  let body = body () in
  build
    ~line_no
    body.has_operations
    (CMatch (scrutinee, (constructor, arg_name, body)))

let sp_failwith ~line_no message = build ~line_no false (CFailwith message)

let verify ~line_no e ghost message =
  assertEqual e.et Type.bool ~pp:(fun () ->
      [`Text "not a boolean expression"; `Line line_no]);
  build ~line_no false (CVerify (e, ghost, message))

let set_delegate ~line_no e =
  assertEqual e.et (Type.option Type.key_hash) ~pp:(fun () ->
      [`Text "not an optional hash"; `Line line_no]);
  build ~line_no false (CSetDelegate e)

let forGroup ~line_no (name, t) e c =
  assertEqual e.et (Type.list t) ~pp:(fun () ->
      [ `Text
          (Printf.sprintf
             "for (%s : %s) in"
             (Printer.variable_to_string (name, t) Iter)
             (Printer.type_to_string t))
      ; `Expr e
      ; `Line line_no ]);
  build ~line_no c.has_operations (CFor ((name, t), e, c))

let whileLoop ~line_no e l =
  assertEqual e.et Type.bool ~pp:(fun () ->
      [`Text "while"; `Expr e; `Line line_no]);
  build ~line_no l.has_operations (CWhile (e, l))

let delItem ~line_no env x y =
  let pp () =
    [`Text "del"; `Expr x; `Text "["; `Expr y; `Text "]"; `Line line_no]
  in
  assertEqual
    x.et
    (Type.map (ref (Type.UnUnknown "")) y.et (unknown env ""))
    ~pp;
  build ~line_no false (CDelItem (x, y))

let updateSet ~line_no x y add =
  let pp () =
    [ `Expr x
    ; `Text "."
    ; `Text (if add then "add" else "remove")
    ; `Text "("
    ; `Expr y
    ; `Text ")"
    ; `Line line_no ]
  in
  assertEqual x.et (Type.set y.et) ~pp;
  build ~line_no false (CUpdateSet (x, y, add))

let rec set ~line_no env x y =
  assertEqual x.et y.et ~pp:(fun () ->
      [`Text "set"; `Expr x; `Text "="; `Expr y; `Line line_no]);
  match x.e with
  | EStorage | EItem _ | ELocal _ | EAttr (_, _) ->
    ( match y.e with
    | EUpdate_map (map, key, {e = EVariant ("None", {e = ECst {l = Unit}})})
      when x = map ->
        delItem ~line_no env x key
    | EUpdate_map (map, key, {e = EVariant ("Some", v)}) when x = map ->
        set ~line_no env (Expr.item ~line_no env map key None) v
    | _ -> build ~line_no false (CSetVar (x, y)) )
  | _ ->
      raise
        (Basics.SmartExcept
           [ `Text "Type Error"
           ; `Br
           ; `Expr x
           ; `Text "is not a variable"
           ; `Line line_no ])

let defineLocal ~line_no name e t =
  assertEqual e.et t ~pp:(fun () ->
      [ `Text "cannot define local"
      ; `Text name
      ; `Text "with"
      ; `Expr e
      ; `Text "and type"
      ; `Type t
      ; `Line line_no ]);
  build ~line_no false (CDefineLocal ((name, t), e))

let dropLocal ~line_no name = build ~line_no false (CDropLocal name)

let transfer ~line_no ~arg ~amount ~destination =
  let cmd = build ~line_no true (CTransfer {arg; amount; destination}) in
  let pp () =
    [ `Text "transfer("
    ; `Exprs [arg; amount; destination]
    ; `Text ")"
    ; `Line line_no ]
  in
  assertEqual destination.et (Type.contract arg.et) ~pp;
  assertEqual amount.et Type.token ~pp;
  cmd

let seq ~line_no l =
  let rec removePass = function
    | {c = CSeq l} -> List.concat (List.map removePass l)
    | x -> [x]
  in
  let l = List.concat (List.map removePass l) in
  match l with
  | [x] -> build ~line_no:x.line_no x.has_operations x.c
  | _ -> build ~line_no (List.exists (fun x -> x.has_operations) l) (CSeq l)

let setType ~line_no e t =
  assertEqual t e.et ~pp:(fun () ->
      [`Expr e; `Text "is not compatible with type"; `Type t; `Line line_no]);
  seq ~line_no []

let lambda_result ~line_no e = build ~line_no false (CLambdaResult e)
