(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = string

(** {1 Contracts} *)

val full_html :
  contract:tcontract -> def:string -> onlyDefault:bool -> line_no:int -> t

(** {1 Helpers} *)

type tab

val tab : ?active:unit -> string -> t -> tab
(** tab ?active name inner_html *)

val tabs : string -> tab list -> t

val showLine : int -> t

(** {1 Dynamic UI} *)

val nextInputGuiId : unit -> string

val nextOutputGuiId : unit -> string

val contextSimulationType : Type.t

val inputGui : Type.t -> string -> line_no:int -> t

val delayedInputGui : Type.t -> t

val simulatedContract : tcontract option ref

val simulation : tcontract -> line_no:int -> t
