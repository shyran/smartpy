(* Copyright 2019-2020 Smart Chain Arena LLC. *)

val decompile_contract : Michelson.Michelson_contract.t -> Basics.tcommand

val test : unit -> unit
