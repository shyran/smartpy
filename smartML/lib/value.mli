(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = tvalue [@@deriving eq, show]

val build : uvalue -> Type.t -> t

val literal : Literal.t -> Type.t -> t

val int : Big_int.big_int -> t

val nat : Big_int.big_int -> t

val intOrNat : Type.t -> Big_int.big_int -> t

val mutez : Big_int.big_int -> t

val int_of_value : t -> int

val bool_of_value : t -> bool

val compare : t -> t -> int

val lt : t -> t -> bool

val le : t -> t -> bool

val getType : t -> Type.t

val openV : t -> uvalue

val string : string -> t

val bytes : string -> t

val unString : t -> string

val unAddress : t -> string

val list : t list -> Type.t -> t

val buildSet : t list -> Type.t -> t

val buildMap : (t * t) list -> Type.t -> t

val unit : t

val bool : bool -> t

val unBool : t -> bool

val unList : t -> t list

val unMap : t -> (t * t) list

val unSet : t -> t list

val unOption : t -> t option

val getItem : pp:(unit -> string) -> t -> t -> t option -> t

val unInt : t -> Big_int.big_int

val unMutez : t -> Big_int.big_int

val plus_inner : t -> t -> t

val plus : t -> t -> t

val mul_inner : t -> t -> t

val mul : t -> t -> t

val e_mod : t -> t -> t

val div_inner : t -> t -> t

val div : t -> t -> t

val minus : t -> t -> t

val key_hash : string -> t

val key : string -> t

val secret_key : string -> t

val signature : string -> t

val record : (string * t) list -> t

val pair : t -> t -> t

val unpair : t -> t * t

val variant : string -> t -> Type.t -> t

val none : Type.t -> t

val some : t -> t

val ediv : t -> t -> t

val timestamp : Big_int.big_int -> t

val intXor : t -> t -> 'a

val address : string -> t

val cons : t -> t -> t

val lens_list : (t, t list) Lens.t

val lens_list_nth : int -> (t, t option) Lens.t

val lens_map : (t, (t * t) list) Lens.t

val lens_map_at : key:t -> (t, t option) Lens.t

val lens_set : (t, t list) Lens.t

val lens_set_at : elem:t -> (t, bool) Lens.t

val lens_record : (t, (string * t) list) Lens.t

val lens_record_at : attr:string -> (t, t option) Lens.t

val checkType : 'a -> 'b -> 'c option

val hash : t list -> string

val zero_of_type : Type.t -> t

val nextId : string -> unit -> string
