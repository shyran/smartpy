(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = tcommand

val ifte : line_no:int -> texpr -> t -> t -> t

val ifteSome : line_no:int -> Typing.env -> Expr.t -> t -> t -> t

val mk_match :
  line_no:int -> Typing.env -> texpr -> string -> string -> (unit -> t) -> t

val sp_failwith : line_no:int -> texpr -> t

val verify : line_no:int -> texpr -> bool -> texpr option -> t

val set_delegate : line_no:int -> texpr -> t

val forGroup : line_no:int -> string * Type.t -> texpr -> t -> t

val whileLoop : line_no:int -> texpr -> t -> t

val delItem : line_no:int -> Typing.env -> texpr -> texpr -> t

val updateSet : line_no:int -> texpr -> texpr -> bool -> t

val set : line_no:int -> Typing.env -> texpr -> texpr -> t

val defineLocal : line_no:int -> string -> texpr -> Type.t -> t

val dropLocal : line_no:int -> string -> t

val transfer :
  line_no:int -> arg:texpr -> amount:texpr -> destination:texpr -> t

val seq : line_no:int -> t list -> t

val setType : line_no:int -> texpr -> Type.t -> t

val lambda_result : line_no:int -> texpr -> t
