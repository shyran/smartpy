(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Michelson

type rule_set

val unfold_macros : rule_set

val fold_macros_etc : rule_set

val simplify : rule_set

val normalize_instr : rule_set -> instr -> instr * bool
