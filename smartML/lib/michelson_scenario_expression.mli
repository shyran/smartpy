(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type variable_id = int

type contract_id = int

type expr =
  | Constant           of Michelson.Literal.t
  | Secret_key         of string
  | Variable           of variable_id
  | Contract_data      of contract_id
  | Contract_balance   of contract_id
  | Make_signature     of
      { secret_key : expr
      ; message : expr }
  | Test_account       of expr
  | Account_secret_key of expr
  | Account_public_key of expr
  | Account_key_hash   of expr
  | Michelson          of Michelson.tinstr * expr list
