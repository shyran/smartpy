(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type t = {u : u} [@@deriving show {with_path = false}]

and u =
  | TUnit
  | TBool
  | TInt       of {isNat : bool unknown ref}
  | TTimestamp
  | TString
  | TBytes
  | TRecord    of (string * t) list
  | TVariant   of (string * t) list
  | TSet       of t
  | TMap       of
      { big : bool unknown ref
      ; key : t
      ; item : t }
  | TAddress
  | TContract  of t
  | TKeyHash
  | TKey
  | TSignature
  | TToken
  | TUnknown   of unknownType ref
  | TPair      of t * t
  | TAnnots    of string list * t
  | TList      of t
  | TLambda    of t * t
  | TSecretKey
[@@deriving show {with_path = false}]

and unknownType =
  | UUnknown of string
  | UItems   of t * t
  | URecord  of (string * t) list
  | UVariant of (string * t) list
  | UExact   of t
[@@deriving show {with_path = false}]

and tvariable = string * t [@@deriving show {with_path = false}]

and 'a unknown =
  | UnUnknown of string
  | UnValue   of 'a
  | UnRef     of 'a unknown ref [@deriving show]

let build u = {u}

let unknown_raw x = build (TUnknown x)

let fullUnknown i = unknown_raw (ref (UUnknown i))

let rec getRef r =
  match !r with
  | UnRef r -> getRef r
  | _ -> r

let rec getRepr t =
  match t.u with
  | TUnknown {contents = UExact t} -> getRepr t
  | u -> u

let unit = build TUnit

let address = build TAddress

let contract t = build (TContract t)

let bool = build TBool

let bytes = build TBytes

let variant l = build (TVariant l)

let key_hash = build TKeyHash

let int_raw ~isNat = build (TInt {isNat})

let int () = int_raw ~isNat:(ref (UnValue false))

let nat () = int_raw ~isNat:(ref (UnValue true))

let intOrNat () = int_raw ~isNat:(ref (UnUnknown ""))

let key = build TKey

let secret_key = build TSecretKey

let map big key item = build (TMap {big; key; item})

let set key = build (TSet key)

let record l = build (TRecord l)

let record_or_unit = function
  | [] -> unit
  | l -> record l

let signature = build TSignature

let option t = variant [("None", unit); ("Some", t)]

let tor t u = variant [("Left", t); ("Right", u)]

let string = build TString

let timestamp = build TTimestamp

let token = build TToken

let uvariant name t = unknown_raw (ref (UVariant [(name, t)]))

let account =
  record
    [ ("seed", string)
    ; ("address", address)
    ; ("public_key", key)
    ; ("public_key_hash", key_hash)
    ; ("secret_key", secret_key) ]

let pair t1 t2 = build (TPair (t1, t2))

let annots a t = build (TAnnots (a, t))

let list t = build (TList t)

let lambda t1 t2 = build (TLambda (t1, t2))
