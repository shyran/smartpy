(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

(** {1 Scenarios} *)

type account_or_address =
  | Account of Interpreter.Primitive_implementations.account
  | Address of string

type 'expr action =
  | New_contract of
      { id : int
      ; contract : 'expr tcontract_
      ; line_no : int }
  | Compute      of
      { id : int
      ; expression : 'expr
      ; line_no : int }
  | Simulation   of
      { id : int
      ; line_no : int }
  | Message      of
      { id : int
      ; valid : bool
      ; params : 'expr
      ; line_no : int
      ; title : string
      ; messageClass : string
      ; source : account_or_address option
      ; sender : account_or_address option
      ; time : int
      ; amount : 'expr
      ; message : string }
  | Error        of {message : string}
  | Html         of
      { tag : string
      ; inner : string
      ; line_no : int }
  | Verify       of
      { condition : 'expr
      ; line_no : int }
  | Show         of
      { expression : 'expr
      ; html : bool
      ; stripStrings : bool
      ; line_no : int }
  | Exception    of smart_except list

type t = {actions : texpr action list}
(** A scenario is a list of actions. *)

(** {1 Out-of-browser} *)

val run_scenario_filename :
     primitives:Interpreter.Primitive_implementations.t
  -> filename:string
  -> output_dir:string
  -> string
(** Load and execute the scenario out-of-browser. Returns a possibly
     empty string representing the error. *)

(** {1 In-browser} *)

val run_scenario_browser :
  primitives:Interpreter.Primitive_implementations.t -> scenario:string -> unit
(** Load and execute the scenario in-browser. *)

(** {1 Sandbox} *)

val load_from_string :
     primitives:Interpreter.Primitive_implementations.t
  -> scenario_state:scenario_state
  -> Yojson.Basic.t
  -> t
(** Load a scenario. *)

val contract_of_verification_texpr :
     primitives:Interpreter.Primitive_implementations.t
  -> scenario_state:scenario_state
  -> texpr
  -> texpr tcontract_
(** Generate a contract to check, on-chain, some condition. *)
