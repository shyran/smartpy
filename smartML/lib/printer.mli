(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

(** {1 Target Printer Options} *)

module Options : sig
  type t =
    { html : bool
    ; stripStrings : bool
    ; stripped : bool
    ; types : bool
    ; analysis : bool }

  val string : t

  val html : t

  val htmlStripStrings : t

  val types : t

  val analysis : t
end

(** {1 Types} *)

val type_to_string : ?options:Options.t -> Type.t -> string

(** {1 Values} *)

val html_of_data : Options.t -> tvalue -> string

val value_to_string :
  ?deep:bool -> ?noEmptyList:bool -> ?options:Options.t -> tvalue -> string

val literal_to_string : html:bool -> ?strip_strings:unit -> Literal.t -> string

val ppAmount : bool -> Big_int.big_int -> string

(** {1 Expressions, Commands, Contracts} *)

val expr_to_string : ?options:Options.t -> ?protect:unit -> texpr -> string

val variable_to_string :
  ?options:Options.t -> ?protect:unit -> string * Type.t -> vClass -> string

val command_to_string :
  ?indent:string -> ?options:Options.t -> tcommand -> string

val contract_to_string : ?options:Options.t -> tcontract -> string

val html_of_record_list :
  string list * 'a list list -> ('a -> string) -> string

(** {1 Effects} *)

val effect_to_string : ?options:Options.t -> Execution.effect -> string

(** {1 Exceptions} *)

val error_to_string : ?options:Options.t -> Execution.error -> string

val exception_to_string : bool -> exn -> string

val pp_smart_except : bool -> Basics.smart_except -> string
