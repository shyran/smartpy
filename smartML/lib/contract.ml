(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Interpreter

type t = tcontract [@@deriving show {with_path = false}]

let execMessageInner
    ~primitives
    ~scenario_state
    ~title
    ~execMessageClass
    ~context
    ~initContract
    ~channel
    ~params =
  let contract, effects, error, steps =
    interpret_message
      ~primitives
      ~scenario_state
      context
      initContract
      {channel; params}
  in
  let balance, contract =
    match contract with
    | None -> (initContract.balance, None)
    | Some (contract, _event) -> (contract.balance, Some contract)
  in
  let effects = List.map (Printer.effect_to_string ?options:None) effects in
  let rec pp_step (step : Execution.step) =
    let steps =
      match !(step.substeps) with
      | [] -> ""
      | steps ->
          Printf.sprintf
            "<h4>SubSteps</h4><div class='steps'>%s</div>"
            (String.concat "\n" (List.rev_map pp_step steps))
    in
    let locals =
      match step.locals with
      | [] -> ""
      | locals ->
          Printf.sprintf
            "<h4>Locals</h4> %s"
            (Printer.value_to_string
               ~options:Printer.Options.html
               (Value.record locals))
    in
    let iters =
      match step.iters with
      | [] -> ""
      | iters ->
          Printf.sprintf
            "<h4>Iterators</h4> %s"
            (Printer.value_to_string
               ~options:Printer.Options.html
               (Value.record (List.map (fun (n, (v, _)) -> (n, v)) iters)))
    in
    Printf.sprintf
      "<div class='steps'><h4>Command %s</h4><div \
       class='white-space-pre-wrap'>%s</div>%s%s%s<h4>Storage</h4>%s</div>"
      (Html.showLine step.command.line_no)
      (Printer.command_to_string step.command)
      steps
      locals
      iters
      (Printer.value_to_string ~options:Printer.Options.html step.storage)
  in
  let html ok output =
    Printf.sprintf
      "<div class='execMessage execMessage%s%s'>%s<h3>Transaction [%s] by \
       [%s] at time [timestamp(%i)] (<button class='text-button' \
       onClick='showLine(%i)'>line %i</button>)</h3>%s<h3>Balance: \
       %s</h3>%s</div>%s"
      ok
      execMessageClass
      title
      ok
      (context_sender context)
      (context_time context)
      (context_line_no context)
      (context_line_no context)
      (Printer.value_to_string
         ~options:Printer.Options.html
         (* !checkImport *)
         (Value.variant channel params (Type.fullUnknown "")))
      (Printer.ppAmount true balance)
      output
      ( if context_debug context
      then
        Printf.sprintf
          "<h3>Steps</h3><div>%s</div>"
          (String.concat "\n" (List.rev_map pp_step steps))
      else "" )
  in
  match contract with
  | Some {storage} ->
      let output =
        Printf.sprintf
          "<h3>Effects:</h3>%s<h3>Storage:</h3><div class='subtype'>%s</div>"
          (String.concat " " effects)
          (Printer.html_of_data Printer.Options.html storage)
      in
      { Execution.ok = true
      ; contract
      ; effects
      ; error
      ; html = html "OK" output
      ; storage
      ; steps }
  | None ->
      let errorMessage =
        match error with
        | Some error ->
            Printer.error_to_string
              ~options:Printer.Options.htmlStripStrings
              error
        | None -> ""
      in
      let output = Printf.sprintf "<h3>Error:</h3>%s" errorMessage in
      { ok = false
      ; contract
      ; effects
      ; error
      ; html = html "KO" output
      ; storage = Value.unit
      ; steps }
