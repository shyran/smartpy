(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type env =
  { tstorage : Type.t option
  ; entry_point : (string * Type.t) option
  ; entry_points : (string, Type.t) Hashtbl.t
  ; tglobal_params : Type.t option }

val early_env : unit -> env

val import_type : Typing.env -> env -> Sexplib0.Sexp.t -> Type.t

val import_literal : Typing.env -> env -> Sexplib0.Sexp.t list -> Literal.t

val import_expr : Typing.env -> env -> Sexplib0.Sexp.t -> texpr

val import_contract :
     primitives:Interpreter.Primitive_implementations.t
  -> scenario_state:scenario_state
  -> Typing.env
  -> Sexplib0.Sexp.t
  -> texpr tcontract_
(** Parse an s-expression into a contract definition. *)
