(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = string

type tab_ =
  { name : string
  ; tablink : string
  ; content : string }

type tab = int -> tab_

let tab ?active name inner id =
  let tablink =
    Printf.sprintf
      "<button name='button_generatedMichelson' class='tablinks%s' \
       onclick='openTab(event, %i)'>%s</button>"
      (if active <> None then " active" else "")
      id
      name
  in
  let content =
    Printf.sprintf
      "<div class='tabcontent' style='display: %s;'>%s</div>"
      (if active = None then "none" else "block")
      inner
  in
  {name; tablink; content}

let tabs title tabs =
  let tabs = List.mapi (fun i x -> x i) tabs in
  Printf.sprintf
    "<div class='tabs'><div class='tab'><span \
     class='title'>%s</span>%s</div>%s</div>"
    title
    (String.concat "" (List.map (fun x -> x.tablink) tabs))
    (String.concat "" (List.map (fun x -> x.content) tabs))

module HtmlCopyArea = struct
  let ids = ref 0

  let accResult = ref []

  let next () =
    incr ids;
    accResult := [];
    string_of_int !ids

  let push s = accResult := s :: !accResult

  let getResult ~id ?(className = "white-space-pre") name =
    let result = String.concat "\n" (List.rev !accResult) in
    accResult := [];
    let f = function
      | ' ' -> " "
      | '\n' -> "<br>"
      | c -> String.make 1 c
    in
    Printf.sprintf
      "<div class='menu'> <button \
       onClick='copyMichelson(this);'>Copy</button></div><div id='%s%s' \
       class='michelson %s'>%s</div>"
      name
      id
      className
      (Base.String.concat_map result ~f)

  let kpush = Printf.ksprintf push
end

let michelson_html smart_contract =
  let open Compiler in
  let open Michelson in
  let contract = michelson_contract smart_contract in
  let id = HtmlCopyArea.next () in
  let compiled_storage = compile_value smart_contract.storage in
  HtmlCopyArea.push (Literal.to_michelson_string compiled_storage);
  let initialStorageHtml =
    HtmlCopyArea.getResult ~id ~className:"white-space-pre-wrap" "storageCode"
  in
  HtmlCopyArea.kpush
    "parameter %s;"
    (string_of_mtype ~protect:() contract.parameter);
  HtmlCopyArea.kpush
    "storage &nbsp;&nbsp;%s;"
    (string_of_mtype ~protect:() contract.storage);
  HtmlCopyArea.kpush
    "<div class='michelsonLine'>code\n%s;</div>"
    (string_of_michCode ~html:true "&nbsp;&nbsp;" contract.code);
  let codeHtml = HtmlCopyArea.getResult ~id "contractCode" in
  HtmlCopyArea.kpush
    "<div class='white-space-pre'>%s</div>"
    (let mich = To_micheline.literal compiled_storage in
     Micheline.to_json_string mich);
  let initialStorageHtmlJson = HtmlCopyArea.getResult ~id "storageCodeJson" in
  let codeHtmlJson =
    HtmlCopyArea.kpush
      "<div class='white-space-pre'>%s</div>"
      ( try
          let mich = Michelson_contract.to_micheline contract in
          Micheline.to_json_string mich
        with
      | e ->
          Format.asprintf
            "Error: %s\n%s"
            (Printexc.to_string e)
            (Printexc.get_backtrace ()) );
    HtmlCopyArea.getResult ~id "contractCodeJson"
  in
  let tabs =
    tabs
      "Generated Michelson:"
      [ tab
          ~active:()
          "Storage"
          (Printf.sprintf
             "<div><h2>Initial Storage</h2><div>%s</div></div>"
             initialStorageHtml)
      ; tab
          "Code"
          (Printf.sprintf
             "<div><h2>Michelson Code</h2><div>%s</div></div>"
             codeHtml)
      ; tab
          "Storage JSON"
          (Printf.sprintf
             "<div><h2>Initial Storage</h2><div>%s</div></div>"
             initialStorageHtmlJson)
      ; tab
          "Code JSON"
          (Printf.sprintf
             "<div><h2>JSON Representation</h2><div>%s</div></div>"
             codeHtmlJson) ]
  in
  let michelson =
    Printf.sprintf
      "<button class='centertextbutton extramarginbottom' \
       onClick='gotoOrigination(storageCode%s.innerText, \
       contractCode%s.innerText, storageCodeJson%s.innerText, \
       contractCodeJson%s.innerText,storageCode%s.innerHTML, \
       contractCode%s.innerHTML, storageCodeJson%s.innerHTML, \
       contractCodeJson%s.innerHTML)'>Deploy contract</button>%s"
      id
      id
      id
      id
      id
      id
      id
      id
      tabs
  in
  (michelson, Michelson_contract.has_error contract)

let showLine line_no =
  Printf.sprintf
    "<button class=\"text-button\" onClick='showLine(%i)'>(line %i)</button>"
    line_no
    line_no

let full_html ~contract ~def ~onlyDefault ~line_no =
  let myName = "Contract" in
  let tab title content =
    tab ?active:(if def = title then Some () else None) title content
  in
  let michelson, has_error = michelson_html contract in
  let myTabs =
    [ tab
        "SmartPy"
        ( ( if contract.partialContract
          then
            "<span class='partialType'>Warning: unknown types or type \
             errors.</span><br>"
          else "" )
        ^ ( if has_error
          then
            "<span class='partialType'>Warning: errors in the Michelson \
             generated code.</span><br>"
          else "" )
        ^ Printer.contract_to_string ~options:Printer.Options.html contract
        ^ showLine line_no )
    ; tab
        "Storage"
        (Printf.sprintf
           "<div class='contract'>%s</div>"
           (Printer.html_of_data Printer.Options.html contract.storage))
    ; tab
        "Types"
        (Printf.sprintf
           "<div class='contract'><h3>Storage:</h3>%s<p><h3>Entry \
            points</h3>%s</div>"
           (Printer.html_of_data Printer.Options.types contract.storage)
           (Printer.type_to_string
              ~options:Printer.Options.types
              (Type.variant
                 (List.map
                    (fun {channel; paramsType} -> (channel, paramsType))
                    contract.entry_points))))
    ; tab
        "All"
        (Printer.contract_to_string ~options:Printer.Options.analysis contract)
    ; tab "Michelson" michelson
    ; tab "X" "" ]
  in
  if onlyDefault
  then
    Printf.sprintf
      "\n<div class='tabs'>%s</div>"
      (String.concat
         ""
         (List.mapi
            (fun i tab ->
              let tab = tab i in
              if tab.name = def
              then
                Printf.sprintf
                  "<div class='tabcontentshow'>%s</div>"
                  tab.content
              else "")
            myTabs))
  else tabs myName myTabs

let nextInputGuiId = Value.nextId "inputGui."

let nextOutputGuiId = Value.nextId "outputGui."

let nextLazyOutputGuiId = Value.nextId "lazyOutputGui."

let contextSimulationType =
  let t1 =
    Type.record_or_unit
      [ ("sender", Type.string)
      ; ("source", Type.string)
      ; ("timestamp", Type.string)
      ; ("amount", Type.variant [("Tez", Type.string); ("Mutez", Type.string)])
      ]
  in
  let t2 = Type.record [("debug", Type.bool); ("full_output", Type.bool)] in
  Type.record [("context", t1); ("simulation", t2)]

let inputGui t buttonText ~line_no =
  let output = nextOutputGuiId () in
  let id = nextInputGuiId () ^ "." in
  let nextId = Value.nextId id in
  let input = Value_gui.inputGuiR ~nextId t in
  let contextInput = Value_gui.inputGuiR ~nextId contextSimulationType in
  Printf.sprintf
    "<div class='simulationBuilder'><form><h2>Simulation Builder</h2>%s%s\n\
     <br><button type='button' onClick=\"var t = \
     smartmlCtx.call_exn_handler('importType', '%s');if (t) \
     smartmlCtx.call_exn_handler('callGui', '%s', '%s', t, \
     %i)\">%s</button></form>\n\
     <div id='%s'></div></div>"
    contextInput.gui
    input.gui
    (Export.export_type t)
    id
    output
    line_no
    buttonText
    output

let delayedInputGui t =
  let output = nextLazyOutputGuiId () in
  let id = nextInputGuiId () ^ "." in
  Printf.sprintf
    "<button type='button' \
     onClick=\"smartmlCtx.call_exn_handler('addInputGui', '%s', '%s', \
     smartmlCtx.call_exn_handler('importType', '%s'))\">%s</button>\n\
     <div id='%s'></div>"
    id
    output
    (Export.export_type t)
    "Add Another Step"
    output

let simulatedContract = ref None

let simulation c ~line_no =
  simulatedContract := Some c;
  let t =
    Type.variant
      (List.map
         (fun {channel; paramsType} -> (channel, paramsType))
         c.entry_points)
  in
  inputGui t "Simulation" ~line_no
