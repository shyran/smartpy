(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils

(** See also
    {{:https://tezos.gitlab.io/master/whitedoc/michelson.html#domain-specific-data-types}Domain
    Specific Data Types}. *)

type 'm mtype_f = private
  | MTunit
  | MTbool
  | MTnat
  | MTint
  | MTmutez
  | MTstring
  | MTbytes
  | MTtimestamp
  | MTaddress
  | MTkey
  | MTkey_hash
  | MTsignature
  | MToperation
  | MToption    of 'm
  | MTlist      of 'm
  | MTset       of 'm
  | MTcontract  of 'm
  | MTpair      of 'm * 'm
  | MTor        of 'm * 'm
  | MTlambda    of 'm * 'm
  | MTmap       of 'm * 'm
  | MTbig_map   of 'm * 'm
  | MTannot     of string * 'm * bool (* explicit ? *)
  | MTmissing   of string
[@@deriving eq, ord, show]

type mtype = {mt : mtype mtype_f} [@@deriving eq, ord, show]

val cata_mtype : ('a mtype_f -> 'a) -> mtype -> 'a

val mt_unit : mtype

val mt_bool : mtype

val mt_nat : mtype

val mt_int : mtype

val mt_mutez : mtype

val mt_string : mtype

val mt_bytes : mtype

val mt_timestamp : mtype

val mt_address : mtype

val mt_key : mtype

val mt_key_hash : mtype

val mt_signature : mtype

val mt_operation : mtype

val mt_option : mtype -> mtype

val mt_list : mtype -> mtype

val mt_set : mtype -> mtype

val mt_contract : mtype -> mtype

val mt_pair : mtype -> mtype -> mtype

val mt_or : mtype -> mtype -> mtype

val mt_lambda : mtype -> mtype -> mtype

val mt_map : mtype -> mtype -> mtype

val mt_big_map : mtype -> mtype -> mtype

val mt_annot : string -> mtype -> bool -> mtype

val mt_missing : string -> mtype

val remove_annots : mtype -> mtype

type ad_step =
  | A
  | D
[@@deriving eq, show]

module Literal : sig
  type tezos_int = Big_int.big_int

  type t =
    | Nat    of tezos_int
    | Int    of tezos_int
    | Bool   of bool
    | String of string
    | Bytes  of string
    | Unit
    | Mutez  of tezos_int
    | Pair   of t * t
    | None   of mtype
    | Left   of t
    | Right  of t
    | Some   of t
    | Set    of {elements : t list}
    | Map    of {elements : (t * t) list}
    | List   of
        { t : mtype
        ; elements : t list }
  [@@deriving eq, ord, show]

  val int : int -> t

  val nat : int -> t

  val to_michelson_string : t -> string
  (** Convert a literal into a michelson-syntax literal. *)
end

type target =
  | T_params
  | T_storage
  | T_operations
  | T_lambda_parameter
  | T_local            of string
  | T_iter             of string
  | T_variant_arg      of string
[@@deriving eq, show]

type stack_tag =
  | ST_none
  | ST_target of target
  | ST_pair   of stack_tag * stack_tag
[@@deriving eq, show]

type stack_element =
  { se_type : mtype
  ; se_tag : stack_tag }
[@@deriving eq, show]

type stack =
  | Stack_ok     of stack_element list
  | Stack_failed
[@@deriving eq, show]

type 'i instr_f =
  | MIerror            of string
  | MIcomment          of string
  | MImich             of mtype Basics.inline_michelson
  | MIdip              of 'i
  | MIdipn             of int * 'i
  | MIloop             of 'i
  | MIiter             of 'i
  | MImap              of 'i
  | MIdrop
  | MIdup
  | MIdig              of int
  | MIdug              of int
  | MIfailwith
  | MIif               of 'i * 'i
  | MIif_left          of 'i * 'i
  | MIif_some          of 'i * 'i
  | MInil              of mtype
  | MIcons
  | MInone             of mtype
  | MIsome
  | MIpair
  | MIleft             of mtype
  | MIright            of mtype
  | MIpush             of mtype * Literal.t
  | MIseq              of 'i list
  | MIswap
  | MIunpair
  | MIfield            of ad_step list
  | MIsetField         of ad_step list
  | MIcontract         of string option * mtype
  | MIexec
  | MIlambda           of mtype * mtype * 'i
  | MIself
  | MIaddress
  | MIimplicit_account
  | MItransfer_tokens
  | MIcheck_signature
  | MIset_delegate
  | MIeq
  | MIneq
  | MIle
  | MIlt
  | MIge
  | MIgt
  | MIcompare
  | MImul
  | MIadd
  | MIsub
  | MIediv
  | MInot
  | MIand
  | MIor
  | MIconcat
  | MIslice
  | MIsize
  | MIget
  | MIupdate
  | MIsender
  | MIsource
  | MIamount
  | MIbalance
  | MInow
  | MImem
  | MIhash_key
  | MIblake2b
  | MIsha256
  | MIsha512
  | MIabs
  | MIneg
  | MIint
  | MIisnat
  | MIpack
  | MIunpack           of mtype
[@@deriving eq, show, map, fold]

type instr = {instr : instr instr_f} [@@deriving eq, show]

type tinstr =
  { tinstr : tinstr instr_f
  ; stack : stack Result.t }
[@@deriving eq, show]

val pp_instr_rectypes : Format.formatter -> instr -> unit

val show_instr_rectypes : instr -> string

val instr_size : instr -> int

val strip_annots : mtype -> mtype

val is_commutative : 'a instr_f -> bool

val unify_stacks : stack -> stack -> stack option

val ty_if_in : stack_element list -> stack Result.t

val ty_if_some_in1 : stack_element list -> stack Result.t

val ty_if_some_in2 : stack_element list -> stack Result.t

val ty_if_left_in1 : stack_element list -> stack Result.t

val ty_if_left_in2 : stack_element list -> stack Result.t

val ty_iter_in : stack_element list -> stack Result.t

val ty_map_in : stack_element list -> stack Result.t

val ty_loop_in : stack_element list -> stack Result.t

val ty_iter_out : stack -> stack_element list -> stack Result.t

val ty_map_out : stack -> stack_element list -> stack Result.t

val ty_loop_out : stack -> stack_element list -> stack Result.t

val typecheck : mtype -> stack -> instr -> tinstr

val forget_types : tinstr -> instr

(* String conversion *)
val string_of_mtype :
  ?full:unit -> ?protect:unit -> ?annot:string -> mtype -> string

val string_of_michCode :
  html:bool -> ?sub_sequence:unit -> string -> tinstr -> string

val string_of_stack_tag : ?protect:unit -> stack_tag -> string

val string_of_stack_element : ?full:unit -> stack_element -> string

val string_of_ok_stack : ?full:unit -> stack_element list -> string

val string_of_stack : ?full:unit -> stack -> string

val string_of_ad_path : ad_step list -> string

module To_micheline : sig
  val literal : Literal.t -> Micheline.t

  val instruction : tinstr -> Micheline.t list
end

(** A complete Michelson contract (equivalent to the contents of a
   [".tz"] file). *)
module Michelson_contract : sig
  type t = private
    { storage : mtype
    ; parameter : mtype
    ; code : tinstr }
  [@@deriving show]

  val make : ?storage:mtype -> ?parameter:mtype -> tinstr -> t

  val to_micheline : t -> Micheline.t

  val has_error : t -> bool
end

module Traversable (A : Base.Applicative.S) : sig
  val traverse : ('a -> 'b A.t) -> 'a instr_f -> 'b instr_f A.t
end

val cata_instr : ('a instr_f -> 'a) -> instr -> 'a
