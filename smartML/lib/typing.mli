(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Type

type env =
  { unknowns : (string, Type.t) Hashtbl.t
  ; constraints : importConstraint list ref
  ; variant_args : (string, Type.t) Hashtbl.t
  ; contract_data_types : (int, Type.t) Hashtbl.t }

val init_env : contract_data_types:(int, Type.t) Hashtbl.t -> env

val unknown : env -> string -> t

val intType : bool Type.unknown ref -> [> `Int | `Nat | `Unknown ]

val for_variant : env -> string -> t -> t

val key_value : t -> t -> t

val assertEqual : pp:(unit -> smart_except list) -> t -> t -> unit

val checkGetItem : (unit -> smart_except list) -> t -> t -> bool

val checkContains : (unit -> smart_except list) -> t -> t -> bool
