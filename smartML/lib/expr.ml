(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Typing

type t = texpr [@@deriving show {with_path = false}]

let build ~line_no e et = {e; el = line_no; et}

let ppBin ~line_no op x y () = [`Text op; `Exprs [x; y]; `Line line_no]

let checkComparable ~line_no env x y =
  let t1 = x.et in
  let t2 = y.et in
  let pp = ppBin ~line_no "comparison between" x y in
  assertEqual t1 t2 ~pp;
  env.constraints := IsComparable x :: IsComparable y :: !(env.constraints)

let checkBool ~line_no op x y =
  let pp = ppBin ~line_no op x y in
  assertEqual x.et y.et ~pp;
  assertEqual x.et Type.bool ~pp

let add ~line_no env x y =
  let pp = ppBin ~line_no "add" x y in
  assertEqual x.et y.et ~pp;
  let e = build ~line_no (EBinOpInf (BAdd, x, y)) x.et in
  env.constraints := HasAdd (e, x, y) :: !(env.constraints);
  e

let mul ~line_no _env x y =
  let pp = ppBin ~line_no "multiply" x y in
  assertEqual x.et y.et ~pp;
  assertEqual x.et (Type.intOrNat ()) ~pp;
  build ~line_no (EBinOpInf (BMul, x, y)) x.et

let e_mod ~line_no x y =
  let pp = ppBin ~line_no "mod" x y in
  assertEqual x.et y.et ~pp;
  assertEqual x.et (Type.intOrNat ()) ~pp;
  build ~line_no (EBinOpInf (BMod, x, y)) (Type.nat ())

let ediv ~line_no x y =
  let pp () =
    [ `Text "divide"
    ; `Exprs [x; y]
    ; `Br
    ; `Text "They must be of type nat."
    ; `Line line_no ]
  in
  assertEqual x.et y.et ~pp;
  assertEqual x.et (Type.intOrNat ()) ~pp;
  build
    ~line_no
    (EBinOpInf (BEDiv, x, y))
    (Type.option (Type.record [("a", x.et); ("b", Type.nat ())]))

let div ~line_no x y =
  let pp () =
    [ `Text "divide"
    ; `Exprs [x; y]
    ; `Br
    ; `Text "They must be of type"
    ; `Type (Type.nat ())
    ; `Line line_no ]
  in
  assertEqual x.et y.et ~pp;
  assertEqual x.et (Type.nat ()) ~pp;
  build ~line_no (EBinOpInf (BDiv, x, y)) (Type.nat ())

let sub ~line_no env x y =
  let pp = ppBin ~line_no "subtract" x y in
  assertEqual x.et y.et ~pp;
  let t = unknown env "" in
  let e = build ~line_no (EBinOpInf (BSub, x, y)) t in
  env.constraints := HasSub (e, x, y) :: !(env.constraints);
  e

let le ~line_no env x y =
  checkComparable ~line_no env x y;
  build ~line_no (EBinOpInf (BLe, x, y)) Type.bool

let lt ~line_no env x y =
  checkComparable ~line_no env x y;
  build ~line_no (EBinOpInf (BLt, x, y)) Type.bool

let ge ~line_no env x y =
  checkComparable ~line_no env x y;
  build ~line_no (EBinOpInf (BGe, x, y)) Type.bool

let gt ~line_no env x y =
  checkComparable ~line_no env x y;
  build ~line_no (EBinOpInf (BGt, x, y)) Type.bool

let eq ~line_no env x y =
  checkComparable ~line_no env x y;
  build ~line_no (EBinOpInf (BEq, x, y)) Type.bool

let neq ~line_no env x y =
  checkComparable ~line_no env x y;
  build ~line_no (EBinOpInf (BNeq, x, y)) Type.bool

let b_or ~line_no x y =
  checkBool ~line_no "|" x y;
  build ~line_no (EBinOpInf (BOr, x, y)) Type.bool

let b_and ~line_no x y =
  checkBool ~line_no "&" x y;
  build ~line_no (EBinOpInf (BAnd, x, y)) Type.bool

let e_max ~line_no x y =
  let pp = ppBin ~line_no "max" x y in
  assertEqual x.et y.et ~pp;
  assertEqual x.et (Type.intOrNat ()) ~pp;
  build ~line_no (EBinOpPre (BMax, x, y)) x.et

let e_min ~line_no x y =
  let pp = ppBin ~line_no "min" x y in
  assertEqual x.et y.et ~pp;
  assertEqual x.et (Type.intOrNat ()) ~pp;
  build ~line_no (EBinOpPre (BMin, x, y)) x.et

let storage t = build EStorage t

let attr ~line_no x name t =
  assertEqual
    x.et
    (Type.unknown_raw (ref (Type.URecord [(name, t)])))
    ~pp:(fun () ->
      [ `Text "Attribute error"
      ; `Expr x
      ; `Text "has no field"
      ; `Text name
      ; `Line line_no ]);
  build ~line_no (EAttr (name, x)) t

let variant name x tvariant = build (EVariant (name, x)) tvariant

let uvariant env name x =
  build (EVariant (name, x)) (for_variant env name x.et)

let isVariant ~line_no env name x =
  let tvariant =
    Type.unknown_raw (ref (Type.UVariant [(name, unknown env "")]))
  in
  assertEqual x.et tvariant ~pp:(fun () ->
      [ `Text "Incompatible variant types"
      ; `Br
      ; `Type x.et
      ; `Br
      ; `Text "and"
      ; `Br
      ; `Type tvariant
      ; `Line line_no ]);
  build ~line_no (EIsVariant (name, x)) Type.bool

let variant_arg ~line_no env arg_name =
  match Hashtbl.find_opt env.variant_args arg_name with
  | None -> failwith "unbound variant arg"
  | Some t -> build ~line_no (EVariant_arg arg_name) t

let openVariant ~line_no env x name =
  let t = unknown env "" in
  let tv =
    match name with
    | "Some" -> Type.option t
    | _ -> Type.unknown_raw (ref (Type.UVariant [(name, t)]))
  in
  assertEqual x.et tv ~pp:(fun () ->
      [ `Text "Variant error"
      ; `Expr x
      ; `Text "has no constructor"
      ; `Text name
      ; `Line line_no ]);
  build ~line_no (EOpenVariant (name, x)) t

let updateMap map key value = build (EUpdate_map (map, key, value)) map.et

let params t = build (EParams t) t

let local n t = build (ELocal (n, t)) t

(* Concrete Python syntax: items[member] *)
let item ~line_no env items key defaultValue : texpr =
  (* typing of default value *)
  env.constraints := HasGetItem (items, key) :: !(env.constraints);
  let tvalue = unknown env "" in
  assertEqual
    items.et
    (Type.unknown_raw (ref (Type.UItems (key.et, tvalue))))
    ~pp:(fun () ->
      match Type.getRepr items.et with
      | TList _ ->
          [ `Expr items
          ; `Text "is a list, not a map."
          ; `Br
          ; `Text
              (Printf.sprintf
                 " You can use:\n\
                 \ - sp.vector(..) to create a map from a list,\n\
                 \ - sp.matrix(..) to create a map of maps from a list of \
                  lists,\n\
                 \ - sp.cube(..) for a list of lists of lists.")
          ; `Line line_no ]
      | _ -> [`Expr items; `Text "is not a map."; `Br; `Line line_no]);
  build ~line_no (EItem (items, key, defaultValue)) tvalue

(* Concrete Python syntax: member in items *)
let contains ~line_no env (items : texpr) (member : texpr) =
  ( match env with
  | None -> ()
  | Some env ->
      env.constraints := HasContains (items, member) :: !(env.constraints) );
  ( match Type.getRepr items.et with
  | TMap titems ->
      assertEqual member.et titems.key ~pp:(fun () ->
          [ `Expr items
          ; `Text ".contains("
          ; `Expr member
          ; `Text ")"
          ; `Line line_no ])
  | TSet telements ->
      assertEqual member.et telements ~pp:(fun () ->
          [ `Expr items
          ; `Text ".contains("
          ; `Expr member
          ; `Text ")"
          ; `Line line_no ])
  | TUnknown {contents = _} -> ()
  | _ ->
      raise
        (SmartExcept
           [ `Text "Type Error"
           ; `Br
           ; `Expr items
           ; `Text ".contains(..) is undefined."
           ; `Br
           ; `Text "it is only defined on sets and maps"
           ; `Line line_no ]) );
  build ~line_no (EContains (items, member)) Type.bool

let sum ~line_no l =
  let t = Type.intOrNat () in
  assertEqual l.et (Type.list t) ~pp:(fun () ->
      [`Text "sum"; `Expr l; `Line line_no]);
  build ~line_no (ESum l) t

let range ~line_no a b step =
  let pp () =
    [ `Text "Range takes integers, it cannot be applied to"
    ; `Exprs [a; b; step]
    ; `Line line_no ]
  in
  assertEqual a.et b.et ~pp;
  assertEqual a.et step.et ~pp;
  match (Type.getRepr a.et, Type.getRepr b.et, Type.getRepr step.et) with
  | TInt _, TInt _, TInt _ ->
      build ~line_no (ERange (a, b, step)) (Type.list a.et)
  | _ -> raise (SmartExcept (pp ()))

let cons ~line_no x l =
  assertEqual l.et (Type.list x.et) ~pp:(fun () ->
      [`Text "cannot cons"; `Exprs [x; l]; `Line line_no]);
  build ~line_no (ECons (x, l)) l.et

let cst x = build (ECst x) x.t

let unit = cst ~line_no:(-1) Literal.unit

let record ~line_no (l : (_ * _) list) =
  let check_field name t e =
    assertEqual t e.et ~pp:(fun () ->
        [ `Text "record field type mismatch"
        ; `Text name
        ; `Text ":"
        ; `Type t
        ; `Text "and"
        ; `Expr e
        ; `Line line_no ])
  in
  build
    ~line_no
    (ERecord
       (List.map
          (fun ((name, t), e) ->
            check_field name t e;
            (name, e))
          l))
    (Type.record_or_unit (List.map fst l))

let build_list ~line_no ~tvalue ~elems =
  let item k =
    assertEqual k.et tvalue ~pp:(fun () ->
        [ `Text "bad type for list item"
        ; `Expr k
        ; `Text "is not a"
        ; `Type tvalue
        ; `Line line_no ]);
    k
  in
  build ~line_no (EList (List.map item elems)) (Type.list tvalue)

let build_map ~line_no ~big ~tkey ~tvalue ~entries =
  let item (k, v) =
    assertEqual k.et tkey ~pp:(fun () ->
        [ `Text "bad type for map key"
        ; `Expr k
        ; `Text "is not a"
        ; `Type tkey
        ; `Line line_no ]);
    assertEqual v.et tvalue ~pp:(fun () ->
        [ `Text "bad type for map value"
        ; `Expr v
        ; `Text "is not a"
        ; `Type tvalue
        ; `Line line_no ]);
    (k, v)
  in
  build ~line_no (EMap (List.map item entries)) (Type.map big tkey tvalue)

let build_set ~line_no ~telement ~entries =
  let check k =
    assertEqual k.et telement ~pp:(fun () ->
        [ `Text "bad type for set element"
        ; `Expr k
        ; `Text "is not a"
        ; `Type telement
        ; `Line line_no ])
  in
  List.iter check entries;
  build ~line_no (ESet entries) (Type.set telement)

let intXor ~line_no e1 e2 =
  let pp () =
    [ `Text "bad type for xor"
    ; `Expr e1
    ; `Text "or"
    ; `Expr e2
    ; `Text "is not an integer"
    ; `Line line_no ]
  in
  assertEqual e1.et (Type.int ()) ~pp;
  assertEqual e2.et (Type.int ()) ~pp;
  assertEqual e1.et e2.et ~pp;
  build ~line_no (EInt_x_or (e1, e2)) (Type.int ())

let hash_key ~line_no e =
  assertEqual Type.key e.et ~pp:(fun () ->
      [`Text "sp.hash_key("; `Expr e; `Text ")"; `Line line_no]);
  build ~line_no (EHash_key e) Type.key_hash

let hashCrypto ~line_no algo e =
  assertEqual Type.bytes e.et ~pp:(fun () ->
      [ `Text
          (Printf.sprintf
             "sp.%s("
             (String.lowercase_ascii (string_of_hash_algo algo)))
      ; `Expr e
      ; `Text ")"
      ; `Line line_no ]);
  build ~line_no (EHash (algo, e)) Type.bytes

let pack e = build (EPack e) Type.bytes

let unpack ~line_no e t =
  let pp () = [`Text "unpack"; `Expr e; `Type t; `Line line_no] in
  assertEqual e.et Type.bytes ~pp;
  build ~line_no (EUnpack (e, t)) (Type.option t)

let check_signature ~line_no pk signature message =
  let pp e () = [`Text "sp.check_signature"; `Expr e; `Line line_no] in
  assertEqual Type.key pk.et ~pp:(pp pk);
  assertEqual Type.signature signature.et ~pp:(pp signature);
  assertEqual Type.bytes message.et ~pp:(pp message);
  build ~line_no (ECheck_signature (pk, signature, message)) Type.bool

let account_of_seed ~seed ~line_no =
  build ~line_no (EAccount_of_seed {seed}) Type.account

let make_signature ~secret_key ~message ~message_format ~line_no =
  let pp e () = [`Text "sp.make_signature"; `Expr e; `Line line_no] in
  assertEqual Type.secret_key secret_key.et ~pp:(pp secret_key);
  assertEqual Type.bytes message.et ~pp:(pp message);
  build
    ~line_no
    (EMake_signature {secret_key; message; message_format})
    Type.signature

let scenario_var ~line_no id t = build ~line_no (EScenario_var id) t

let reduce ~line_no e = build ~line_no (EReduce e) e.et

let split_tokens ~line_no mutez quantity total =
  let pp () =
    [ `Text "sp.split_tokens("
    ; `Exprs [mutez; quantity; total]
    ; `Text ")"
    ; `Line line_no ]
  in
  assertEqual Type.token mutez.et ~pp;
  assertEqual (Type.nat ()) quantity.et ~pp;
  assertEqual (Type.nat ()) total.et ~pp;
  build ~line_no (ESplit_tokens (mutez, quantity, total)) Type.token

let now = build ~line_no:(-1) ENow Type.timestamp

let add_seconds ~line_no t s =
  let pp () =
    [`Text "sp.add_seconds("; `Exprs [t; s]; `Text ")"; `Line line_no]
  in
  assertEqual Type.timestamp t.et ~pp;
  assertEqual (Type.int ()) s.et ~pp;
  build ~line_no (EAdd_seconds (t, s)) Type.timestamp

let notE ~line_no x =
  assertEqual x.et Type.bool ~pp:(fun () ->
      [`Text "not"; `Expr x; `Line line_no]);
  build ~line_no (ENot x) Type.bool

let absE ~line_no x =
  assertEqual x.et (Type.int ()) ~pp:(fun () ->
      [`Text "abs"; `Expr x; `Line line_no]);
  build ~line_no (EAbs x) (Type.nat ())

let to_int ~line_no x =
  assertEqual x.et (Type.nat ()) ~pp:(fun () ->
      [`Text "sp.toInt"; `Expr x; `Line line_no]);
  build ~line_no (EToInt x) (Type.int ())

let is_nat ~line_no x =
  assertEqual x.et (Type.int ()) ~pp:(fun () ->
      [`Text "sp.isNat"; `Expr x; `Line line_no]);
  build ~line_no (EIsNat x) (Type.option (Type.nat ()))

let negE ~line_no x =
  assertEqual x.et (Type.intOrNat ()) ~pp:(fun () ->
      [`Text "-"; `Expr x; `Line line_no]);
  build ~line_no (ENeg x) (Type.int ())

let signE ~line_no x =
  assertEqual x.et (Type.int ()) ~pp:(fun () ->
      [`Text "sp.sign"; `Expr x; `Line line_no]);
  build ~line_no (ESign x) (Type.int ())

let slice ~line_no typing_env ~offset ~length ~buffer =
  let pp expr () = [`Text "sp.slice"; `Expr expr; `Line line_no] in
  assertEqual offset.et (Type.nat ()) ~pp:(pp offset);
  assertEqual length.et (Type.nat ()) ~pp:(pp length);
  typing_env.constraints := HasSlice buffer :: !(typing_env.constraints);
  build ~line_no (ESlice {offset; length; buffer}) (Type.option buffer.et)

let concat_list ~line_no typing_env l =
  let t = unknown typing_env "" in
  let pp expr () = [`Text "sp.concat"; `Expr expr; `Line line_no] in
  assertEqual l.et (Type.list t) ~pp:(pp l);
  build ~line_no (EConcat_list l) t

let size ~line_no env s =
  env.constraints := HasSize s :: !(env.constraints);
  build ~line_no (ESize s) (Type.nat ())

let iterator ~line_no name t = build ~line_no (EIter (name, t)) t

let balance = build ~line_no:(-1) EBalance Type.token

let sender = build ~line_no:(-1) ESender Type.address

let source = build ~line_no:(-1) ESource Type.address

let amount = build ~line_no:(-1) EAmount Type.token

let self t = build ~line_no:(-1) ESelf (Type.contract t)

let self_entry_point name t =
  build ~line_no:(-1) (ESelf_entry_point (name, t)) (Type.contract t)

let contract_address ~line_no env e =
  assertEqual
    e.et
    (Type.contract (unknown env ""))
    ~pp:(fun () -> [`Text "sp.to_address("; `Expr e; `Text ")"; `Line line_no]);
  build ~line_no (EContract_address e) Type.address

let implicit_account ~line_no e =
  assertEqual e.et Type.key_hash ~pp:(fun () ->
      [`Text "sp.implicit_account("; `Expr e; `Text ")"; `Line line_no]);
  build ~line_no (EImplicit_account e) (Type.contract Type.unit)

let listRev ~line_no env e =
  let t = unknown env "" in
  assertEqual e.et (Type.list t) ~pp:(fun () ->
      [`Expr e; `Text ".rev()"; `Line line_no]);
  build ~line_no (EListRev e) (Type.list t)

let listItems ~line_no env e rev =
  let tkey = unknown env "" in
  let tvalue = unknown env "" in
  assertEqual
    e.et
    (Type.map (ref (Type.UnValue false)) tkey tvalue)
    ~pp:(fun () -> [`Expr e; `Text ".items()"; `Line line_no]);
  build ~line_no (EListItems (e, rev)) (Type.list (key_value tkey tvalue))

let listKeys ~line_no env e rev =
  let tkey = unknown env "" in
  let tvalue = unknown env "" in
  assertEqual
    e.et
    (Type.map (ref (Type.UnValue false)) tkey tvalue)
    ~pp:(fun () -> [`Expr e; `Text ".keys()"; `Line line_no]);
  build ~line_no (EListKeys (e, rev)) (Type.list tkey)

let listValues ~line_no env e rev =
  let tkey = unknown env "" in
  let tvalue = unknown env "" in
  assertEqual
    e.et
    (Type.map (ref (Type.UnValue false)) tkey tvalue)
    ~pp:(fun () -> [`Expr e; `Text ".values()"; `Line line_no]);
  build ~line_no (EListValues (e, rev)) (Type.list tvalue)

let listElements ~line_no env e rev =
  let telement = unknown env "" in
  assertEqual e.et (Type.set telement) ~pp:(fun () ->
      [`Expr e; `Text ".elements()"; `Line line_no]);
  build ~line_no (EListElements (e, rev)) (Type.list telement)

let contract ~line_no entry_point arg_type address =
  assertEqual address.et Type.address ~pp:(fun () -> [`Expr address]);
  build
    ~line_no
    (EContract {entry_point; arg_type; address})
    (Type.option (Type.contract arg_type))

let pair ~line_no e1 e2 =
  build ~line_no (EPair (e1, e2)) (Type.pair e1.et e2.et)

let first ~line_no env e =
  let t1 = unknown env "" in
  let t2 = unknown env "" in
  assertEqual e.et (Type.pair t1 t2) ~pp:(fun () ->
      [`Expr e; `Text "is not a pair"; `Line line_no]);
  build ~line_no (EFirst e) t1

let second ~line_no env e =
  let t1 = unknown env "" in
  let t2 = unknown env "" in
  assertEqual e.et (Type.pair t1 t2) ~pp:(fun () ->
      [`Expr e; `Text "is not a pair"; `Line line_no]);
  build ~line_no (ESecond e) t2

let inline_michelson ~line_no michelson exprs =
  let pp () = [`Text "mi.call"; `Exprs exprs; `Line line_no] in
  let rec matchTypes types name tin out =
    match (types, tin) with
    | _, [] -> out @ types
    | [], _ ->
        raise
          (SmartExcept
             [ `Text "Empty stack at instruction"
             ; `Text name
             ; `Rec (pp ())
             ; `Line line_no ])
    | tt1 :: types, tt2 :: tin ->
        assertEqual tt1 tt2 ~pp;
        matchTypes types name tin out
  in
  let typesOut =
    List.fold_left
      (fun types m -> matchTypes types m.name m.typesIn m.typesOut)
      (List.map (fun e -> e.et) exprs)
      michelson
  in
  match typesOut with
  | [] ->
      raise
        (SmartExcept [`Text "Empty output types"; `Rec (pp ()); `Line line_no])
  | [t] -> build ~line_no (EMichelson (michelson, exprs)) t
  | _ ->
      raise
        (SmartExcept
           [`Text "Too many output types"; `Rec (pp ()); `Line line_no])

let call_lambda ~line_no env lambda parameter =
  let t = unknown env "" in
  assertEqual lambda.et (Type.lambda parameter.et t) ~pp:(fun () ->
      [`Expr lambda; `Text "does not apply to"; `Expr parameter; `Line line_no]);
  build ~line_no (ECallLambda (lambda, parameter)) t

let lambda ~line_no id name tParams tResult command =
  build
    ~line_no
    (ELambda {id; name; tParams; command; tResult})
    (Type.lambda tParams tResult)

let lambdaParams ~line_no id name tParams =
  build ~line_no (ELambdaParams {id; name; tParams}) tParams

let rec of_value v : texpr =
  match v.v with
  | Literal l -> cst ~line_no:(-1) l
  | Record entries ->
      build
        ~line_no:(-1)
        (ERecord (entries |> List.map (fun (fld, l) -> (fld, of_value l))))
        v.t
  | Variant (lbl, arg) ->
      build ~line_no:(-1) (EVariant (lbl, of_value arg)) v.t
  | List elems -> build ~line_no:(-1) (EList (List.map of_value elems)) v.t
  | Set elems -> build ~line_no:(-1) (ESet (List.map of_value elems)) v.t
  | Map entries ->
      build
        ~line_no:(-1)
        (EMap (entries |> List.map (fun (k, v) -> (of_value k, of_value v))))
        v.t
  | Pair (v1, v2) -> pair ~line_no:(-1) (of_value v1) (of_value v2)
