(* Copyright 2019-2020 Smart Chain Arena LLC. *)

module Smart_ml_literal = Literal

type t =
  | Int       of string
  | String    of string
  | Bytes     of string
  | Primitive of
      { name : string
      ; annotations : string list
      ; arguments : t list }
  | Sequence  of t list

let unAnnot = function
  | [] -> ""
  | annots -> Printf.sprintf "(%s)" (String.concat ", " annots)

let rec pp indent = function
  | Int i -> Printf.sprintf "%s%s;" indent i
  | String s -> Printf.sprintf "%s'%s';" indent s
  | Bytes s -> Printf.sprintf "%s0x%s;" indent (Utils.Hex.hexcape s)
  | Primitive {name; annotations; arguments = []} ->
      Printf.sprintf "%s%s%s;" indent name (unAnnot annotations)
  | Primitive {name; annotations; arguments} ->
      Printf.sprintf
        "%s%s%s{\n%s\n%s};"
        indent
        name
        (unAnnot annotations)
        (String.concat
           "\n"
           (List.map (fun x -> pp (indent ^ "  ") x) arguments))
        indent
  | Sequence [] -> Printf.sprintf "%sSeq{}" indent
  | Sequence l ->
      Printf.sprintf
        "%sSeq{\n%s\n%s};"
        indent
        (String.concat "\n" (List.map (pp (indent ^ "  ")) l))
        indent

let to_json_string (t : t) =
  let indent = ref "" in
  let buf = Buffer.create 42 in
  let lastNewLine = ref true in
  let add s =
    let newLine = s = "\n" in
    if not (newLine && !lastNewLine)
    then (
      lastNewLine := newLine;
      Buffer.add_string buf s;
      if newLine then Buffer.add_string buf !indent )
  in
  let string s = add (Printf.sprintf "%S" s) in
  let iter_sep sep f l =
    let rec it = function
      | [] -> ()
      | [a] -> f a
      | a :: l ->
          f a;
          add sep;
          it l
    in
    it l
  in
  let rec iter = function
    | Int i -> dict [("int", i)]
    | String s -> dict [("string", s)]
    | Bytes s -> dict [("bytes", Utils.Hex.hexcape s)]
    | Primitive {name; annotations; arguments} ->
        let needsIndent =
          match arguments with
          | [] -> false
          | _ -> not (List.mem name ["Elt"; "DIG"; "DUG"; "PUSH"])
        in
        if needsIndent then add "\n";
        add "{";
        string "prim";
        add ":";
        string name;
        ( match arguments with
        | [] -> ()
        | _ ->
            add ",";
            string "args";
            add ":";
            add "[";
            let oldIndent = !indent in
            if needsIndent
            then (
              indent := !indent ^ " ";
              add "\n" );
            iter_sep "," iter arguments;
            indent := oldIndent;
            add "]";
            if needsIndent then add "\n" );
        ( match annotations with
        | [] -> ()
        | annotations ->
            add ",";
            string "annots";
            add ":";
            add "[";
            iter_sep "," string annotations;
            add "]" );
        add "}"
    | Sequence l ->
        add "[";
        iter_sep "," iter l;
        add "]"
  and dict l =
    add "{";
    iter_sep
      ","
      (fun (k, v) ->
        string k;
        add ":";
        string v)
      l;
    add "}"
  in
  iter t;
  Buffer.contents buf

let left x = Primitive {name = "Left"; annotations = []; arguments = [x]}

let right x = Primitive {name = "Right"; annotations = []; arguments = [x]}

let annotName t = String.sub t 1 (String.length t - 1)

let extractAnnot default = function
  | n1 :: n2 :: _ ->
      if String.sub n1 0 1 = "%"
      then annotName n1
      else if String.sub n2 0 1 = "%"
      then annotName n2
      else annotName n1 ^ "_" ^ annotName n2
  | [n] -> annotName n
  | [] -> default

let identity x = failwith ("Not handled " ^ pp "" x)

let error prefix x = Value.string ("Not handled " ^ prefix ^ " " ^ pp "" x)

let rec unfoldRecord ?(deep = false) = function
  | Primitive {name = "pair"; annotations; arguments = [t1; t2]}
    when deep || annotations = [] ->
      let get1, l1 = unfoldRecord t1 in
      let get2, l2 = unfoldRecord t2 in
      let get = function
        | Primitive {name = "Pair"; arguments = [x1; x2]} -> get1 x1 @ get2 x2
        | x ->
            failwith
              (Printf.sprintf
                 "Error reading a pair (%s, %s): %s"
                 (pp "" t1)
                 (pp "" t2)
                 (pp "" x))
      in
      (get, l1 @ l2)
  | Primitive {annotations = _ :: _} as x -> ((fun x -> [x]), [Some x])
  | _ -> ((fun _ -> assert false), [None])

and unfoldVariant ?(deep = false) = function
  | Primitive {name = "or"; annotations; arguments = [t1; t2]}
    when deep || annotations = [] ->
    ( match (unfoldVariant t1, unfoldVariant t2) with
    | Some (get1, l1), Some (get2, l2) ->
        let get t = function
          | Primitive {name = "Left"; arguments = [x]} -> get1 t x
          | Primitive {name = "Right"; arguments = [x]} -> get2 t x
          | x ->
              failwith
                (Printf.sprintf
                   "Error reading a or (%s, %s): %s"
                   (pp "" t1)
                   (pp "" t2)
                   (pp "" x))
        in
        Some (get, l1 @ l2)
    | _ -> None )
  | Primitive {annotations = _ :: _ as annotations} as x ->
      let name = extractAnnot "?" annotations in
      let _, get, _tt = typeOfStorageType x in
      Some ((fun t x -> Value.variant name (get x) t), [(name, x)])
  | _ -> None

and typeOfStorageType : _ -> _ * (t -> Value.t) * _ = function
  | Primitive {name = "pair"; annotations; arguments = [t1; t2]} as x ->
      let getRecord, l = unfoldRecord ~deep:true x in
      ( match
          List.fold_left
            (fun l x ->
              match (l, x) with
              | Some l, Some x -> Some (x :: l)
              | _ -> None)
            (Some [])
            l
        with
      | None ->
          let n1, get1, t1 = typeOfStorageType t1 in
          let n2, get2, t2 = typeOfStorageType t2 in
          let n = extractAnnot (Printf.sprintf "%s_%s" n1 n2) annotations in
          let get x =
            match x with
            | Primitive {name = "Pair"; arguments = [x1; x2]} ->
                Value.pair (get1 x1) (get2 x2)
            | p -> failwith (Printf.sprintf "Bad pair aaa %s " (pp "" p))
          in
          (n, get, Type.pair t1 t2)
      | Some l ->
          let l = List.rev l in
          let lrec = List.map typeOfStorageType l in
          let get x =
            let values = getRecord x in
            Value.record
              (List.map2 (fun value (a, get, _t) -> (a, get value)) values lrec)
          in
          let n =
            extractAnnot
              (String.concat "_" (List.map (fun (x, _, _) -> x) lrec))
              annotations
          in
          ( n
          , get
          , Type.record_or_unit (List.map (fun (n, _, v) -> (n, v)) lrec) ) )
  | Primitive {name = "or"; annotations; arguments = [t1; t2]} as x ->
    ( match unfoldVariant ~deep:true x with
    | None ->
        let n1, get1, tt1 = typeOfStorageType t1 in
        let n2, get2, tt2 = typeOfStorageType t2 in
        let n = extractAnnot (Printf.sprintf "%s_%s" n1 n2) annotations in
        let target = Type.variant [(n1, tt1); (n2, tt2)] in
        let get x =
          match x with
          | Primitive {name = "Left"; arguments = [x]} ->
              Value.variant "left" (get1 x) target
          | Primitive {name = "Right"; arguments = [x]} ->
              Value.variant "right" (get2 x) target
          | x ->
              failwith
                (Printf.sprintf
                   "Error reading a or (%s, %s): %s"
                   (pp "" t1)
                   (pp "" t2)
                   (pp "" x))
        in
        (n, get, target)
    | Some (get, l) ->
        let lrec = List.map (fun (n, x) -> (n, typeOfStorageType x)) l in
        let target =
          Type.variant (List.map (fun (n, (_, _, t)) -> (n, t)) lrec)
        in
        let n =
          extractAnnot
            (String.concat "_" (List.map (fun (x, _) -> x) lrec))
            annotations
        in
        (n, get target, target) )
  | Primitive {name = "unit"; annotations; _} ->
      let get _ = Value.unit in
      (extractAnnot "unit" annotations, get, Type.unit)
  | Primitive {name = "bool"; annotations} ->
      let get = function
        | Primitive {name = "False"; _} -> Value.bool false
        | Primitive {name = "True"; _} -> Value.bool true
        | x -> identity x
      in
      (extractAnnot "bool" annotations, get, Type.bool)
  | Primitive {name = "mutez"; annotations; _} ->
      let get = function
        | Int i | String i -> Value.mutez (Big_int.big_int_of_string i)
        | x -> error "mutez" x
      in
      (extractAnnot "mutez" annotations, get, Type.token)
  | Primitive {name = "timestamp"; annotations; _} ->
      let get = function
        | Int i | String i -> Value.string i (* TODO *)
        | x -> error "timestamp" x
      in
      (extractAnnot "timestamp" annotations, get, Type.timestamp)
  | Primitive {name = "nat"; annotations; _} ->
      let get = function
        | Int i | String i -> Value.nat (Big_int.big_int_of_string i)
        | x -> error "nat" x
      in
      (extractAnnot "nat" annotations, get, Type.nat ())
  | Primitive {name = "int"; annotations; _} ->
      let get = function
        | Int i | String i -> Value.int (Big_int.big_int_of_string i)
        | x -> error "int" x
      in
      (extractAnnot "int" annotations, get, Type.int ())
  | Primitive {name = "string"; annotations; _} ->
      let get = function
        | String i -> Value.string i
        | x -> error "string" x
      in
      (extractAnnot "string" annotations, get, Type.string)
  | Primitive {name = "key"; annotations; _} ->
      let get = function
        | String i -> Value.key i
        | x -> error "key" x
      in
      (extractAnnot "key" annotations, get, Type.key)
  | Primitive {name = "signature"; annotations; _} ->
      let get = function
        | String i -> Value.signature i
        | x -> error "signature" x
      in
      (extractAnnot "signature" annotations, get, Type.signature)
  | Primitive {name = "bytes"; annotations; _} ->
      let get = function
        | String i -> Value.bytes (Utils.Hex.unhex i)
        | x -> error "bytes" x
      in
      (extractAnnot "bytes" annotations, get, Type.bytes)
  | Primitive {name = "key_hash"; annotations; _} ->
      let get = function
        | String i -> Value.key_hash i
        | x -> error "key_hash" x
      in
      (extractAnnot "key_hash" annotations, get, Type.key_hash)
  | Primitive {name = "contract"; annotations; _} ->
      let get = function
        | String i -> Value.address i
        | x -> error "contract" x
      in
      (extractAnnot "contract" annotations, get, Type.address)
  | Primitive {name = "address"; annotations; _} ->
      let get = function
        | String i -> Value.address i
        | x -> error "address" x
      in
      (extractAnnot "address" annotations, get, Type.address)
  | Primitive {name = "list"; annotations; arguments = [t]} ->
      let n, get, t = typeOfStorageType t in
      let get = function
        | Sequence l -> Value.list (List.map get l) t
        | _ -> assert false
      in
      (extractAnnot n annotations, get, Type.list t)
  | Primitive {name = "option"; annotations; arguments = [t]} ->
      let n, get, t = typeOfStorageType t in
      let get = function
        | Primitive {name = "Some"; arguments = [x]; _} -> Value.some (get x)
        | Primitive {name = "None"; arguments = []; _} -> Value.none t
        | _ -> assert false
      in
      (extractAnnot n annotations, get, Type.option t)
  | Primitive {name = "map"; annotations = n; arguments = [t1; t2]} ->
      let _, get1, t1 = typeOfStorageType t1 in
      let _, get2, t2 = typeOfStorageType t2 in
      let get = function
        | Sequence l ->
            let pairOfStorage = function
              | Primitive {name = "Elt"; arguments = [k; v]} -> (get1 k, get2 v)
              | p -> failwith (Printf.sprintf "Bad pair storage %s " (pp "" p))
            in
            Value.buildMap
              (List.map pairOfStorage l)
              (Type.map (ref (Type.UnValue false)) t1 t2)
        | _ -> assert false
      in
      (extractAnnot "map" n, get, Type.map (ref (Type.UnValue false)) t1 t2)
  | Primitive {name = "set"; annotations = n; arguments = [t]} ->
      let n_inner, get, t = typeOfStorageType t in
      let get = function
        | Sequence l -> Value.buildSet (List.map get l) t
        | _ -> assert false
      in
      (extractAnnot (Printf.sprintf "set(%s)" n_inner) n, get, Type.set t)
  | Primitive {name = "big_map"; annotations = n; arguments = [t1; t2]} ->
      let _, _get1, t1 = typeOfStorageType t1 in
      let _, _get2, t2 = typeOfStorageType t2 in
      let get _ =
        Value.buildMap [] (Type.map (ref (Type.UnValue true)) t1 t2)
      in
      (extractAnnot "bigMap" n, get, Type.map (ref (Type.UnValue true)) t1 t2)
  | Primitive {name = "lambda"; annotations; arguments = [t1; t2]} ->
      let _n1, _get1, t1 = typeOfStorageType t1 in
      let _n2, _get2, t2 = typeOfStorageType t2 in
      let get x =
        Value.string
          (Printf.sprintf
             "Lambda (%s, %s) = %s"
             (Printer.type_to_string t1)
             (Printer.type_to_string t2)
             (pp "" x))
      in
      (extractAnnot "lambda" annotations, get, Type.string)
  | Primitive {name = "operation"; annotations; arguments = []} ->
      let get _x = Value.string "operation" in
      (extractAnnot "operation" annotations, get, Type.string)
  | p -> failwith ("Parse type error " ^ pp "" p)

let unString = function
  | `String s -> s
  | _ -> assert false

let rec parse (json : Yojson.Basic.t) =
  match json with
  | `String s -> String s
  | `Int _ -> assert false
  | `Bool b ->
      Primitive
        { name = (if b then "true" else "false")
        ; annotations = []
        ; arguments = [] }
  | `Float _ -> assert false
  | `Null -> String "NULL"
  | `Assoc l ->
    ( match l with
    | [("int", `String s)] -> Int s
    | [("string", `String s)] -> String s
    | [("bytes", `String s)] -> String s (* used in tzstats parsing *)
    | _ ->
        let name =
          match List.assoc_opt "prim" l with
          | Some (`String name) -> name
          | _ -> assert false
        in
        let annotations =
          match List.assoc_opt "annots" l with
          | Some (`List annots) -> List.map unString annots
          | _ -> []
        in
        let arguments =
          match List.assoc_opt "args" l with
          | Some (`List args) -> List.map parse args
          | _ -> []
        in
        Primitive {name; annotations; arguments} )
  | `List l -> Sequence (List.map parse l)

let int s = Int s

let string s = String s

let bytes s = Bytes s

let primitive name ?(annotations = []) arguments =
  Primitive {name; annotations; arguments}

let sequence l = Sequence l
