(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type env =
  { tstorage : Type.t option
  ; entry_point : (string * Type.t) option
  ; entry_points : (string, Type.t) Hashtbl.t
  ; tglobal_params : Type.t option }

let rec import_type typing_env import_env =
  let open Type in
  let rec import_type = function
    | Base.Sexp.Atom "unit" -> unit
    | Atom "bool" -> bool
    | Atom "int" -> int ()
    | Atom "nat" -> nat ()
    | Atom "intOrNat" -> intOrNat ()
    | Atom "string" -> string
    | Atom "bytes" -> bytes
    | Atom "mutez" -> token
    | Atom "timestamp" -> timestamp
    | Atom "address" -> address
    | Atom "key_hash" -> key_hash
    | Atom "key" -> key
    | Atom "signature" -> signature
    | List [Atom "unknown"; Atom id] -> Typing.unknown typing_env id
    | List (Atom "record" :: l) -> record_or_unit (List.map importField l)
    | List (Atom "variant" :: l) -> variant (List.map importField l)
    | List [Atom "list"; t] -> list (import_type t)
    | List [Atom "option"; t] -> option (import_type t)
    | List [Atom "contract"; t] -> contract (import_type t)
    | List [Atom "pair"; t1; t2] -> pair (import_type t1) (import_type t2)
    | List [Atom "set"; t] -> set (import_type t)
    | List [Atom "map"; k; v] ->
        map (ref (UnValue false)) (import_type k) (import_type v)
    | List [Atom "bigmap"; k; v] ->
        map (ref (UnValue true)) (import_type k) (import_type v)
    | List [Atom "type_of"; e; _] -> (import_expr typing_env import_env e).et
    | List l as t ->
        failwith
          ( "Type format error list "
          ^ Base.Sexp.to_string t
          ^ "  "
          ^ string_of_int (List.length l) )
    | Atom _ as t ->
        failwith ("Type format error atom " ^ Base.Sexp.to_string t)
  and importField = function
    | List [Atom name; v] -> (name, import_type v)
    | l -> failwith ("Type field format error " ^ Base.Sexp.to_string l)
  in
  import_type

and import_literal typing_env import_env =
  let import_type t = import_type typing_env import_env t in
  let open Literal in
  let rec import_literal = function
    | [Base.Sexp.Atom "unit"] -> unit
    | [Atom "string"; Atom n] -> string n
    | [Atom "bytes"; Atom n] ->
        let n =
          Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
        in
        bytes (Utils.Hex.unhex n)
    | [Atom "int"; Atom n] -> int (Big_int.big_int_of_string n)
    | [Atom "intOrNat"; Atom n] ->
        intOrNat (Type.intOrNat ()) (Big_int.big_int_of_string n)
    | [Atom "nat"; Atom n] -> nat (Big_int.big_int_of_string n)
    | [Atom "timestamp"; Atom n] -> timestamp (Big_int.big_int_of_string n)
    | [Atom "bool"; Atom "True"] -> bool true
    | [Atom "bool"; Atom "False"] -> bool false
    | [Atom "key"; Atom n] -> key n
    | [Atom "secret_key"; Atom n] -> secret_key n
    | [Atom "signature"; Atom n] -> signature n
    | [Atom "address"; Atom n] -> address n
    | [Atom "key_hash"; Atom n] -> key_hash n
    | [Atom "mutez"; List l] -> mutez (unInt (import_literal l))
    | [Atom "mutez"; Atom n] -> mutez (Big_int.big_int_of_string n)
    | [Atom "setType"; List v; t] ->
        let v = import_literal v in
        let t = import_type t in
        Typing.assertEqual t v.t ~pp:(fun () ->
            [`Literal v; `Text "is not compatible with"; `Type t]);
        v
    | l ->
        Format.kasprintf
          failwith
          "Literal format error: %a"
          Base.Sexp.pp
          (List l)
  in
  import_literal

and import_expr_inline_michelson import_env typing_env =
  let import_type t = import_type import_env typing_env t in
  let rec import_expr_inline_michelson = function
    | Base.Sexp.List [Atom "call_michelson"; instr; Atom _line_no] ->
        import_expr_inline_michelson instr
    | Base.Sexp.List (Atom "op" :: Atom name :: args) ->
        let rec extractTypes acc = function
          | [] -> assert false
          | Base.Sexp.Atom "out" :: out ->
              (List.rev acc, List.map import_type out)
          | t :: args -> extractTypes (import_type t :: acc) args
        in
        let typesIn, typesOut = extractTypes [] args in
        {name; typesIn; typesOut}
    | input ->
        failwith
          (Printf.sprintf
             "Cannot parse inline michelson %s"
             (Base.Sexp.to_string input))
  in
  import_expr_inline_michelson

and import_expr (typing_env : Typing.env) (import_env : env) =
  let import_literal l = import_literal typing_env import_env l in
  let import_type t = import_type typing_env import_env t in
  let import_expr_inline_michelson =
    import_expr_inline_michelson typing_env import_env
  in
  let open Expr in
  let rec import_expr = function
    | Base.Sexp.List (Atom f :: args) as input ->
      ( match (f, args) with
      | "sender", [] -> sender
      | "self", [] ->
        ( match import_env.tglobal_params with
        | None -> failwith "import: params type yet unknown"
        | Some t -> self t )
      | "self_entry_point", [Atom name; Atom line_no] ->
          let name =
            if name = ""
            then
              match import_env.entry_point with
              | None -> failwith "import: params type yet unknown"
              | Some (name, _) -> name
            else name
          in
          ( match Hashtbl.find_opt import_env.entry_points name with
          | None ->
              raise
                (SmartExcept
                   [ `Text "Import Error"
                   ; `Text (Printf.sprintf "Missing entry point %s" name)
                   ; `Line (int_of_string line_no) ])
          | Some t -> self_entry_point name t )
      | "source", [] -> source
      | "now", [] -> now
      | "amount", [] -> amount
      | "balance", [] -> balance
      | "nat", [e; Atom line_no] ->
          let e = import_expr e in
          Typing.assertEqual e.et (Type.nat ()) ~pp:(fun () ->
              [ `Text "sp.nat("
              ; `Expr e
              ; `Text ")"
              ; `Line (int_of_string line_no) ]);
          e
      | "eq", [e1; e2; Atom line_no] ->
          let e1 = import_expr e1 in
          let e2 = import_expr e2 in
          eq ~line_no:(int_of_string line_no) typing_env e1 e2
      | "neq", [e1; e2; Atom line_no] ->
          neq
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e1)
            (import_expr e2)
      | "le", [e1; e2; Atom line_no] ->
          le
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e1)
            (import_expr e2)
      | "lt", [e1; e2; Atom line_no] ->
          lt
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e1)
            (import_expr e2)
      | "ge", [e1; e2; Atom line_no] ->
          ge
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e1)
            (import_expr e2)
      | "gt", [e1; e2; Atom line_no] ->
          gt
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e1)
            (import_expr e2)
      | "add", [e1; e2; Atom line_no] ->
          add
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e1)
            (import_expr e2)
      | "sub", [e1; e2; Atom line_no] ->
          sub
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e1)
            (import_expr e2)
      | "mul", [e1; e2; Atom line_no] ->
          mul
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e1)
            (import_expr e2)
      | "ediv", [e1; e2; Atom line_no] ->
          ediv
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "truediv", [e1; e2; Atom line_no] ->
          div
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "floordiv", [e1; e2; Atom line_no] ->
          div
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "mod", [e1; e2; Atom line_no] ->
          e_mod
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "or", [e1; e2; Atom line_no] ->
          b_or
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "and", [e1; e2; Atom line_no] ->
          b_and
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "max", [e1; e2; Atom line_no] ->
          e_max
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "min", [e1; e2; Atom line_no] ->
          e_min
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "sum", [a; Atom line_no] ->
          sum ~line_no:(int_of_string line_no) (import_expr a)
      | "to_address", [e; Atom line_no] ->
          contract_address
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
      | "implicit_account", [e; Atom line_no] ->
          implicit_account ~line_no:(int_of_string line_no) (import_expr e)
      | "cons", [e1; e2; Atom line_no] ->
          cons
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "range", [e1; e2; e3; Atom line_no] ->
          range
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
            (import_expr e3)
      | "literal", [List l; Atom line_no] ->
          cst ~line_no:(int_of_string line_no) (import_literal l)
      | "list", t :: Atom line_no :: l ->
          build_list
            ~line_no:(int_of_string line_no)
            ~tvalue:(import_type t)
            ~elems:(List.map import_expr l)
      | "first", [e; Atom line_no] ->
          first ~line_no:(int_of_string line_no) typing_env (import_expr e)
      | "second", [e; Atom line_no] ->
          second ~line_no:(int_of_string line_no) typing_env (import_expr e)
      | "tuple", [e1; e2; Atom line_no] ->
          pair
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "tuple", _ ->
          failwith
            (Printf.sprintf
               "Only supported tuples are pairs: %s"
               (Base.Sexp.to_string input))
      | "neg", [e1; Atom line_no] ->
          negE ~line_no:(int_of_string line_no) (import_expr e1)
      | "abs", [e1; Atom line_no] ->
          absE ~line_no:(int_of_string line_no) (import_expr e1)
      | "toInt", [e1; Atom line_no] ->
          to_int ~line_no:(int_of_string line_no) (import_expr e1)
      | "isNat", [e1; Atom line_no] ->
          is_nat ~line_no:(int_of_string line_no) (import_expr e1)
      | "sign", [e1; Atom line_no] ->
          signE ~line_no:(int_of_string line_no) (import_expr e1)
      | "invert", [e1; Atom line_no] ->
          notE ~line_no:(int_of_string line_no) (import_expr e1)
      | "contractData", [Atom e1; Atom line_no] ->
          let line_no = int_of_string line_no in
          let id = int_of_string e1 in
          let t =
            match Hashtbl.find_opt typing_env.contract_data_types id with
            | None ->
                raise
                  (SmartExcept
                     [ `Text "Import Error"
                     ; `Text
                         (Printf.sprintf
                            "Missing contract data type for id %i"
                            id)
                     ; `Line line_no ])
            | Some t -> t
          in
          build
            ~line_no
            (EContract_data
               (cst ~line_no (Literal.int (Big_int.big_int_of_string e1))))
            t
      | "contract", [Atom entry_point; t; e; Atom line_no] ->
          let entry_point =
            match entry_point with
            | "" -> None
            | _ -> Some entry_point
          in
          contract
            ~line_no:(int_of_string line_no)
            entry_point
            (import_type t)
            (import_expr e)
      | "data", [] ->
        ( match import_env.tstorage with
        | None -> failwith "import: storage type yet unknown"
        | Some t -> storage ~line_no:(-1) t )
      | "attr", [x; Atom name; Atom line_no] ->
          let t = Typing.unknown typing_env "" in
          attr ~line_no:(int_of_string line_no) (import_expr x) name t
      | "isVariant", [x; Atom name; Atom line_no] ->
          isVariant
            ~line_no:(int_of_string line_no)
            typing_env
            name
            (import_expr x)
      | "variant_arg", [Atom arg_name; Atom line_no] ->
          variant_arg ~line_no:(int_of_string line_no) typing_env arg_name
      | "openVariant", [x; Atom name; Atom line_no] ->
          openVariant
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr x)
            name
      | "variant", [Atom name; x; Atom line_no] ->
          uvariant
            typing_env
            ~line_no:(int_of_string line_no)
            name
            (import_expr x)
      | "hashCrypto", [Atom algo; e; Atom line_no] ->
          let algo =
            match algo with
            | "BLAKE2B" -> BLAKE2B
            | "SHA256" -> SHA256
            | "SHA512" -> SHA512
            | _ -> failwith ("Unknown hash algorithm: " ^ algo)
          in
          hashCrypto ~line_no:(int_of_string line_no) algo (import_expr e)
      | "hash_key", [e; Atom line_no] ->
          hash_key ~line_no:(int_of_string line_no) (import_expr e)
      | "pack", [e; Atom line_no] ->
          pack ~line_no:(int_of_string line_no) (import_expr e)
      | "unpack", [e; t; Atom line_no] ->
          unpack
            ~line_no:(int_of_string line_no)
            (import_expr e)
            (import_type t)
      | "getLocal", [Atom name; t; Atom line_no] ->
          local ~line_no:(int_of_string line_no) name (import_type t)
      | "params", [Atom line_no] ->
          let line_no = int_of_string line_no in
          ( match import_env.entry_point with
          | None -> failwith "import_expr: params type yet unknown"
          | Some (_, t) -> params ~line_no t )
      | "update_map", [map; key; v; Atom line_no] ->
          updateMap
            ~line_no:(int_of_string line_no)
            (import_expr map)
            (import_expr key)
            (import_expr v)
      | "getItem", [a; pos; Atom line_no] ->
          item
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr a)
            (import_expr pos)
            None
      | "getItemDefault", [a; pos; def] ->
          item
            ~line_no:(-1)
            typing_env
            (import_expr a)
            (import_expr pos)
            (Some (import_expr def))
      | "add_seconds", [t; s; Atom line_no] ->
          add_seconds
            ~line_no:(int_of_string line_no)
            (import_expr t)
            (import_expr s)
      | "iter", [Atom name; t; Atom line_no] ->
          iterator ~line_no:(int_of_string line_no) name (import_type t)
      | "rev", [e; Atom line_no] ->
          listRev ~line_no:(int_of_string line_no) typing_env (import_expr e)
      | "items", [e; Atom line_no] ->
          listItems
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
            false
      | "keys", [e; Atom line_no] ->
          listKeys
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
            false
      | "values", [e; Atom line_no] ->
          listValues
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
            false
      | "elements", [e; Atom line_no] ->
          listElements
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
            false
      | "rev_items", [e; Atom line_no] ->
          listItems
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
            true
      | "rev_keys", [e; Atom line_no] ->
          listKeys
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
            true
      | "rev_values", [e; Atom line_no] ->
          listValues
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
            true
      | "rev_elements", [e; Atom line_no] ->
          listElements
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr e)
            true
      | "contains", [items; x; Atom line_no] ->
          contains
            ~line_no:(int_of_string line_no)
            (Some typing_env)
            (import_expr items)
            (import_expr x)
      | "check_signature", [pk; s; msg; Atom line_no] ->
          check_signature
            ~line_no:(int_of_string line_no)
            (import_expr pk)
            (import_expr s)
            (import_expr msg)
      | "reduce", [e; Atom line_no] ->
          reduce ~line_no:(int_of_string line_no) (import_expr e)
      | "scenario_var", [Atom id; Atom line_no] ->
          let t = Typing.unknown typing_env "" in
          scenario_var
            ~line_no:(int_of_string line_no)
            (Expr.of_value (Value.int (Big_int.big_int_of_string id)))
            t
      | "make_signature", [sk; msg; Atom fmt; Atom line_no] ->
          let secret_key = import_expr sk in
          let message = import_expr msg in
          let message_format =
            match String.lowercase_ascii fmt with
            | "raw" -> `Raw
            | "hex" -> `Hex
            | other ->
                Format.kasprintf
                  failwith
                  "make_signature: Wrong message format : %S (l. %s)"
                  other
                  line_no
          in
          make_signature
            ~secret_key
            ~message
            ~message_format
            ~line_no:(int_of_string line_no)
      | "account_of_seed", [Atom seed; Atom line_no] ->
          account_of_seed ~seed ~line_no:(int_of_string line_no)
      | "split_tokens", [mutez; quantity; total; Atom line_no] ->
          split_tokens
            ~line_no:(int_of_string line_no)
            (import_expr mutez)
            (import_expr quantity)
            (import_expr total)
      | "slice", [ofs; len; buf; Atom line_no] ->
          let offset = import_expr ofs in
          let length = import_expr len in
          let buffer = import_expr buf in
          slice
            typing_env
            ~offset
            ~length
            ~buffer
            ~line_no:(int_of_string line_no)
      | "concat", [l; Atom line_no] ->
          concat_list
            typing_env
            (import_expr l)
            ~line_no:(int_of_string line_no)
      | "size", [s; Atom line_no] ->
          size typing_env (import_expr s) ~line_no:(int_of_string line_no)
      | "setType", [e; t; Atom line_no] ->
          let e = import_expr e in
          let t = import_type t in
          Typing.assertEqual t e.et ~pp:(fun () ->
              [ `Expr e
              ; `Text "is not compatible with"
              ; `Type t
              ; `Line (int_of_string line_no) ]);
          e
      | "map", tkey :: tvalue :: Atom line_no :: entries ->
          build_map
            ~line_no:(int_of_string line_no)
            ~big:(ref (Type.UnValue false))
            ~tkey:(import_type tkey)
            ~tvalue:(import_type tvalue)
            ~entries:(List.map import_map_entry entries)
      | "set", telement :: Atom line_no :: entries ->
          build_set
            ~line_no:(int_of_string line_no)
            ~telement:(import_type telement)
            ~entries:(List.map import_expr entries)
      | "big_map", tkey :: tvalue :: Atom line_no :: entries ->
          build_map
            ~line_no:(int_of_string line_no)
            ~big:(ref (Type.UnValue true))
            ~tkey:(import_type tkey)
            ~tvalue:(import_type tvalue)
            ~entries:(List.map import_map_entry entries)
      | "record", Atom line_no :: l ->
          let import_exprField = function
            | Base.Sexp.List [Atom name; v] ->
                let e = import_expr v in
                ((name, e.et), e)
            | l ->
                failwith
                  ("Expression field format error " ^ Base.Sexp.to_string l)
          in
          record ~line_no:(int_of_string line_no) (List.map import_exprField l)
      | "call_michelson", instr :: Atom line_no :: args ->
          inline_michelson
            ~line_no:(int_of_string line_no)
            [import_expr_inline_michelson instr]
            (List.map import_expr args)
      | "seq_michelson", Atom line_no :: Atom length :: args ->
          let rec split n acc args =
            if n = 0
            then (List.rev acc, args)
            else
              match args with
              | [] -> assert false
              | a :: args -> split (n - 1) (a :: acc) args
          in
          let ops, args = split (int_of_string length) [] args in
          inline_michelson
            ~line_no:(int_of_string line_no)
            (List.map import_expr_inline_michelson ops)
            (List.map import_expr args)
      | "lambda", [Atom id; Atom name; Atom line_no; List commands] ->
          let line_no = int_of_string line_no in
          let tParams =
            Typing.unknown typing_env (Printf.sprintf "lambda %s" id)
          in
          let tResult =
            Typing.unknown typing_env (Printf.sprintf "lambda result %s" id)
          in
          lambda
            ~line_no
            (int_of_string id)
            name
            tParams
            tResult
            (Command.seq
               ~line_no
               (import_commands typing_env import_env commands))
      | "lambdaParams", [Atom id; Atom name; Atom line_no; tParams] ->
          let tParamsInit =
            Typing.unknown typing_env (Printf.sprintf "lambda %s" id)
          in
          let tParams = import_type tParams in
          let line_no = int_of_string line_no in
          Typing.assertEqual tParamsInit tParams ~pp:(fun () ->
              [`Text "Lambda parameter type mismatch"; `Line line_no]);
          lambdaParams ~line_no (int_of_string id) name tParams
      | "call_lambda", [lambda; parameter; Atom line_no] ->
          call_lambda
            ~line_no:(int_of_string line_no)
            typing_env
            (import_expr lambda)
            (import_expr parameter)
      | s, l ->
        ( try cst ~line_no:(-1) (import_literal (Atom s :: l)) with
        | _ ->
            failwith
              (Printf.sprintf
                 "Expression format error (a %i) %s"
                 (List.length l)
                 (Base.Sexp.to_string_hum input)) ) )
    | x -> failwith ("Expression format error (b) " ^ Base.Sexp.to_string_hum x)
  and import_map_entry = function
    | Base.Sexp.List [k; v] -> (import_expr k, import_expr v)
    | e ->
        failwith
          (Printf.sprintf "import_map_entry: '%s'" (Base.Sexp.to_string e))
  in
  import_expr

and import_command typing_env import_env =
  let import_type t = import_type typing_env import_env t in
  let import_expr e = import_expr typing_env import_env e in
  let import_commands cs = import_commands typing_env import_env cs in
  let open Command in
  function
  | Base.Sexp.List (Atom f :: args) as input ->
    ( match (f, args) with
    | "failwith", [x; Atom line_no] ->
        sp_failwith ~line_no:(int_of_string line_no) (import_expr x)
    | "verify", [x; ghost; Atom line_no] ->
        verify
          ~line_no:(int_of_string line_no)
          (import_expr x)
          (ghost = Atom "True")
          None
    | "verify", [x; ghost; message; Atom line_no] ->
        verify
          ~line_no:(int_of_string line_no)
          (import_expr x)
          (ghost = Atom "True")
          (Some (import_expr message))
    | "set_delegate", [x; Atom line_no] ->
        set_delegate ~line_no:(int_of_string line_no) (import_expr x)
    | "transfer", [e1; e2; e3; Atom line_no] ->
        transfer
          ~line_no:(int_of_string line_no)
          ~arg:(import_expr e1)
          ~amount:(import_expr e2)
          ~destination:(import_expr e3)
    | "defineLocal", [Atom name; expr; t; Atom line_no] ->
        defineLocal
          ~line_no:(int_of_string line_no)
          name
          (import_expr expr)
          (import_type t)
    | "dropLocal", [Atom name; Atom line_no] ->
        dropLocal ~line_no:(int_of_string line_no) name
    | "whileBlock", [e; List l; Atom line_no] ->
        let line_no = int_of_string line_no in
        whileLoop ~line_no (import_expr e) (seq ~line_no (import_commands l))
    | "forGroup", [Atom name; t; e; List l; Atom line_no] ->
        let e = import_expr e in
        let line_no = int_of_string line_no in
        forGroup
          ~line_no
          (name, import_type t)
          e
          (seq ~line_no (import_commands l))
    | ( "match"
      , [scrutinee; Atom constructor; Atom arg_name; List body; Atom line_no] )
      ->
        let line_no = int_of_string line_no in
        mk_match
          ~line_no
          typing_env
          (import_expr scrutinee)
          constructor
          arg_name
          (fun () -> seq ~line_no (import_commands body))
    | "setType", [e; t; Atom line_no] ->
        setType
          ~line_no:(int_of_string line_no)
          (import_expr e)
          (import_type t)
    | "set", [v; e; Atom line_no] ->
        set
          ~line_no:(int_of_string line_no)
          typing_env
          (import_expr v)
          (import_expr e)
    | "delItem", [e; k; Atom line_no] ->
        delItem
          ~line_no:(int_of_string line_no)
          typing_env
          (import_expr e)
          (import_expr k)
    | "updateSet", [e; k; Atom b; Atom line_no] ->
        updateSet
          ~line_no:(int_of_string line_no)
          (import_expr e)
          (import_expr k)
          (b = "True")
    | "lambdaResult", [e; Atom line_no] ->
        lambda_result ~line_no:(int_of_string line_no) (import_expr e)
    | _ -> failwith ("Command format error (a) " ^ Base.Sexp.to_string input)
    )
  | input -> failwith ("Command format error (b) " ^ Base.Sexp.to_string input)

and import_commands typing_env import_env =
  let import_expr e = import_expr typing_env import_env e in
  let import_command c = import_command typing_env import_env c in
  let open Command in
  let rec import_commands = function
    | Base.Sexp.List [Atom "ifBlock"; e; List tBlock; Atom line_no]
      :: List [Atom "elseBlock"; List eBlock] :: rest ->
        let line_no = int_of_string line_no in
        let c =
          ifte
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands tBlock))
            (seq ~line_no (import_commands eBlock))
        in
        c :: import_commands rest
    | List [Atom "ifBlock"; e; List tBlock; Atom line_no] :: rest ->
        let c =
          let line_no = int_of_string line_no in
          ifte
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands tBlock))
            (seq ~line_no [])
        in
        c :: import_commands rest
    | List [Atom "ifSomeBlock"; e; Atom _; List tBlock; Atom line_no]
      :: List [Atom "elseBlock"; List eBlock] :: rest ->
        let line_no = int_of_string line_no in
        let c =
          ifteSome
            ~line_no
            typing_env
            (import_expr e)
            (seq ~line_no (import_commands tBlock))
            (seq ~line_no (import_commands eBlock))
        in
        c :: import_commands rest
    | List [Atom "ifSomeBlock"; e; Atom _; List tBlock; Atom line_no] :: rest
      ->
        let line_no = int_of_string line_no in
        let c =
          ifteSome
            ~line_no
            typing_env
            (import_expr e)
            (seq ~line_no (import_commands tBlock))
            (seq ~line_no [])
        in
        c :: import_commands rest
    | x :: l ->
        let x = import_command x in
        (* This sequencing is important for type inferrence. *)
        x :: import_commands l
    | [] -> []
  in
  import_commands

(** Import the expression in an environment where params and storage
   types aren't known yet. For contract constructors. *)
let early_env () =
  { tstorage = None
  ; entry_point = None
  ; entry_points = Hashtbl.create 5
  ; tglobal_params = None }

let import_expr_early typing_env = import_expr typing_env (early_env ())

let rec assocList (n : string) = function
  | Base.Sexp.Atom (a : string) :: List l :: _ when Stdlib.(a = n) -> l
  | _ :: l -> assocList n l
  | _ -> failwith ("Cannot find " ^ n)

let import_contract ~primitives ~scenario_state typing_env = function
  | Base.Sexp.Atom _ -> failwith "Parse error contract"
  | List l ->
      let storage = assocList "storage" l in
      let messages = assocList "messages" l in
      let flags = assocList "flags" l in
      let storage = import_expr_early typing_env (Base.Sexp.List storage) in
      let entry_points = Hashtbl.create 6 in
      let messages =
        List.map
          (function
            | Base.Sexp.List [Atom name; List command] ->
                let t = Typing.unknown typing_env "" in
                Hashtbl.replace entry_points name t;
                ((name, command), t)
            | x -> failwith ("Message format error " ^ Base.Sexp.to_string x))
          messages
      in
      let tparameter =
        match messages with
        | [] -> Type.unit
        | [(_, t)] -> t
        | _ ->
            Type.variant (List.map (fun ((name, _), t) -> (name, t)) messages)
      in
      let build_entry_point ((name, command), t) =
        let import_env =
          { entry_point = Some (name, t)
          ; tstorage = Some storage.et
          ; entry_points
          ; tglobal_params = Some tparameter }
        in
        let body =
          Command.seq
            ~line_no:(-1)
            (import_commands typing_env import_env command)
        in
        match import_env.entry_point with
        | Some (_, paramsType) ->
            (typing_env, {channel = name; paramsType; body})
        | None -> assert false
      in
      let entry_points = List.map build_entry_point messages in
      let fixEnv =
        Fixer.
          { partial = false
          ; check_variables = false
          ; storage = storage.et
          ; variable_names = []
          ; substContractData = Hashtbl.create 0
          ; reducer = Interpreter.reducer ~primitives ~scenario_state
          ; lambda_param = None }
      in
      let importFlag = function
        | Base.Sexp.Atom "unit_failwith" -> Unit_failwith
        | x -> failwith ("Unknown Contract Flag: " ^ Base.Sexp.to_string x)
      in
      let flags = List.map importFlag flags in
      Fixer.fix_contract
        fixEnv
        typing_env
        ~storage
        ~entry_points
        ~tparameter
        ~flags
