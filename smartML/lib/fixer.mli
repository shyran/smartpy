(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type fix_env =
  { mutable partial : bool
  ; storage : Type.t
  ; check_variables : bool
  ; mutable variable_names : string list
        (** Names of local variables already in use. *)
  ; substContractData : (int, texpr) Hashtbl.t
  ; reducer : line_no:int -> texpr -> texpr
  ; lambda_param : Type.t option }

val fix_type : fix_env -> Type.t -> Type.t

val fix_value : fix_env -> tvalue -> tvalue

val fix_expr : fix_env -> Typing.env -> texpr -> texpr

val fix_command : fix_env -> Typing.env -> tcommand -> tcommand

val fix_contract :
     fix_env
  -> Typing.env
  -> storage:texpr
  -> entry_points:(Typing.env * entry_point) list
  -> tparameter:Type.t
  -> flags:Basics.flag list
  -> texpr tcontract_
