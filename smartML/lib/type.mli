(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type t = private {u : u}

and u = private
  | TUnit
  | TBool
  | TInt       of {isNat : bool unknown ref}
  | TTimestamp
  | TString
  | TBytes
  | TRecord    of (string * t) list
  | TVariant   of (string * t) list
  | TSet       of t
  | TMap       of
      { big : bool unknown ref
      ; key : t
      ; item : t }
  | TAddress
  | TContract  of t
  | TKeyHash
  | TKey
  | TSignature
  | TToken
  | TUnknown   of unknownType ref
  | TPair      of t * t
  | TAnnots    of string list * t
  | TList      of t
  | TLambda    of t * t
  | TSecretKey

and unknownType =
  | UUnknown of string
  | UItems   of t * t
  | URecord  of (string * t) list
  | UVariant of (string * t) list
  | UExact   of t

and tvariable = string * t

and 'a unknown =
  | UnUnknown of string
  | UnValue   of 'a
  | UnRef     of 'a unknown ref
[@@deriving show]

val fullUnknown : string -> t

val getRef : 'a unknown ref -> 'a unknown ref

val getRepr : t -> u

val unit : t

val address : t

val contract : t -> t

val bool : t

val bytes : t

val variant : (string * t) list -> t

val key_hash : t

val int_raw : isNat:bool unknown ref -> t

val int : unit -> t

val nat : unit -> t

val intOrNat : unit -> t

val key : t

val secret_key : t

val map : bool unknown ref -> t -> t -> t

val set : t -> t

val record : (string * t) list -> t

val record_or_unit : (string * t) list -> t

val signature : t

val option : t -> t

val tor : t -> t -> t

val string : t

val timestamp : t

val token : t

val uvariant : string -> t -> t

val unknown_raw : unknownType ref -> t

val account : t

val pair : t -> t -> t

val annots : string list -> t -> t

val list : t -> t

val lambda : t -> t -> t
