(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type context_ =
  { sender : string
  ; source : string
  ; time : int
  ; amount : Big_int.big_int
  ; line_no : int
  ; debug : bool }

type context
(** The initial state of the execution environment, a.k.a. this
    provides a “pseudo-blockchain.” *)

val context_sender : context -> string

val context_time : context -> int

val context_line_no : context -> int

val context_debug : context -> bool

val context :
     ?sender:string
  -> ?source:string
  -> time:int
  -> amount:Big_int.big_int
  -> line_no:int
  -> debug:bool
  -> unit
  -> context
(** Build a {!context}. *)

(** Parametrized implementations of low-level functions (cryptography,
    hashes, etc.), to be filled-in by the execution context (["js_of_ocaml"],
    native, etc.). *)
module Primitive_implementations : sig
  type hash = string -> string

  type account =
    { pkh : string
    ; pk : string
    ; sk : string }

  type t =
    { hash_blake2b : hash
    ; hash_sha256 : hash
    ; hash_sha512 : hash
    ; hash_key : hash
    ; account_of_seed : string -> account
    ; check_signature : public_key:string -> signature:string -> string -> bool
    ; sign : secret_key:string -> string -> string }

  val faked_in_ocaml : t
  (** Pure OCaml fake implementations. *)

  val blake2b : t -> hash

  val sha256 : t -> hash

  val sha512 : t -> hash

  val self_test : t -> (string, string) result list
  (** Run a quick unit test on the primitives, to check that they are
      consistent w.r.t. assumptions made throughout the library. *)
end

val pack_value : Value.t -> string
(** Serialize a value into the same binary format as Michelson in the
    protocol.  *)

val interpret_message :
     primitives:Primitive_implementations.t
  -> scenario_state:scenario_state
  -> context
  -> tcontract
  -> tmessage
  -> (tcontract * tcommand) option
     * Execution.effect list
     * Execution.error option
     * Execution.step list
(** Evaluation of a contract call ({!tmessage}) within a {!context}. *)

val interpret_expr_external :
     primitives:Primitive_implementations.t
  -> no_env:Basics.smart_except list
  -> scenario_state:scenario_state
  -> texpr
  -> tvalue
(** Evaluation of an expression ({!texpr}) within a {!context}. *)

val reducer :
     primitives:Primitive_implementations.t
  -> scenario_state:scenario_state
  -> line_no:int
  -> texpr
  -> texpr
