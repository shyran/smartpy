(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

module Options = struct
  type t =
    { html : bool
    ; stripStrings : bool
    ; stripped : bool
    ; types : bool
    ; analysis : bool }

  let string =
    { html = false
    ; stripStrings = false
    ; stripped = false
    ; types = false
    ; analysis = false }

  let html = {string with html = true}

  let htmlStripStrings = {html with stripStrings = true}

  let types = {html with types = true}

  let analysis = {types with analysis = true}
end

let rec type_to_string ?(options = Options.string) ?(indent = "") t =
  let open Type in
  let shiftIdent = if options.html then "&nbsp;&nbsp;" else "  " in
  match getRepr t with
  | TBool -> "sp.TBool"
  | TString -> "sp.TString"
  | TTimestamp -> "sp.TTimestamp"
  | TBytes -> "sp.TBytes"
  | TInt {isNat} ->
    ( match Typing.intType isNat with
    | `Unknown -> "sp.TIntOrNat"
    | `Nat -> "sp.TNat"
    | `Int -> "sp.TInt" )
  | TRecord l ->
      if options.html
      then
        Printf.sprintf
          "<br>%s<span class='record'>{</span>%s<br>%s<span \
           class='record'>}</span>"
          indent
          (String.concat
             ""
             (List.map
                (fun (s, t) ->
                  Printf.sprintf
                    "<br>%s<span class='record'>%s</span>: %s;"
                    (indent ^ shiftIdent)
                    s
                    (type_to_string
                       ~options
                       ~indent:(indent ^ shiftIdent ^ shiftIdent)
                       t))
                l))
          indent
      else
        Printf.sprintf
          "sp.TRecord(%s)"
          (String.concat
             ", "
             (List.map
                (fun (s, t) ->
                  Printf.sprintf "%s = %s" s (type_to_string ~options t))
                l))
  | TVariant l ->
    ( match l with
    | [("None", _unit); ("Some", t)] ->
        Printf.sprintf
          "sp.TOption(%s)"
          (type_to_string ~options ~indent:(indent ^ shiftIdent ^ shiftIdent) t)
    | _ ->
        if options.html
        then
          let ppVariant (s, t) =
            let t =
              match getRepr t with
              | TUnit -> ""
              | _ ->
                  Printf.sprintf
                    " %s"
                    (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
            in
            Printf.sprintf
              "<br>%s<span class='variant'>| %s</span>%s"
              indent
              s
              t
          in
          String.concat "" (List.map ppVariant l)
        else
          Printf.sprintf
            "sp.TVariant(%s)"
            (String.concat
               ", "
               (List.map
                  (fun (s, t) ->
                    Printf.sprintf "%s = %s" s (type_to_string ~options t))
                  l)) )
  | TSet element ->
      Printf.sprintf
        "sp.TSet(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) element)
  | TMap {big; key; item} ->
      let name =
        match !(getRef big) with
        | UnUnknown _ -> "sp.TMap??"
        | UnValue true -> "sp.TBigMap"
        | UnValue false -> "sp.TMap"
        | UnRef _ -> assert false
      in
      Printf.sprintf
        "%s(%s, %s)"
        name
        (type_to_string ~options ~indent:(indent ^ shiftIdent) key)
        (type_to_string ~options ~indent:(indent ^ shiftIdent) item)
  | TToken -> "sp.TMutez"
  | TUnit -> "sp.TUnit"
  | TAddress -> "sp.TAddress"
  | TKeyHash -> "sp.TKeyHash"
  | TKey -> "sp.TKey"
  | TSecretKey -> "sp.TSecretKey"
  | TSignature -> "sp.TSignature"
  | TContract t ->
      Printf.sprintf
        "sp.TContract(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
  | TUnknown {contents = UExact t} -> type_to_string t
  | TUnknown {contents = UUnknown _} ->
      if options.html
      then "<span class='partialType'>unknown</span>"
      else "unknown"
  | TUnknown {contents = URecord l} ->
      Printf.sprintf
        "TRecord++(%s)"
        (String.concat
           ", "
           (List.map
              (fun (s, t) ->
                Printf.sprintf "%s = %s" s (type_to_string ~options t))
              l))
  | TUnknown {contents = UVariant l} ->
      Printf.sprintf
        "TVariant++(%s)"
        (String.concat
           " | "
           (List.map
              (fun (s, t) ->
                Printf.sprintf "%s %s" s (type_to_string ~options t))
              l))
  | TUnknown {contents = UItems (tk, tv)} ->
      Printf.sprintf
        "<span class='partialType'>TUnknown</span>(%s, %s)"
        (type_to_string ~options tk)
        (type_to_string ~options tv)
  | TPair (t1, t2) ->
      Printf.sprintf
        "sp.TPair(%s, %s)"
        (type_to_string ~options t1)
        (type_to_string ~options t2)
  | TAnnots (annots, t) ->
      Printf.sprintf
        "sp.TAnnots(%s, %s)"
        (type_to_string ~options t)
        (String.concat ", " (List.map (Printf.sprintf "%S") annots))
  | TList t ->
      Printf.sprintf
        "sp.TList(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
  | TLambda (t1, t2) ->
      Printf.sprintf
        "sp.TLambda(%s, %s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t1)
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t2)

let type_to_string ?(options = Options.string) s =
  if options.html
  then
    Printf.sprintf "<span class='type'>%s</span>" (type_to_string ~options s)
  else type_to_string ~options s

let ppAmount html amount =
  let oneMillion = Big_int.big_int_of_int 1000000 in
  let quotient, modulo = Big_int.quomod_big_int amount oneMillion in
  if Big_int.compare_big_int modulo Big_int.zero_big_int = 0
  then
    if html
    then
      Printf.sprintf
        "%s<img height=20 width=20 src='./svgs/tezos-xtz-logo.svg' alt='tz'/>"
        (Big_int.string_of_big_int quotient)
    else Printf.sprintf "sp.tez(%s)" (Big_int.string_of_big_int quotient)
  else if html
  then
    Printf.sprintf
      "%s&#x03bc;<img height=20 width=20 src='./svgs/tezos-xtz-logo.svg' \
       alt='tz'/>"
      (Big_int.string_of_big_int amount)
  else Printf.sprintf "sp.mutez(%s)" (Big_int.string_of_big_int amount)

let literal_to_string ~html ?strip_strings =
  let to_string (v : Literal.t) =
    match v.l with
    | Unit -> "sp.unit"
    | Bool x -> String.capitalize_ascii (string_of_bool x)
    | Int x -> Big_int.string_of_big_int x
    | String s when strip_strings = Some () -> s
    | String s -> Printf.sprintf "'%s'" s
    | Bytes s -> Printf.sprintf "sp.bytes('0x%s')" (Utils.Hex.hexcape s)
    | Mutez i -> ppAmount html i
    | Address s -> Printf.sprintf "sp.address('%s')" s
    | Timestamp i ->
        Printf.sprintf "sp.timestamp(%s)" (Big_int.string_of_big_int i)
    | Key s -> Printf.sprintf "sp.key('%s')" s
    | Secret_key s -> Printf.sprintf "sp.secret_key('%s')" s
    | Key_hash s -> Printf.sprintf "sp.key_hash('%s')" s
    | Signature s -> Printf.sprintf "sp.signature('%s')" s
  in
  to_string

let unrec = function
  | {v = Record l} -> List.map snd l
  | x -> [x]

let is_range_keys l =
  let rec aux n = function
    | [] -> true
    | ({v = Literal {l = Int a}}, _) :: l ->
        Big_int.eq_big_int (Big_int.big_int_of_int n) a && aux (n + 1) l
    | (_, _) :: _ -> false
  in
  aux 0 l

let is_vector v =
  match v.v with
  | Map l -> if is_range_keys l then Some (List.map snd l) else None
  | _ -> None

let is_matrix l =
  match is_vector l with
  | None -> None
  | Some vect ->
      let new_line lines a =
        match (lines, is_vector a) with
        | None, _ | _, None -> None
        | Some lines, Some v -> Some (v :: lines)
      in
      ( match List.fold_left new_line (Some []) vect with
      | Some lines
        when 2 <= List.length lines
             && List.exists (fun line -> 2 <= List.length line) lines ->
          Some ([], List.rev lines)
      | _ -> None )

let html_of_record_list (columns, data) f =
  let ppRow row =
    Printf.sprintf
      "<tr>%s</tr>"
      (String.concat
         ""
         (List.map
            (fun s -> Printf.sprintf "<td class='data'>%s</td>" (f s))
            row))
  in
  Printf.sprintf
    "<table class='recordList'><tr>%s</tr>%s</table>"
    (String.concat
       "\n"
       (List.map
          (fun s ->
            Printf.sprintf
              "<td class='dataColumn'>%s</td>"
              (String.capitalize_ascii s))
          columns))
    (String.concat "\n" (List.map ppRow data))

let rec value_to_string
    ?(deep = false) ?(noEmptyList = false) ?(options = Options.string) v =
  match v.v with
  | Literal {l = Bool x} -> String.capitalize_ascii (string_of_bool x)
  | Literal {l = Int x} ->
      let pp = Big_int.string_of_big_int x in
      if options.html && not deep
      then Printf.sprintf "<input type='text' value='%s' readonly></input>" pp
      else pp
  | Literal {l = String s} ->
      if options.stripStrings then s else Printf.sprintf "'%s'" s
  | Literal {l = Bytes s} ->
      if options.html
      then
        Printf.sprintf "<span class='bytes'>0x%s</span>" (Utils.Hex.hexcape s)
      else Printf.sprintf "sp.bytes('0x%s')" (Utils.Hex.hexcape s)
  | Record l ->
    ( match options with
    | {html = true} ->
        html_of_record_list
          (List.map fst l, [List.map snd l])
          (fun s -> value_to_string ~noEmptyList:true ~options s)
    | _ ->
        Printf.sprintf
          "sp.record(%s)"
          (String.concat
             ", "
             (List.map
                (fun (n, x) ->
                  Printf.sprintf "%s = %s" n (value_to_string ~options x))
                l)) )
  | Variant (name, v) when options.html ->
      Printf.sprintf
        "<div class='subtype'><select class='selection'><option \
         value='%s'>%s</option></select>%s</div>"
        name
        (String.capitalize_ascii name)
        (value_to_string v ~deep ~noEmptyList ~options)
  | Variant ("None", {v = Literal {l = Unit}}) ->
      if options.html then "None" else "sp.none"
  | Variant (name, {v = Literal {l = Unit}}) -> name
  | Variant ("Some", v) ->
      if options.html
      then value_to_string ~options v (* TODO ? *)
      else Printf.sprintf "sp.some(%s)" (value_to_string ~options v)
  | Variant (x, v) -> Printf.sprintf "%s(%s)" x (value_to_string ~options v)
  | List [] when noEmptyList -> ""
  | List l when deep ->
      if options.html
      then
        Printf.sprintf
          "[%s]"
          (String.concat
             ", "
             (List.map (value_to_string ~options ~deep:true) l))
      else
        Printf.sprintf
          "sp.list([%s])"
          (String.concat ", " (List.map (value_to_string ~options) l))
  | List l when options.html ->
      let l =
        match l with
        | {v = Record r} :: _ as l ->
            ( List.map fst r
            , List.map
                (function
                  | {v = Record r} -> List.map snd r
                  | _ -> assert false)
                l )
        | _ -> ([""], List.map (fun x -> [x]) l)
      in
      html_of_record_list l (fun s ->
          value_to_string ~noEmptyList:true ~deep:true ~options s)
  | List l ->
      Printf.sprintf
        "sp.list([%s])"
        (String.concat ", " (List.map (value_to_string ~options) l))
  | Set [] when noEmptyList -> ""
  | Set set ->
      if options.html
      then
        html_of_record_list
          ([""], List.map (fun x -> [x]) set)
          (fun s -> value_to_string ~noEmptyList:true ~deep:true ~options s)
      else
        Printf.sprintf
          "sp.set([%s])"
          (String.concat ", " (List.map (value_to_string ~options) set))
  | Map map ->
      if options.html
      then
        match is_matrix v with
        | Some (columns, l) ->
            html_of_record_list (columns, l) (fun s ->
                value_to_string ~noEmptyList:true ~options s)
        | None ->
            let result =
              match Type.getRepr v.t with
              | TMap {item} ->
                ( match Type.getRepr item with
                | TRecord r ->
                    Some
                      (html_of_record_list
                         ( "Key" :: List.map fst r
                         , List.map (fun (x, y) -> x :: unrec y) map )
                         (fun s ->
                           value_to_string ~noEmptyList:true ~options s))
                | _ -> None )
              | _ -> None
            in
            ( match result with
            | None ->
                html_of_record_list
                  (["Key"; "Value"], List.map (fun (x, y) -> [x; y]) map)
                  (fun s -> value_to_string ~noEmptyList:true ~options s)
            | Some t -> t )
      else
        Printf.sprintf
          "{%s}"
          (String.concat
             ", "
             (List.map
                (fun (k, v) ->
                  Printf.sprintf
                    "%s : %s"
                    (value_to_string ~options k)
                    (value_to_string ~options v))
                map))
  | Literal {l = Unit} -> if options.html then "" else "Unit"
  | Literal {l = Key_hash s} ->
      if options.html
      then Printf.sprintf "<span class='key'>%s</span>" s
      else Printf.sprintf "sp.key_hash('%s')" s
  | Literal {l = Key s} ->
    ( match options with
    | {html = true} -> Printf.sprintf "<span class='key'>%s</span>" s
    | _ -> Printf.sprintf "sp.key('%s')" s )
  | Literal {l = Secret_key s} ->
    ( match options with
    | {html = true} -> Printf.sprintf "<span class='key'>%s</span>" s
    | _ -> Printf.sprintf "sp.secret_key('%s')" s )
  | Literal {l = Signature s} ->
    ( match options with
    | {html = true} -> Printf.sprintf "<span class='signature'>%s</span>" s
    | _ -> Printf.sprintf "sp.signature('%s')" s )
  | Literal {l = Address s} ->
    ( match options with
    | {html = true} -> Printf.sprintf "<span class='address'>%s</span>" s
    | _ -> Printf.sprintf "sp.address('%s')" s )
  | Literal {l = Timestamp i} ->
    ( match options with
    | {html = true} ->
        Printf.sprintf
          "<span class='timestamp'>timestamp(%s)</span>"
          (Big_int.string_of_big_int i)
    | _ -> Printf.sprintf "sp.timestamp(%s)" (Big_int.string_of_big_int i) )
  | Literal {l = Mutez i} ->
      let amount = ppAmount options.html i in
      if options.html
      then Printf.sprintf "<span class='token'>%s</span>" amount
      else amount
  | Pair (v1, v2) ->
      if options.html
      then
        html_of_record_list
          ([], [[v1; v2]])
          (fun s -> value_to_string ~noEmptyList:true ~options s)
      else
        Printf.sprintf
          "(%s, %s)"
          (value_to_string ~noEmptyList:true ~options v1)
          (value_to_string ~noEmptyList:true ~options v2)

let vclass_to_string = function
  | Storage -> "storage"
  | Local -> "local"
  | Param -> "param"
  | Iter -> "iter"

let variable_to_string ?(options = Options.string) ?protect (s, t) vclass =
  let prot s = if protect = Some () then Printf.sprintf "(%s)" s else s in
  match options with
  | {html = false; types = false} -> s
  | {types = true} ->
      prot
        (Printf.sprintf
           "<span class='%s'>%s</span> : <span class='type'>%s</span>"
           (vclass_to_string vclass)
           s
           (type_to_string t))
  | {types = false} ->
      Printf.sprintf "<span class='%s'>%s</span>" (vclass_to_string vclass) s

let string_of_binOpInfix = function
  | BNeq -> "!="
  | BEq -> "=="
  | BAnd -> "&"
  | BOr -> "|"
  | BAdd -> "+"
  | BSub -> "-"
  | BDiv -> "//"
  | BEDiv -> "ediv"
  | BMul -> "*"
  | BMod -> "%"
  | BLt -> "<"
  | BLe -> "<="
  | BGt -> ">"
  | BGe -> ">="

let string_of_binOpPrefix = function
  | BMax -> "max"
  | BMin -> "min"

let rec expr_to_string ?(options = Options.string) ?protect e =
  let prot s = if protect = Some () then Printf.sprintf "(%s)" s else s in
  let htmlClass name s =
    if options.html
    then Printf.sprintf "<span class='%s'>%s</span>" name s
    else s
  in
  let putSelf s = Printf.sprintf "%s.%s" (htmlClass "self" "self") s in
  match e.e with
  | EReduce x -> Printf.sprintf "sp.reduce(%s)" (expr_to_string ~options x)
  | EIter t -> variable_to_string ~options ?protect t Iter
  | EListRev e ->
      Printf.sprintf "%s.rev()" (expr_to_string ~options ~protect:() e)
  | EListItems (e, false) ->
      Printf.sprintf "%s.items()" (expr_to_string ~options ~protect:() e)
  | EListKeys (e, false) ->
      Printf.sprintf "%s.keys()" (expr_to_string ~options ~protect:() e)
  | EListValues (e, false) ->
      Printf.sprintf "%s.values()" (expr_to_string ~options ~protect:() e)
  | EListElements (e, false) ->
      Printf.sprintf "%s.elements()" (expr_to_string ~options ~protect:() e)
  | EListItems (e, true) ->
      Printf.sprintf "%s.rev_items()" (expr_to_string ~options ~protect:() e)
  | EListKeys (e, true) ->
      Printf.sprintf "%s.rev_keys()" (expr_to_string ~options ~protect:() e)
  | EListValues (e, true) ->
      Printf.sprintf "%s.rev_values()" (expr_to_string ~options ~protect:() e)
  | EListElements (e, true) ->
      Printf.sprintf
        "%s.rev_elements()"
        (expr_to_string ~options ~protect:() e)
  | EParams _ -> Printf.sprintf "params"
  | ELocal (name, _t) -> Printf.sprintf "%s.value" name
  | EStorage -> putSelf "data"
  | EPack e -> Printf.sprintf "sp.pack(%s)" (expr_to_string ~options e)
  | ECallLambda (lambda, parameter) ->
      Printf.sprintf
        "%s.call(%s)"
        (expr_to_string ~options ~protect:() lambda)
        (expr_to_string ~options parameter)
  | ELambda _ (* {id; name; command; expr} *) ->
      Printf.sprintf "sp.build_lambda('???')"
  | ELambdaParams {id} -> Printf.sprintf "lparams_%i" id
  | EUnpack (e, t) ->
      Printf.sprintf
        "sp.unpack(%s, %s)"
        (expr_to_string ~options e)
        (type_to_string t)
  | ECst v ->
      htmlClass
        "constant"
        (literal_to_string
           ~html:false
           ?strip_strings:(if options.stripStrings then Some () else None)
           v)
  | EBinOpPre (f, x, y) ->
      Printf.sprintf
        "sp.%s(%s, %s)"
        (string_of_binOpPrefix f)
        (expr_to_string ~options x)
        (expr_to_string ~options y)
  | EBinOpInf (BEDiv, x, y) ->
      prot
        (Printf.sprintf
           "sp.ediv(%s, %s)"
           (expr_to_string ~options x)
           (expr_to_string ~options y))
  | EBinOpInf (op, x, y) ->
      prot
        (Printf.sprintf
           "%s %s %s"
           (expr_to_string ~options ~protect:() x)
           (string_of_binOpInfix op)
           (expr_to_string ~options ~protect:() y))
  | ENot x ->
      prot (Printf.sprintf "~ %s" (expr_to_string ~options ~protect:() x))
  | EAbs x -> prot (Printf.sprintf "abs(%s)" (expr_to_string ~options x))
  | EToInt x ->
      prot (Printf.sprintf "sp.to_int(%s)" (expr_to_string ~options x))
  | EIsNat x ->
      prot (Printf.sprintf "sp.is_nat(%s)" (expr_to_string ~options x))
  | ENeg x ->
      prot (Printf.sprintf "- %s" (expr_to_string ~options ~protect:() x))
  | ESign x -> prot (Printf.sprintf "sp.sign(%s)" (expr_to_string ~options x))
  | EItem (t, x, None) ->
      Printf.sprintf
        "%s[%s]"
        (expr_to_string ~options t)
        (expr_to_string ~options x)
  | EItem (t, x, Some d) ->
      Printf.sprintf
        "%s.get(%s, %s)"
        (expr_to_string ~options t)
        (expr_to_string ~options x)
        (expr_to_string ~options d)
  | ESplit_tokens ({e = ECst {l = Mutez tok}}, quantity, {e = ECst {l = Int v}})
    when Big_int.eq_big_int tok (Big_int.big_int_of_int 1000000)
         && Big_int.eq_big_int v (Big_int.big_int_of_int 1) ->
      Printf.sprintf "sp.tez(%s)" (expr_to_string ~options quantity)
  | ESplit_tokens ({e = ECst {l = Mutez tok}}, quantity, {e = ECst {l = Int v}})
    when Big_int.eq_big_int tok (Big_int.big_int_of_int 1)
         && Big_int.eq_big_int v (Big_int.big_int_of_int 1) ->
      Printf.sprintf "sp.mutez(%s)" (expr_to_string ~options quantity)
  | ESplit_tokens (e1, e2, e3) ->
      Printf.sprintf
        "sp.split_tokens(%s, %s, %s)"
        (expr_to_string ~options e1)
        (expr_to_string ~options e2)
        (expr_to_string ~options e3)
  | ERange (a, b, {e = ECst {l = Int step}})
    when Big_int.eq_big_int step (Big_int.big_int_of_int 1) ->
      Printf.sprintf
        "sp.range(%s, %s)"
        (expr_to_string ~options a)
        (expr_to_string ~options b)
  | ECons (e1, e2) ->
      Printf.sprintf
        "sp.cons(%s, %s)"
        (expr_to_string ~options e1)
        (expr_to_string ~options e2)
  | EInt_x_or (e1, e2) ->
      Printf.sprintf
        "sp.intXor(%s, %s)"
        (expr_to_string ~options e1)
        (expr_to_string ~options e2)
  | ESum e -> Printf.sprintf "sp.sum(%s)" (expr_to_string ~options e)
  | ERange (e1, e2, e3) ->
      Printf.sprintf
        "sp.range(%s, %s, %s)"
        (expr_to_string ~options e1)
        (expr_to_string ~options e2)
        (expr_to_string ~options e3)
  | EAdd_seconds (e1, e2) ->
      Printf.sprintf
        "sp.add_seconds(%s, %s)"
        (expr_to_string ~options e1)
        (expr_to_string ~options e2)
  | ECheck_signature (e1, e2, e3) ->
      Printf.sprintf
        "sp.check_signature(%s, %s, %s)"
        (expr_to_string ~options e1)
        (expr_to_string ~options e2)
        (expr_to_string ~options e3)
  | EUpdate_map (e1, e2, e3) ->
      Printf.sprintf
        "sp.updateMap(%s, %s, %s)"
        (expr_to_string ~options e1)
        (expr_to_string ~options e2)
        (expr_to_string ~options e3)
  | EContract_data e ->
      Printf.sprintf "sp.contractData(%s)" (expr_to_string ~options e)
  | EScenario_var e ->
      Printf.sprintf "sp.scenario_var(%s)" (expr_to_string ~options e)
  | ERecord l ->
      Printf.sprintf
        "sp.record(%s)"
        (String.concat
           ", "
           (List.map
              (fun (f, e) ->
                Printf.sprintf "%s = %s" f (expr_to_string ~options e))
              l))
  | EList l ->
      Printf.sprintf
        "sp.list([%s])"
        (String.concat ", " (List.map (expr_to_string ~options) l))
  | EMap l ->
      Printf.sprintf
        "{%s}"
        (String.concat
           ", "
           (List.map
              (fun (k, e) ->
                Printf.sprintf
                  "%s : %s"
                  (expr_to_string ~options k)
                  (expr_to_string ~options e))
              l))
  | ESet l ->
      Printf.sprintf
        "sp.set([%s])"
        (String.concat ", " (List.map (expr_to_string ~options) l))
  | EBalance -> "sp.balance"
  | ESender -> "sp.sender"
  | ESource -> "sp.source"
  | EAmount -> "sp.amount"
  | ENow -> "sp.now"
  | EAttr (name, x) -> Printf.sprintf "%s.%s" (expr_to_string ~options x) name
  | EVariant_arg arg_name -> arg_name
  | EOpenVariant ("Some", {e = EIsNat x}) ->
      Printf.sprintf "sp.as_nat(%s)" (expr_to_string ~options x)
  | EOpenVariant ("Some", x) ->
      Printf.sprintf "%s.open_some()" (expr_to_string ~options x)
  | EOpenVariant (name, x) ->
      Printf.sprintf "%s.open_variant('%s')" (expr_to_string ~options x) name
  | EIsVariant ("Some", x) ->
      Printf.sprintf "%s.is_some()" (expr_to_string ~protect:() ~options x)
  | EIsVariant (name, x) ->
      Printf.sprintf "%s.is_variant('%s')" (expr_to_string ~options x) name
  | EVariant ("None", {e = ECst {l = Unit}}) -> "sp.none"
  | EVariant ("Some", x) ->
      Printf.sprintf "sp.some(%s)" (expr_to_string ~options x)
  | EVariant (name, x) ->
      Printf.sprintf "variant('%s', %s)" name (expr_to_string ~options x)
  | EContains (items, member) ->
      prot
        (Printf.sprintf
           "%s.contains(%s)"
           (expr_to_string ~options ~protect:() items)
           (expr_to_string ~options member))
  | EHash_key e -> Printf.sprintf "sp.hash_key(%s)" (expr_to_string ~options e)
  | EHash (algo, e) ->
      Printf.sprintf
        "sp.%s(%s)"
        (String.lowercase_ascii (string_of_hash_algo algo))
        (expr_to_string ~options e)
  | EContract {arg_type; entry_point = None; address} ->
      Printf.sprintf
        "sp.contract(%s, %s)"
        (type_to_string arg_type)
        (expr_to_string ~options address)
  | EContract {arg_type; entry_point = Some ep; address} ->
      Printf.sprintf
        "sp.contract(%s, %s, entry_point='%s')"
        (type_to_string arg_type)
        (expr_to_string ~options address)
        ep
  | ESelf -> "sp.self"
  | ESelf_entry_point _ -> "sp.self_entry_point"
  | EContract_address e ->
      Printf.sprintf "sp.to_address(%s)" (expr_to_string ~options e)
  | EImplicit_account e ->
      Printf.sprintf "sp.implicit_account(%s)" (expr_to_string ~options e)
  | EPair (e1, e2) ->
      Printf.sprintf
        "(%s, %s)"
        (expr_to_string ~options e1)
        (expr_to_string ~options e2)
  | EFirst e -> Printf.sprintf "sp.fst(%s)" (expr_to_string ~options e)
  | ESecond e -> Printf.sprintf "sp.snd(%s)" (expr_to_string ~options e)
  | ESlice {offset; length; buffer} ->
      prot
        (Printf.sprintf
           "sp.slice(%s, %s, %s)"
           (expr_to_string ~options buffer)
           (expr_to_string ~options offset)
           (expr_to_string ~options length))
  | EConcat_list e ->
      Printf.sprintf "sp.concat(%s)" (expr_to_string ~options e)
  | ESize e -> Printf.sprintf "sp.len(%s)" (expr_to_string ~options e)
  | EAccount_of_seed {seed} -> Printf.sprintf "sp.test_account(%S)" seed
  | EMake_signature {secret_key; message; message_format} ->
      Printf.sprintf
        "sp.make_signature(secret_key = %s, message = %s, message_format = %s)"
        (expr_to_string ~options secret_key)
        (expr_to_string ~options message)
        ( match message_format with
        | `Hex -> "'Hex'"
        | `Raw -> "'Raw'" )
  | EMichelson (michelson, exprs) ->
      let mich {name} exprs =
        Printf.sprintf
          "mi.%s(%s)"
          name
          (String.concat ", " (List.map (expr_to_string ~options) exprs))
      in
      ( match (michelson, exprs) with
      | [x], _ -> mich x exprs
      | michs, [] ->
          Printf.sprintf
            "mi.seq(%s)"
            (String.concat ", " (List.map (fun x -> mich x []) michs))
      | michs, _ ->
          Printf.sprintf
            "mi.seq(%s, %s)"
            (String.concat ", " (List.map (fun x -> mich x []) michs))
            (String.concat ", " (List.map (expr_to_string ~options) exprs)) )

let rec command_to_string ?(indent = "") ?(options = Options.string) command =
  let shiftIdent =
    match options with
    | {html = true} -> "&nbsp;&nbsp;"
    | _ -> "  "
  in
  match command.c with
  | CFailwith message ->
      Printf.sprintf
        "%ssp.failwith(%s)"
        indent
        (expr_to_string ~options message)
  | CTransfer {arg; amount; destination} ->
    ( match destination.e with
    | EOpenVariant ("Some", {e = EContract {arg_type = {u = TUnit}; address}})
      ->
        Printf.sprintf
          "%ssp.send(%s, %s)"
          indent
          (expr_to_string ~options address)
          (expr_to_string ~options amount)
    | _ ->
        Printf.sprintf
          "%ssp.transfer(%s, %s, %s)"
          indent
          (expr_to_string ~options arg)
          (expr_to_string ~options destination)
          (expr_to_string ~options amount) )
  | CIf (c, t, e) ->
    ( match options with
    | {html = true} ->
        let ifblock =
          Printf.sprintf
            "%ssp.if %s:<br>%s"
            indent
            (expr_to_string ~options c)
            (command_to_string ~options ~indent:(indent ^ shiftIdent) t)
        in
        let elseblock =
          match e.c with
          | CSeq [] | CSeq [{c = CSeq []}] -> ""
          | _ ->
              Printf.sprintf
                "<br>%ssp.else:<br>%s"
                indent
                (command_to_string ~options ~indent:(indent ^ shiftIdent) e)
        in
        ifblock ^ elseblock
    | _ ->
        let ifblock =
          Printf.sprintf
            "%ssp.if %s:\n%s"
            indent
            (expr_to_string ~options ~protect:() c)
            (command_to_string ~options ~indent:(indent ^ shiftIdent) t)
        in
        let elseblock =
          match e.c with
          | CSeq [] | CSeq [{c = CSeq []}] -> ""
          | _ ->
              Printf.sprintf
                "\n%ssp.else:\n%s"
                indent
                (command_to_string ~options ~indent:(indent ^ shiftIdent) e)
        in
        ifblock ^ elseblock )
  | CMatch (scrutinee, (constructor, arg_name, c)) ->
    ( match options with
    | {html = true} ->
        Printf.sprintf
          "%swith %s.match('%s') as %s:<br>%s"
          indent
          (expr_to_string ~options scrutinee)
          constructor
          arg_name
          (command_to_string ~options ~indent:(indent ^ shiftIdent) c)
    | _ ->
        Printf.sprintf
          "%swith %s.match('%s') as %s:\n%s"
          indent
          (expr_to_string ~options scrutinee)
          constructor
          arg_name
          (command_to_string ~options ~indent:(indent ^ shiftIdent) c) )
  | CSeq [] -> Printf.sprintf "%spass" indent
  | CSeq l ->
    ( match options with
    | {html = true} ->
        String.concat "<br> " (List.map (command_to_string ~options ~indent) l)
    | {html = false} ->
        String.concat "\n" (List.map (command_to_string ~options ~indent) l) )
  | CFor (var, e, c) ->
    ( match options with
    | {html = true} ->
        Printf.sprintf
          "%ssp.for %s in %s:\n<br>%s"
          indent
          (variable_to_string ~options var Iter)
          (expr_to_string e)
          (command_to_string ~options ~indent:(indent ^ shiftIdent) c)
    | _ ->
        Printf.sprintf
          "%ssp.for %s in %s:\n%s"
          indent
          (variable_to_string ~options var Iter)
          (expr_to_string e)
          (command_to_string ~options ~indent:(indent ^ shiftIdent) c) )
  | CWhile (e, c) ->
    ( match options with
    | {html = true} ->
        Printf.sprintf
          "%ssp.while %s:\n<br>%s"
          indent
          (expr_to_string ~options e)
          (command_to_string ~options ~indent:(indent ^ shiftIdent) c)
    | _ -> "while loop" )
  | CDefineLocal ((name, t), e) ->
      Printf.sprintf
        "%s%s = sp.local(%S, %s, %s)"
        indent
        (variable_to_string ~options (name, t) Local)
        (variable_to_string ~options (name, t) Local)
        (expr_to_string ~options e)
        (type_to_string ~options t)
  | CDropLocal name -> Printf.sprintf "%s%s.drop()" indent name
  | CSetVar (s, t) ->
    ( match t.e with
    | ECons (u, s') when s = s' ->
        Printf.sprintf
          "%s%s.push(%s)"
          indent
          (expr_to_string ~options s)
          (expr_to_string ~options u)
    | EBinOpInf (((BAdd | BSub | BMul | BDiv) as op), s', u) when s.e = s'.e ->
        Printf.sprintf
          "%s%s %s= %s"
          indent
          (expr_to_string ~options s)
          (string_of_binOpInfix op)
          (expr_to_string ~options u)
    | _ ->
      ( match s.e with
      | EItem _ | EAttr (_, _) ->
          Printf.sprintf
            "%s%s = %s"
            indent
            (expr_to_string ~options s)
            (expr_to_string ~options t)
      | ELocal _ ->
          Printf.sprintf
            "%s%s = %s"
            indent
            (expr_to_string ~options s)
            (expr_to_string ~options t)
      | _ ->
          Printf.sprintf
            "%s%s.set(%s)"
            indent
            (expr_to_string ~options s)
            (expr_to_string ~options t) ) )
  | CDelItem (expr, item) ->
      Printf.sprintf
        "%s%s %s[%s]"
        indent
        (if options.html then "<span class='keyword'>del</span>" else "del")
        (expr_to_string ~options expr)
        (expr_to_string ~options item)
  | CUpdateSet (expr, element, add) ->
      Printf.sprintf
        "%s%s.%s(%s)"
        indent
        (expr_to_string ~options expr)
        (if add then "add" else "remove")
        (expr_to_string ~options element)
  | CVerify (e, ghost, message) ->
      Printf.sprintf
        "%ssp.%s(%s%s)"
        indent
        (if ghost then "staticVerify" else "verify")
        (expr_to_string ~options e)
        ( match message with
        | None -> ""
        | Some msg ->
            Printf.sprintf ", message = %s" (expr_to_string ~options msg) )
  | CSetDelegate e ->
      Printf.sprintf "%ssp.set_delegate(%s)" indent (expr_to_string e)
  | CLambdaResult e -> Printf.sprintf "%sreturn %s" indent (expr_to_string e)

let unRecordList = function
  | {v = Record l} -> (List.map fst l, [List.map snd l])
  | _data -> (["data"], [])

let html_of_data options data =
  html_of_record_list (unRecordList data) (fun s ->
      value_to_string ~noEmptyList:true ~options s)
  ^
  (*  Value.html_of_record_list (unRecordList data) (fun s -> value_to_string ~noEmptyList:true ~options (!checkImport s)) ^*)
  match options with
  | {html = true; types = true} ->
      type_to_string ~options data.t ^ "<br><br><br>"
  | _ -> ""

let init_html_of_data options (x, v) =
  Printf.sprintf
    "%s = %s"
    (variable_to_string ~options x Storage)
    (value_to_string v)

let init_html_of_data options : tvalue -> string list = function
  | {v = Record l} ->
      List.map (fun (n, v) -> init_html_of_data options ((n, v.t), v)) l
  | {v = Literal {l = Unit}} -> []
  | _ -> assert false

let contract_to_string
    ?(options = Options.string) {balance; storage; entry_points} =
  match options with
  | {html = false; stripped} ->
      let init =
        if stripped
        then ""
        else
          Printf.sprintf
            "  def __init__(self):\n    self.init(%s)\n"
            (String.concat ", " (init_html_of_data options storage))
      in
      Printf.sprintf
        "import smartpy as sp\n\nclass Contract(sp.Contract):\n%s%s"
        init
        (String.concat
           "\n"
           (List.map
              (fun {channel; body} ->
                Printf.sprintf
                  "%s  def %s(self, params):\n%s"
                  (if stripped then "" else "\n  @sp.entry_point\n")
                  channel
                  (command_to_string ~options ~indent:"    " body))
              entry_points))
  | {stripped} ->
      let init =
        if stripped
        then ""
        else
          Printf.sprintf
            "<div class='on'><span class='keyword'>def</span> \
             __init__(self)<span class='keyword'>:</span><div \
             class='indent5'>&nbsp;&nbsp;self.init(%s)</div></div><br>"
            (String.concat
               ",<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
               (init_html_of_data options storage))
      in
      Printf.sprintf
        "<div class='contract'><h3>Balance: %s</h3>\n\
         <h3>Storage:</h3>\n\
         %s\n\
         <h3>Entry points:</h3>\n\
         %s\n\
         %s</div>"
        (ppAmount true balance)
        (html_of_data options storage)
        init
        (String.concat
           "\n<br>"
           (List.map
              (fun {channel; body} ->
                Printf.sprintf
                  "<div class='on'>%s<span class='keyword'>def</span> \
                   %s(<span class='self'>self</span>, params)<span \
                   class='keyword'>:</span><br><div \
                   class='indent5'>%s</div></div>"
                  (if stripped then "" else "@sp.entry_point<br>")
                  channel
                  (command_to_string ~options ~indent:"&nbsp;&nbsp;" body))
              entry_points))

let effect_to_string ?(options = Options.string) effect =
  let open Execution in
  if options.html
  then
    match effect with
    | Sending {arg; destination; amount} ->
        Printf.sprintf
          "<div class='effect'>Sending(%s&lArr;%s,%s)</div>"
          (value_to_string ~options destination)
          (value_to_string ~options amount)
          (value_to_string ~options arg)
    | SetDelegate None -> "<div class='effect'>Remove Delegate</div>"
    | SetDelegate (Some d) ->
        Printf.sprintf
          "<div class='effect'>Set Delegate(%s)</div>"
          (value_to_string ~options d)
    | Map (s, k, v) ->
        Printf.sprintf
          "<div class='effect'>MapEvent:%s(%s &mapsto; %s)</div>"
          (variable_to_string ~options s Storage)
          (value_to_string ~options k)
          (value_to_string ~options v)
  else
    match effect with
    | Sending {arg; destination; amount} ->
        Printf.sprintf
          "Sending(%s, %s, %s)"
          (value_to_string ~options arg)
          (value_to_string ~options destination)
          (value_to_string ~options amount)
    | SetDelegate None -> "Remove Delegate"
    | SetDelegate (Some d) ->
        Printf.sprintf "Set Delegate: %s" (value_to_string ~options d)
    | Map (s, k, v) ->
        Printf.sprintf
          "MapEvent:%s(%s, %s)"
          (variable_to_string ~options s Storage)
          (value_to_string ~options k)
          (value_to_string ~options v)

let error_to_string ?(options = Options.string) effect =
  let open Execution in
  if options.html
  then
    match effect with
    | Exec_error v ->
        Printf.sprintf
          "<div class='error'>Failure: %s</div>"
          (value_to_string ~options v)
    | Exec_channel_not_found s ->
        Printf.sprintf "<div class='error'>ChannelNotFound: '%s'</div>" s
    | Exec_wrong_condition (e, line_no, message) ->
        let message =
          match message with
          | None -> ""
          | Some msg -> Printf.sprintf " [%s]" (value_to_string msg)
        in
        Printf.sprintf
          "<div class='error'>WrongCondition in <button class='text-button' \
           onClick='showLine(%i)'>line %i</button>: %s%s</div>"
          line_no
          line_no
          (expr_to_string ~options e)
          message
  else
    match effect with
    | Exec_error v -> Printf.sprintf "Failure: %s" (value_to_string ~options v)
    | Exec_channel_not_found s -> Printf.sprintf "ChannelNotFound: '%s'" s
    | Exec_wrong_condition (e, line_no, message) ->
        let message =
          match message with
          | None -> ""
          | Some msg -> Printf.sprintf " [%s]" (value_to_string msg)
        in
        Printf.sprintf
          "WrongCondition in line %i: %s%s"
          line_no
          (expr_to_string ~options e)
          message

let ppType html t =
  if html
  then Printf.sprintf "<span class='type'>%s</span>" (type_to_string t)
  else type_to_string t

let ppExpr html e =
  if html
  then
    let pp =
      if 0 <= e.el
      then
        Printf.sprintf
          "<button class='text-button' onClick='showLine(%i)'>%s</button>"
          e.el
      else fun x -> x
    in
    pp (Printf.sprintf "(%s : %s)" (expr_to_string e) (ppType html e.et))
  else
    Printf.sprintf
      "(%s : %s)%s"
      (expr_to_string e)
      (ppType html e.et)
      (if 0 <= e.el then Printf.sprintf " (line %i)" e.el else "")

let rec pp_smart_except html = function
  | `Literal literal -> literal_to_string ~html literal
  | `Value value -> value_to_string value
  | `Expr expr -> ppExpr html expr
  | `Exprs exprs -> String.concat ", " (List.map (ppExpr html) exprs)
  | `Line i ->
      if html
      then
        Printf.sprintf
          "<button class='text-button' onClick='showLine(%i)'>line %i</button>"
          i
          i
      else Printf.sprintf "(line %i)" i
  | `Text s -> s
  | `Type t -> ppType html t
  | `Br -> if html then "<br>" else "\n"
  | `Rec l -> String.concat " " (List.map (pp_smart_except html) l)

let exception_to_string html = function
  | Failure s -> s
  | SmartExcept l -> pp_smart_except html (`Rec l)
  | Yojson.Basic.Util.Type_error (msg, _t) -> "Yojson exception - " ^ msg
  | e -> Printexc.to_string e
