(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Value

type inputGuiResult =
  { gui : string
  ; get : bool -> t }

let textInputGui path ?default nextId initialType cont =
  let id = nextId () in
  let gui = Printf.sprintf "<input type='text' id='%s'>" id in
  let get _withZeros =
    let s = SmartDom.getText id in
    try
      match (s, default) with
      | "", Some x -> cont x
      | x, _ -> cont x
    with
    | exn ->
        let error =
          match exn with
          | Failure error -> Printf.sprintf " (%s)" error
          | _ -> ""
        in
        failwith
          (Printf.sprintf
             "Cannot convert '%s' into %s for path %s%s"
             s
             initialType
             (String.concat "." (List.rev path))
             error)
  in
  {gui; get}

let error s = string s

let parseIntOrNat isNat s =
  let i = Big_int.big_int_of_string s in
  match Typing.intType isNat with
  | `Nat ->
      if Big_int.compare_big_int i Big_int.zero_big_int < 0
      then failwith (Printf.sprintf "%s is not a nat" s);
      nat i
  | `Unknown -> intOrNat (Type.intOrNat ()) i
  | `Int -> int i
  | `Error -> assert false

let rec inputGuiR path nextId initialType =
  match Type.getRepr initialType with
  | TBool ->
      let id = nextId () in
      let gui = Printf.sprintf "<input type='checkbox' id='%s'>" id in
      let get _ = bool (SmartDom.isChecked id) in
      {gui; get}
  | TInt {isNat} -> textInputGui path nextId "int" (parseIntOrNat isNat)
  | TString -> textInputGui path nextId "string" string
  | TBytes -> textInputGui path nextId "bytes" bytes (* TODO *)
  | TToken ->
      textInputGui path nextId "token" (fun i ->
          mutez (Big_int.big_int_of_string i))
  | TKeyHash -> textInputGui path nextId "key_hash" Value.key_hash
  | TTimestamp ->
      textInputGui path nextId "timestamp" (fun i ->
          timestamp (Big_int.big_int_of_string i))
  | TAddress ->
      textInputGui path nextId "address" (fun s ->
          if not
               ( Base.String.is_prefix ~prefix:"tz" s
               || Base.String.is_prefix ~prefix:"KT" s )
          then failwith (Printf.sprintf "Badly formed address '%s'" s);
          address s)
  | TKey -> textInputGui path nextId "key" key
  | TSignature -> textInputGui path nextId "signature" signature
  | TVariant l ->
      let monoSelectionId = nextId () in
      let putRadio i (name, t) =
        let input = inputGuiR (name :: path) nextId t in
        let id = nextId () in
        let gui =
          Printf.sprintf
            "<div id='%s' class='%s'>%s</div>"
            id
            (if i = 0 then "" else "hidden")
            input.gui
        in
        let get x = variant name (input.get x) initialType in
        (name, id, {gui; get})
      in
      let radios = List.mapi putRadio l in
      let gui =
        Printf.sprintf
          "<div class='subtype'><select class='selection' id='%s' \
           onchange=\"setSelectVisibility('%s',%s)\">%s</select>%s</div>"
          monoSelectionId
          monoSelectionId
          (String.concat
             ","
             (List.map
                (fun (name, id, _) -> Printf.sprintf "'%s','%s'" name id)
                radios))
          (String.concat
             ""
             (List.map
                (fun (name, _, _) ->
                  Printf.sprintf
                    "<option value='%s'>%s</option>"
                    name
                    (String.capitalize_ascii name))
                radios))
          (String.concat "" (List.map (fun (_, _, input) -> input.gui) radios))
      in
      let get context =
        let selected = SmartDom.getText monoSelectionId in
        match
          List.find_opt (fun (name, _id, _input) -> name = selected) radios
        with
        | Some (_, _, input) -> input.get context
        | None ->
            error
              ( "Missing constructor "
              ^ selected
              ^ " in "
              ^ Printer.type_to_string initialType )
      in
      {gui; get}
  | TRecord l ->
      let subs =
        List.map (fun (name, t) -> (name, inputGuiR (name :: path) nextId t)) l
      in
      let gui =
        Printer.html_of_record_list
          (List.map fst subs, [List.map (fun (_, input) -> input.gui) subs])
          (fun x -> x)
      in
      let get context =
        record (List.map (fun (name, input) -> (name, input.get context)) subs)
      in
      {gui; get}
  | TUnit -> {gui = ""; get = (fun _ -> unit)}
  | TPair (t1, t2) ->
      let subs =
        List.map
          (fun (name, t) -> (name, inputGuiR (name :: path) nextId t))
          [("1", t1); ("2", t2)]
      in
      let gui =
        Printer.html_of_record_list
          ([], [List.map (fun (_, input) -> input.gui) subs])
          (fun x -> x)
      in
      let get context =
        match List.map (fun (_name, input) -> input.get context) subs with
        | [v1; v2] -> Value.pair v1 v2
        | _ -> assert false
      in
      {gui; get}
  | TUnknown _ ->
      let notImplemented =
        Printf.sprintf
          "InputGui of partial types (%s) not implemented"
          (Printer.type_to_string initialType)
      in
      {gui = notImplemented; get = (fun _ -> error notImplemented)}
  | TAnnots (_, t) -> inputGuiR path nextId t
  | TMap _ | TSet _ | TList _ | TContract _ | TLambda _ | TSecretKey ->
      let notImplemented =
        Printf.sprintf
          "InputGui of %s not implemented"
          (Printer.type_to_string initialType)
      in
      {gui = notImplemented; get = (fun _ -> error notImplemented)}

let inputGuiR ?(path = []) ~nextId initialType =
  inputGuiR path nextId initialType
