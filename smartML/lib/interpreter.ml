(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Utils

type context_ =
  { sender : string
  ; source : string
  ; time : int
  ; amount : Big_int.big_int
  ; line_no : int
  ; debug : bool }

type context = context_ ref

let context ?(sender = "") ?(source = sender) ~time ~amount ~line_no ~debug ()
    =
  ref {sender; source; time; amount; line_no; debug}

let context_sender c = !c.sender

let context_time c = !c.time

let context_line_no c = !c.line_no

let context_debug c = !c.debug

exception WrongCondition of texpr * int * tvalue option
(** Internal exception: raised by {!interpret_command} and caught by {!interpret_contract}. *)

module Primitive_implementations = struct
  type hash = string -> string

  type account =
    { pkh : string
    ; pk : string
    ; sk : string }

  type t =
    { hash_blake2b : hash
    ; hash_sha256 : hash
    ; hash_sha512 : hash
    ; hash_key : string -> string
    ; account_of_seed : string -> account
    ; check_signature : public_key:string -> signature:string -> string -> bool
    ; sign : secret_key:string -> string -> string }

  let faked_in_ocaml =
    let hash prefix s = prefix ^ Digest.string s in
    let fake_pk_prefix = "edpkFake" in
    let fake_pkh_prefix = "tz0Fake" in
    let account_of_seed seed =
      { pkh = fake_pkh_prefix ^ seed
      ; pk = fake_pk_prefix ^ seed
      ; sk = "edskFake" ^ seed }
    in
    let sign ~secret_key message =
      "fakesig" ^ hash "s56" (secret_key ^ message)
    in
    let hash_key s =
      fake_pkh_prefix ^ Base.String.chop_prefix_exn s ~prefix:fake_pk_prefix
    in
    let check_signature ~public_key ~signature bytes =
      Dbg.p
        Format.(
          fun ppf () ->
            pp_open_hovbox ppf 2;
            fprintf ppf "fake-check-signature:";
            pp_print_space ppf ();
            fprintf ppf "pk: %S@ sig: %S@ bytes: %S" public_key signature bytes);
      try
        let secret_key =
          let pos = String.length fake_pk_prefix in
          let seed =
            String.sub public_key pos (String.length public_key - pos)
          in
          (account_of_seed seed).sk
        in
        let redo = sign ~secret_key bytes in
        redo = signature
      with
      | _ -> false
    in
    { hash_blake2b = hash "b2b"
    ; hash_sha256 = hash "s56"
    ; hash_sha512 = hash "s12"
    ; hash_key
    ; account_of_seed
    ; check_signature
    ; sign }

  let blake2b p = p.hash_blake2b

  let sha256 p = p.hash_sha256

  let sha512 p = p.hash_sha512

  let check_signature p = p.check_signature

  let self_test p =
    let open Format in
    let results = ref [] in
    let { hash_blake2b = _
        ; hash_sha256 = _
        ; hash_sha512 = _
        ; hash_key
        ; account_of_seed : string -> account
        ; check_signature :    public_key:string
                            -> signature:string
                            -> string
                            -> bool
        ; sign : secret_key:string -> string -> string } =
      p
    in
    let assert_bool cond s =
      results :=
        ( try if cond () then Ok s else Error s with
        | e -> Error (sprintf "%s -> Exn: %s" s (Printexc.to_string e)) )
        :: !results
    in
    let assertf cond fmt = Format.kasprintf (assert_bool cond) fmt in
    let a1 = account_of_seed "a1" in
    let a2 = account_of_seed "a2" in
    assertf (fun () -> a1 <> a2) "seed works: neq";
    let a3 = account_of_seed "a1" in
    assertf (fun () -> a1 = a3) "seed works: eq";
    let b1 = "hello world!" in
    assertf
      (fun () ->
        let s1 = sign ~secret_key:a1.sk b1 in
        check_signature ~public_key:a1.pk ~signature:s1 b1)
      "sign makes sense: ok";

    assertf
      (fun () ->
        let s1 = sign ~secret_key:a1.sk b1 in
        not (check_signature ~public_key:a2.pk ~signature:s1 b1))
      "sign makes sense: not ok";

    assertf (fun () -> hash_key a1.pk = a1.pkh) "hash_key hashes the key";
    List.rev !results
end

type root =
  | R_storage
  | R_local   of string

type step =
  | S_attr      of string
  | S_item_map  of tvalue
  | S_item_list of int

type path =
  { root : root
  ; steps : step list }

let string_of_step = function
  | S_attr attr -> "." ^ attr
  | S_item_list i -> Printf.sprintf "[%d]" i
  | S_item_map key -> Printf.sprintf "[%s]" (Printer.value_to_string key)

let _string_of_steps steps = String.concat "" (List.map string_of_step steps)

let extend_path {root; steps} steps' = {root; steps = steps @ steps'}

let ( @. ) = Lens.( @. )

type lambda =
  { tParams : Type.t
  ; command : tcommand
  ; expr : texpr }

type variable_value =
  | Iter        of (tvalue * path option)
  | Heap_ref    of tvalue ref
  | Variant_arg of tvalue

type env =
  { parameters : tvalue
  ; primitives : Primitive_implementations.t
  ; scenario_state : scenario_state
  ; context : context_
  ; variables : (string * variable_value) list ref
  ; lambdas : (int * lambda) list ref
  ; storage : tvalue ref
  ; balance : Big_int.big_int ref
  ; effects : Execution.effect list ref
  ; debug : bool
  ; steps : Execution.step list ref }

let lens_heap_ref =
  Lens.bi
    (function
      | Heap_ref x -> x
      | _ -> failwith "not a heap reference")
    (fun x -> Heap_ref x)

let lens_of_root env = function
  | R_storage -> Lens.ref env.storage
  | R_local name ->
      Lens.ref env.variables
      @. Lens.assoc_exn ~equal:( = ) ~key:name ~err:"local not found"
      @. lens_heap_ref
      @. Lens.unref

(** Convert a step into a lens that points to an optional value,
   accompanied by an error message for when the value is missing. *)
let lens_of_step ~line_no = function
  | S_item_list i ->
      ( Value.lens_list_nth i
      , Printf.sprintf "Line %d: Index '%d' out of range." line_no i )
  | S_item_map key ->
      ( Value.lens_map_at ~key
      , Printf.sprintf
          "Line %d: Key '%s' not found."
          line_no
          (Printer.value_to_string key) )
  | S_attr attr ->
      ( Value.lens_record_at ~attr
      , Printf.sprintf "Line %d: Impossible: attribute not found" line_no )

let rec lens_of_steps ~line_no acc = function
  | [s] ->
      let l, err = lens_of_step ~line_no s in
      (acc @. l, err)
  | s :: steps ->
      let l, err = lens_of_step ~line_no s in
      lens_of_steps ~line_no (acc @. l @. Lens.some ~err) steps
  | [] -> (Lens.option ~err:"lens_of_steps", "lens_of_steps")

let lens_of_path ~line_no env {root; steps} =
  let l, err = lens_of_steps ~line_no Lens.id steps in
  (lens_of_root env root @. l, err)

let pack_value v =
  (*
     to generate the documentation for the binary format:
          tezos-codec describe alpha.script.expr binary schema
  *)
  let buf = Buffer.create 42 in
  (* The Michelson-data prefix/watermark: *)
  Buffer.add_string buf "\x05";
  let int8 buf i = Buffer.add_char buf (Char.chr i) in
  let tag buf t = int8 buf t in
  let prim_0_int buf p =
    tag buf 3;
    Buffer.add_char buf (Char.chr p)
  in
  let prim_1_int buf p =
    tag buf 5;
    Buffer.add_char buf (Char.chr p)
  in
  let prim_2_int buf p =
    tag buf 7;
    Buffer.add_char buf (Char.chr p)
  in
  let binary_string buf b =
    let lgth = String.length b in
    Buffer.add_char buf ((lgth lsr 24) land 0xFF |> Char.chr);
    Buffer.add_char buf ((lgth lsr 16) land 0xFF |> Char.chr);
    Buffer.add_char buf ((lgth lsr 8) land 0xFF |> Char.chr);
    Buffer.add_char buf ((lgth lsr 0) land 0xFF |> Char.chr);
    Buffer.add_string buf b
  in
  let michelson = Compiler.compile_value v in
  let z_big_int buf bi =
    (* See tezos: src/lib_data_encoding/binary_writer.ml:150 *)
    (* Dbg.on := true ; *)
    let open Big_int in
    Dbg.f "z_big_int: %a" pp_big_int bi;
    let original_sign =
      if sign_big_int bi = 1 || sign_big_int bi = 0 then 0 else 0b0100_0000
    in
    let absed = abs_big_int bi in
    let rec pack_bits buf how current_bi =
      let n_bits, ones, sign =
        match how with
        | `First_six -> (6, 0b11_1111, original_sign)
        | `Remaing_chunks -> (7, 0b111_1111, 0)
      in
      let n_little = and_big_int current_bi (big_int_of_int ones) in
      let last_one =
        if eq_big_int n_little current_bi then 0 else 0b1000_0000
      in
      Dbg.f "  last: %x , sign: %x, 6: %a" last_one sign pp_big_int n_little;
      int8 buf (last_one lor sign lor int_of_big_int n_little);
      if last_one = 0
      then ()
      else
        pack_bits buf `Remaing_chunks (shift_right_big_int current_bi n_bits)
    in
    pack_bits buf `First_six absed
  in
  let rec pack_michelson buf mich =
    let open Michelson.Literal in
    match mich with
    | Unit -> prim_0_int buf 11
    | Bool true -> prim_0_int buf 10
    | Bool false -> prim_0_int buf 3
    | None _ -> prim_0_int buf 6 (* Untested *)
    | Bytes b ->
        tag buf 10;
        binary_string buf b
    | String s ->
        tag buf 1;
        binary_string buf s
    | Left more ->
        prim_1_int buf 5;
        pack_michelson buf more (* Untested *)
    | Right more ->
        prim_1_int buf 8;
        pack_michelson buf more (* Untested *)
    | Pair (l, r) ->
        prim_2_int buf 7;
        pack_michelson buf l;
        pack_michelson buf r
    | Nat n | Int n | Mutez n ->
        tag buf 0;
        z_big_int buf n
    | Some one ->
        prim_1_int buf 7;
        pack_michelson buf one (* Untested *)
    | Map {elements} ->
        tag buf 2;
        let tmp_buf = Buffer.create 42 in
        Base.List.iter elements ~f:(fun (k, v) ->
            prim_2_int tmp_buf 4 (* Elt  *);
            pack_michelson tmp_buf k;
            pack_michelson tmp_buf v);
        binary_string buf (Buffer.contents tmp_buf)
    | Set {elements} | List {elements} ->
        tag buf 2;
        let tmp_buf = Buffer.create 42 in
        Base.List.iter elements ~f:(fun el -> pack_michelson tmp_buf el);
        binary_string buf (Buffer.contents tmp_buf)
  in
  pack_michelson buf michelson;
  Buffer.contents buf

let interpret_expr ?primitives env scenario_state =
  let env e =
    match env with
    | `Env env -> env
    | `NoEnv no_env ->
        raise
          (SmartExcept
             [`Text "Missing environment"; `Br; `Expr e; `Br; `Rec no_env])
  in
  let rec interp e =
    let with_primitives f =
      match primitives with
      | None ->
          failwith
            (Printf.sprintf
               "Missing crypto primitives for evaluating %s"
               (Printer.expr_to_string e))
      | Some primitives -> f primitives
    in
    match e.e with
    | EParams _ -> (env e).parameters
    | EStorage -> !((env e).storage)
    | ELocal (name, t) ->
      ( match List.assoc_opt name !((env e).variables) with
      | Some (Heap_ref {contents = x}) ->
          Typing.assertEqual t x.t ~pp:(fun () ->
              [ `Text (Printf.sprintf "Bad get data or param [%s]" name)
              ; `Type t
              ; `Text "is not"
              ; `Type x.t
              ; `Line e.el ]);
          x
      | _ -> failwith ("Not a local: " ^ name) )
    | ECst x -> Value.literal x x.t
    | EBinOpInf (BEq, x, y) -> Value.bool (Value.equal (interp x) (interp y))
    | EBinOpInf (BNeq, x, y) ->
        Value.bool (not (Value.equal (interp x) (interp y)))
    | EBinOpInf (BAdd, x, y) -> Value.plus (interp x) (interp y)
    | EBinOpInf (BMul, x, y) -> Value.mul (interp x) (interp y)
    | EBinOpInf (BMod, x, y) -> Value.e_mod (interp x) (interp y)
    | EBinOpInf (BDiv, x, y) -> Value.div (interp x) (interp y)
    | EBinOpInf (BSub, x, y) -> Value.minus (interp x) (interp y)
    | EBinOpInf (BLt, x, y) -> Value.bool (Value.lt (interp x) (interp y))
    | EBinOpInf (BLe, x, y) -> Value.bool (Value.le (interp x) (interp y))
    | EBinOpInf (BGt, y, x) -> Value.bool (Value.lt (interp x) (interp y))
    | EBinOpInf (BGe, y, x) -> Value.bool (Value.le (interp x) (interp y))
    | EBinOpInf (BAnd, x, y) ->
        Value.bool (Value.unBool (interp x) && Value.unBool (interp y))
    | EBinOpInf (BOr, x, y) ->
        Value.bool (Value.unBool (interp x) || Value.unBool (interp y))
    | EBinOpInf (BEDiv, x, y) -> Value.ediv (interp x) (interp y)
    | ENot x -> Value.bool (not (Value.unBool (interp x)))
    | EAbs x -> Value.nat (Big_int.abs_big_int (Value.unInt (interp x)))
    | EToInt x -> Value.int (Value.unInt (interp x))
    | EIsNat x ->
        let x = Value.unInt (interp x) in
        if 0 <= Big_int.compare_big_int x Big_int.zero_big_int
        then Value.some (Value.nat x)
        else Value.none (Type.nat ())
    | ENeg x -> Value.int (Big_int.minus_big_int (Value.unInt (interp x)))
    | ESign x ->
        let x = Value.unInt (interp x) in
        let result = Big_int.compare_big_int x Big_int.zero_big_int in
        Value.int (Big_int.big_int_of_int result)
    | ESplit_tokens (mutez, quantity, total) ->
        Value.mutez
          (Big_int.div_big_int
             (Big_int.mult_big_int
                (Value.unMutez (interp mutez))
                (Value.unInt (interp quantity)))
             (Value.unInt (interp total)))
    | ECons (x, y) -> Value.cons (interp x) (interp y)
    | EInt_x_or (x, y) -> Value.intXor (interp x) (interp y)
    | EBinOpPre (BMax, x, y) ->
        Value.intOrNat
          x.et
          (max (Value.unInt (interp x)) (Value.unInt (interp y)))
    | EBinOpPre (BMin, x, y) ->
        Value.intOrNat
          x.et
          (min (Value.unInt (interp x)) (Value.unInt (interp y)))
    | ERecord l -> Value.record (List.map (fun (s, e) -> (s, interp e)) l)
    | ESender -> Value.address (env e).context.sender
    | ESource -> Value.address (env e).context.source
    | ENow -> Value.timestamp (Big_int.big_int_of_int (env e).context.time)
    | EAmount -> Value.mutez (env e).context.amount
    | EBalance -> Value.mutez !((env e).balance)
    | EItem (items, key, def) ->
        let def =
          match def with
          | None -> None
          | Some def -> Some (interp def)
        in
        Value.getItem (interp items) (interp key) def ~pp:(fun () ->
            Printf.sprintf
              "%s[%s]"
              (Printer.expr_to_string items)
              (Printer.expr_to_string key))
    | ESum l ->
      ( match (interp l).v with
      | List l | Set l ->
          let {v; t} =
            List.fold_left
              (fun x y -> Value.plus_inner x y)
              (Value.zero_of_type e.et)
              l
          in
          Value.build v t
      | Map l ->
          let {v; t} =
            List.fold_left
              (fun x (_, y) -> Value.plus_inner x y)
              (Value.zero_of_type e.et)
              l
          in
          Value.build v t
      | _ -> failwith ("bad sum " ^ Printer.expr_to_string e) )
    | ERange (a, b, step) ->
        let item = a.et in
        ( match ((interp a).v, (interp b).v, (interp step).v) with
        | Literal {l = Int a}, Literal {l = Int b}, Literal {l = Int step} ->
            let a = Big_int.int_of_big_int a in
            let b = Big_int.int_of_big_int b in
            let step = Big_int.int_of_big_int step in
            if step = 0
            then failwith (Printf.sprintf "Range with 0 step")
            else if step * (b - a) < 0
            then Value.list [] item
            else
              let rec aux a acc =
                if (b - a) * step <= 0
                then List.rev acc
                else
                  aux
                    (a + step)
                    (Value.intOrNat item (Big_int.big_int_of_int a) :: acc)
              in
              Value.list (aux a []) item
        | _ -> failwith ("bad range" ^ Printer.expr_to_string e) )
    | EAttr (name, x) ->
      ( match (interp x).v with
      | Record l ->
        ( match List.assoc_opt name l with
        | Some v -> v
        | None ->
            failwith
              (Printf.sprintf
                 "Missing field in record [%s] [%s] [%s]."
                 name
                 (Printer.expr_to_string e)
                 (String.concat ", " (List.map fst l))) )
      | _ -> failwith ("Not a record " ^ Printer.expr_to_string e) )
    | EIsVariant (name, x) ->
      ( match (interp x).v with
      | Variant (cons, _) -> Value.bool (cons = name)
      | _ ->
          failwith
            (Printf.sprintf
               "Not a variant %s %s"
               (Printer.expr_to_string e)
               (Printer.value_to_string (interp x))) )
    | EOpenVariant (name, x) ->
      ( match (interp x).v with
      | Variant (cons, v) ->
          if cons <> name
          then
            failwith
              (Printf.sprintf
                 "Not the proper variant constructor [%s] != [%s] in [%s]."
                 name
                 cons
                 (Printer.expr_to_string e))
          else v
      | _ ->
          failwith
            (Printf.sprintf
               "Not a variant %s %s"
               (Printer.expr_to_string e)
               (Printer.value_to_string (interp x))) )
    | EVariant (name, x) -> Value.variant name (interp x) e.et
    | EIter (name, _) ->
      ( match List.assoc_opt name !((env e).variables) with
      | Some (Iter (v, _)) -> v
      | _ ->
          failwith
            (Printf.sprintf
               "Missing var %s in env [%s]"
               name
               (String.concat
                  "; "
                  (List.map (fun (x, _) -> x) !((env e).variables)))) )
    | EListRev inner ->
        let l = interp inner in
        ( match (l.v, Type.getRepr inner.et) with
        | Basics.List l, TList t -> Value.list (List.rev l) t
        | _ -> assert false )
    | EListItems (inner, rev) ->
        let l = interp inner in
        ( match (l.v, Type.getRepr inner.et) with
        | Basics.Map l, TMap {key; item} ->
            Value.list
              ((if rev then List.rev_map else List.map)
                 (fun (k, v) -> Value.record [("key", k); ("value", v)])
                 l)
              (Typing.key_value key item)
        | _ -> assert false )
    | EListKeys (inner, rev) ->
        let l = interp inner in
        ( match (l.v, Type.getRepr inner.et) with
        | Basics.Map l, TMap {key} ->
            Value.list ((if rev then List.rev_map else List.map) fst l) key
        | _ -> assert false )
    | EListValues (inner, rev) ->
        let l = interp inner in
        ( match (l.v, Type.getRepr inner.et) with
        | Basics.Map l, TMap {item} ->
            Value.list ((if rev then List.rev_map else List.map) snd l) item
        | _ -> assert false )
    | EListElements (inner, rev) ->
        let l = interp inner in
        ( match (l.v, Type.getRepr inner.et) with
        | Basics.Set l, TSet telement ->
            Value.list (if rev then List.rev l else l) telement
        | _ -> assert false )
    | EAdd_seconds (t, s) ->
      ( match ((interp t).v, (interp s).v) with
      | Literal {l = Timestamp t}, Literal {l = Int s} ->
          Value.timestamp (Big_int.add_big_int t s)
      | _ -> failwith ("Cannot add timestamp " ^ Printer.expr_to_string e) )
    | EContains (items, member) ->
        let member = interp member in
        ( match (interp items).v with
        | Map l ->
            Value.bool (List.exists (fun (x, _) -> Value.equal x member) l)
        | Set l -> Value.bool (List.exists (fun x -> Value.equal x member) l)
        | _ -> failwith ("Cannot compute " ^ Printer.expr_to_string e) )
    | EPack e -> Value.bytes (pack_value (interp e))
    | ESlice {offset; length; buffer} ->
        Basics.(
          ( match ((interp offset).v, (interp length).v, (interp buffer).v) with
          | ( Literal {l = Int ofs_bi; _}
            , Literal {l = Int len_bi; _}
            , Literal {l = String s; _} ) ->
            ( try
                Value.some
                  (Value.string
                     (String.sub
                        s
                        (Big_int.int_of_big_int ofs_bi)
                        (Big_int.int_of_big_int len_bi)))
              with
            | _ -> Value.none Type.string )
          | ( Literal {l = Int ofs_bi; _}
            , Literal {l = Int len_bi; _}
            , Literal {l = Bytes s; _} ) ->
            ( try
                Value.some
                  (Value.bytes
                     (String.sub
                        s
                        (Big_int.int_of_big_int ofs_bi)
                        (Big_int.int_of_big_int len_bi)))
              with
            | _ -> Value.none Type.bytes )
          | _ ->
              Format.kasprintf
                failwith
                "Error while slicing string/bytes: %s"
                (Printer.expr_to_string e) ))
    | EConcat_list l ->
        let vals =
          match (interp l).v with
          | List sl ->
              Base.List.map sl ~f:(fun sb ->
                  match sb.v with
                  | Literal {l = String s; _} | Literal {l = Bytes s; _} -> s
                  | _ ->
                      Format.kasprintf
                        failwith
                        "Error while concatenating string/bytes:@ not a list \
                         of strings/bytes:@ %s"
                        (Printer.expr_to_string e))
          | _ ->
              Format.kasprintf
                failwith
                "Error while concatenating string/bytes:@ not a list:@ %s"
                (Printer.expr_to_string e)
        in
        ( match Type.getRepr e.et with
        | TString -> Value.string (String.concat "" vals)
        | TBytes -> Value.bytes (String.concat "" vals)
        | _ ->
            raise
              (SmartExcept
                 [ `Text "Bad type in concat (should apply to lists of "
                 ; `Type Type.string
                 ; `Text "or"
                 ; `Type Type.bytes
                 ; `Text ")"
                 ; `Expr e
                 ; `Br
                 ; `Line e.el ]) )
    | ESize s ->
        let result length = Value.nat (Big_int.big_int_of_int length) in
        ( match (interp s).v with
        | Literal {l = String s; _} | Literal {l = Bytes s; _} ->
            result (String.length s)
        | List l | Set l -> result (List.length l)
        | Map l -> result (List.length l)
        | _ ->
            raise
              (SmartExcept [`Text "Bad type for sp.len"; `Expr s; `Line s.el])
        )
    | EHash (algo, e) ->
        let v = interp e in
        let as_string =
          match v.v with
          | Literal {l = Bytes b} -> b
          | Literal _ | Record _
           |Variant (_, _)
           |List _ | Set _ | Map _ | Pair _ ->
              Format.kasprintf
                failwith
                "Type-error: calling hash:%s(%s) on non-bytes value."
                (String.lowercase_ascii (string_of_hash_algo algo))
                (Printer.value_to_string v)
        in
        let hash =
          with_primitives (fun primitives ->
              let hash =
                match algo with
                | BLAKE2B -> Primitive_implementations.blake2b
                | SHA256 -> Primitive_implementations.sha256
                | SHA512 -> Primitive_implementations.sha512
              in
              hash primitives as_string)
        in
        Value.bytes hash
    | ECheck_signature (pk_expr, sig_expr, msg_expr) ->
        Dbg.on := false;
        let public_key =
          (interp pk_expr).v
          |> function
          | Literal {l = Key k} -> k
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: check_signature expects a key, not: %s"
                (Printer.expr_to_string e)
        in
        let signature =
          (interp sig_expr).v
          |> function
          | Literal {l = Signature s} -> s
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: check_signature expects a signature, not: %s"
                (Printer.expr_to_string e)
        in
        let msg =
          (interp msg_expr).v
          |> function
          | Literal {l = Bytes b} -> b
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: check_signature expects bytes (3rd argument), \
                 not: %s"
                (Printer.expr_to_string e)
        in
        Value.bool
          (Primitive_implementations.check_signature
             (env e).primitives
             ~public_key
             ~signature
             msg)
    | EHash_key pk_expr ->
        let public_key =
          (interp pk_expr).v
          |> function
          | Literal {l = Key k} -> k
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: hash_key expects a key, not: %s"
                (Printer.expr_to_string e)
        in
        with_primitives (fun p -> Value.key_hash (p.hash_key public_key))
    | EMap entries ->
        Value.buildMap
          (List.fold_left
             (fun entries (key, value) ->
               let key = interp key in
               (key, interp value)
               :: Base.List.Assoc.remove ~equal:Value.equal entries key)
             []
             entries)
          e.et
    | EList items -> Value.build (List (List.map interp items)) e.et
    | EContract_data id ->
        let id = Big_int.int_of_big_int (Value.unInt (interp id)) in
        ( match Hashtbl.find_opt scenario_state.contracts id with
        | Some t -> (t : tcontract).storage
        | None ->
            raise
              (SmartExcept
                 [ `Text "Missing contract for id: "
                 ; `Text (string_of_int id)
                 ; `Br
                 ; `Line e.el ]) )
    | EScenario_var id ->
        let id = Big_int.int_of_big_int (Value.unInt (interp id)) in
        ( match Hashtbl.find_opt scenario_state.variables id with
        | Some x -> x
        | None ->
            raise
              (SmartExcept
                 [ `Text "Missing scenario variable for id: "
                 ; `Text (string_of_int id)
                 ; `Br
                 ; `Line e.el ]) )
    | ESet l ->
      begin
        match Type.getRepr e.et with
        | TSet t -> Value.buildSet (List.map interp l) t
        | _ -> assert false
      end
    | EContract {arg_type = _; address} -> Value.some (interp address)
    | EPair (e1, e2) -> Value.pair (interp e1) (interp e2)
    | EVariant_arg n ->
      ( match List.assoc_opt n !((env e).variables) with
      | Some (Variant_arg x) -> x
      | _ -> failwith (Printf.sprintf "Not a variant argument: %s" n) )
    | ECallLambda _ -> Value.unit
    | ELambda _ ->
        (*{id; name = _; tParams; command; expr}*)
        (*lambdas := (id, {tParams; command; expr}) :: !lambdas;*)
        Value.unit
        (* TODO *)
    | ELambdaParams {id = _; name = _; tParams = _} -> assert false
    | EReduce x -> interp x
    | EAccount_of_seed {seed} ->
        with_primitives (fun p ->
            let account = p.Primitive_implementations.account_of_seed seed in
            let seed = Value.string seed in
            let pkh = Value.key_hash account.pkh in
            let address = Value.address account.pkh in
            let pk = Value.key account.pk in
            let sk = Value.secret_key account.sk in
            Value.record
              [ ("seed", seed)
              ; ("address", address)
              ; ("public_key", pk)
              ; ("public_key_hash", pkh)
              ; ("secret_key", sk) ])
    | EMake_signature {secret_key; message; message_format} ->
        let secret_key =
          (interp secret_key).v
          |> function
          | Literal {l = Secret_key k} -> k
          | _ ->
              raise
                (SmartExcept
                   [ `Text "Type error in secret key"
                   ; `Expr e
                   ; `Expr secret_key
                   ; `Line e.el ])
        in
        let message =
          (interp message).v
          |> function
          | Literal {l = Bytes k} ->
            ( match message_format with
            | `Raw -> k
            | `Hex ->
                Utils.Hex.unhex
                  Base.(
                    String.chop_prefix k ~prefix:"0x"
                    |> Option.value ~default:k) )
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: make_signature expects bytes, not: %s"
                (Printer.expr_to_string e)
        in
        with_primitives (fun p ->
            p.Primitive_implementations.sign ~secret_key message
            |> Value.signature)
    | EFirst x -> fst (Value.unpair (interp x))
    | ESecond x -> snd (Value.unpair (interp x))
    | ESelf | ESelf_entry_point _ | EUnpack _ | EUpdate_map _ | EMichelson _
     |EContract_address _ | EImplicit_account _ ->
        failwith
          ( "Interpreter. Instruction not supported in interpreter: "
          ^ Printer.expr_to_string e )
  in
  interp

let interpret_expr_external ~primitives ~no_env ~scenario_state =
  interpret_expr ~primitives (`NoEnv no_env) scenario_state

let interpret_expr env =
  interpret_expr ~primitives:env.primitives (`Env env) env.scenario_state

let path_of_expr env =
  let rec of_expr acc e =
    match e.e with
    | EStorage -> Some {root = R_storage; steps = acc}
    | ELocal (name, _) -> Some {root = R_local name; steps = acc}
    | EItem (cont, key, None) ->
        let key = interpret_expr env key in
        ( match Type.getRepr cont.et with
        | TMap _ -> of_expr (S_item_map key :: acc) cont
        | _ ->
            raise
              (SmartExcept
                 [ `Text "Interpreter Error"
                 ; `Br
                 ; `Text "GetItem"
                 ; `Expr e
                 ; `Expr cont
                 ; `Text "is not a map"
                 ; `Line e.el ]) )
    | EAttr (name, expr) -> of_expr (S_attr name :: acc) expr
    | EIter (name, _) ->
      ( match List.assoc name !(env.variables) with
      | Iter (_, Some p) -> Some (extend_path p acc)
      | Iter (_, None) -> None (* Iteratee not an l-expression. *)
      | _ -> failwith "not an iter" )
    | _ -> None
  in
  of_expr []

let addStep ?(sub_steps = ref []) upperSteps env c _elements =
  let buildStep () =
    { Execution.iters =
        Base.List.filter_map
          ~f:(function
            | n, Iter (v, _) -> Some (n, (v, None))
            | _ -> None)
          !(env.variables)
    ; locals =
        Base.List.filter_map
          ~f:(function
            | n, Heap_ref x -> Some (n, !x)
            | _ -> None)
          !(env.variables)
    ; storage = !(env.storage)
    ; balance = !(env.balance)
    ; effects = [] (*env.effects*)
    ; command = c
    ; substeps = sub_steps }
  in
  if env.debug then upperSteps := buildStep () :: !upperSteps

let add_variable name value vars =
  match List.assoc_opt name vars with
  | Some _ -> failwith ("Variable '" ^ name ^ "' already defined")
  | None -> (name, value) :: vars

let rec interpret_command
    upper_steps env ({line_no} as initialCommand : tcommand) =
  match initialCommand.c with
  | CFailwith message ->
      failwith
        (Printf.sprintf
           "Failure [%s] in line %i"
           (Printer.value_to_string (interpret_expr env message))
           line_no)
  | CIf (c, t, e) ->
      let sub_steps = ref [] in
      let condition = interpret_expr env c in
      if Value.unBool condition
      then interpret_command sub_steps env t
      else interpret_command sub_steps env e;
      addStep
        ~sub_steps
        upper_steps
        env
        initialCommand
        [("condition", condition)]
  | CMatch (scrutinee, (constructor, arg_name, body)) ->
      let sub_steps = ref [] in
      ( match (interpret_expr env scrutinee).v with
      | Variant (cons, arg) when cons = constructor ->
          interpret_command
            sub_steps
            { env with
              variables =
                ref (add_variable arg_name (Variant_arg arg) !(env.variables))
            }
            body;
          addStep
            ~sub_steps
            upper_steps
            env
            initialCommand
            [("match", scrutinee)]
      | Variant _ -> ()
      | _ -> assert false )
  | CSeq l -> List.iter (fun x -> interpret_command upper_steps env x) l
  | CTransfer {destination; arg; amount} ->
      let destination = interpret_expr env destination in
      let arg = interpret_expr env arg in
      let amount = interpret_expr env amount in
      env.effects :=
        Execution.Sending {arg; destination; amount} :: !(env.effects);
      ( match amount.v with
      | Literal {l = Mutez amount} ->
          env.balance := Big_int.sub_big_int !(env.balance) amount
      | _ -> assert false );
      addStep upper_steps env initialCommand []
  | CSetVar (lhs, rhs) ->
      ( match path_of_expr env lhs with
      | None ->
          failwith
            (Printf.sprintf
               "Line %d: Cannot assign to '%s'"
               initialCommand.line_no
               (Printer.expr_to_string lhs))
      | Some lhs ->
          let lhs, _err = lens_of_path ~line_no env lhs in
          let rhs = Some (interpret_expr env rhs) in
          Lens.set lhs rhs () );
      addStep upper_steps env initialCommand []
  | CDelItem (map, key) ->
      ( match path_of_expr env map with
      | None ->
          failwith
            (Printf.sprintf
               "Line %d: Cannot assign to '%s'"
               line_no
               (Printer.expr_to_string map))
      | Some path ->
          let key = interpret_expr env key in
          let l, _err =
            lens_of_path ~line_no env (extend_path path [S_item_map key])
          in
          Lens.set l None () );
      addStep upper_steps env initialCommand []
  | CUpdateSet (set, elem, add) ->
      ( match path_of_expr env set with
      | None ->
          failwith
            (Printf.sprintf
               "Line %d: Cannot remove from '%s'"
               line_no
               (Printer.expr_to_string set))
      | Some path ->
          let elem = interpret_expr env elem in
          let l, err = lens_of_path ~line_no env path in
          Lens.set (l @. Lens.some ~err @. Value.lens_set_at ~elem) add () );
      addStep upper_steps env initialCommand []
  | CDefineLocal ((name, _), e) ->
      env.variables :=
        add_variable
          name
          (Heap_ref (ref (interpret_expr env e)))
          !(env.variables);

      addStep upper_steps env initialCommand []
  | CDropLocal name ->
      env.variables := List.filter (fun (n, _) -> n <> name) !(env.variables);
      addStep upper_steps env initialCommand []
  | CFor ((name, _), iteratee, body) ->
      let sub_steps = ref [] in
      let iteratee_v = interpret_expr env iteratee in
      let iteratee_l = path_of_expr env iteratee in
      ( match iteratee_v.v with
      | List elems ->
          let step i v =
            let path =
              Base.Option.map iteratee_l ~f:(fun p ->
                  extend_path p [S_item_list i])
            in
            interpret_command
              sub_steps
              { env with
                variables =
                  ref (add_variable name (Iter (v, path)) !(env.variables)) }
              body
          in
          List.iteri step elems
      | _ -> assert false );
      addStep ~sub_steps upper_steps env initialCommand []
  | CWhile (e, cmd) ->
      let sub_steps = ref [] in
      while Value.unBool (interpret_expr env e) do
        interpret_command sub_steps env cmd
      done;
      addStep ~sub_steps upper_steps env initialCommand []
  | CVerify (x, _, message) ->
      let v = interpret_expr env x in
      addStep upper_steps env initialCommand [];
      ( match v.v with
      | Literal {l = Bool true} -> ()
      | Literal {l = Bool false} ->
          raise
            (WrongCondition
               (x, line_no, Base.Option.map ~f:(interpret_expr env) message))
      | _ ->
          failwith
            (Printf.sprintf
               "Failed condition [%s] in line %i"
               (Printer.value_to_string v)
               line_no) )
  | CSetDelegate e ->
      env.effects :=
        Execution.SetDelegate (Value.unOption (interpret_expr env e))
        :: !(env.effects)
  | CLambdaResult _e -> failwith "LAMBDA RESULT"

let interpret_message
    ~primitives
    ~scenario_state
    context
    {balance; storage; entry_points; tparameter; partialContract}
    {channel; params} =
  let filtered_entry_points =
    List.filter (fun {channel = x} -> channel = x) entry_points
  in
  match filtered_entry_points with
  | [] -> (None, [], Some (Execution.Exec_channel_not_found channel), [])
  | _ :: _ :: _ -> failwith "Too many channels"
  | [{channel = dt; paramsType; body = e}] ->
      Typing.assertEqual params.t paramsType ~pp:(fun () ->
          [`Text "Bad params type"; `Value params; `Line !context.line_no]);

      (* fix ? *)
      ( match Value.checkType dt storage with
      | Some _ as error -> (None, [], error, [])
      | None ->
          let env =
            { parameters = params
            ; storage = ref storage
            ; variables = ref []
            ; lambdas = ref []
            ; primitives
            ; scenario_state
            ; context = !context
            ; balance = {contents = Big_int.add_big_int !context.amount balance}
            ; effects = {contents = []}
            ; debug = !context.debug
            ; steps = ref [] }
          in
          ( try
              interpret_command env.steps env e;
              ( Some
                  ( { balance = !(env.balance)
                    ; storage = !(env.storage)
                    ; tparameter
                    ; entry_points
                    ; partialContract
                    ; flags = [] }
                  , e )
              , !(env.effects)
              , None
              , !(env.steps) )
            with
          | WrongCondition (c, line_no, message) ->
              ( None
              , []
              , Some (Execution.Exec_wrong_condition (c, line_no, message))
              , !(env.steps) )
          | Failure f ->
              ( None
              , []
              , Some (Execution.Exec_error (Value.string f))
              , !(env.steps) ) ) )

let reducer ~primitives ~scenario_state ~line_no x =
  Expr.of_value
    (interpret_expr_external
       ~primitives
       ~no_env:[`Text "sp.reduce"; `Expr x; `Br; `Line line_no]
       ~scenario_state
       x)
