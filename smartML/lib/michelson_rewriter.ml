(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** Michelson-to-michelson code simplification. *)

open Michelson

(** {1 Helpers} *)

let rec strip_common_prefix eq xs ys =
  match (xs, ys) with
  | x :: xs, y :: ys when eq x y -> strip_common_prefix eq xs ys
  | _ -> (xs, ys)

let strip_common_suffix eq xs ys =
  let xs', ys' = strip_common_prefix eq (List.rev xs) (List.rev ys) in
  (List.rev xs', List.rev ys')

type 'a ppable_list = 'a list [@@deriving show {with_path = false}]

let pp_no_breaks f () =
  let fs = Format.pp_get_formatter_out_functions f () in
  Format.pp_set_formatter_out_functions
    f
    {fs with out_newline = (fun () -> ()); out_indent = (fun _ -> ())}

let map_fst f (x, y) = (f x, y)

let _map_snd f (x, y) = (x, f y)

(** {1 Rule sets} *)

let wrap = List.map (fun instr -> {instr})

let unwrap = List.map (fun {instr} -> instr)

let unwrapped_eq = equal_instr_f equal_instr

let to_seq = function
  | MIseq is -> unwrap is
  | i -> [i]

let of_seq = function
  | [i] -> i
  | is -> MIseq (wrap is)

type unwrapped_seq = instr instr_f list

type rule_set =
  { name : string
  ; rule_f :
         unwrapped_seq
      -> unwrapped_seq
      -> unwrapped_seq option * unwrapped_seq option }

let default_options x y = function
  | None, Some y -> Some (x, y)
  | Some x, None -> Some (x, y)
  | Some x, Some y -> Some (x, y)
  | None, None -> None

let _trace_rule_set {name; rule_f} =
  { name
  ; rule_f =
      (fun trail front ->
        let r = rule_f trail front in
        ( match default_options trail front (rule_f trail front) with
        | None -> ()
        | Some (trail', front') ->
            let trail_old, trail_new =
              strip_common_suffix unwrapped_eq trail trail'
            in
            let front_old, front_new =
              strip_common_suffix unwrapped_eq front front'
            in
            let instrs_old = List.rev trail_old @ front_old in
            let instrs_new = List.rev trail_new @ front_new in
            let pp f xs = pp_ppable_list pp_instr_rectypes f (wrap xs) in
            Format.printf
              "%a%s:    %a\n%s  -> %a\n"
              pp_no_breaks
              ()
              name
              pp
              instrs_old
              (String.make (String.length name) ' ')
              pp
              instrs_new );
        r) }

let rewrite x = (None, Some x)

let _rewrite_trail x = (Some x, None)

let rewrite_trail_and_front x y = (Some x, Some y)

let rewrite_none = (None, None)

(** {1 Rule helpers} *)

let mk_rule_set name rule_f = {name; rule_f}

let _seqw instrs = MIseq (wrap instrs)

let useqw instrs = {instr = MIseq (wrap instrs)}

let useq instrs = {instr = MIseq instrs}

let cAr = MIfield [A]

let cDr = MIfield [D]

(** {1 Our concrete rule sets} *)

(** Does the instruction operate only on the top of the stack? *)
let is_mono = function
  | MIfield _ -> true
  | _ -> false

let is_bin = function
  | MIadd | MImul | MIsub | MIand | MIor -> true
  | _ -> false

let rec fails = function
  | MIfailwith -> true
  | MIseq xs ->
    ( match Base.List.last xs with
    | Some x -> fails x.instr
    | None -> false )
  | MIif (l, r) | MIif_left (l, r) | MIif_some (l, r) ->
      fails l.instr && fails r.instr
  | MImap x | MIiter x | MIloop x | MIdip x | MIdipn (_, x) -> fails x.instr
  | _ -> false

(** Does the instruction push something on top of the stack, without
   modying or looking at anything beneath? *)
let is_pure_push = function
  | MInil _ -> true
  | MIpush _ -> true
  | MIsender -> true
  | MIamount -> true
  | MInow -> true
  | _ -> false

(** Does the instruction push something on top of the stack, without
   modying anything beneath? *)
let rec is_pushy = function
  | MIdup -> true
  | MIseq s -> Base.Option.is_some (drop_pushy_seq_rev (unwrap (List.rev s)))
  | MIif (a, b) -> is_pushy a.instr && is_pushy b.instr
  | MIif_left (a, b) -> is_pushy a.instr && is_pushy b.instr
  | MIif_some (a, b) -> is_pushy a.instr && is_pushy b.instr
  | x -> is_pure_push x
  [@@deriving eq, show {with_path = false}, map]

and drop_pushy_seq_rev = function
  | MIswap :: MIdip p :: rest when is_pushy p.instr -> Some rest
  | instr :: rest when is_pushy instr -> Some rest
  (* A more thorough analysis could reveal other sequences that behave
     like PUSH. *)
  | _ -> None

let unfold_macros =
  (* _trace_rule_set @@ *)
  mk_rule_set "unfold_macros" (fun _trail ->
    function
    (* Unfold UNPAIR: *)
    | MIunpair :: rest ->
        rewrite (MIdup :: MIfield [D] :: MIswap :: MIfield [A] :: rest)
    (* Unfold SET_C[AD]+R: *)
    | MIsetField [A] :: rest -> rewrite (cDr :: MIswap :: MIpair :: rest)
    | MIsetField [D] :: rest -> rewrite (cAr :: MIpair :: rest)
    | MIsetField (A :: ops) :: rest ->
        rewrite
          ( MIdup
          :: MIdip (useqw [cAr; MIsetField ops])
          :: cDr
          :: MIswap
          :: MIpair
          :: rest )
    | MIsetField (D :: ops) :: rest ->
        rewrite
          ( MIdup
          :: MIdip (useqw [cDr; MIsetField ops])
          :: cAr
          :: MIpair
          :: rest )
    | _ -> rewrite_none)

let fold_macros_etc =
  (* _trace_rule_set @@ *)
  mk_rule_set "fold_macros_etc" (fun _trail ->
    function
    (* PUSH-PUSH and PUSH-DUP appear to have the same cost gas-wise,
     but for the latter storage is cheaper: *)
    | MIpush (t1, l1) :: MIpush (t2, l2) :: rest
      when equal_mtype t1 t2 && Literal.equal l1 l2 ->
        rewrite (MIpush (t1, l1) :: MIdup :: rest)
    | MIpush ({mt = MToption t}, Literal.None _) :: rest ->
        rewrite (MInone t :: rest)
    | _ -> rewrite_none)

let simplify =
  let open Big_int in
  (* _trace_rule_set @@ *)
  mk_rule_set "simplify" (fun trail ->
    function
    | MIcomment a :: MIcomment b :: rest when a = b ->
        rewrite (MIcomment b :: rest)
    (* Flatten sequences: *)
    | MIseq is :: rest -> rewrite (unwrap is @ rest)
    (* Superfluous SWAP: *)
    | MIswap :: MIswap :: rest -> rewrite rest
    | MIdup :: MIswap :: rest -> rewrite (MIdup :: rest)
    (* DROP after creation/modification: *)
    | MIdrop :: rest ->
      ( match drop_pushy_seq_rev trail with
      | Some trail' -> rewrite_trail_and_front trail' rest
      | None -> rewrite_none )
    | mono :: MIdrop :: rest when is_mono mono -> rewrite (MIdrop :: rest)
    (* Remove DIPs: *)
    | MIdip {instr = MIdrop} :: rest -> rewrite (MIswap :: MIdrop :: rest)
    | MIdip {instr = MIseq []} :: rest -> rewrite rest
    | MIdip i1 :: MIdip i2 :: rest -> rewrite (MIdip (useq [i1; i2]) :: rest)
    | MIdup :: MIdip {instr} :: rest when is_mono instr ->
        rewrite (MIdup :: instr :: MIswap :: rest)
    (* Push literals: *)
    | MIpush (t, l) :: MIsome :: rest ->
        rewrite (MIpush (mt_option t, Literal.Some l) :: rest)
    | MIpush (tl, x) :: MIleft tr :: rest ->
        rewrite (MIpush (mt_or tl tr, Literal.Left x) :: rest)
    | MIpush (tr, x) :: MIright tl :: rest ->
        rewrite (MIpush (mt_or tl tr, Literal.Right x) :: rest)
    | MIpush (t2, l2) :: MIpush (t1, l1) :: MIpair :: rest ->
        rewrite (MIpush (mt_pair t1 t2, Literal.Pair (l1, l2)) :: rest)
    | MIpush (ts, Literal.List {t; elements})
      :: MIpush (_, l1) :: MIcons :: rest ->
        rewrite
          (MIpush (ts, Literal.List {t; elements = l1 :: elements}) :: rest)
    (* Pairs *)
    | MIpair :: MIfield [D] :: rest -> rewrite (MIdrop :: rest)
    | MIpair :: MIfield [A] :: rest -> rewrite (MIswap :: MIdrop :: rest)
    | MIfield op1 :: MIfield op2 :: rest ->
        rewrite (MIfield (op1 @ op2) :: rest)
    (* DIP after DUP: *)
    | MIdup :: MIdip {instr = MIdup} :: rest -> rewrite (MIdup :: MIdup :: rest)
    | MIdup :: MIdip {instr = MIdrop} :: rest -> rewrite rest
    (* Commutative operations: *)
    | MIswap :: comBin :: rest when is_commutative comBin ->
        rewrite (comBin :: rest)
    | MIswap :: MIcompare :: MIeq :: rest -> rewrite (MIcompare :: MIeq :: rest)
    | MIswap :: MIcompare :: MIneq :: rest ->
        rewrite (MIcompare :: MIneq :: rest)
    | MIswap :: MIcompare :: MIlt :: rest -> rewrite (MIcompare :: MIgt :: rest)
    | MIswap :: MIcompare :: MIgt :: rest -> rewrite (MIcompare :: MIlt :: rest)
    (* Bubble up DROP: *)
    | mono :: MIswap :: MIdrop :: rest when is_mono mono ->
        rewrite (MIswap :: MIdrop :: mono :: rest)
    | push :: MIswap :: MIdrop :: rest when is_pure_push push ->
        rewrite (MIdrop :: push :: rest)
    | MIswap :: MIdrop :: MIdrop :: rest -> rewrite (MIdrop :: MIdrop :: rest)
    | MIdig i :: MIdrop :: MIdrop :: rest when i >= 1 ->
        rewrite (MIdrop :: MIdig (i - 1) :: MIdrop :: rest)
    | MIdip i :: MIdrop :: rest -> rewrite (MIdrop :: i.instr :: rest)
    (* Bubble up DIP: *)
    | mono :: MIdip i :: rest when is_mono mono ->
        rewrite (MIdip i :: mono :: rest)
    | p :: MIdip {instr} :: rest when is_pure_push p ->
        rewrite (instr :: p :: rest)
    (* Bubble up SWAP: *)
    | p :: MIswap :: mono :: rest when is_mono mono && is_pure_push p ->
        rewrite (mono :: p :: MIswap :: rest)
    | m1 :: MIswap :: m2 :: MIswap :: rest when is_mono m1 && is_mono m2 ->
        rewrite (MIswap :: m2 :: MIswap :: m1 :: rest)
    (* DIG & DUG: *)
    | (MIdig 0 | MIdug 0) :: rest -> rewrite rest
    | (MIdig 1 | MIdug 1) :: rest -> rewrite (MIswap :: rest)
    | MIdig n1 :: MIdug n2 :: rest when n1 = n2 -> rewrite rest
    | MIdug n1 :: MIdig n2 :: rest when n1 = n2 -> rewrite rest
    | MIdup :: MIdug n :: MIdrop :: rest when n > 0 ->
        rewrite (MIdug (n - 1) :: rest)
    | MIdup :: MIdip {instr = MIswap} :: rest ->
        rewrite (MIdup :: MIdug 2 :: rest)
    | push :: MIswap :: MIdup :: MIdug 2 :: rest when is_pure_push push ->
        rewrite (MIdup :: push :: MIswap :: rest)
    | MIdup :: push :: bin :: MIswap :: MIdrop :: rest
      when is_pure_push push && is_bin bin ->
        rewrite (push :: bin :: rest)
    (* Conditionals: *)
    | MIif ({instr = MIseq []}, {instr = MIseq []}) :: rest ->
        rewrite (MIdrop :: rest)
    | MInot :: MIif (a, b) :: rest -> rewrite (MIif (b, a) :: rest)
    | MIif
        ( {instr = MIseq [{instr = MIpush ({mt = MTbool}, Literal.Bool b)}]}
        , {instr = x} )
      :: MInot :: rest ->
        rewrite
          ( MIif
              ( { instr =
                    MIseq [{instr = MIpush (mt_bool, Literal.Bool (not b))}] }
              , {instr = MIseq [{instr = x}; {instr = MInot}]} )
          :: rest )
    | MIif
        ( {instr = x}
        , {instr = MIseq [{instr = MIpush ({mt = MTbool}, Literal.Bool b)}]} )
      :: MInot :: rest ->
        rewrite
          ( MIif
              ( {instr = MIseq [{instr = x}; {instr = MInot}]}
              , { instr =
                    MIseq [{instr = MIpush (mt_bool, Literal.Bool (not b))}] }
              )
          :: rest )
    | MIif_some ({instr = MIseq [{instr = MIdrop}]}, {instr = MIseq []})
      :: rest ->
        rewrite (MIdrop :: rest)
    | MIif_left
        ({instr = MIseq [{instr = MIdrop}]}, {instr = MIseq [{instr = MIdrop}]})
      :: rest ->
        rewrite (MIdrop :: rest)
    | MIif_left
        ( { instr =
              MIseq
                [ {instr = MIdrop}
                ; {instr = MIpush ({mt = MTbool}, Literal.Bool b1)} ] }
        , { instr =
              MIseq
                [ {instr = MIdrop}
                ; {instr = MIpush ({mt = MTbool}, Literal.Bool b2)} ] } )
      :: MIif (x, y) :: rest
      when b1 = not b2 ->
        rewrite
          ( MIif_left
              ( {instr = MIseq [{instr = MIdrop}; (if b1 then x else y)]}
              , {instr = MIseq [{instr = MIdrop}; (if b1 then y else x)]} )
          :: rest )
    | MIif_some
        ( { instr =
              MIseq
                [ {instr = MIdrop}
                ; {instr = MIpush ({mt = MTbool}, Literal.Bool b1)} ] }
        , {instr = MIseq [{instr = MIpush ({mt = MTbool}, Literal.Bool b2)}]}
        )
      :: MIif (x, y) :: rest
      when b1 = not b2 ->
        rewrite
          ( MIif_some
              ( {instr = MIseq [{instr = MIdrop}; (if b1 then x else y)]}
              , if b1 then y else x )
          :: rest )
    (* Constant folding: *)
    | MIpush ({mt = MTint}, Literal.Int a)
      :: MIpush ({mt = MTint}, Literal.Int b) :: MIcompare :: rest ->
        rewrite (MIpush (mt_int, Literal.int (compare_big_int b a)) :: rest)
    | MIpush ({mt = MTnat}, Literal.Nat a)
      :: MIpush ({mt = MTnat}, Literal.Nat b) :: MIcompare :: rest ->
        rewrite (MIpush (mt_int, Literal.int (compare_big_int b a)) :: rest)
    | MIpush ({mt = MTint}, Literal.Int a) :: MIeq :: rest ->
        rewrite
          (MIpush (mt_bool, Literal.Bool (eq_big_int a zero_big_int)) :: rest)
    | MIpush ({mt = MTint}, Literal.Int a) :: MIlt :: rest ->
        rewrite
          (MIpush (mt_bool, Literal.Bool (lt_big_int a zero_big_int)) :: rest)
    | MIpush ({mt = MTint}, Literal.Int a) :: MIgt :: rest ->
        rewrite
          (MIpush (mt_bool, Literal.Bool (gt_big_int a zero_big_int)) :: rest)
    | MIpush ({mt = MTbool}, Literal.Bool true) :: MIif (a, _) :: rest ->
        rewrite (a.instr :: (if fails a.instr then [] else rest))
    | MIpush ({mt = MTbool}, Literal.Bool false) :: MIif (_, b) :: rest ->
        rewrite (b.instr :: (if fails b.instr then [] else rest))
    | MIfailwith :: _ :: _ -> rewrite [MIfailwith]
    | MIpush (t, Literal.Map {elements})
      :: MIpush (_, Literal.Some value) :: MIpush (_, key) :: MIupdate :: rest
      ->
        let cmp_key (k1, _) (k2, _) = Literal.compare k1 k2 in
        let elements =
          List.sort
            cmp_key
            ( (key, value)
            :: Base.List.Assoc.remove elements ~equal:Literal.equal key )
        in
        rewrite (MIpush (t, Literal.Map {elements}) :: rest)
    | MIpush (t, Literal.Set {elements})
      :: MIpush (_, Literal.Bool true) :: MIpush (_, key) :: MIupdate :: rest
      ->
        let elements =
          List.sort
            Literal.compare
            (key :: List.filter (fun x -> not (Literal.equal key x)) elements)
        in
        rewrite (MIpush (t, Literal.Set {elements}) :: rest)
    (* Pushing the same thing twice (will be unfolded again): *)
    | MIpush (t, l) :: MIdup :: rest ->
        rewrite (MIpush (t, l) :: MIpush (t, l) :: rest)
    | _ -> rewrite_none)

(** {1 Normalization machinery} *)

module type CHANGING = sig
  type 'a t = 'a * bool

  include Base.Applicative.S with type 'a t := 'a t

  include Base.Monad.S with type 'a t := 'a t

  val fixpoint : ('a -> 'a t) -> 'a -> 'a t
end

module Changing : CHANGING = struct
  module Basic = struct
    type 'a t = 'a * bool

    let return x = (x, false)

    let apply (f, changed) (x, changed') = (f x, changed || changed')

    let map = `Custom (fun (x, changed) ~f -> (f x, changed))

    let bind (x, changed) ~f =
      let y, changed' = f x in
      (y, changed || changed')
  end

  include Basic
  include Base.Applicative.Make (Basic)
  include Base.Monad.Make (Basic)

  (** Compute a fixpoint of a function that tells us whether its output
      differs from its input. *)
  let fixpoint f =
    let rec fix changed x =
      let y, changed' = f x in
      if changed' then fix true y else (x, changed)
    in
    fix false
end

(** Apply the given function to each node in the tree, bottom-up. *)
let bottom_up_once transform =
  let module C = Traversable (Changing) in
  let open Changing.Monad_infix in
  let rec up {instr} =
    C.traverse up instr
    >>= fun instr -> transform instr >>| fun instr -> {instr}
  in
  fun instr ->
    let {instr}, changed = up {instr} in
    (instr, changed)

(** Performs one pass of simplification on an instruction. Opens only
   a top-level `MIseq` instsruction, does not go deeper.  *)
let rewrite_flat rules =
  let rec rewrite acc changed = function
    | [] -> (List.rev acc, changed)
    | i :: is as is0 ->
      ( match default_options acc is0 (rules.rule_f acc is0) with
      | None -> rewrite (i :: acc) changed is
      | Some (a :: acc, is) ->
          (* Rewind by one instruction, so as to catch more redexes in a single pass: *)
          rewrite acc true (a :: is)
      | Some ([], is) -> rewrite [] true is )
  in
  function
  | MIseq [x] -> (x.instr, true)
  | i ->
      let is, changed = rewrite [] false (to_seq i) in
      (of_seq is, changed)

let normalize_flat rules = Changing.fixpoint (rewrite_flat rules)

(** One recursive pass of rewriting on an instruction, bottom-up. *)
let rewrite rules = bottom_up_once (rewrite_flat rules)

(** Two different strategies: *)
let _normalize1 rules =
  Changing.fixpoint (bottom_up_once (normalize_flat rules))

let _normalize2 rules = Changing.fixpoint (rewrite rules)

let normalize = _normalize1

let normalize_instr rules {instr} =
  map_fst (fun instr -> {instr}) (normalize rules instr)
