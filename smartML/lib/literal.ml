(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils

type u =
  | Unit
  | Bool       of bool
  | Int        of Big_int.big_int
  | String     of string
  | Bytes      of string
  | Timestamp  of Big_int.big_int
  | Mutez      of Big_int.big_int
  | Address    of string
  | Key        of string
  | Secret_key of string
  | Key_hash   of string
  | Signature  of string
[@@deriving eq, ord, show {with_path = false}]

type t =
  { l : u
  ; t : Type.t }
[@@deriving show {with_path = false}]

let map_type f {l; t} = {l; t = f t}

let equal {l = l1} {l = l2} = equal_u l1 l2

let compare {l = l1} {l = l2} = compare_u l1 l2

let build l t = {l; t}

let unit = build Unit Type.unit

let bool x = build (Bool x) Type.bool

let int x = build (Int x) (Type.int ())

let nat x = build (Int x) (Type.nat ())

let intOrNat t x =
  match Type.getRepr t with
  | TInt _ -> build (Int x) t
  | _ -> assert false

let string s = build (String s) Type.string

let bytes s = build (Bytes s) Type.bytes

let timestamp i = build (Timestamp i) Type.timestamp

let mutez i = build (Mutez i) Type.token

let address s = build (Address s) Type.address

let key s = build (Key s) Type.key

let secret_key s = build (Secret_key s) Type.secret_key

let key_hash s = build (Key_hash s) Type.key_hash

let signature s = build (Signature s) Type.signature

let unBool = function
  | {l = Bool b} -> b
  | _ -> failwith "Not a bool"

let unInt = function
  | {l = Int b} -> b
  | _ -> failwith "Not a int"
