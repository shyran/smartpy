(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** Compiler from {!Basics.tcontract} to Michelson. *)

open Michelson

val mtype_of_type : Type.t -> mtype

val compile_value : Basics.tvalue -> Literal.t
(** Compile a value into a Michelson literal. *)

val michelson_contract : Basics.tcontract -> Michelson_contract.t
(** Compile a smart-contract to the Michelson intermediate representation. *)

val michelson_storage : Basics.tcontract -> string
(** Compile a smart contract storage to a string. *)

val michelson_contract_michelson : Michelson_contract.t -> string

val michelson_contract_micheline : Michelson_contract.t -> string
