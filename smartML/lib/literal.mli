(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type t = private
  { l : u
  ; t : Type.t }
[@@deriving eq, ord, show]

and u = private
  | Unit
  | Bool       of bool
  | Int        of Big_int.big_int
  | String     of string
  | Bytes      of string
  | Timestamp  of Big_int.big_int
  | Mutez      of Big_int.big_int
  | Address    of string
  | Key        of string
  | Secret_key of string
  | Key_hash   of string
  | Signature  of string
[@@deriving eq, ord, show]

val map_type : (Type.t -> Type.t) -> t -> t

val unit : t

val bool : bool -> t

val int : Big_int.big_int -> t

val nat : Big_int.big_int -> t

val intOrNat : Type.t -> Big_int.big_int -> t

val string : string -> t

val bytes : string -> t

val timestamp : Big_int.big_int -> t

val mutez : Big_int.big_int -> t

val address : string -> t

val key : string -> t

val secret_key : string -> t

val key_hash : string -> t

val signature : string -> t

val unBool : t -> bool

val unInt : t -> Big_int.big_int
