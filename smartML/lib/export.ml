(* Copyright 2019-2020 Smart Chain Arena LLC. *)

let rec export_type t =
  match Type.getRepr t with
  | TBool -> "bool"
  | TString -> "string"
  | TInt {isNat} ->
    ( match !(Type.getRef isNat) with
    | UnUnknown _ -> "intOrNat"
    | UnValue true -> "nat"
    | UnValue false -> "int"
    | UnRef _ -> assert false )
  | TTimestamp -> "timestamp"
  | TBytes -> "bytes"
  | TRecord l ->
      Printf.sprintf
        "(record %s)"
        (String.concat
           " "
           (List.map
              (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
              l))
  | TVariant l ->
      Printf.sprintf
        "(variant %s)"
        (String.concat
           " "
           (List.map
              (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
              l))
  | TSet element -> Printf.sprintf "(set %s)" (export_type element)
  | TMap {key; item} ->
      Printf.sprintf "(map %s %s)" (export_type key) (export_type item)
      (* TODO big ? *)
  | TToken -> "mutez"
  | TUnit -> "unit"
  | TAddress -> "address"
  | TContract t -> Printf.sprintf "(contract %s)" (export_type t)
  | TKeyHash -> "key_hash"
  | TKey -> "key"
  | TSecretKey -> "secret_key"
  | TSignature -> "signature"
  | TUnknown {contents = UExact t} -> export_type t
  | TUnknown {contents = UUnknown _} -> "unknown"
  | TUnknown {contents = URecord l} -> export_type (Type.record l)
  | TUnknown {contents = UVariant l} -> export_type (Type.record l)
  | TUnknown {contents = UItems _} ->
      (*export_type (build (TItems l))*) "ITEMS"
  | TPair (t1, t2) ->
      Printf.sprintf "(pair %s %s)" (export_type t1) (export_type t2)
  | TAnnots (a, t) ->
      Printf.sprintf
        "(annots %s (%s))"
        (export_type t)
        (String.concat "" (List.map (Printf.sprintf "%S") a))
  | TList t -> Printf.sprintf "(list %s)" (export_type t)
  | TLambda (t1, t2) ->
      Printf.sprintf "(lambda %s %s)" (export_type t1) (export_type t2)
