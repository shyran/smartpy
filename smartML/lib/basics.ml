(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
module Literal = Literal

type vClass =
  | Storage
  | Local
  | Param
  | Iter

type 'v record_f = (string * 'v) list [@@deriving show {with_path = false}]

let sort_record r =
  Base.List.sort r ~compare:(fun (lbl1, _) (lbl2, _) -> compare lbl1 lbl2)

let equal_record_f eq_val x y =
  let eq_entry (lbl1, val1) (lbl2, val2) = lbl1 = lbl2 && eq_val val1 val2 in
  Base.List.equal eq_entry (sort_record x) (sort_record y)

let compare_record_f cmp_val x y =
  let cmp_entry (lbl1, val1) (lbl2, val2) =
    match compare lbl1 lbl2 with
    | 0 -> cmp_val val1 val2
    | c -> c
  in
  Base.List.compare cmp_entry (sort_record x) (sort_record y)

type 'v value_f =
  | Literal of Literal.t
  | Record  of 'v record_f
  | Variant of string * 'v
  | List    of 'v list
  | Set     of 'v list
  | Map     of ('v * 'v) list
  | Pair    of 'v * 'v
[@@deriving eq, ord, show]

type uvalue = tvalue value_f

and tvalue =
  { v : uvalue
  ; t : Type.t }
[@@deriving show {with_path = false}]

(** Equality (igoring [tvalue.t]). *)
let rec equal_tvalue {v = v1} {v = v2} = equal_value_f equal_tvalue v1 v2

type binOpInfix =
  | BNeq
  | BEq
  | BAnd
  | BOr
  | BAdd
  | BSub
  | BDiv
  | BEDiv
  | BMul
  | BMod
  | BLt
  | BLe
  | BGt
  | BGe
[@@deriving show {with_path = false}]

type binOpPrefix =
  | BMax
  | BMin
[@@deriving show {with_path = false}]

type 't inline_michelson =
  { name : string
  ; typesIn : 't list
  ; typesOut : 't list }
[@@deriving show {with_path = false}, eq]

type hash_algo =
  | BLAKE2B
  | SHA256
  | SHA512
[@@deriving show {with_path = false}]

let string_of_hash_algo = function
  | BLAKE2B -> "BLAKE2B"
  | SHA256 -> "SHA256"
  | SHA512 -> "SHA512"

type texpr =
  { e : uexpr
  ; el : int
  ; et : Type.t }
[@@deriving show {with_path = false}]

and uexpr =
  | EItem             of texpr * texpr * texpr option
  | EStorage
  | EParams           of Type.t
  | ELocal            of Type.tvariable
  | EAttr             of string * texpr
  | EVariant_arg      of string
  | EOpenVariant      of string * texpr
  | EVariant          of string * texpr
  | EIsVariant        of string * texpr
  | EIter             of Type.tvariable
  | EPair             of texpr * texpr
  | EFirst            of texpr
  | ESecond           of texpr
  | EListRev          of texpr
  | EListItems        of texpr * bool
  | EListKeys         of texpr * bool
  | EListValues       of texpr * bool
  | EListElements     of texpr * bool
  | EPack             of texpr
  | EUnpack           of texpr * Type.t
  | ECst              of Literal.t
  | EBinOpInf         of binOpInfix * texpr * texpr
  | EBinOpPre         of binOpPrefix * texpr * texpr
  | ENot              of texpr
  | EAbs              of texpr
  | EToInt            of texpr
  | EIsNat            of texpr
  | ENeg              of texpr
  | ESign             of texpr
  | EContains         of texpr * texpr
  | ERecord           of (string * texpr) list
  | EList             of texpr list
  | EMap              of (texpr * texpr) list
  | ESet              of texpr list
  | EHash             of hash_algo * texpr
  | EHash_key         of texpr
  | EMichelson        of Type.t inline_michelson list * texpr list
  | ECallLambda       of texpr * texpr
  | ELambda           of
      { id : int
      ; name : string
      ; tParams : Type.t
      ; command : tcommand
      ; tResult : Type.t }
  | ELambdaParams     of
      { id : int
      ; name : string
      ; tParams : Type.t }
  | EBalance
  | ESender
  | ESource
  | ENow
  | EAmount
  | EContract         of
      { entry_point : string option
      ; arg_type : Type.t
      ; address : texpr }
  | ESelf
  | ESelf_entry_point of string * Type.t
  | EContract_address of texpr
  | EImplicit_account of texpr
  | ESlice            of
      { offset : texpr (* nat *)
      ; length : texpr (* nat *)
      ; buffer : texpr }
  | EConcat_list      of texpr
  | ESize             of texpr
  | EAccount_of_seed  of {seed : string}
  | ESplit_tokens     of texpr * texpr * texpr
  | ECons             of texpr * texpr
  | EInt_x_or         of texpr * texpr
  | ESum              of texpr
  | ERange            of texpr * texpr * texpr
  | EAdd_seconds      of texpr * texpr
  | ECheck_signature  of texpr * texpr * texpr
  | EContract_data    of texpr
  | EScenario_var     of texpr
  | EUpdate_map       of texpr * texpr * texpr
  | EReduce           of texpr
  | EMake_signature   of
      { secret_key : texpr
      ; message : texpr
      ; message_format : [ `Raw | `Hex ] }
[@@deriving show {with_path = false}]

and tcommand =
  { c : ucommand
  ; line_no : int
  ; has_operations : bool }
[@@deriving show {with_path = false}]

and ucommand =
  | CFailwith     of texpr
  | CVerify       of texpr * bool (* ghost *) * texpr option (* message *)
  | CIf           of texpr * tcommand * tcommand
  | CMatch        of texpr * (string * string * tcommand)
  | CDefineLocal  of Type.tvariable * texpr
  | CDropLocal    of string
  | CSetVar       of texpr * texpr
  | CDelItem      of texpr * texpr
  | CUpdateSet    of texpr * texpr * bool
  | CSeq          of tcommand list
  | CTransfer     of
      { arg : texpr
      ; amount : texpr
      ; destination : texpr }
  | CFor          of Type.tvariable * texpr * tcommand
  | CWhile        of texpr * tcommand
  | CSetDelegate  of texpr
  | CLambdaResult of texpr
[@@deriving show {with_path = false}]

type tmessage =
  { channel : string
  ; params : tvalue }
[@@deriving show {with_path = false}]

type importConstraint =
  | HasAdd       of texpr * texpr * texpr
  | HasSub       of texpr * texpr * texpr
  | IsComparable of texpr
  | HasGetItem   of texpr * texpr
  | HasContains  of texpr * texpr
  | HasSize      of texpr
  | HasSlice     of texpr

type entry_point =
  { channel : string
  ; paramsType : Type.t
  ; body : tcommand }
[@@deriving show {with_path = false}]

type flag = Unit_failwith [@@deriving show]

type 'a tcontract_ =
  { balance : Big_int.big_int
  ; storage : 'a
  ; tparameter : Type.t
  ; entry_points : entry_point list
  ; partialContract : bool
  ; flags : flag list }
[@@deriving show]

type tcontract = tvalue tcontract_ [@@deriving show]

module Execution = struct
  type error =
    | Exec_error             of tvalue
    | Exec_channel_not_found of string
    | Exec_wrong_condition   of texpr * int * tvalue option
  [@@deriving show {with_path = false}]

  type effect =
    | Sending     of
        { arg : tvalue
        ; destination : tvalue
        ; amount : tvalue }
    | SetDelegate of tvalue option
    | Map         of Type.tvariable * tvalue * tvalue
  [@@deriving show {with_path = false}]

  type step =
    { command : tcommand
    ; iters : (string * (tvalue * string option)) list
    ; locals : (string * tvalue) list
    ; storage : tvalue
    ; balance : Big_int.big_int
    ; effects : string list
    ; substeps : step list ref }
  [@@deriving show {with_path = false}]

  type 'html exec_message =
    { ok : bool
    ; contract : tcontract option
    ; effects : string list
    ; error : error option
    ; html : 'html
    ; storage : tvalue
    ; steps : step list }
  [@@deriving show {with_path = false}]
end

type smart_except =
  [ `Expr of texpr
  | `Exprs of texpr list
  | `Value of tvalue
  | `Literal of Literal.t
  | `Line of int
  | `Text of string
  | `Type of Type.t
  | `Br
  | `Rec of smart_except list
  ]

exception SmartExcept of smart_except list

let setEqualUnknownOption ~pp r1 r2 =
  let r1 = Type.getRef r1 in
  let r2 = Type.getRef r2 in
  if r1 != r2
  then
    match (!r1, !r2) with
    | UnUnknown _, _ -> r1 := UnRef r2
    | _, UnUnknown _ -> r2 := UnRef r1
    | UnValue a, UnValue b when a = b -> if !r1 != !r2 then r1 := UnRef r2
    | _ -> raise (SmartExcept (pp ()))

type scenario_state =
  { contracts : (int, tcontract) Hashtbl.t
  ; contract_data_types : (int, Type.t) Hashtbl.t
  ; variables : (int, tvalue) Hashtbl.t }

let scenario_state () =
  { contracts = Hashtbl.create 5
  ; contract_data_types = Hashtbl.create 5
  ; variables = Hashtbl.create 5 }
