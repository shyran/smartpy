(* Copyright 2019-2020 Smart Chain Arena LLC. *)

module Literal_bak = Literal
open Michelson
module Literal = Literal_bak

(* Something that represents a value. *)
module type VALUE_DOMAIN = sig
  type t

  val _local : string -> Type.t -> t

  val _storage : Type.t -> t

  val of_literal : Michelson.Literal.t -> t

  val mul : t -> t -> t

  val add : t -> t -> t

  val sub : t -> t -> t

  val ediv : t -> t -> t

  val abs : t -> t

  val neg : t -> t

  val vnot : t -> t

  val is_nat : t -> t

  val to_int : t -> t

  val vand : t -> t -> t

  val vor : t -> t -> t

  val eq : t -> t -> t

  val neq : t -> t -> t

  val le : t -> t -> t

  val lt : t -> t -> t

  val ge : t -> t -> t

  val gt : t -> t -> t

  val compare : t -> t -> t

  val some : t -> t

  val pair : t -> t -> t

  val left : t -> t

  val right : t -> t

  val cons : t -> t -> t

  val _concat_bin : t -> t -> t

  val _concat_list : t -> t

  val update : t -> t -> t -> t

  val mem : t -> t -> t

  val get : t -> t -> t

  val now : t

  val balance : t

  val amount : t

  val sender : t

  val source : t

  val size : t -> t

  val slice : t -> t -> t -> t

  val field : ad_step list -> t -> t
end

let type_of_mtype =
  cata_mtype (function
      | MTunit -> Type.unit
      | MTbool -> Type.bool
      | MTnat -> Type.nat ()
      | MTint -> Type.int ()
      | MTmutez -> Type.token
      | MTstring -> Type.string
      | MTbytes -> Type.bytes
      | MTtimestamp -> Type.timestamp
      | MTaddress -> Type.address
      | MTkey -> Type.key
      | MTkey_hash -> Type.key_hash
      | MTsignature -> Type.signature
      | MToperation -> Type.unit (* FIXME *)
      | MToption t -> Type.option t
      | MTlist t -> Type.list t
      | MTset t -> Type.set t
      | MTcontract t -> Type.contract t
      | MTpair (t, u) -> Type.pair t u
      | MTor (t, u) -> Type.tor t u
      | MTlambda (t, u) -> Type.lambda t u
      | MTmap (t, u) -> Type.map (ref (Type.UnValue false)) t u
      | MTbig_map (t, u) -> Type.map (ref (Type.UnValue true)) t u
      | MTannot (a, t, _) -> Type.annots [a] t
      | MTmissing _ -> failwith "type_of_mtype: MTmissing")

module Expr_domain : VALUE_DOMAIN with type t = Typing.env -> Basics.texpr =
struct
  type t = Typing.env -> Basics.texpr

  let line_no = -1

  let _local n t _env = Expr.local ~line_no n t

  let _storage t _env = Expr.storage ~line_no t

  let rec of_literal = function
    | Michelson.Literal.Nat x -> fun _env -> Expr.cst ~line_no (Literal.nat x)
    | Michelson.Literal.Int x -> fun _env -> Expr.cst ~line_no (Literal.int x)
    | Michelson.Literal.List {t; elements} ->
        fun env ->
          Expr.build_list
            ~line_no:(-1)
            ~tvalue:(type_of_mtype t)
            ~elems:(List.map (fun x -> of_literal x env) elements)
    | x -> failwith ("TODO of_literal " ^ Michelson.Literal.show x)

  let mul x y env = Expr.mul ~line_no env (x env) (y env)

  let add x y env = Expr.add ~line_no env (x env) (y env)

  let sub x y env = Expr.sub ~line_no env (x env) (y env)

  let ediv x y env = Expr.ediv ~line_no (x env) (y env)

  let abs x env = Expr.absE ~line_no (x env)

  let neg x env = Expr.negE ~line_no (x env)

  let vnot x env = Expr.notE ~line_no (x env)

  let is_nat x env = Expr.is_nat ~line_no (x env)

  let to_int x env = Expr.to_int ~line_no (x env)

  let vand x y env = Expr.b_and ~line_no (x env) (y env)

  let vor x y env = Expr.b_or ~line_no (x env) (y env)

  let eq x y env = Expr.eq ~line_no env (x env) (y env)

  let neq x y env = Expr.neq ~line_no env (x env) (y env)

  let le x y env = Expr.le ~line_no env (x env) (y env)

  let lt x y env = Expr.lt ~line_no env (x env) (y env)

  let ge x y env = Expr.ge ~line_no env (x env) (y env)

  let gt x y env = Expr.gt ~line_no env (x env) (y env)

  let compare _env = failwith "TODO"

  let some x env = Expr.uvariant ~line_no env "some" (x env)

  let pair x y env = Expr.pair ~line_no (x env) (y env)

  let left x env = Expr.uvariant ~line_no env "left" (x env)

  let right x env = Expr.uvariant ~line_no env "right" (x env)

  let cons x y env = Expr.cons ~line_no (y env) (x env)

  let car x env =
    match Basics.((x env).e) with
    | EPair (x, _) -> x
    | _ -> Expr.first ~line_no env (x env)

  let cdr x env =
    match Basics.((x env).e) with
    | EPair (_, x) -> x
    | _ -> Expr.second ~line_no env (x env)

  let _concat_bin _env = failwith "TODO"

  let _concat_list _env = failwith "TODO"

  let update _env = failwith "TODO"

  let mem _env = failwith "TODO"

  let get _env = failwith "TODO"

  let now _env = Expr.now

  let balance _env = Expr.balance

  let amount _env = Expr.amount

  let sender _env = Expr.sender

  let source _env = Expr.source

  let size x env = Expr.size ~line_no env (x env)

  let slice offset length buffer env =
    Expr.slice
      ~line_no
      env
      ~offset:(offset env)
      ~length:(length env)
      ~buffer:(buffer env)

  let rec field p x =
    match p with
    | [] -> x
    | A :: p -> field p (car x)
    | D :: p -> field p (cdr x)
end

module type STACK_DOMAIN = sig
  type t

  type elem

  val const : elem -> t -> t

  val unop : (elem -> elem) -> t -> t

  val unop_2 : (elem -> elem * elem) -> t -> t

  val binop : (elem -> elem -> elem) -> t -> t

  val ternop : (elem -> elem -> elem -> elem) -> t -> t

  val push : Michelson.Literal.t -> t -> t

  val swap : t -> t

  val dig : int -> t -> t

  val dug : int -> t -> t

  val dup : int -> t -> t

  val drop : t -> t

  val seq : (t -> t) list -> t -> t

  val dip : int -> (t -> t) -> t -> t

  val fail : t -> t

  val if_then : (t -> t) -> (t -> t) -> t -> t

  val loop : (t -> t) -> t -> t
end

module Decompiler_domain (V : VALUE_DOMAIN with type t = Expr_domain.t) =
(* : STACK_DOMAIN with type elem = Expr_domain.t *)
struct
  type stack =
    | Stack_ok     of Basics.texpr list
    | Stack_failed

  type elem = Expr_domain.t

  type t =
    { commands : Basics.tcommand list (* Commands emitted so far. *)
    ; stack : stack (* Current stack. *)
    ; local_counter : int
    ; tstorage : mtype
    ; typing_env : Typing.env (* TODO Emit untyped commands instead. *) }

  let line_no = -1

  let initial_stack ~tparam ~tstorage =
    Stack_ok
      [ Expr.pair
          ~line_no
          (Expr.params ~line_no (type_of_mtype tparam))
          (Expr.storage ~line_no (type_of_mtype tstorage)) ]

  let initial ~tparam ~tstorage =
    { commands = []
    ; stack = initial_stack ~tparam ~tstorage
    ; local_counter = 0
    ; tstorage
    ; typing_env = Typing.init_env ~contract_data_types:(Hashtbl.create 5) }

  let get_commands {stack; commands; tstorage; typing_env} =
    let line_no = -1 in
    match stack with
    | Stack_ok [{e = EPair ({e = EList []}, storage)}] ->
        commands
        @ [ Command.set
              ~line_no
              typing_env
              (Expr.storage ~line_no:(-1) (type_of_mtype tstorage))
              storage ]
    | _ -> failwith "run: Unexpected final stack"

  let normalize : t -> t =
   fun _ ->
    let _norm _search_for_storage = failwith "TODO" in
    failwith "TODO"

  let _unify_stacks s1 s2 =
    match (s1, s2) with
    | s1, Stack_failed -> s1
    | Stack_failed, s2 -> s2
    | Stack_ok _s1, Stack_ok _s2 -> failwith "TODO"

  (* The stacks must have the same type (or be FAILED). Ensure the
     output stacks are the same by assigning the same locals to the
     same positions. *)
  let normalize_unify : t -> t -> t * t = fun _ -> failwith "TODO"

  let _normalize_to : stack -> t -> t = fun _ -> failwith "TODO"

  let if_then a b s =
    match s with
    | {stack = Stack_ok (cond :: tail); commands} ->
        let s = normalize {s with stack = Stack_ok tail} in
        let a = a s in
        let b = b s in
        assert (a.typing_env == b.typing_env);
        let a, b = normalize_unify a b in
        let line_no = -1 in
        { commands =
            commands
            @ [ Command.ifte
                  ~line_no
                  cond
                  (Command.seq ~line_no a.commands)
                  (Command.seq ~line_no b.commands) ]
        ; stack = a.stack
        ; local_counter = max a.local_counter b.local_counter
        ; tstorage = a.tstorage
        ; typing_env = a.typing_env }
    | _ -> failwith "if_then"

  let loop body s =
    match s.stack with
    | Stack_ok (_cond :: tail) ->
        let s = normalize {s with stack = Stack_ok tail} in
        let body = body s in
        ( match body.stack with
        | Stack_ok (_cond :: _tail) -> failwith "TODO"
        | _ -> failwith "loop" )
    (* TODO Important special case: the pre-loop and post-body conditions are
       the same SmartPy expression. *)
    | _ -> failwith "loop"

  let map_stack_ok f = function
    | Stack_ok stack -> Stack_ok (f stack)
    | Stack_failed -> failwith "map_stack_ok: failed"

  let map_stack f r = {r with stack = f r.stack}

  let on_ok f = map_stack (map_stack_ok f)

  let fail = map_stack (fun _ -> Stack_failed)

  let const x s = on_ok (fun stack -> x s.typing_env :: stack) s

  let unop op s =
    on_ok
      (function
        | x :: stack -> op (fun _ -> x) s.typing_env :: stack
        | _ -> failwith "unop")
      s

  let unop_2 op s =
    on_ok
      (function
        | x :: stack ->
            let y1, y2 = op (fun _ -> x) in
            y1 s.typing_env :: y2 s.typing_env :: stack
        | _ -> failwith "unop_2")
      s

  let binop op s =
    on_ok
      (function
        | x :: y :: stack -> op (fun _ -> x) (fun _ -> y) s.typing_env :: stack
        | _ -> failwith "binop")
      s

  let ternop op s =
    on_ok
      (function
        | x :: y :: z :: stack ->
            op (fun _ -> x) (fun _ -> y) (fun _ -> z) s.typing_env :: stack
        | _ -> failwith "ternop")
      s

  let push l s = on_ok (fun stack -> V.of_literal l s.typing_env :: stack) s

  let swap =
    on_ok (function
        | x :: y :: stack -> y :: x :: stack
        | _ -> failwith "swap")

  let dig n =
    on_ok (fun stack ->
        let hi, lo = Base.List.split_n stack n in
        match lo with
        | [] -> failwith "dig"
        | x :: lo -> (x :: hi) @ lo)

  let dug n =
    on_ok (function
        | x :: tail ->
            if n > List.length tail
            then failwith "dug"
            else
              let hi, lo = Base.List.split_n tail n in
              hi @ (x :: lo)
        | [] -> failwith "dug")

  let is_simple x =
    match Basics.(x.e) with
    | ECst _ -> true
    | _ -> false

  (* DUP on a non-literal leads to allocation of a new local. (TODO) *)
  let dup n = function
    | {stack = Stack_ok stack} as state ->
        assert (n < List.length stack);
        ( match Base.List.split_n stack n with
        | hi, x :: lo ->
            if is_simple x
            then {state with stack = Stack_ok (x :: stack)}
            else
              let v_name = "x" ^ string_of_int state.local_counter in
              let v_type = x.et in
              let l = Expr.local ~line_no v_name v_type in
              { state with
                commands =
                  state.commands
                  @ [Command.defineLocal ~line_no v_name x v_type]
              ; (* TODO drop local *)
                stack = Stack_ok ((l :: hi) @ (l :: lo))
              ; local_counter = state.local_counter + 1 }
        | _ -> failwith "dup" )
    | _ -> failwith "dup: failed"

  let drop =
    on_ok (function
        | _ :: xs -> xs
        | _ -> failwith "drop")

  let _dip n instr = function
    | Stack_ok stack ->
        let hi, lo = Base.List.split_n stack n in
        map_stack_ok (fun stack -> hi @ stack) (instr (Stack_ok lo))
    | Stack_failed -> Stack_failed

  let dip _n _instr = failwith "TODO"

  let seq xs stack = List.fold_left (fun stack f -> f stack) stack xs
end

module AbstractInterpreter
    (V : VALUE_DOMAIN)
    (S : STACK_DOMAIN with type elem = V.t) =
struct
  open S

  let interpret_f = function
    | MIerror _ -> failwith "interpret: MIerror"
    | MIcomment _ -> fun x -> x
    | MImich _ -> failwith "interpret: MImich"
    | MIdip instr -> dip 1 instr
    | MIdipn (n, instr) -> dip n instr
    | MIloop body -> fun x -> loop body x
    | MIiter _ -> failwith "interpret: MIiter"
    | MImap _ -> failwith "interpret: MImap"
    | MIdrop -> drop
    | MIdup -> dup 0
    | MIdig n -> dig n
    | MIdug n -> dug n
    | MIfailwith -> fail
    | MIif (a, b) -> fun x -> if_then a b x
    | MIif_left _ -> failwith "interpret: MIif_left"
    | MIif_some _ -> failwith "interpret: MIif_some"
    | MInil t -> push (List {t; elements = []})
    | MIcons -> binop V.cons
    | MInone t -> push (None t)
    | MIsome -> unop V.some
    | MIpair -> binop V.pair
    | MIleft _ -> unop V.left
    | MIright _ -> unop V.right
    | MIpush (_t, l) -> push l
    | MIseq xs -> seq xs
    | MIswap -> swap
    | MIunpair -> unop_2 (fun x -> (V.field [A] x, V.field [D] x))
    | MIfield p -> unop (V.field p)
    | MIsetField _ -> failwith "interpret: MIsetField"
    | MIcontract _ -> failwith "interpret: MIcontract"
    | MIself -> failwith "interpret: MIself"
    | MIexec -> failwith "interpret: MIexec"
    | MIaddress -> failwith "interpret: MIaddress"
    | MIimplicit_account -> failwith "interpret: MIimplicit_account"
    | MItransfer_tokens -> failwith "interpret: MItransfer_tokens"
    | MIcheck_signature -> failwith "interpret: MIcheck_signature"
    | MIset_delegate -> failwith "interpret: MIset_delegate"
    | MIeq -> binop V.eq
    | MIneq -> binop V.neq
    | MIle -> binop V.le
    | MIlt -> binop V.lt
    | MIge -> binop V.ge
    | MIgt -> binop V.gt
    | MIcompare -> binop V.compare
    | MImul -> binop V.mul
    | MIadd -> binop V.add
    | MIsub -> binop V.sub
    | MIediv -> binop V.ediv
    | MInot -> unop V.vnot
    | MIand -> binop V.vand
    | MIor -> binop V.vor
    | MIconcat -> failwith "interpret: MIconcat"
    | MIslice -> ternop V.slice
    | MIsize -> unop V.size
    | MIget -> binop V.get
    | MIupdate -> ternop V.update
    | MIsender -> const V.sender
    | MIsource -> const V.source
    | MIamount -> const V.amount
    | MIbalance -> const V.balance
    | MInow -> const V.now
    | MImem -> binop V.mem
    | MIhash_key -> failwith "interpret: MIhash_key"
    | MIblake2b -> failwith "interpret: MIblake2b"
    | MIsha256 -> failwith "interpret: MIsha256"
    | MIsha512 -> failwith "interpret: MIsha512"
    | MIabs -> unop V.abs
    | MIneg -> unop V.neg
    | MIint -> unop V.to_int
    | MIisnat -> unop V.is_nat
    | MIpack -> failwith "interpret: MIpack"
    | MIunpack _ -> failwith "interpret: MIunpack"
    | MIlambda _ -> failwith "interpret: MIlambda"

  let interpret = cata_instr interpret_f
end

module D = Decompiler_domain (Expr_domain)
module A = AbstractInterpreter (Expr_domain) (D)

let decompile ~tparam ~tstorage ~code =
  Command.seq
    ~line_no:(-1)
    (D.get_commands (A.interpret code (D.initial ~tparam ~tstorage)))

let decompile_contract Michelson_contract.{parameter; storage; code} =
  decompile ~tparam:parameter ~tstorage:storage ~code:(forget_types code)

let mseq xs = MIseq (List.map (fun instr -> {instr}) xs)

let _c1 () =
  decompile
    ~tparam:mt_unit
    ~tstorage:mt_unit
    ~code:{instr = mseq [MIunpair; MIdrop; MInil mt_operation; MIpair]}

let _c2 () =
  decompile
    ~tparam:mt_int
    ~tstorage:mt_int
    ~code:{instr = mseq [MIunpair; MIadd; MInil mt_operation; MIpair]}

let _c3 () =
  decompile
    ~tparam:mt_int
    ~tstorage:mt_int
    ~code:
      {instr = mseq [MIunpair; MIadd; MIdup; MImul; MInil mt_operation; MIpair]}

let _c4 () =
  decompile
    ~tparam:mt_int
    ~tstorage:mt_int
    ~code:
      { instr =
          mseq
            [ MIunpair
            ; MIadd
            ; MIdup
            ; MImul
            ; MIdup
            ; MIadd
            ; MIdup
            ; MImul
            ; MIpush (mt_int, Michelson.Literal.int 1)
            ; MIadd
            ; MInil mt_operation
            ; MIpair ] }

let test () =
  print_endline "Decompiler test.";
  print_endline (Printer.command_to_string (_c4 ()));
  print_endline "Decompiler test done."
