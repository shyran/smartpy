(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Basics

type account_or_address =
  | Account of Interpreter.Primitive_implementations.account
  | Address of string

let address = function
  | Account x -> x.pkh
  | Address x -> x

type 'expr action =
  | New_contract of
      { id : int
      ; contract : 'expr tcontract_
      ; line_no : int }
  | Compute      of
      { id : int
      ; expression : 'expr
      ; line_no : int }
  | Simulation   of
      { id : int
      ; line_no : int }
  | Message      of
      { id : int
      ; valid : bool
      ; params : 'expr
      ; line_no : int
      ; title : string
      ; messageClass : string
      ; source : account_or_address option
      ; sender : account_or_address option
      ; time : int
      ; amount : 'expr
      ; message : string }
  | Error        of {message : string}
  | Html         of
      { tag : string
      ; inner : string
      ; line_no : int }
  | Verify       of
      { condition : 'expr
      ; line_no : int }
  | Show         of
      { expression : 'expr
      ; html : bool
      ; stripStrings : bool
      ; line_no : int }
  | Exception    of smart_except list

type t = {actions : texpr action list}

let action_of_json ~primitives ~scenario_state ~typing_env x =
  let module M = (val json_getter x : JsonGetter) in
  let open M in
  let typing_env = typing_env () in
  let import_expr_string s =
    Import.import_expr
      typing_env
      (Import.early_env ())
      (Parsexp.Single.parse_string_exn (string s))
  in
  match string "action" with
  | "newContract" ->
      let contract =
        Import.import_contract
          ~primitives
          ~scenario_state
          typing_env
          (Parsexp.Single.parse_string_exn (string "export"))
      in
      let id = int "id" in
      Hashtbl.replace scenario_state.contract_data_types id contract.storage.et;
      New_contract {id; contract; line_no = int "line_no"}
  | "compute" ->
      let expression = import_expr_string "expression" in
      Compute {id = int "id"; expression; line_no = int "line_no"}
  | "simulation" -> Simulation {id = int "id"; line_no = int "line_no"}
  | "message" ->
      let amount = import_expr_string "amount" in
      let of_seed id =
        match string id with
        | "none" -> None
        | seed ->
            if Base.String.is_prefix seed ~prefix:"seed:"
            then
              Some
                (Account
                   (primitives
                      .Interpreter.Primitive_implementations.account_of_seed
                      (String.sub seed 5 (String.length seed - 5))))
            else if Base.String.is_prefix seed ~prefix:"address:"
            then
              let address = String.sub seed 8 (String.length seed - 8) in
              let address =
                let address =
                  Import.import_expr
                    typing_env
                    (Import.early_env ())
                    (Parsexp.Single.parse_string_exn address)
                in
                Value.unAddress
                  (Interpreter.interpret_expr_external
                     ~primitives
                     ~no_env:
                       [ `Text "Computing address"
                       ; `Expr address
                       ; `Line address.el ]
                     ~scenario_state
                     address)
              in
              Some (Address address)
            else assert false
      in
      let sender = of_seed "sender" in
      let source = of_seed "source" in
      Message
        { id = int "id"
        ; valid = bool "valid"
        ; params = import_expr_string "params"
        ; line_no = int "line_no"
        ; title = string "title"
        ; messageClass = string "messageClass"
        ; sender
        ; source
        ; time = int "time"
        ; amount
        ; message = string "message" }
  | "error" -> Error {message = string "message"}
  | "html" ->
      Html {tag = string "tag"; inner = string "inner"; line_no = int "line_no"}
  | "verify" ->
      Verify
        {condition = import_expr_string "condition"; line_no = int "line_no"}
  | "show" ->
      Show
        { expression = import_expr_string "expression"
        ; html = bool "html"
        ; stripStrings = bool "stripStrings"
        ; line_no = int "line_no" }
  | action -> failwith ("Unknown action: '" ^ action ^ "'")

let load_from_string ~primitives ~scenario_state j =
  let actions = ref [] in
  let typing_env () =
    Typing.init_env ~contract_data_types:scenario_state.contract_data_types
  in
  ( try
      List.iter
        (fun action ->
          actions :=
            action_of_json ~primitives ~scenario_state ~typing_env action
            :: !actions)
        (Yojson.Basic.Util.to_list j)
    with
  | SmartExcept exn -> actions := Exception exn :: !actions
  | exn ->
      actions :=
        Exception [`Text (Printer.exception_to_string false exn)] :: !actions
  );
  {actions = List.rev (List.map (fun x -> x) !actions)}

let pp_contract_types contract =
  Printf.sprintf
    "Storage: %s\nParams: %s"
    (Printer.type_to_string contract.storage.t)
    (Printer.type_to_string
       (Type.variant
          (List.map
             (fun {channel; paramsType} -> (channel, paramsType))
             contract.entry_points)))

let contract_of_verification_texpr ~primitives ~scenario_state condition =
  let substContractData = Hashtbl.create 10 in
  let fixEnv =
    Fixer.
      { partial = false
      ; check_variables = false
      ; storage = Type.unit
      ; variable_names = []
      ; substContractData
      ; reducer = Interpreter.reducer ~primitives ~scenario_state
      ; lambda_param = None }
  in
  let paramsType =
    Type.record_or_unit
      (List.sort
         compare
         (Hashtbl.fold
            (fun key value l ->
              (Printf.sprintf "k%03i" key, value.storage.t) :: l)
            scenario_state.contracts
            []))
  in
  let params = Expr.params ~line_no:(-1) (Fixer.fix_type fixEnv paramsType) in
  Hashtbl.iter
    (fun key contract ->
      let getData =
        Expr.attr
          ~line_no:params.el
          params
          (Printf.sprintf "k%03i" key)
          contract.storage.t
      in
      Typing.assertEqual getData.et contract.storage.t ~pp:(fun () ->
          [`Text "Substitution"; `Expr getData; `Type contract.storage.t]);
      Hashtbl.add substContractData key getData)
    scenario_state.contracts;
  let typing_env =
    Typing.init_env ~contract_data_types:scenario_state.contract_data_types
  in
  let condition = Fixer.fix_expr fixEnv typing_env condition in
  let entryPoint =
    { channel = "verify"
    ; paramsType
    ; body = Command.verify ~line_no:condition.el condition false None }
  in
  let contract =
    Fixer.fix_contract
      fixEnv
      typing_env
      ~storage:(Expr.cst ~line_no:0 Literal.unit)
      ~entry_points:[(typing_env, entryPoint)]
      ~tparameter:paramsType
      ~flags:[]
  in
  contract

let run ~primitives ~scenario_state {actions} output_dir =
  let result = ref [] in
  let errors : smart_except list list ref = ref [] in
  let table_of_contents = ref [] in
  let tocid = ref 0 in
  let appendError s l =
    match output_dir with
    | None -> errors := l :: !errors
    | Some _ ->
        result := s :: !result;
        errors := l :: !errors
  in
  let in_browser = Base.Option.is_none output_dir in
  let appendIn s = if in_browser then result := s :: !result in
  let appendOut s =
    match output_dir with
    | Some _ -> result := s :: !result
    | None -> ()
  in
  let write_file file_name contents =
    let out = open_out file_name in
    output_string out contents;
    close_out out;
    appendOut
      (Printf.sprintf
         " => %s %d"
         file_name
         (List.length (String.split_on_char '\n' contents)))
  in
  let handle_action step = function
    | New_contract {id; contract; line_no} ->
        let contract =
          { contract with
            storage =
              Interpreter.interpret_expr_external
                ~primitives
                ~no_env:[`Text "Compute storage"]
                ~scenario_state
                contract.storage }
        in
        let t = Hashtbl.find scenario_state.contract_data_types id in
        Typing.assertEqual t contract.storage.t ~pp:(fun () ->
            [ `Text
                (Printf.sprintf
                   "Contract type mismatch for contract id = %i"
                   id)
            ; `Br
            ; `Type contract.storage.t
            ; `Br
            ; `Text "is not"
            ; `Br
            ; `Type t
            ; `Br
            ; `Line line_no ]);
        Hashtbl.replace scenario_state.contracts id contract;
        appendOut "Creating contract";
        appendOut (" -> " ^ Compiler.michelson_storage contract);
        ( match output_dir with
        | None ->
            appendIn
              (Html.full_html
                 ~contract
                 ~def:"SmartPy"
                 ~onlyDefault:false
                 ~line_no)
        | Some output_dir ->
            write_file
              (Printf.sprintf
                 "%s/testContractTypes.%d.%d.tz"
                 output_dir
                 id
                 step)
              (pp_contract_types contract);
            let compiledContract = Compiler.michelson_contract contract in
            if Michelson.Michelson_contract.has_error compiledContract
            then
              appendError
                "Error in generated contract"
                [`Text "Error in generated Michelson contract"];
            write_file
              (Printf.sprintf "%s/testContractCode.%d.%d.tz" output_dir id step)
              (Compiler.michelson_contract_michelson compiledContract);
            write_file
              (Printf.sprintf
                 "%s/testContractCode.%d.%d.tz.json"
                 output_dir
                 id
                 step)
              (Compiler.michelson_contract_micheline compiledContract);
            write_file
              (Printf.sprintf "%s/testPrettyPrint.%d.%d.py" output_dir id step)
              (Printer.contract_to_string contract) )
    | Compute {id; expression; line_no} ->
        let value =
          Interpreter.interpret_expr_external
            ~primitives
            ~no_env:
              [`Text "Computing expression"; `Expr expression; `Line line_no]
            ~scenario_state
            expression
        in
        Hashtbl.replace scenario_state.variables id value
    | Simulation {id; line_no} ->
      ( match Hashtbl.find_opt scenario_state.contracts id with
      | None -> assert false
      | Some contract -> appendIn (Html.simulation contract ~line_no) )
    | Message
        { id
        ; valid
        ; params
        ; line_no
        ; title
        ; messageClass
        ; sender
        ; source
        ; time
        ; amount
        ; message } ->
        let contract =
          match Hashtbl.find_opt scenario_state.contracts id with
          | None ->
              raise
                (SmartExcept
                   [ `Text (Printf.sprintf "Missing contract in scenario %i" id)
                   ; `Line line_no ])
          | Some contract -> contract
        in
        let amount =
          Value.unMutez
            (Interpreter.interpret_expr_external
               ~primitives
               ~no_env:[`Text "Computing amount"; `Expr amount; `Line amount.el]
               ~scenario_state
               amount)
        in
        let result =
          Contract.execMessageInner
            ~primitives
            ~scenario_state
            ~title
            ~execMessageClass:
              (if messageClass <> "" then " " ^ messageClass else messageClass)
            ~context:
              (Interpreter.context
                 ?sender:(Base.Option.map sender ~f:address)
                 ?source:(Base.Option.map source ~f:address)
                 ~time
                 ~amount
                 ~line_no
                 ~debug:false
                 ())
            ~initContract:contract
            ~channel:message
            ~params:
              (Interpreter.interpret_expr_external
                 ~primitives
                 ~no_env:
                   [`Text "Computing params"; `Expr params; `Line params.el]
                 ~scenario_state
                 params)
        in
        appendOut
          (Printf.sprintf
             "Executing %s(%s)..."
             message
             (Printer.expr_to_string params));
        ( match result.error with
        | None ->
            let contract =
              Base.Option.value_exn ~message:"No contract" result.contract
            in
            Hashtbl.replace scenario_state.contracts id contract;
            let storage = Compiler.michelson_storage contract in
            if valid
            then appendOut (Printf.sprintf " -> %s" storage)
            else
              appendError
                (Printf.sprintf
                   " !!! Unexpected accepted transaction ERROR !!! -> %s"
                   storage)
                [ `Text
                    "Unexpected accepted transaction, please use \
                     .run(valid=True, ..)"
                ; `Br
                ; `Text message
                ; `Expr params
                ; `Line line_no ]
        | Some error ->
            if valid
            then
              appendError
                (Printf.sprintf
                   " -> !!! Unexpected ERROR !!! %s"
                   (Printer.error_to_string error))
                [ `Text
                    "Unexpected error in transaction, please use \
                     .run(valid=False, ..)"
                ; `Br
                ; `Text
                    (Printer.error_to_string
                       ~options:Printer.Options.html
                       error)
                ; `Br
                ; `Text message
                ; `Expr params
                ; `Line line_no ]
            else
              appendOut
                (Printf.sprintf
                   " -> --- Expected failure in transaction --- %s"
                   (Printer.error_to_string error)) );

        appendIn result.html
    | Exception exn ->
        appendError (Printer.pp_smart_except false (`Rec exn)) exn
    | Error {message} ->
        appendError
          (Printf.sprintf " !!! Python Error: %s" message)
          [`Text "Python Error"; `Text message]
    | Html {tag; inner} ->
        let toc i =
          incr tocid;
          table_of_contents := (i, !tocid, inner) :: !table_of_contents;
          appendIn (Printf.sprintf "<span id='label%i'></span>" !tocid)
        in
        ( match tag with
        | "h1" -> toc 1
        | "h2" -> toc 2
        | "h3" -> toc 3
        | "h4" -> toc 4
        | _ -> () );
        appendOut "Comment...";
        appendOut (Printf.sprintf " %s: %s" tag inner);
        appendIn (Printf.sprintf "<%s>%s</%s>" tag inner tag)
    | Verify {condition; line_no} ->
        appendOut
          (Printf.sprintf "Verifying %s..." (Printer.expr_to_string condition));
        ( match output_dir with
        | None -> ()
        | Some output_dir ->
            let contract =
              contract_of_verification_texpr
                ~primitives
                ~scenario_state
                condition
            in
            let contract =
              { contract with
                storage =
                  Interpreter.interpret_expr_external
                    ~primitives
                    ~no_env:[`Text "Compute storage"]
                    ~scenario_state
                    contract.storage }
            in
            write_file
              (Printf.sprintf "%s/testVerify.%d.tz" output_dir step)
              (Compiler.michelson_contract_michelson
                 (Compiler.michelson_contract contract)) );
        let value =
          Interpreter.interpret_expr_external
            ~primitives
            ~no_env:
              [`Text "Computing condition"; `Expr condition; `Line condition.el]
            ~scenario_state
            condition
        in
        let result = Value.bool_of_value value in
        if result
        then appendOut " OK"
        else (
          appendIn
            (Printf.sprintf
               "Verification Error: <br>%s<br> is false."
               (Printer.expr_to_string condition));
          appendError
            " KO"
            [ `Text "Verification Error"
            ; `Br
            ; `Expr condition
            ; `Br
            ; `Text "is false"
            ; `Line line_no ] )
    | Show {expression; html; stripStrings} ->
        appendOut
          (Printf.sprintf "Computing %s..." (Printer.expr_to_string expression));
        let value =
          Interpreter.interpret_expr_external
            ~primitives
            ~no_env:
              [ `Text "Computing expression"
              ; `Expr expression
              ; `Line expression.el ]
            ~scenario_state
            expression
        in
        let result =
          Printer.value_to_string ~options:Printer.Options.string value
        in
        let options =
          if html
          then
            if stripStrings
            then Printer.Options.htmlStripStrings
            else Printer.Options.html
          else Printer.Options.string
        in
        appendIn (Printer.value_to_string ~options value);
        appendOut (Printf.sprintf " => %s" result)
  in
  ( try List.iteri (fun i action -> handle_action i action) actions with
  | SmartExcept l as exn ->
      let s = Printer.exception_to_string false exn in
      appendError (" (Exception) " ^ s) l
  | exn ->
      let s = Printer.exception_to_string false exn in
      appendError (" (Exception) " ^ s) [`Text s] );
  let result = String.concat "\n" (List.rev !result) in
  let table_of_contents =
    let goto d d' l =
      let x = ref d in
      while !x <> d' do
        if !x < d'
        then (
          if in_browser then l := "<ul>" :: !l;
          incr x )
        else (
          if in_browser then l := "</ul>" :: !l;
          decr x )
      done
    in
    let d, table_of_contents =
      List.fold_left
        (fun (d, l) (d', id, s') ->
          let l = ref l in
          goto d d' l;
          let link =
            if in_browser
            then Printf.sprintf "<li><a href='#label%i'>%s</a>" id s'
            else Printf.sprintf "%s %s" (String.sub "\n####" 0 d') s'
          in
          (d', link :: !l))
        (1, [])
        (List.rev !table_of_contents)
    in
    let table_of_contents = ref table_of_contents in
    goto d 1 table_of_contents;
    String.concat "" (List.rev !table_of_contents)
  in
  let result =
    Base.String.substr_replace_all
      result
      ~pattern:"[[TABLEOFCONTENTS]]"
      ~with_:table_of_contents
  in
  (result, List.rev !errors)

let run_scenario_filename ~primitives ~filename ~output_dir =
  let scenario_state = scenario_state () in
  if not (Sys.file_exists filename)
  then
    Printf.sprintf
      "Missing scenario file %s (maybe a test is missing in the SmartPy script)"
      filename
  else
    try
      let scenario =
        load_from_string
          ~primitives
          ~scenario_state
          (Yojson.Basic.from_file filename)
      in
      let result, errors =
        run ~primitives ~scenario_state scenario (Some output_dir)
      in
      print_endline result;
      match errors with
      | [] -> ""
      | l ->
          Printer.exception_to_string
            false
            (SmartExcept (List.concat (List.map (fun l -> [`Rec l; `Br]) l)))
    with
    | exn ->
        let error = Printer.exception_to_string false exn in
        print_endline error;
        error

let run_scenario_browser ~primitives ~scenario =
  let scenario_state = scenario_state () in
  let scenario =
    try
      load_from_string
        ~primitives
        ~scenario_state
        (Yojson.Basic.from_string scenario)
    with
    | exn -> failwith (Printer.exception_to_string true exn)
  in
  let result, errors = run ~primitives ~scenario_state scenario None in
  SmartDom.setText "outputPanel" result;
  match errors with
  | [] -> ()
  | l ->
      raise
        (SmartExcept
           [ `Text "Error in Scenario"
           ; `Br
           ; `Rec (List.concat (List.map (fun l -> [`Rec l; `Br]) l)) ])
