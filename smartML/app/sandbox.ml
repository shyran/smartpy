(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Base
open Flextesa
open Internal_pervasives

let failf fmt = Fmt.kstr (fun s -> fail (`Scenario_error s)) fmt

module Monotone_id = struct
  let _foo = ref 0

  let get () = !_foo

  let next () =
    Caml.incr _foo;
    !_foo
end

module Util = struct
  (* This should be in flextesa, but we're on mainnet, which lags a bit. *)
  let client_cmd ?(succeed = false) state ~client args =
    Running_processes.run_cmdf
      state
      "sh -c %s"
      ( Tezos_client.client_command client ~state args
      |> Genspio.Compile.to_one_liner
      |> Caml.Filename.quote )
    >>= fun res ->
    match res#status with
    | Unix.WEXITED n when n <> 0 && succeed ->
        failf "Cmd %s did not succeed" (String.concat ~sep:" " args)
    | _ -> return res
end

module Scenario = struct
  module Amount = struct
    type t = Mutez of int64 [@@deriving show]

    let to_tez_string = function
      | Mutez i -> sprintf "%f" (Float.of_int64 i /. 1_000_000.)
  end

  module Parameters = struct
    type t = Raw of string [@@deriving show]

    let to_michelson_string = function
      | Raw s -> s
  end

  module Action = struct
    type kind =
      | Call                  of
          { amount : Amount.t
          ; parameters : Parameters.t }
      | Storage_json_greps_to of {re : [ `Posix of string ]}
    [@@deriving show]

    type expectation =
      [ `Succeed
      | `Fail
      ]
    [@@deriving show]

    type t =
      { kind : kind
      ; should : expectation }
    [@@deriving show]

    let make kind ~should = {kind; should}
  end

  type scenario =
    | With_contract of
        { source : string
        ; balance : Amount.t
        ; parameters : Parameters.t
        ; actions : Action.t list }
  [@@deriving show]

  let name_of_scenario s =
    let prefix =
      match s with
      | With_contract {source; _} ->
          Caml.Filename.(basename source |> chop_extension)
    in
    Fmt.strf
      "%s-%s"
      prefix
      (String.sub
         ~pos:0
         ~len:8
         Caml.Digest.(string (show_scenario s) |> to_hex))

  type t =
    { name : string
    ; scenario : scenario }
  [@@deriving show]

  let encoding =
    let open Tezos_data_encoding.Data_encoding in
    let action =
      let open Action in
      let should_encoding =
        string_enum [("succeed", `Succeed); ("fail", `Fail)]
      in
      let should_field = dft "should" should_encoding `Succeed in
      union
        [ case
            ~title:"call"
            (Tag 0)
            (conv
               (function
                 | {kind = Call {amount = Mutez a; parameters = Raw p}; should}
                   ->
                     ((), Some a, Some p, should)
                 | _ -> assert false)
               (fun ((), amopt, popt, should) ->
                 make
                   ~should
                   (Call
                      { amount = Mutez (Option.value ~default:0L amopt)
                      ; parameters = Raw (Option.value ~default:"Unit" popt) }))
               (obj4
                  (req "action" (constant "call"))
                  (opt "amount-mutez" int64)
                  (opt "parameters-raw" string)
                  should_field))
            (function
              | {kind = Call _; _} as x -> Some x
              | _ -> None)
            (fun x -> x)
        ; case
            ~title:"storage-json-greps-to"
            (Tag 1)
            (conv
               (function
                 | {kind = Storage_json_greps_to {re = `Posix posreg}; should}
                   ->
                     ((), posreg, should)
                 | _ -> assert false)
               (fun ((), posreg, should) ->
                 make ~should (Storage_json_greps_to {re = `Posix posreg}))
               (obj3
                  (req "action" (constant "storage-greps-to"))
                  (req "posix-re" string)
                  should_field))
            (function
              | {kind = Storage_json_greps_to _; _} as x -> Some x
              | _ -> None)
            (fun x -> x) ]
    in
    let with_contract_scenario =
      conv
        (function
          | With_contract
              {source; balance = Mutez mu; parameters = Raw param; actions} ->
              (source, Some mu, Some param, actions))
        (fun (source, balance_mutez, parameters_raw, actions) ->
          With_contract
            { source
            ; balance = Mutez (Option.value balance_mutez ~default:0L)
            ; parameters = Raw (Option.value parameters_raw ~default:"Unit")
            ; actions })
        (obj4
           (req "source" string)
           (opt "balance-mutez" int64)
           (opt "parameters-raw" string)
           (dft "do" (list action) []))
    in
    list
      (conv
         (fun {name; scenario} -> (name, scenario))
         (fun (name, scenario) -> {name; scenario})
         (obj2
            (req "scenario" string)
            (req "run-with-contract" with_contract_scenario)))

  module Codec = struct
    let to_json t =
      let open Tezos_data_encoding.Data_encoding in
      Json.construct encoding t

    let of_json j =
      let open Tezos_data_encoding.Data_encoding in
      try Json.destruct encoding j with
      | Json_encoding.Cannot_destruct (p, exn) ->
          Fmt.failwith
            "Parsing Scenario (%s): %a"
            (Json_query.json_pointer_of_path p)
            Exn.pp
            exn
  end

  module Example = struct
    let one =
      { name = "Example-one"
      ; scenario =
          With_contract
            { source = "some/contract.tz"
            ; balance = Mutez 10_000_000L
            ; parameters = Raw "Pair 42 tz1cCC45Z5LutWVUqaXLrNdHF6qstHEaNUWa"
            ; actions =
                Action.
                  [ make
                      ~should:`Succeed
                      (Call
                         { amount = Mutez 1_000_000L
                         ; parameters = Raw "Left (Right 42)" })
                  ; make
                      ~should:`Succeed
                      (Storage_json_greps_to {re = `Posix "bytes.*3aa230f039"})
                  ; make
                      ~should:`Fail
                      (Call
                         { amount = Mutez 0L
                         ; parameters =
                             Raw
                               "Left (Left (Pair \
                                edpktrLk3ZDygmmxKHvWfduv68nLRvdc5sVTAV27qsMNsgsihK82Ts \
                                None))" }) ] } }

    let all = [one]
  end

  let pp_quick ppf {name; scenario} =
    let open Fmt in
    let sce ppf = function
      | With_contract {source; _} -> string ppf (Caml.Filename.basename source)
    in
    pf ppf "%s: %a" name sce scenario

  module Scenario_result = struct
    type step =
      | Call          of string list * Process_result.t
      | Check_storage of string list option * string
      | Origination   of string list * Process_result.t * string list

    type res =
      { status : [ `Ok | `Ko ]
      ; expectation : Action.expectation
      ; result : step
      ; balance_update : string * string }

    let one status result ~expectation ~balance_before ~balance_after =
      { status
      ; result
      ; expectation
      ; balance_update = (balance_before, balance_after) }

    let one_ok ~should o =
      match should with
      | `Succeed -> one `Ok o ~expectation:should
      | `Fail -> one `Ko o ~expectation:should

    let one_error o ~should =
      match should with
      | `Succeed -> one `Ko o ~expectation:should
      | `Fail -> one `Ok o ~expectation:should

    type t = res list
  end

  let run state {scenario; _} ~client ~baker_account ~contractor_account :
      (Scenario_result.t, _) Asynchronous_result.t =
    let open Scenario_result in
    (* let {Tezos_client.Keyed.client; key_name; _} = client in *)
    let baking_key_name = Tezos_protocol.Account.name baker_account in
    let contractor_key_name = Tezos_protocol.Account.name contractor_account in
    let bake () =
      Util.client_cmd
        state
        ~client
        ["bake"; "for"; baking_key_name; "--force"; "--minimal-timestamp"]
      >>= fun res ->
      match res#status with
      | Unix.WEXITED 0 -> return ()
      | _ -> fail (`Scenario_error "bake failed")
    in
    let get_contractor_balance () =
      Util.client_cmd
        state
        ~client
        ["get"; "balance"; "for"; contractor_key_name]
      >>= fun res ->
      match res#status with
      | Unix.WEXITED 0 -> return (String.concat ~sep:"" res#out)
      | _ -> fail (`Scenario_error "get-balance failed")
    in
    let go = function
      | With_contract {source; balance; parameters; actions} as s ->
          get_contractor_balance ()
          >>= fun balance_before ->
          let name = name_of_scenario s in
          let burn_cap = "1_000_000" in
          let cmd =
            [ "--wait"
            ; "none"
            ; "originate"
            ; "contract"
            ; name
            ; "transferring"
            ; Amount.to_tez_string balance
            ; "from"
            ; contractor_key_name
            ; "running"
            ; source
            ; "--init"
            ; Parameters.to_michelson_string parameters
            ; "--burn-cap"
            ; burn_cap ]
          in
          Util.client_cmd state ~client cmd
          >>= fun p_result ->
          bake ()
          >>= fun () ->
          get_contractor_balance ()
          >>= fun balance_after ->
          ( match p_result#status with
          | Unix.WEXITED 0 ->
              Util.client_cmd state ~client ["show"; "known"; "contract"; name]
              >>= fun show_contract_output ->
              let origination_result =
                one_ok
                  ~should:`Succeed
                  (Origination (cmd, p_result, show_contract_output#out))
                  ~balance_after
                  ~balance_before
              in
              let contract_address =
                String.concat ~sep:"" show_contract_output#out
              in
              List.fold actions ~init:(return []) ~f:(fun prev_m action ->
                  prev_m
                  >>= fun prev ->
                  get_contractor_balance ()
                  >>= fun balance_before ->
                  let should = action.should in
                  match action.kind with
                  | Call {amount; parameters} ->
                      let cmd =
                        [ "--wait"
                        ; "none"
                        ; "transfer"
                        ; Amount.to_tez_string amount
                        ; "from"
                        ; contractor_key_name
                        ; "to"
                        ; name
                        ; "--burn-cap"
                        ; burn_cap
                        ; "--arg"
                        ; Parameters.to_michelson_string parameters ]
                      in
                      Util.client_cmd state ~client cmd
                      >>= fun call_result ->
                      bake ()
                      >>= fun () ->
                      get_contractor_balance ()
                      >>= fun balance_after ->
                      let what = Call (cmd, call_result) in
                      let result =
                        match call_result#status with
                        | Unix.WEXITED 0 ->
                            one_ok what ~balance_before ~balance_after ~should
                        | _ ->
                            one_error
                              what
                              ~balance_before
                              ~balance_after
                              ~should
                      in
                      return (result :: prev)
                  | Storage_json_greps_to {re = `Posix posreg} ->
                      Util.client_cmd
                        state
                        ~client
                        [ "rpc"
                        ; "get"
                        ; Fmt.strf
                            "/chains/main/blocks/head/context/contracts/%s/storage"
                            contract_address ]
                      >>= fun rpc_result ->
                      get_contractor_balance ()
                      >>= fun balance_after ->
                      let re = Re.Posix.compile_pat posreg in
                      ( match rpc_result#status with
                      | Unix.WEXITED 0 ->
                          let storage =
                            rpc_result#out |> String.concat ~sep:" "
                          in
                          let result_data =
                            Check_storage (Some rpc_result#out, posreg)
                          in
                          ( match Re.execp re storage with
                          | false ->
                              return
                                (one_error
                                   result_data
                                   ~balance_before
                                   ~should
                                   ~balance_after)
                          | true ->
                              return
                                (one_ok
                                   result_data
                                   ~balance_before
                                   ~should
                                   ~balance_after)
                          | exception _ -> fail (`Scenario_error "re.execp") )
                      | _ ->
                          return
                            (one_error
                               ~should
                               (Check_storage (None, posreg))
                               ~balance_before
                               ~balance_after) )
                      >>= fun result -> return (result :: prev))
              >>= fun more_results ->
              return (origination_result :: List.rev more_results)
          | _ ->
              return
                [ one_error
                    ~should:`Succeed
                    ~balance_before
                    ~balance_after
                    (Origination (cmd, p_result, [])) ] )
          >>= fun results ->
          Console.say
            state
            EF.(
              desc_list
                (af "Debug: origination: %s" source)
                [ desc (wf "Srtdout") (ocaml_string_list p_result#out)
                ; desc (wf "Error") (ocaml_string_list p_result#err) ])
          >>= fun () -> return results
    in
    go scenario

  let pp_results_quick ppf results =
    let open Scenario_result in
    let open Fmt in
    let thing ppf = function
      | Origination _ -> string ppf "Origination"
      | Call _ -> string ppf "Call"
      | Check_storage _ -> string ppf "Check-storage"
    in
    let one ppf {status; result; expectation; _} =
      pf
        ppf
        "%s (%s): %a"
        ( match status with
        | `Ko -> "KO"
        | `Ok -> "OK" )
        ( match expectation with
        | `Fail -> "expected failure"
        | `Succeed -> "expected success" )
        thing
        result
    in
    pf ppf "%a" (Dump.list one) results

  let pp_full_results ppf scenarios_and_results =
    let open Scenario_result in
    let open Fmt in
    let thing ~balance_update prefix ppf =
      let cmd_result thing_name (cmd, result) =
        let output_list ppf name l =
          string ppf name;
          cut ppf ();
          List.iter l ~f:(fun line ->
              pf ppf "| %s" line;
              cut ppf ())
        in
        vbox
          ~indent:2
          (fun ppf () ->
            pf ppf "%s %s:" prefix thing_name;
            cut ppf ();
            pf
              ppf
              "Balance-update: %s -> %s"
              (fst balance_update)
              (snd balance_update);
            cut ppf ();
            pf ppf "Command:@ %a" (Dump.list string) cmd;
            cut ppf ();
            pf ppf "Status: %s" (Process_result.status_to_string result#status);
            cut ppf ();
            output_list ppf "Std-error:" result#err;
            output_list ppf "Std-output:" result#out)
          ppf
          ()
      in
      function
      | Origination (cmd, result, _) -> cmd_result "Origination" (cmd, result)
      | Call (cmd, result) -> cmd_result "Contract-call" (cmd, result)
      | Check_storage (stoopt, re) ->
          vbox
            ~indent:2
            (fun ppf () ->
              pf ppf "%s Check-storage against %S" prefix re;
              Option.iter stoopt ~f:(fun sto ->
                  cut ppf ();
                  List.iter sto ~f:(fun l ->
                      string ppf l;
                      cut ppf ())))
            ppf
            ()
    in
    let result ppf {result; status; balance_update; expectation} =
      let wrap ppf f = box ~indent:0 f ppf () in
      let statstr =
        let exp =
          match expectation with
          | `Fail -> "expected failure"
          | `Succeed -> "expected success"
        in
        let st =
          match status with
          | `Ok -> "SUCCESS"
          | `Ko -> "FAILURE"
        in
        Fmt.strf "%s(%s)->" st exp
      in
      (* function
       * | {result= Ok t; balance_update} -> *)
      wrap ppf (fun ppf () -> thing statstr ppf result ~balance_update)
      (* )
      (* | `Not_implemented s -> Fmt.kstrf (text ppf) "Not-implemented: %s" s *)
      | {result= Error t; balance_update} ->
          wrap ppf (fun ppf () -> thing "FAILURE->" ppf t ~balance_update) *)
    in
    let scenario ppf ({name; scenario}, results) =
      pf ppf "### Scenario %S ###" name;
      cut ppf ();
      vbox
        ~indent:2
        (fun ppf () -> pf ppf "Scenario:@ %a" pp_scenario scenario)
        ppf
        ();
      cut ppf ();
      vbox
        ~indent:2
        (fun ppf () ->
          string ppf "Results:";
          cut ppf ();
          List.iter results ~f:(fun res ->
              string ppf "* ";
              result ppf res;
              cut ppf ()))
        ppf
        ();
      cut ppf ();
      let successes, failures =
        List.fold results ~init:(0, 0) ~f:(fun (r, f) ->
          function
          | {status = `Ok; _} -> (r + 1, f)
          | _ -> (r, f + 1))
      in
      let plural p n = if n = 1 then "" else p in
      pf
        ppf
        "Test-summary: %d success%s, %d failure%s"
        successes
        (plural "es" successes)
        failures
        (plural "s" failures)
    in
    vbox (list ~sep:cut scenario) ppf scenarios_and_results

  module Just_originate = struct
    let contract name ~path ~parameters =
      { name
      ; scenario =
          With_contract
            { source = path
            ; balance = Amount.Mutez 0L
            ; parameters = Parameters.Raw parameters
            ; actions = [] } }

    let cmdliner_converter () =
      let open Cmdliner in
      let open Arg in
      pair ~sep:':' file string

    let cmdliner_term () =
      let open Cmdliner in
      let open Term in
      pure (fun l ->
          List.mapi l ~f:(fun ith (path, parameters) ->
              contract (Fmt.strf "cli%d" ith) ~path ~parameters))
      $ Arg.(
          value
            (opt_all
               (cmdliner_converter ())
               []
               (info
                  ["originate"]
                  ~doc:
                    "Originate a contract with parameters, format: \
                     `path:parameters`.")))
  end

  let cmdliner_term () =
    let open Cmdliner in
    let open Term in
    pure (fun jsons originations ->
        originations
        @ List.concat_map jsons ~f:(fun f ->
              let i = Caml.open_in f in
              let j = Ezjsonm.from_channel i in
              Caml.close_in i;
              try Codec.of_json j with
              | Json_encoding.Cannot_destruct (p, exn) ->
                  Fmt.failwith
                    "Parsing Scenario (%s): %a"
                    (Json_query.json_pointer_of_path p)
                    Exn.pp
                    exn))
    $ Arg.(
        value
          (opt_all
             file
             []
             (info ["run-scenarios"] ~doc:"Run a JSON list of test scenarios.")))
    $ Just_originate.cmdliner_term ()
end

(** For now, {!Scenario} and {!Smartml_scenario} are separate for
    backwards compatibility. *)
module Smartml_scenario = struct
  module History = struct
    module Event = struct
      type action = Smart_ml.Basics.texpr Smart_ml.Scenario.action

      type kind =
        | Origination    of
            { id : int
            ; address : string }
        | Call           of
            { address : string
            ; entry_point : string
            ; arg : string }
        | Action_failure of
            { message : string
            ; expected : bool
            ; process : Process_result.t option
            ; attach : (string * string) list }
        | Verification   of
            { contract : string
            ; argument : string
            ; process : Process_result.t }

      type t =
        { balance_update : string * string
        ; event : kind
        ; action : action option }
    end

    type t =
      { initial_balance : string
      ; history : Event.t list }

    let make ~initial_balance () = {initial_balance; history = []}

    let append state hist = {state with history = state.history @ hist}

    let balance state =
      match state.history with
      | {balance_update = _, b; _} :: _ -> b
      | [] -> state.initial_balance

    let find_contract state contract_id =
      List.find_map state.history ~f:(function
          | { event = Origination {id; address}
            ; action = Some (New_contract {contract; _})
            ; _ }
            when id = contract_id ->
              Some (address, List.length contract.entry_points)
          | _ -> None)

    let not_implemented state action =
      let bal = balance state in
      let open Smart_ml.Scenario in
      let expected =
        match action with
        | Simulation _ | Html _ | Error _ | Show _ | Exception _ -> true
        | New_contract _ | Message _ | Compute _ | Verify _ -> false
      in
      let name =
        match action with
        | New_contract _ -> "New_contract"
        | Compute _ -> "Compute"
        | Simulation _ -> "Simulation"
        | Message _ -> "Message"
        | Error _ -> "Error"
        | Html _ -> "Html"
        | Verify _ -> "Verify"
        | Show _ -> "Show"
        | Exception _ -> "Exception"
      in
      append
        state
        [ { balance_update = (bal, bal)
          ; action = Some action
          ; event =
              Action_failure
                { message = Fmt.str "%s: not implemented" name
                ; expected
                ; attach = []
                ; process = None } } ]

    let origination state ~action ~balance_before ~balance_after ~id ~address =
      append
        state
        [ { balance_update = (balance_before, balance_after)
          ; action = Some action
          ; event = Origination {id; address} } ]

    let call
        state ~action ~balance_before ~balance_after ~address ~entry_point ~arg
        =
      append
        state
        [ { balance_update = (balance_before, balance_after)
          ; action = Some action
          ; event = Call {address; entry_point; arg} } ]

    let verification ?action state ~process ~tz_contract ~arg =
      let bal = balance state in
      append
        state
        [ { balance_update = (bal, bal)
          ; action
          ; event =
              Verification {contract = tz_contract; argument = arg; process} }
        ]

    let failure
        state ?(attach = []) ?action ?process ?(expected = false) message =
      let bal = balance state in
      append
        state
        [ { balance_update = (bal, bal)
          ; action
          ; event = Action_failure {message; expected; process; attach} } ]

    let pp_quick ppf t =
      let action_code =
        let open Smart_ml.Scenario in
        function
        | Some (New_contract _) -> "O"
        | Some (Message _) -> "M"
        | Some (Verify _) -> "V"
        | _ -> "_"
      in
      let oks, kos =
        List.fold t.history ~init:(0, 0) ~f:(fun (o, k) ev ->
            match ev.event with
            | Action_failure {expected = false; _} -> (o, k + 1)
            | Action_failure {expected = true; _}
             |Origination _ | Call _ | Verification _ ->
                (o + 1, k))
      in
      let summary =
        List.fold t.history ~init:"" ~f:(fun prev ev ->
            let symb =
              match ev.event with
              | Action_failure {expected = false; _} -> "-"
              | Action_failure {expected = true; _}
               |Origination _ | Call _ | Verification _ ->
                  "+"
            in
            let newh = Fmt.str "%s%s%s" prev symb (action_code ev.action) in
            newh)
      in
      Fmt.pf ppf "{OK: %d; KO: %d; (%s)}" oks kos summary
  end

  type t =
    { name : string
    ; sc : Smart_ml.Scenario.t
    ; scenario_state : Smart_ml.Basics.scenario_state }

  let make name sc scenario_state = {name; sc; scenario_state}

  let pp_quick ppf t =
    Fmt.pf ppf "{%s %d actions}" t.name (List.length t.sc.actions)

  let primitives =
    let open Smart_ml.Interpreter.Primitive_implementations in
    let account_of_seed s =
      let open Tezos_protocol.Account in
      let a = of_name s in
      {pk = pubkey a; pkh = pubkey_hash a; sk = private_key a}
    in
    let sign ~secret_key bytes =
      let open Tezos_crypto.Ed25519 in
      let key =
        Secret_key.of_b58check_exn
          ( match String.chop_prefix secret_key ~prefix:"unencrypted:" with
          | None -> secret_key
          | Some sk -> sk )
      in
      sign key (Bytes.of_string bytes) |> to_b58check
    in
    {faked_in_ocaml with account_of_seed; sign}

  let bake state =
    Util.client_cmd
      state
      ~client:state#client
      ["bake"; "for"; state#baking_key_name; "--force"; "--minimal-timestamp"]
    >>= fun res ->
    match res#status with
    | Unix.WEXITED 0 -> return ()
    | _ -> fail (`Scenario_error "bake failed")

  let get_contractor_balance state contractor_key_name =
    Util.client_cmd
      state
      ~client:state#client
      ["get"; "balance"; "for"; contractor_key_name]
    >>= fun res ->
    match res#status with
    | Unix.WEXITED 0 -> return (String.concat ~sep:"" res#out)
    | _ -> fail (`Scenario_error "get-balance failed")

  let call_and_bake
      ?call_as
      ?(expect_failure = false)
      ?attach
      state
      ~action
      ~name
      ~history
      cmd
      continue =
    ( match call_as with
    | None -> return state#contractor_key_name
    | Some sk ->
        let name = Fmt.str "caller%s" Caml.Digest.(string sk |> to_hex) in
        Util.client_cmd
          state
          ~client:state#client
          ~succeed:true
          ["import"; "secret"; "key"; name; sk; "--force"]
        >>= fun _ ->
        Util.client_cmd
          state
          ~client:state#client
          ~succeed:true
          [ "--wait"
          ; "none"
          ; "transfer"
          ; "10"
          ; "from"
          ; state#contractor_key_name
          ; "to"
          ; name
          ; "--burn-cap"
          ; "1" ]
        >>= fun _ -> bake state >>= fun () -> return name )
    >>= fun contractor_name ->
    get_contractor_balance state contractor_name
    >>= fun balance_before ->
    Util.client_cmd state ~client:state#client (cmd ~from:contractor_name)
    >>= fun p_result ->
    bake state
    >>= fun () ->
    get_contractor_balance state contractor_name
    >>= fun balance_after ->
    match p_result#status with
    | Unix.WEXITED 0 when not expect_failure ->
        continue (p_result, balance_before, balance_after)
    | Unix.WEXITED 0 (* expect_failure *) ->
        return
          (History.failure
             history
             ~action
             ~process:p_result
             (Fmt.str "%s returned zero while expected to fail." name))
    | _ ->
        Dbg.e
          EF.(
            desc
              (wf "%s failed" name)
              (markdown_verbatim (String.concat ~sep:"\n" p_result#err)));
        return
          (History.failure
             ?attach
             history
             ~action
             ~process:p_result
             ~expected:expect_failure
             (Fmt.str "%s returned non-zero." name))

  let run state (scenario : t) ~client ~baker_account ~contractor_account :
      (History.t, _) Asynchronous_result.t =
    (* let {Tezos_client.Keyed.client; key_name; _} = client in *)
    let state =
      object
        method application_name = state#application_name

        method paths = state#paths

        method runner = state#runner

        method client = client

        method baker_account = baker_account

        method contractor_account = contractor_account

        method baking_key_name = Tezos_protocol.Account.name baker_account

        method contractor_key_name =
          Tezos_protocol.Account.name contractor_account

        method scenario_state = scenario.scenario_state
      end
    in
    let register_contract state ~id c =
      Caml.Hashtbl.replace state#scenario_state.Smart_ml.Basics.contracts id c
    in
    bake state
    >>= fun () ->
    get_contractor_balance state state#contractor_key_name
    >>= fun balance ->
    let history = History.make ~initial_balance:balance () in
    List.fold
      scenario.sc.actions
      ~init:(return history)
      ~f:(fun prevm action ->
        prevm
        >>= fun history ->
        Smart_ml.Scenario.(
          match (action : _ action) with
          | New_contract {id; contract; _} as action ->
              let contract =
                { contract with
                  storage =
                    Smart_ml.Interpreter.interpret_expr_external
                      ~primitives
                      ~no_env:[`Text "Compute storage"]
                      ~scenario_state:state#scenario_state
                      contract.storage }
              in
              register_contract state ~id contract;
              let storage = Smart_ml.Compiler.michelson_storage contract in
              let contract_tz =
                Smart_ml.Compiler.(
                  michelson_contract contract |> michelson_contract_michelson)
              in
              let name =
                Fmt.str
                  "contract-%s-%d-%s-%d"
                  (String.map scenario.name ~f:(function
                      | ('a' .. 'z' | 'A' .. 'Z' | '0' .. '9') as c -> c
                      | _ -> '_'))
                  id
                  Caml.Digest.(string contract_tz |> to_hex)
                  (Monotone_id.next ())
              in
              let burn_cap = "1_000_000" in
              let balance = "0" in
              let tmpfile = Caml.Filename.temp_file name ".tz" in
              System.write_file state tmpfile ~content:contract_tz
              >>= fun () ->
              Dbg.f (fun pf ->
                  pf
                    "originating: %s %d %s %s"
                    name
                    id
                    Caml.Digest.(
                      string (Smart_ml.Compiler.michelson_storage contract)
                      |> to_hex)
                    tmpfile);
              let cmd ~from =
                [ "--wait"
                ; "none"
                ; "originate"
                ; "contract"
                ; name
                ; "transferring"
                ; balance
                ; "from"
                ; from
                ; "running"
                ; tmpfile
                ; "--init"
                ; storage
                ; "--burn-cap"
                ; burn_cap ]
              in
              call_and_bake
                state
                ~name:"Origination"
                ~history
                cmd
                ~action
                ~attach:
                  [ ("name", name)
                  ; ("contract.tz", contract_tz)
                  ; ("init.tz", storage) ]
                (fun (_result, balance_before, balance_after) ->
                  Util.client_cmd
                    state
                    ~client
                    ["show"; "known"; "contract"; name]
                  >>= fun show_contract_output ->
                  let address =
                    String.concat ~sep:"" show_contract_output#out
                  in
                  Dbg.f (fun pp ->
                      pp
                        "show contract: %S"
                        (String.concat ~sep:"" show_contract_output#out));
                  return
                    (History.origination
                       history
                       ~action
                       ~balance_before
                       ~balance_after
                       ~id
                       ~address))
          | Message
              { id
              ; valid
              ; params
              ; line_no = _
              ; title = _
              ; messageClass = _
              ; sender
              ; source = _
              ; time = _
              ; amount
              ; message } as action ->
              Dbg.e EF.(wf "Calling %d#%s" id message);
              ( match History.find_contract history id with
              | None ->
                  return
                    (History.failure
                       history
                       ~action
                       (Fmt.str
                          "Cannot find address contract %d to call %S"
                          id
                          message))
              | Some (address, nb_of_entry_points) ->
                  let smartml_value =
                    Smart_ml.Interpreter.interpret_expr_external
                      ~primitives
                      ~no_env:
                        [ `Text "Computing params"
                        ; `Expr params
                        ; `Line params.el ]
                      ~scenario_state:state#scenario_state
                      params
                  in
                  let mich = Smart_ml.Compiler.compile_value smartml_value in
                  let arg =
                    Smart_ml.Michelson.Literal.to_michelson_string mich
                  in
                  Dbg.e
                    EF.(
                      desc_list
                        (haf "valueeeee")
                        [ wf
                            "texpr: %s"
                            (Smart_ml.Printer.expr_to_string params)
                        ; wf "mich: %s" (Smart_ml.Michelson.Literal.show mich)
                        ; wf "arg: %s" arg ]);
                  let amount =
                    Smart_ml.Value.unMutez
                      (Smart_ml.Interpreter.interpret_expr_external
                         ~primitives
                         ~no_env:
                           [ `Text "Computing amount"
                           ; `Expr amount
                           ; `Line amount.el ]
                         ~scenario_state:state#scenario_state
                         amount)
                  in
                  let cmd ~from =
                    [ "--wait"
                    ; "none"
                    ; "transfer"
                    ; Big_int.string_of_big_int amount
                    ; "from"
                    ; from
                    ; "to"
                    ; address
                    ; "--arg"
                    ; arg
                    ; "--burn-cap"
                    ; "20" ]
                    @
                    if nb_of_entry_points = 1
                    then []
                    else ["--entrypoint"; message]
                  in
                  let call_as =
                    Option.map sender ~f:(function
                        | Account s -> s.sk
                        | Address address ->
                            Fmt.failwith
                              "Cannot use the sandbox with an explicit \
                               address %s"
                              address)
                  in
                  call_and_bake
                    state
                    ~name:"Contract-call"
                    ~history
                    ?call_as
                    ~expect_failure:(not valid)
                    cmd
                    ~action
                    (fun (_result, balance_before, balance_after) ->
                      Dbg.e
                        EF.(
                          wf
                            "call: %d#%s: %s -> %s"
                            id
                            message
                            balance_before
                            balance_after);
                      return
                        (History.call
                           history
                           ~action
                           ~balance_before
                           ~balance_after
                           ~address
                           ~entry_point:message
                           ~arg)) )
          | Verify {condition; _} as action ->
              let contract =
                contract_of_verification_texpr
                  ~primitives
                  ~scenario_state:state#scenario_state
                  condition
              in
              let contract =
                { contract with
                  storage =
                    Smart_ml.Interpreter.interpret_expr_external
                      ~primitives
                      ~no_env:[`Text "Compute storage"]
                      ~scenario_state:state#scenario_state
                      contract.storage }
              in
              let tz_contract =
                Smart_ml.Compiler.michelson_contract_michelson
                  (Smart_ml.Compiler.michelson_contract contract)
              in
              (*
let all_addresses =
              Caml.Hashtbl.fold
                (fun key _ [] ->
                   match History.find_contract history id with
                  prev_m
                  >>= fun prev ->
                  storage_of_contract key >>= fun sto -> return (sto :: prev))
                state#scenario_state.contracts
                (return [])
              let address
 *)
              let storage_of_contract id =
                match History.find_contract history id with
                | None ->
                    failf "contract not found: %d (making verify parameter)" id
                | Some (addr, _) ->
                    Util.client_cmd
                      state
                      ~client
                      ["get"; "contract"; "storage"; "for"; addr]
                    >>= fun res ->
                    ( match res#status with
                    | Unix.WEXITED 0 -> return (String.concat ~sep:" " res#out)
                    | _ -> fail (`Scenario_error "get-storage failed") )
              in
              Asynchronous_result.bind_on_result
                (Caml.Hashtbl.fold
                   (fun key _ prev_m ->
                     prev_m
                     >>= fun prev ->
                     storage_of_contract key >>= fun sto -> return (sto :: prev))
                   state#scenario_state.contracts
                   (return []))
                ~f:(function
                  | Ok arg_list ->
                      let arg =
                        match arg_list with
                        | [] -> "Unit"
                        | [one] -> one
                        | one :: more ->
                            (* XXX tricky part: TODO check order Vs compilation of records *)
                            List.fold more ~init:one ~f:(fun prev x ->
                                Fmt.str "(Pair %s %s)" prev x)
                      in
                      Dbg.e
                        EF.(
                          desc_list
                            (af "Verify WIP")
                            [ wf "Param: %s" arg
                            ; wf
                                "Contract: %s"
                                (String.subo tz_contract ~len:50) ]);
                      Util.client_cmd
                        state
                        ~client
                        [ "run"
                        ; "script"
                        ; tz_contract
                        ; "on"
                        ; "storage"
                        ; "Unit"
                        ; "and"
                        ; "input"
                        ; arg
                        ; "-G"
                        ; "1000000000" ]
                      >>= fun res ->
                      ( match res#status with
                      | Unix.WEXITED 0 ->
                          return
                            (History.verification
                               history
                               ~action
                               ~process:res
                               ~tz_contract
                               ~arg)
                      | _ ->
                          return
                            (History.failure
                               history
                               ~action
                               ~process:res
                               ~attach:
                                 [ ("contract.tz", tz_contract)
                                 ; ("parameters.tz", arg) ]
                               (Fmt.str "Verification failed")) )
                  | Error e ->
                      return
                        (History.failure
                           history
                           ~action
                           ~attach:[("contract.tz", tz_contract)]
                           (Fmt.str
                              "Verification failed because: %s"
                              ( match e with
                              | `Scenario_error s -> s
                              | `Lwt_exn e -> Exn.to_string e ))))
          | (Compute _ | Simulation _ | Error _ | Html _ | Show _ | Exception _)
            as action ->
              return (History.not_implemented history action)
          | exception e ->
              return
                (History.failure
                   history
                   (Fmt.str
                      "Failed to finish parsing action: %s"
                      ( match e with
                      | Smart_ml.Basics.SmartExcept e ->
                          String.concat
                            ~sep:" "
                            (List.map
                               ~f:(Smart_ml.Printer.pp_smart_except false)
                               e)
                      | other -> Exn.to_string other )))))

  let full_results_md
      ?(extras_prefix = "_files/data-") (full_results : (t * History.t) list) =
    let open Fmt in
    let main = Buffer.create 42 in
    let extras = ref [] in
    let new_extra name content =
      extras := (name, String.concat ~sep:"\n" content) :: !extras
    in
    let ppf = with_buffer ~like:stdout main in
    let in_vbox ppf f = vbox (fun ppf () -> f ppf) ppf () in
    let title ppf char s =
      in_vbox ppf (fun ppf ->
          string ppf s;
          cut ppf ();
          string ppf (String.map s ~f:(fun _ -> char));
          cut ppf ())
    in
    let italic ppf s = pf ppf "*%s*" s in
    let bold ppf s = pf ppf "**%s**" s in
    let nl ppf = cut ppf () in
    in_vbox ppf (fun ppf ->
        kstr (title ppf '=') "Test Results (%d)" (List.length full_results);
        nl ppf;
        List.iteri full_results ~f:(fun result_index (sce, history) ->
            kstr (title ppf '-') "Test `%s`" sce.name;
            nl ppf;
            pf ppf "Initial balance: %s@,@," history.initial_balance;
            List.iteri
              history.history
              ~f:
                History.Event.(
                  fun event_index ev ->
                    let name =
                      str "%s%d-%d" extras_prefix result_index event_index
                    in
                    pf ppf "* ";
                    Option.iter ev.action ~f:(function
                        | New_contract {line_no; _} -> pf ppf "(:%d) " line_no
                        | Message {line_no; _} -> pf ppf "(:%d) " line_no
                        | Verify {line_no; _} -> pf ppf "(:%d) " line_no
                        | _ -> ());
                    ( match ev.event with
                    | Origination {id; address} ->
                        italic ppf "Origination:";
                        pf ppf " %d → `%s`" id address
                    | Call {address; entry_point; arg} ->
                        let argstr =
                          if String.length arg > 20
                          then (
                            let path = name ^ "-arg.tz" in
                            new_extra path [arg];
                            str "[`ARG`](%s)" path )
                          else str "`%s`" arg
                        in
                        italic ppf "Call:";
                        pf
                          ppf
                          " `%s#%s(` %s `)`"
                          (String.sub address ~pos:0 ~len:11)
                          entry_point
                          argstr
                    | Verification {contract; argument; process} ->
                        let tz = name ^ "-verify.tz" in
                        let arg = name ^ "-param.tz" in
                        let out = name ^ "-out.log" in
                        let err = name ^ "-err.log" in
                        new_extra tz [contract];
                        new_extra arg [argument];
                        new_extra out process#out;
                        new_extra err process#err;
                        italic ppf "Successful Verification:";
                        pf
                          ppf
                          "@,  ([tz](%s), [arg](%s), [out](%s), [err](%s))"
                          tz
                          arg
                          out
                          err
                    | Action_failure {message; expected; process; attach} ->
                        if expected
                        then pf ppf "*Expected Failure:*"
                        else bold ppf "Failure:";
                        pf ppf " “%s”" message;
                        Option.iter process ~f:(fun p ->
                            pf
                              ppf
                              " → `...%s`"
                              ( List.last p#err
                              |> Option.value ~default:"NO OUTPUT" ));
                        List.iter attach ~f:(fun (k, content) ->
                            let f = str "%s-%s" name k in
                            new_extra f [content];
                            pf ppf "@,    * [%s](%s)" k f);
                        ( match process with
                        | None -> ()
                        | Some pres ->
                            let out = name ^ "-out.log" in
                            let err = name ^ "-err.log" in
                            new_extra out pres#out;
                            new_extra err pres#err;
                            pf ppf "@,    * [stdout](%s), [stderr](%s)" out err
                        ) );
                    nl ppf;
                    let ba, bb = ev.balance_update in
                    if String.equal ba bb
                    then ()
                    else pf ppf "    * %s → %s@," ba bb);
            nl ppf));
    flush ppf ();
    (Buffer.contents main, !extras)

  let cmdliner_term () =
    let open Cmdliner in
    let open Term in
    pure (fun jsons ->
        let scenarios =
          List.map jsons ~f:(fun filename ->
              let scenario_state = Smart_ml.Basics.scenario_state () in
              make
                Caml.Filename.(basename filename |> chop_extension)
                (Smart_ml.Scenario.load_from_string
                   ~primitives
                   ~scenario_state
                   (Yojson.Basic.from_file filename))
                scenario_state)
        in
        scenarios)
    $ Arg.(
        value
          (opt_all
             file
             []
             (info ["scenario"] ~doc:"Run a JSON/SmartML test scenarios.")))
end

let fail_test s = fail (`Scenario_error s)

let start
    state
    ~base_port
    ~size
    ~node_exec
    ~client_exec
    ~admin_exec
    ~scenarios
    ~full_scenarios
    ~output_results_path =
  Helpers.clear_root state
  >>= fun () ->
  Helpers.System_dependencies.precheck
    state
    `Or_fail
    ~executables:[node_exec; client_exec; admin_exec]
  >>= fun () ->
  Interactive_test.Pauser.generic
    state
    EF.[af "Ready to start"; af "Root path deleted."]
  >>= fun () ->
  let protocol, baker_account, contractor_account =
    let open Tezos_protocol in
    let d = default () in
    let baker =
      (Tezos_protocol.Account.of_name "main-baker", 400_000_000_000_000L)
    in
    let contractor =
      (Tezos_protocol.Account.of_name "contractor", 1_000_000_000_000L)
    in
    ( { d with
        time_between_blocks = [1; 0]
      ; hash = "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
      ; bootstrap_accounts = [baker; contractor] }
    , fst baker
    , fst contractor )
  in
  Test_scenario.network_with_protocol
    ~protocol
    ~size
    ~base_port
    state
    ~node_exec
    ~client_exec
  >>= fun (nodes, protocol) ->
  let make_admin = Tezos_admin_client.of_client ~exec:admin_exec in
  Interactive_test.Pauser.add_commands
    state
    Interactive_test.Commands.(
      all_defaults state ~nodes
      @ [ secret_keys state ~protocol
        ; Log_recorder.Operations.show_all state
        ; arbitrary_command_on_clients
            state
            ~command_names:["all-clients"; "cc"]
            ~make_admin
            ~clients:
              (List.map nodes ~f:(Tezos_client.of_node ~exec:client_exec)) ]);
  Interactive_test.Pauser.generic state EF.[af "About to really start playing"]
  >>= fun () ->
  let client n =
    Tezos_client.of_node ~exec:client_exec (List.nth_exn nodes n)
  in
  let baker_0 =
    Tezos_client.Keyed.make
      (client 0)
      ~key_name:(Tezos_protocol.Account.name baker_account)
      ~secret_key:(Tezos_protocol.Account.private_key baker_account)
  in
  Tezos_client.Keyed.initialize state baker_0
  >>= fun _ ->
  let contractor_0 =
    Tezos_client.Keyed.make
      (client 0)
      ~key_name:(Tezos_protocol.Account.name contractor_account)
      ~secret_key:(Tezos_protocol.Account.private_key contractor_account)
  in
  Tezos_client.Keyed.initialize state contractor_0
  >>= fun _ ->
  let first_bakes = 1 in
  Loop.n_times first_bakes (fun nth ->
      ksprintf (Tezos_client.Keyed.bake state baker_0) "initial-bake %d" nth)
  >>= fun () ->
  List.fold ~init:(return []) scenarios ~f:(fun prevm scenario ->
      prevm
      >>= fun results ->
      let ith = List.length results + 1 in
      Console.sayf
        state
        Fmt.(
          fun ppf () ->
            pf ppf "[%d] Running scenario:@ %a" ith Scenario.pp scenario)
      >>= fun () ->
      Scenario.run
        state
        ~client:(client 0)
        scenario
        ~baker_account
        ~contractor_account
      >>= fun result -> return ((scenario, result) :: results))
  >>= fun results ->
  ( match output_results_path with
  | None -> return ""
  | Some s ->
      Lwt_exception.catch
        (fun () ->
          Lwt_io.with_file ~mode:Lwt_io.output s (fun o ->
              let buf = Buffer.create 42 in
              let ppf = Fmt.with_buffer ~like:Fmt.stdout buf in
              Fmt.pf ppf "%a\n%!" Scenario.pp_full_results results;
              Lwt_io.write o (Buffer.contents buf)))
        ()
      >>= fun () -> return (Fmt.strf " (output to %S)" s) )
  >>= fun output_msg ->
  Console.sayf state (fun ppf () ->
      Fmt.pf ppf "%a\n%!" Scenario.pp_full_results results)
  >>= fun () ->
  List.fold ~init:(return []) full_scenarios ~f:(fun prevm scenar ->
      prevm
      >>= fun results ->
      Smartml_scenario.run
        state
        scenar
        ~client:(client 0)
        ~baker_account
        ~contractor_account
      >>= fun result -> return ((scenar, result) :: results))
  >>= fun full_results ->
  ( match output_results_path with
  | None -> return ""
  | Some s ->
      let path = Caml.Filename.chop_extension s ^ "-full" in
      let main, extras =
        Smartml_scenario.full_results_md ~extras_prefix:"./data-" full_results
      in
      Running_processes.run_cmdf state "mkdir -p %s" path
      >>= fun _ ->
      List_sequential.iter
        (("index.md", main) :: extras)
        ~f:(fun (file, content) ->
          System.write_file state (path // file) ~content)
      >>= fun () -> return (Fmt.strf " (output to %S)" path) )
  >>= fun full_output_msg ->
  Interactive_test.Pauser.generic
    state
    EF.
      [ haf "End of sandbox test:"
      ; desc_list
          (af "Results%s:" output_msg)
          (List.map results ~f:(fun (s, r) ->
               af "%a: %a" Scenario.pp_quick s Scenario.pp_results_quick r))
      ; desc_list
          (af "Results WIP: (%s)" full_output_msg)
          (List.map full_results ~f:(fun (s, r) ->
               af
                 "%a: %a"
                 Smartml_scenario.pp_quick
                 s
                 Smartml_scenario.History.pp_quick
                 r)) ]
  >>= fun () ->
  if List.for_all (List.concat_map ~f:snd results) ~f:(function
         | {status = `Ok; _} -> true
         | _ -> false)
  then return ()
  else fail_test "Some tests failed …"

let cmd_scenario_example () =
  let open Cmdliner in
  let open Term in
  let term =
    pure (fun () ->
        Fmt.pr
          "%s\n%!"
          (Ezjsonm.value_to_string
             ~minify:false
             (Scenario.Codec.to_json Scenario.Example.all)))
    $ pure ()
  in
  (term, info "show-scenario-example" ~doc:"Dump example scenarios as JSON.")

let cmd_smartbox () =
  let pp_error ppf = function
    | `Scenario_error s -> Fmt.pf ppf "%s" s
    | #Test_scenario.Inconsistency_error.t as e ->
        Fmt.pf ppf "%a" Test_scenario.Inconsistency_error.pp e
    | #Process_result.Error.t as e -> Fmt.pf ppf "%a" Process_result.Error.pp e
    | `Lwt_exn _ as e -> Fmt.pf ppf "%a" Lwt_exception.pp e
    | `Sys_error _ as e -> Fmt.pf ppf "%a" System_error.pp e
    | `Client_command_error _ as e -> Tezos_client.Command_error.pp ppf e
    | `Precheck_failure _ as e -> Helpers.System_dependencies.Error.pp ppf e
    | `Admin_command_error _ as e -> Tezos_admin_client.Command_error.pp ppf e
  in
  let open Cmdliner in
  let open Term in
  Test_command_line.Run_command.make
    ~pp_error
    ( pure
        (fun node_exec
             client_exec
             admin_exec
             size
             (`Base_port base_port)
             scenarios
             full_scenarios
             output_results_path
             state
             ->
          ( state
          , Interactive_test.Pauser.run_test state ~pp_error (fun () ->
                start
                  state
                  ~base_port
                  ~size
                  ~node_exec
                  ~client_exec
                  ~output_results_path
                  ~scenarios
                  ~full_scenarios
                  ~admin_exec) ))
    $ Tezos_executable.cli_term `Node "tezos"
    $ Tezos_executable.cli_term `Client "tezos"
    $ Tezos_executable.cli_term `Admin "tezos"
    $ Arg.(value (opt int 1 (info ["size"; "S"] ~doc:"Size of the Network.")))
    $ Arg.(
        pure (fun p -> `Base_port p)
        $ value
            (opt
               int
               46_000
               (info ["base-port"] ~doc:"Base port number to build upon.")))
    $ Scenario.cmdliner_term ()
    $ Smartml_scenario.cmdliner_term ()
    $ Arg.(
        value
          (opt
             (some string)
             None
             (info ["output-results"] ~doc:"Output results to a file.")))
    $ Test_command_line.cli_state ~name:"smartbox" () )
    (info ~doc:"Sandbox to originate SmartML contracts" "smartbox")
