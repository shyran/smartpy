(* Copyright 2019-2020 Smart Chain Arena LLC. *)

module Hashtbl_ = Hashtbl
open Base

module Io = struct
  let sexp_of_file_exn path f =
    Parsexp.Conv_single.parse_string_exn
      Caml.(
        let i = open_in path in
        let b = Buffer.create 42 in
        try
          while true do
            Buffer.add_char b (input_char i)
          done;
          "<nope>"
        with
        | _ ->
            close_in i;
            Buffer.contents b)
      f

  let contract_of_file_exn path =
    let scenario_state = Smart_ml.Basics.scenario_state () in
    try
      let contract =
        sexp_of_file_exn
          path
          (Smart_ml.Import.import_contract
             ~primitives:
               Smart_ml.Interpreter.Primitive_implementations.faked_in_ocaml
             ~scenario_state
             (Smart_ml.Typing.init_env ~contract_data_types:(Hashtbl_.create 5)))
      in
      { contract with
        storage =
          Smart_ml.Interpreter.interpret_expr_external
            ~primitives:
              Smart_ml.Interpreter.Primitive_implementations.faked_in_ocaml
            ~no_env:[`Text "Compute storage"]
            ~scenario_state
            contract.storage }
    with
    | exn -> failwith (Smart_ml.Printer.exception_to_string false exn)

  let pp_to_file path f =
    let open Caml in
    let open Format in
    let o = open_out path in
    let ppf = formatter_of_out_channel o in
    pp_set_margin ppf 80;
    f ppf;
    pp_print_flush ppf ();
    flush o;
    ()
end

let say fmt = Fmt.epr Caml.(fmt ^^ "\n%!")

module Tezos_michelson = Babylonian_michelson

let () =
  let open Cmdliner in
  let help = Term.(ret (pure (`Help (`Auto, None))), info "smartml") in
  let open Term in
  let input_smart_ml_arg () =
    Arg.(
      required
        (pos
           0
           (some file)
           None
           (info
              []
              ~docv:"INPUT-FILE"
              ~docs:"ARGUMENTS"
              ~doc:"Input file, by default, a SmartML S-Expression.")))
  in
  let output_michelson_arg () =
    Arg.(
      pure (fun output_format ->
        function
        | Some "-" -> (Option.value ~default:`Michelson output_format, None)
        | x ->
          ( match output_format with
          | Some s -> (s, x)
          | None ->
            ( match Option.map ~f:Caml.Filename.extension x with
            | Some ".json" -> (`Json, x)
            | Some ".tz" -> (`Michelson, x)
            | _ -> (`Michelson, x) ) ))
      $ value
          (opt
             (enum [("json", `Json); ("michelson", `Michelson)] |> some)
             None
             (info
                ["output-format"; "F"]
                ~doc:
                  "Output format of the michelson (by default guess from the \
                   extension or .tz if none)."))
      $ value
          (pos
             1
             (some string)
             None
             (info
                []
                ~docv:"OUTPUT-FILE"
                ~docs:"ARGUMENTS"
                ~doc:"Output .tz or .json file (or `-` for stdout).")))
  in
  let compile_cmd =
    ( pure
        (fun input_file
             (output_format, output_file_opt)
             output_binary_size
             options
             ()
             ->
          let smartml_contract = Io.contract_of_file_exn input_file in
          let compiled =
            Tezos_michelson.of_smart_ml ~options smartml_contract
          in
          let output ppf =
            match output_format with
            | `Michelson ->
                Fmt.pf ppf "@[%a@]@\n%!" Tezos_michelson.pp compiled
            | `Json ->
                Fmt.pf
                  ppf
                  "%s"
                  (Ezjsonm.to_string
                     ~minify:false
                     (Tezos_michelson.to_json compiled))
          in
          ( match output_file_opt with
          | Some s -> Io.pp_to_file s output
          | None -> output Fmt.stdout );
          Option.iter output_binary_size ~f:(fun file ->
              Io.pp_to_file
                file
                Fmt.(
                  fun ppf ->
                    pf ppf "%d\n" (Tezos_michelson.binary_size compiled)));
          say
            "Done (options: %a): %s → %a (%d bytes)."
            Tezos_michelson.Of_smart_ml_michelson.Options.pp
            options
            input_file
            Fmt.(option ~none:(const string "stdout") string)
            output_file_opt
            (Tezos_michelson.binary_size compiled))
      $ input_smart_ml_arg ()
      $ output_michelson_arg ()
      $ Arg.(
          value
            (opt
               (some string)
               None
               (info
                  ["write-binary-size"]
                  ~doc:"Write computed binary size to a file.")))
      $ Tezos_michelson.Of_smart_ml_michelson.Options.cli_term ()
      $ pure ()
    , Term.info "compile" ~doc:"Compiler from SmartML to Michelson" )
  in
  let init_storage_cmd =
    ( pure (fun input_file (output_format, output_file_opt) () ->
          let smartml_contract = Io.contract_of_file_exn input_file in
          let compiled =
            Tezos_michelson.storage_initialization smartml_contract
          in
          let output ppf =
            match output_format with
            | `Michelson ->
                Fmt.pf ppf "@[%a@]@\n%!" Tezos_michelson.pp compiled
            | `Json ->
                Fmt.pf
                  ppf
                  "%s"
                  (Ezjsonm.to_string
                     ~minify:false
                     (Tezos_michelson.to_json compiled))
          in
          ( match output_file_opt with
          | Some s -> Io.pp_to_file s output
          | None -> output Fmt.stdout );
          say
            "Done: %s → %a."
            input_file
            Fmt.(option ~none:(const string "stdout") string)
            output_file_opt)
      $ input_smart_ml_arg ()
      $ output_michelson_arg ()
      $ pure ()
    , Term.info "initialization" ~doc:"Get the initialization of the storage"
    )
  in
  let decompile_cmd =
    ( pure (fun () ->
          try Smart_ml.Decompiler.test () with
          | exn ->
              Stdlib.print_endline
                (Smart_ml.Printer.exception_to_string false exn);
              exit (`Error `Exn))
      $ pure ()
    , Term.info "decompile" ~doc:"Decompile a contract (experimental)" )
  in
  let pack_value_cmd =
    ( ( pure (fun literal using ->
            let sexp = Parsexp.Conv_single.parse_string_exn literal Fn.id in
            let expr =
              Smart_ml.Import.import_expr
                (Smart_ml.Typing.init_env
                   ~contract_data_types:(Hashtbl_.create 5))
                (Smart_ml.Import.early_env ())
                sexp
            in
            let value =
              Smart_ml.Interpreter.interpret_expr_external
                ~primitives:
                  Smart_ml.Interpreter.Primitive_implementations.faked_in_ocaml
                ~no_env:
                  [`Text "Computing expression"; `Expr expr; `Line expr.el]
                ~scenario_state:(Smart_ml.Basics.scenario_state ())
                expr
            in
            let from_tezos =
              Smart_ml.Compiler.compile_value value
              |> Tezos_michelson.Of_smart_ml_michelson.literal
              |> Tezos_michelson.of_node
            in
            say
              "Packing equivalent to Michelson literal: %a"
              Tezos_michelson.pp
              from_tezos;
            Fmt.pr
              "0x%a\n%!"
              (fun ppf () ->
                match using with
                | `Tezos_protocol ->
                    let hex =
                      from_tezos
                      |> Tezos_data_encoding.Data_encoding.Binary.to_bytes_exn
                           Tezos_michelson.Tz_proto.Script_repr.expr_encoding
                      |> Tezos_stdlib.MBytes.to_hex
                    in
                    Fmt.pf ppf "05%a" Hex.pp hex
                | `Smart_ml ->
                    let hex =
                      Smart_ml.Interpreter.pack_value value |> Hex.of_string
                    in
                    Fmt.pf ppf "%a" Hex.pp hex)
              ())
      $ Arg.(
          required
            (pos
               0
               (some string)
               None
               (info [] ~doc:"Expression literal (S-Expr)")))
      $ Arg.(
          value
            (opt
               (enum
                  [("tezos-protocol", `Tezos_protocol); ("smartml", `Smart_ml)])
               `Smart_ml
               (info
                  ["using"]
                  ~doc:"How to do the generation (should be always equal).")))
      )
    , Term.info
        "pack-value"
        ~doc:"Output michelson-binary bytes for a given value." )
  in
  let pretty_print_sexp_cmd =
    ( ( pure (fun input_path output_path ->
            Io.pp_to_file output_path (fun ppf ->
                Base.Sexp.pp_hum ppf (Io.sexp_of_file_exn input_path Fn.id)))
      $ Arg.(required (pos 0 (some string) None (info [] ~doc:"Input file")))
      $ Arg.(required (pos 1 (some string) None (info [] ~doc:"Output file")))
      )
    , Term.info "pp-sexp" ~doc:"Pretty print an s-expression file." )
  in
  let key_of_name_command () =
    let open Cmdliner in
    let open Term in
    ( ( pure (fun n ->
            let open Flextesa.Tezos_protocol.Account in
            let account = of_name n in
            Fmt.pr
              "%s,%s,%s,%s\n%!"
              (name account)
              (pubkey account)
              (pubkey_hash account)
              (private_key account))
      $ Arg.(
          required
            (pos
               0
               (some string)
               None
               (info [] ~docv:"NAME" ~doc:"String to generate the data from.")))
      )
    , info
        "key-of-name"
        ~doc:"Make an unencrypted key-pair deterministically from a string."
        ~man:
          [ `P
              "`smartml key-of-name hello-world` generates a key-pair of the \
               `unencrypted:..` kind and outputs it as a 4 values separated \
               by commas: `name,pub-key,pub-key-hash,private-uri` (hence \
               compatible with the `--add-bootstrap-account` option of some \
               of the flextesa-sandbox scenarios)." ] )
  in
  let ocaml_cmd =
    ( Term.(
        pure (fun input_file output_file () ->
            let smartml_contract = Io.contract_of_file_exn input_file in
            Io.pp_to_file
              output_file
              Fmt.(
                fun ppf ->
                  pf
                    ppf
                    "@[<2>let contract =@,%a@]@."
                    Smart_ml.Basics.pp_tcontract
                    smartml_contract);
            say "Done: %s." input_file)
        $ Arg.(
            required
              (pos
                 0
                 (some file)
                 None
                 (info
                    []
                    ~docv:"INPUT-FILE"
                    ~docs:"ARGUMENTS"
                    ~doc:"Input file, by default, a SmartML S-Expression.")))
        $ Arg.(
            required
              (pos
                 1
                 (some string)
                 None
                 (info
                    []
                    ~docv:"OUTPUT-FILE"
                    ~docs:"ARGUMENTS"
                    ~doc:"Output .ml file.")))
        $ pure ())
    , Term.info
        "ocaml"
        ~doc:
          "Try to output a contract as an OCaml module (syntactic validity \
           not guaranteed)." )
  in
  Term.exit
    (Term.eval_choice
       (help : unit Term.t * _)
       [ Sandbox.cmd_smartbox ()
       ; Sandbox.cmd_scenario_example ()
       ; ocaml_cmd
       ; compile_cmd
       ; decompile_cmd
       ; init_storage_cmd
       ; key_of_name_command ()
       ; pack_value_cmd
       ; pretty_print_sexp_cmd ])
