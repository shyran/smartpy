(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Base
module Tz_client = Tezos_client_005_PsBabyM1
module Tz_proto = Tezos_protocol_005_PsBabyM1.Protocol

type t =
  Tz_proto.Michelson_v1_primitives.prim
  Tz_proto.Environment.Micheline.canonical

let of_node = Tz_proto.Environment.Micheline.strip_locations

let int, string, bytes, prim, seq =
  let open Tz_proto.Environment.Micheline in
  ( (fun z -> Int (42, z))
  , (fun s -> String (43, s))
  , (fun s -> Bytes (44, s))
  , (fun ?(annot = []) p args -> Prim (45, p, args, annot))
  , fun l -> Seq (46, l) )

module Prim = Tz_proto.Michelson_v1_primitives

let full ~storage ~code ~parameter =
  of_node
    (seq
       [ Prim (0, Prim.K_storage, [storage], [])
       ; Prim (1, Prim.K_parameter, [parameter], [])
       ; Prim (2, Prim.K_code, [code], []) ])

let push_string ?annot s =
  prim Prim.I_PUSH [prim Prim.T_string []; string s] ?annot

let make_failwith s =
  seq
    [ push_string s
    ; prim Prim.I_FAILWITH [] ~annot:["% From_Athenian_michelson"] ]

let pp : Caml.Format.formatter -> t -> unit =
  Tz_client.Michelson_v1_printer.print_expr_unwrapped

let push_drop_comment s =
  seq [Fmt.kstrf (push_string ~annot:["@comment"]) "%s" s; prim Prim.I_DROP []]

module Of_smart_ml_michelson = struct
  open Smart_ml.Michelson

  let rec mtype {mt} =
    match mt with
    | MTint -> prim Prim.T_int []
    | MTbool -> prim Prim.T_bool []
    | MTstring -> prim Prim.T_string []
    | MTnat -> prim Prim.T_nat []
    | MTaddress -> prim Prim.T_address []
    | MTbytes -> prim Prim.T_bytes []
    | MTmutez -> prim Prim.T_mutez []
    | MTkey_hash -> prim Prim.T_key_hash []
    | MTtimestamp -> prim Prim.T_timestamp []
    | MTannot (_, t, _) -> mtype t
    | MTpair (a, b) -> prim Prim.T_pair [mtype a; mtype b]
    | MToperation -> prim Prim.T_operation []
    | MTmap (ct, ft) -> prim Prim.T_map [mtype ct; mtype ft]
    | MTor (left, right) -> prim Prim.T_or [mtype left; mtype right]
    | MTunit -> prim Prim.T_unit []
    | MTkey -> prim Prim.T_key []
    | MTsignature -> prim Prim.T_signature []
    | MToption o -> prim Prim.T_option [mtype o]
    | MTlist l -> prim Prim.T_list [mtype l]
    | MTset e -> prim Prim.T_set [mtype e]
    | MTcontract x -> prim Prim.T_contract [mtype x]
    | MTlambda (x, f) -> prim Prim.T_lambda [mtype x; mtype f]
    | MTbig_map (k, v) -> prim Prim.T_big_map [mtype k; mtype v]
    | MTmissing msg -> Fmt.kstrf failwith "Cannot compile missing type: %s" msg

  let rec literal lit =
    let z_big bi = Z.of_string (Big_int.string_of_big_int bi) |> int in
    match (lit : Literal.t) with
    | Literal.Nat bi -> z_big bi
    | Literal.Int bi -> z_big bi
    | Literal.Bool false -> prim Prim.D_False []
    | Literal.Bool true -> prim Prim.D_True []
    | Literal.String s -> string s
    | Literal.Unit -> prim Prim.D_Unit []
    | Literal.Mutez bi -> z_big bi
    | Literal.Bytes b -> Tz_proto.Environment.MBytes.of_string b |> bytes
    | Literal.Pair (left, right) ->
        prim Prim.D_Pair [literal left; literal right]
    | Literal.None (_ : mtype) ->
        (* We are in the literal None case, not the NONE instruction *)
        prim Prim.D_None []
    | Literal.Some l -> prim Prim.D_Some [literal l]
    | Literal.Left e -> prim Prim.D_Left [literal e]
    | Literal.Right e -> prim Prim.D_Right [literal e]
    | Literal.Map {elements} ->
        seq
        @@ List.map ~f:(fun (k, v) -> prim Prim.D_Elt [literal k; literal v])
        @@ List.sort ~compare:(fun (k1, _) (k2, _) -> Literal.compare k1 k2)
        @@ elements
    | Literal.Set {elements} ->
        seq
          (List.map
             ~f:(fun k -> literal k)
             (List.sort ~compare:Literal.compare elements))
    | Literal.List {elements; t = _} ->
        seq (List.map ~f:(fun k -> literal k) elements)

  let rec c_r_macro s =
    (*
         See http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#syntactic-conveniences
         > CA(\rest=[AD]+)R / S  =>  CAR ; C(\rest)R / S
         > CD(\rest=[AD]+)R / S  =>  CDR ; C(\rest)R / S
      *)
    match s.[0] with
    | 'A' -> prim Prim.I_CAR [] :: c_r_macro (String.drop_prefix s 1)
    | 'D' -> prim Prim.I_CDR [] :: c_r_macro (String.drop_prefix s 1)
    | exception _ -> []
    | other -> Fmt.kstrf failwith "c_r_macro: wrong char: '%c' (of %S)" other s

  let dip_seq ?annot l = prim Prim.I_DIP [seq l] ?annot

  let dip ?annot i = dip_seq ?annot i

  let loop_seq ?annot l = prim Prim.I_LOOP [seq l] ?annot

  let loop ?annot i = loop_seq ?annot i

  let rec set_c_r_macro s =
    (*
         See http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#syntactic-conveniences
         > SET_CA(\rest=[AD]+)R / S   =>
             { DUP ; DIP { CAR ; SET_C(\rest)R } ; CDR ; SWAP ; PAIR } / S
         > SET_CD(\rest=[AD]+)R / S   =>
             { DUP ; DIP { CDR ; SET_C(\rest)R } ; CAR ; PAIR } / S
         Then,
         > SET_CAR  =>  CDR ; SWAP ; PAIR
         > SET_CDR  =>  CAR ; PAIR
      *)
    match s.[0] with
    | 'A' when String.length s > 1 ->
        [ prim Prim.I_DUP []
        ; dip_seq (prim Prim.I_CAR [] :: set_c_r_macro (String.drop_prefix s 1))
        ; prim Prim.I_CDR []
        ; prim Prim.I_SWAP []
        ; prim Prim.I_PAIR [] ]
    | 'A' -> [prim Prim.I_CDR []; prim Prim.I_SWAP []; prim Prim.I_PAIR []]
    | 'D' when String.length s > 1 ->
        [ prim Prim.I_DUP []
        ; dip_seq (prim Prim.I_CDR [] :: set_c_r_macro (String.drop_prefix s 1))
        ; prim Prim.I_CAR []
        ; prim Prim.I_PAIR [] ]
    | 'D' -> [prim Prim.I_CAR []; prim Prim.I_PAIR []]
    | exception _ ->
        Fmt.kstrf failwith "set_c_r_macro: called with no chars: S" s
    | other ->
        Fmt.kstrf failwith "set_c_r_macro: wrong char: '%c' (of %S)" other s

  let rec duuup_macro =
    (*
        > DUU(\rest=U* )P / S  =>  DIP (DU(\rest)P) ; SWAP / S
      *)
    function
    | 1 -> [dip [prim Prim.I_DUP []]; prim Prim.I_SWAP []]
    | n when n >= 2 ->
        [ dip_seq (duuup_macro (n - 1)) (* ~annot:[Fmt.strf "%%du%dp" n] *)
        ; prim Prim.I_SWAP [] ]
    | other -> Fmt.kstrf failwith "duuup_macro: %d" other

  let rec diiip_macro n i =
    (*
        > DII(\rest=I* )P code / S  =>  DIP (DI(\rest)P code) / S
      *)
    match n with
    | 1 -> dip i
    | n when n >= 2 ->
        dip [diiip_macro (n - 1) i] (* ~annot:[Fmt.strf "%%di%dp" n] *)
    | other -> Fmt.kstrf failwith "diiip_macro: %d" other

  let if_some_macro t e =
    (* src/proto_alpha/lib_client/michelson_v1_macros.ml
           let expand_if_some = function
             | Prim (loc, "IF_SOME", [right; left], annot) ->
                 ok @@ Some (Seq (loc, [Prim (loc, "IF_NONE", [left; right], annot)]))
        *)
    seq [prim Prim.I_IF_NONE [seq e; seq t]]

  module Options = struct
    type t = {push_drop_comments : bool} [@@deriving show {with_path = false}]

    let make ~push_drop_comments = {push_drop_comments}

    let default =
      (* We'll set the default to false when we are a bit more confident. *)
      make ~push_drop_comments:true

    let cli_term () =
      let open Cmdliner in
      let open Term in
      pure (fun push_drop_comments -> make ~push_drop_comments)
      $ ( pure ( || )
        $ Arg.(
            value
              (flag
                 (info
                    ["push-drop-comments"]
                    ~doc:
                      "Add push+drop comments in the michelson (the default).")))
        $ Arg.(
            pure not
            $ value
                (flag
                   (info
                      ["no-push-drop-comments"]
                      ~doc:"Do not add push+drop comments in the michelson.")))
        )

    let comment {push_drop_comments; _} s =
      if push_drop_comments then [push_drop_comment s] else []
  end

  let rec instruction_list ~options {tinstr; stack = _} =
    let continue = instruction_list ~options in
    let comment = Options.comment options in
    let one s = [s] in
    let one_prim ?annot p l = one (prim ?annot p l) in
    match tinstr with
    | MIseq l -> List.concat_map ~f:continue l
    | MIcomment s -> comment s
    | MImich _ -> assert false
    | MIpush (t, v) -> one_prim Prim.I_PUSH [mtype t; literal v]
    | MIlambda (t1, t2, c) ->
        one_prim Prim.I_LAMBDA [mtype t1; mtype t2; seq (continue c)]
    | MIexec -> one_prim Prim.I_EXEC []
    | MIdig n -> one_prim Prim.I_DIG [int (Z.of_int n)]
    | MIdug n -> one_prim Prim.I_DUG [int (Z.of_int n)]
    | MIfield [D] -> one_prim Prim.I_CDR []
    | MIfield [A] -> one_prim Prim.I_CAR []
    | MIfield op ->
        Fmt.kstrf comment "field:%S" (string_of_ad_path op)
        @ c_r_macro (string_of_ad_path op)
    | MIsetField op ->
        Fmt.kstrf comment "setField:%S" (string_of_ad_path op)
        @ set_c_r_macro (string_of_ad_path op)
    | MIcompare -> one_prim Prim.I_COMPARE []
    | MIeq -> one_prim Prim.I_EQ []
    | MImul -> one_prim Prim.I_MUL []
    | MIge -> one_prim Prim.I_GE []
    | MIlt -> one_prim Prim.I_LT []
    | MInot -> one_prim Prim.I_NOT []
    | MIneq -> one_prim Prim.I_NEQ []
    | MIle -> one_prim Prim.I_LE []
    | MIgt -> one_prim Prim.I_GT []
    | MIget -> one_prim Prim.I_GET []
    | MIadd -> one_prim Prim.I_ADD []
    | MIsub -> one_prim Prim.I_SUB []
    | MIediv -> one_prim Prim.I_EDIV []
    | MInone mt -> one_prim Prim.I_NONE [mtype mt]
    | MIsome -> one_prim Prim.I_SOME []
    | MIupdate -> one_prim Prim.I_UPDATE []
    | MIsender -> one_prim Prim.I_SENDER []
    | MIsource -> one_prim Prim.I_SOURCE []
    | MIamount -> one_prim Prim.I_AMOUNT []
    | MIbalance -> one_prim Prim.I_BALANCE []
    | MInow -> one_prim Prim.I_NOW []
    | MIcons -> one_prim Prim.I_CONS []
    | MImem -> one_prim Prim.I_MEM []
    | MIhash_key -> one_prim Prim.I_HASH_KEY []
    | MIblake2b -> one_prim Prim.I_BLAKE2B []
    | MIsha256 -> one_prim Prim.I_SHA256 []
    | MIsha512 -> one_prim Prim.I_SHA512 []
    | MIabs -> one_prim Prim.I_ABS []
    | MIneg -> one_prim Prim.I_NEG []
    | MIint -> one_prim Prim.I_INT []
    | MIisnat -> one_prim Prim.I_ISNAT []
    | MIdrop -> one_prim Prim.I_DROP []
    | MIif (t, e) -> one_prim Prim.I_IF [seq (continue t); seq (continue e)]
    | MIdup -> one_prim Prim.I_DUP []
    | MIfailwith -> one_prim Prim.I_FAILWITH []
    | MIif_some (t, e) -> one (if_some_macro (continue t) (continue e))
    | MIpair -> one_prim Prim.I_PAIR []
    | MIleft t -> one_prim Prim.I_LEFT [mtype t]
    | MIright t -> one_prim Prim.I_RIGHT [mtype t]
    | MIunpair ->
        (* UNPAIR / S => DUP ; CAR ; DIP { CDR } / S *)
        comment "unpair"
        @ [prim Prim.I_DUP []; prim Prim.I_CAR []; dip [prim Prim.I_CDR []]]
    | MIdip i -> one (dip (continue i))
    | MIdipn (n, i) -> one (diiip_macro n (continue i))
    | MIloop i -> one (loop (continue i))
    | MIswap -> one_prim Prim.I_SWAP []
    | MInil ft -> one_prim Prim.I_NIL [mtype ft]
    | MIif_left (t, e) ->
        one_prim Prim.I_IF_LEFT [seq (continue t); seq (continue e)]
    | MIerror s -> Fmt.kstrf comment "Error: %s" s
    | MIiter c -> one_prim Prim.I_ITER [seq (continue c)]
    | MImap c -> one_prim Prim.I_MAP [seq (continue c)]
    | MIand -> one_prim Prim.I_AND []
    | MIor -> one_prim Prim.I_OR []
    | MIconcat -> one_prim Prim.I_CONCAT []
    | MIslice -> one_prim Prim.I_SLICE []
    | MIsize -> one_prim Prim.I_SIZE []
    | MIpack -> one_prim Prim.I_PACK []
    | MIunpack t -> one_prim Prim.I_UNPACK [mtype t]
    | MIcontract (None, t) -> one_prim Prim.I_CONTRACT [mtype t]
    | MIcontract (Some entry_point, t) ->
        one_prim ~annot:["%" ^ entry_point] Prim.I_CONTRACT [mtype t]
    | MIself -> one_prim Prim.I_SELF []
    | MIaddress -> one_prim Prim.I_ADDRESS []
    | MIimplicit_account -> one_prim Prim.I_IMPLICIT_ACCOUNT []
    | MItransfer_tokens -> one_prim Prim.I_TRANSFER_TOKENS []
    | MIcheck_signature -> one_prim Prim.I_CHECK_SIGNATURE []
    | MIset_delegate -> one_prim Prim.I_SET_DELEGATE []

  let instruction ~options i =
    match instruction_list ~options i with
    | [one] -> one
    | more -> seq more
end

let of_smart_ml ~options smartml_contract =
  let pre_compiled = Smart_ml.Compiler.michelson_contract smartml_contract in
  let delay_error f x =
    try f x with
    | Failure s ->
        make_failwith
          ( try String.sub ~pos:0 ~len:800 s with
          | _ -> s )
    | e -> make_failwith (Exn.to_string e)
  in
  full
    ~storage:(delay_error Of_smart_ml_michelson.mtype pre_compiled.storage)
    ~code:
      (delay_error
         (Of_smart_ml_michelson.instruction ~options)
         pre_compiled.code)
    ~parameter:(delay_error Of_smart_ml_michelson.mtype pre_compiled.parameter)

let storage_initialization (smartml_contract : Smart_ml.Basics.tcontract) =
  smartml_contract.Smart_ml.Basics.storage
  |> Smart_ml.Compiler.compile_value
  |> Of_smart_ml_michelson.literal
  |> of_node

let to_string e =
  let open Caml in
  let buf = Buffer.create 1000 in
  let fmt = Format.formatter_of_buffer buf in
  Tz_client.Michelson_v1_printer.print_expr_unwrapped fmt e;
  Format.pp_print_flush fmt ();
  Buffer.contents buf

let to_json full =
  let open Tz_proto.Environment.Micheline in
  match
    Tezos_data_encoding.Data_encoding.Json.construct
      (canonical_encoding ~variant:"vaaaarrriiiant" Prim.prim_encoding)
      full
  with
  | (`O _ | `A _) as o -> (o : Ezjsonm.t)
  | _other -> Fmt.kstrf failwith "JSON-of-script-repr: not a json object"

(** Number of bytes that the contract costs. *)
let binary_size compiled =
  Tezos_data_encoding.Data_encoding.Binary.length
    Tz_proto.Script_repr.expr_encoding
    compiled
