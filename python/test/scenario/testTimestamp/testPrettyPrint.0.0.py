import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(out = False)

  @sp.entry_point
  def ep(self, params):
    self.data.out = sp.now > sp.add_seconds(sp.now, 1)