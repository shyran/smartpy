import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(multisigs = {}, nbMultisigs = 0)

  @sp.entry_point
  def build(self, params):
    self.data.multisigs[self.data.nbMultisigs] = params.contract
    self.data.nbMultisigs += 1

  @sp.entry_point
  def sign(self, params):
    sp.verify(params.id == sp.sender)
    sp.verify(params.contractName == self.data.multisigs[params.contractId].name)
    sp.for group in self.data.multisigs[params.contractId].groups:
      sp.for participant in group.participants:
        sp.if (participant.id == params.id):
          sp.verify(~ participant.hasVoted)
          participant.hasVoted = True
          group.weight += participant.weight
          group.voters += 1
          sp.if (((~ group.ok) & (group.thresholdVoters <= group.voters)) & (group.thresholdWeight <= group.weight)):
            group.ok = True
            self.data.multisigs[params.contractId].weight += group.contractWeight
            self.data.multisigs[params.contractId].groupsOK += 1
            sp.if (((~ self.data.multisigs[params.contractId].ok) & (self.data.multisigs[params.contractId].thresholdGroupsOK <= self.data.multisigs[params.contractId].groupsOK)) & (self.data.multisigs[params.contractId].thresholdWeight <= self.data.multisigs[params.contractId].weight)):
              self.data.multisigs[params.contractId].ok = True