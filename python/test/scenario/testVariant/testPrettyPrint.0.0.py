import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(r = 0, x = A(-1), y = sp.some(-42), z = Left(-10))

  @sp.entry_point
  def ep1(self, params):
    with self.data.x.match('A') as arg:
      self.data.x = variant('B', -2)
      self.data.r = arg
    self.data.x = variant('C', 3)

  @sp.entry_point
  def ep3(self, params):
    with self.data.z.match('Left') as arg:
      self.data.r = arg

  @sp.entry_point
  def ep4(self, params):
    with self.data.x.match('A') as a1:
      with self.data.z.match('Right') as a2:
        self.data.r = a1 + a2

  @sp.entry_point
  def ep5(self, params):
    sp.if self.data.x.is_variant('Toto'):
      self.data.r = 42

  @sp.entry_point
  def ep6(self, params):
    self.data.r = self.data.x.open_variant('Toto')

  @sp.entry_point
  def options(self, params):
    sp.if self.data.y.is_some():
      self.data.r = 44 + self.data.y.open_some()
      self.data.y = sp.none
    sp.else:
      self.data.r = 3
      self.data.y = sp.some(12)