import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(b0 = sp.some(sp.bytes('0xaa')), l0 = 0, l1 = 0, s0 = sp.some('hello'))

  @sp.entry_point
  def concatenating(self, params):
    self.data.s0 = sp.some(sp.concat(params.s))
    self.data.b0 = sp.some(sp.concat(sp.list([params.b0, params.b1, sp.concat(params.sb)])))

  @sp.entry_point
  def concatenating2(self, params):
    self.data.s0 = sp.some(params.s1 + params.s2)
    self.data.b0 = sp.some(params.b1 + params.b2)

  @sp.entry_point
  def slicing(self, params):
    self.data.s0 = sp.slice(params.s, 2, 5)
    self.data.b0 = sp.slice(params.b, 1, 2)
    self.data.l0 = sp.len(params.s)
    with self.data.s0.match('Some') as arg:
      self.data.l0 += sp.len(arg)
    self.data.l1 = sp.len(params.b)