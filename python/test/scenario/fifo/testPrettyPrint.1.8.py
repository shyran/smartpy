import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(fif = sp.record(first = 0, last = -1, saved = {}))

  @sp.entry_point
  def pop(self, params):
    sp.verify(self.data.fif.first < self.data.fif.last)
    del self.data.fif.saved[self.data.fif.first]
    self.data.fif.first += 1

  @sp.entry_point
  def push(self, params):
    self.data.fif.last += 1
    self.data.fif.saved[self.data.fif.last] = params