import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(administrator = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), balances = {}, paused = False, totalSupply = 0)

  @sp.entry_point
  def approve(self, params):
    sp.verify((sp.sender == self.data.administrator) | ((~ self.data.paused) & (params.f == sp.sender)))
    sp.verify((self.data.balances[params.f].approvals.get(params.t, 0) == 0) | (params.amount == 0))
    self.data.balances[params.f].approvals[params.t] = params.amount

  @sp.entry_point
  def burn(self, params):
    sp.verify(sp.sender == self.data.administrator)
    sp.verify(self.data.balances[params.address].balance >= params.amount)
    self.data.balances[params.address].balance -= params.amount
    self.data.totalSupply -= params.amount

  @sp.entry_point
  def getAdministrator(self, params):
    sp.transfer(self.data.administrator, sp.contract(sp.TAddress, params.target).open_some(), sp.tez(0))

  @sp.entry_point
  def getAllowance(self, params):
    sp.transfer(sp.as_nat(self.data.balances[params.arg.owner].approvals[params.arg.spender]), sp.contract(sp.TNat, params.target).open_some(), sp.tez(0))

  @sp.entry_point
  def getBalance(self, params):
    sp.transfer(sp.as_nat(self.data.balances[params.arg.owner].balance), sp.contract(sp.TNat, params.target).open_some(), sp.tez(0))

  @sp.entry_point
  def getTotalSupply(self, params):
    sp.transfer(sp.as_nat(self.data.totalSupply), sp.contract(sp.TNat, params.target).open_some(), sp.tez(0))

  @sp.entry_point
  def mint(self, params):
    sp.verify(sp.sender == self.data.administrator)
    sp.if (~ (self.data.balances.contains(params.address))):
      self.data.balances[params.address] = sp.record(approvals = {}, balance = 0)
    self.data.balances[params.address].balance += params.amount
    self.data.totalSupply += params.amount

  @sp.entry_point
  def setAdministrator(self, params):
    sp.verify(sp.sender == self.data.administrator)
    self.data.administrator = params

  @sp.entry_point
  def setPause(self, params):
    sp.verify(sp.sender == self.data.administrator)
    self.data.paused = params

  @sp.entry_point
  def transfer(self, params):
    sp.verify((sp.sender == self.data.administrator) | ((~ self.data.paused) & ((params.f == sp.sender) | (self.data.balances[params.f].approvals[sp.sender] >= params.amount))))
    sp.if (~ (self.data.balances.contains(params.t))):
      self.data.balances[params.t] = sp.record(approvals = {}, balance = 0)
    sp.verify(self.data.balances[params.f].balance >= params.amount)
    self.data.balances[params.f].balance -= params.amount
    self.data.balances[params.t].balance += params.amount
    sp.if ((params.f != sp.sender) & (self.data.administrator != sp.sender)):
      self.data.balances[params.f].approvals[sp.sender] -= params.amount