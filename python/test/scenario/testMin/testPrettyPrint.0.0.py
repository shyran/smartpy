import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(r = 0)

  @sp.entry_point
  def ep1(self, params):
    a = sp.local("a", 0, sp.TIntOrNat)
    b = sp.local("b", 0, sp.TIntOrNat)
    self.data.r = sp.min(a.value, b.value)
    b.drop()
    a.drop()

  @sp.entry_point
  def ep2(self, params):
    a = sp.local("a", 0, sp.TIntOrNat)
    b = sp.local("b", 0, sp.TIntOrNat)
    self.data.r = sp.max(a.value, b.value)
    b.drop()
    a.drop()