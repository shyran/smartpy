# Lists - Example for illustrative purposes only.

import smartpy as sp

class TestLists(sp.Contract):
    def __init__(self):
        self.init(a = sp.none, b = 0, c = "", d = 0, e = "", f = [], g = [])

    @sp.entry_point
    def test(self, params):
        result = sp.record(l   = params.l,
                           lr  = params.l.rev(),
                           # from maps
                           mi  = params.m.items(),
                           mir = params.m.rev_items(),
                           mk  = params.m.keys(),
                           mkr = params.m.rev_keys(),
                           mv  = params.m.values(),
                           mvr = params.m.rev_values(),
                           # from sets
                           s   = params.s.elements(),
                           sr  = params.s.rev_elements(),
                           )
        self.data.a = sp.some(result)
        self.data.b = sp.sum(result.l)
        self.data.c = sp.concat(result.mk) # string list concatenation
        self.data.d = sp.sum(result.sr)    # int    list sum
        self.data.e = ""

        # iterations
        with sp.for_('x', result.mv) as x:
            with sp.if_(sp.snd(x)):
                self.data.e += sp.fst(x)

        # ranges
        with sp.for_('i', sp.range(0, 5)) as i:
            self.data.f.push(i * i)

        self.data.g = sp.range(1, 12)

@sp.add_test(name = "Lists")
def test():
    c1 = TestLists()
    scenario = sp.test_scenario()
    scenario += c1
    scenario += c1.test(l = [1, 2, 3],
                        m = {'a' : ('aa', True),
                             'b' : ('bb', False),
                             'c' : ('cc', True)},
                        s = sp.set([1, 12, 100]))
    target = sp.record(a = sp.some(
        sp.record(l   = sp.list([1, 2, 3]),
                  lr  = sp.list([3, 2, 1]),
                  mi  =  sp.list([sp.record(key = 'a', value = ('aa', True)),
                                  sp.record(key = 'b', value = ('bb', False)),
                                  sp.record(key = 'c', value = ('cc', True))]),
                  mir = sp.list([sp.record(key = 'c', value = ('cc', True)),
                                 sp.record(key = 'b', value = ('bb', False)),
                                 sp.record(key = 'a', value = ('aa', True))]),
                  mk  = sp.list(['a', 'b', 'c']),
                  mkr = sp.list(['c', 'b', 'a']),
                  mv  = sp.list([('aa', True), ('bb', False), ('cc', True)]),
                  mvr = sp.list([('cc', True), ('bb', False), ('aa', True)]),
                  s   = sp.list([1, 12, 100]),
                  sr  = sp.list([100, 12, 1]))),
                       b = 6,
                       c = 'abc',
                       d = 113,
                       e = 'aacc',
                       f = [16, 9, 4, 1, 0],
                       g = range(1, 12))
    scenario.verify_equal(c1.data, target)

