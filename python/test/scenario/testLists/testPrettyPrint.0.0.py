import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(a = sp.none, b = 0, c = '', d = 0, e = '', f = sp.list([]), g = sp.list([]))

  @sp.entry_point
  def test(self, params):
    self.data.a = sp.some(sp.record(l = params.l, lr = params.l.rev(), mi = params.m.items(), mir = params.m.rev_items(), mk = params.m.keys(), mkr = params.m.rev_keys(), mv = params.m.values(), mvr = params.m.rev_values(), s = params.s.elements(), sr = params.s.rev_elements()))
    self.data.b = sp.sum(params.l)
    self.data.c = sp.concat(params.m.keys())
    self.data.d = sp.sum(params.s.rev_elements())
    self.data.e = ''
    sp.for x in params.m.values():
      sp.if sp.snd(x):
        self.data.e += sp.fst(x)
    sp.for i in sp.range(0, 5):
      self.data.f.push(i * i)
    self.data.g = sp.range(1, 12)