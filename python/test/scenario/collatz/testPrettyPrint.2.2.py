import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(counter = 0, onEven = sp.address('KT1GA7as5gMhU51cj9899cpXNDNLGCC2gFUC'), onOdd = sp.address('KT1LuGLEUp2xazHiEaDUAiKQYAXK26AQgynV'))

  @sp.entry_point
  def another_entry_point(self, params):
    pass

  @sp.entry_point
  def run(self, params):
    sp.if (params > 1):
      self.data.counter += 1
      sp.if ((params % 2) == 0):
        sp.transfer(sp.record(k = sp.self_entry_point, x = params), sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat), self.data.onEven).open_some(), sp.tez(0))
      sp.else:
        sp.transfer(sp.record(k = sp.self_entry_point, x = params), sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat), self.data.onOdd).open_some(), sp.tez(0))