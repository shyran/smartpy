import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(value = 0, ggg = sp.some(42), fff = sp.none)

    @sp.entry_point
    def f(self, params):
        sp.local('toto', sp.build_lambda(lambda x: x + 123))

    @sp.entry_point
    def h(self, params):
        def sqrt(x):
            sp.verify(x >= 0)
            y = sp.local('y', x)
            with sp.while_(y.value * y.value > x):
                y.value = (x // y.value + y.value) // 2
            sp.verify((y.value * y.value <= x) & (x < (y.value + 1) * (y.value + 1)))
            return y.value
        self.data.fff = sp.some(sp.build_lambda(sqrt))

    @sp.entry_point
    def hh(self, params):
        self.data.value = self.data.fff.open_some().call(params)

    @sp.entry_point
    def i(self, params):
        def check1(x):
            sp.verify(x >= 0)
        def check2(x):
            sp.verify(x >= 0)
            return x - 2
        def check3(x):
            sp.verify(x >= 0)
            return True
        def check4(x):
            def check3bis(x):
                sp.verify(x >= 0)
                return False
            sp.local('ch3b', sp.build_lambda(check3bis))
            sp.verify(x >= 0)
            return 3 * x
        sp.local('ch1', sp.build_lambda(check1))
        sp.local('ch2', sp.build_lambda(check2))
        sp.local('ch3', sp.build_lambda(check3))
        ch4 = sp.local('ch4', sp.build_lambda(check4))
        self.data.value = ch4.value.call(12)

@sp.add_test(name = "Lambdas")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract()
    scenario += c1

