import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(fff = sp.none, ggg = sp.some(42), value = 0)

  @sp.entry_point
  def f(self, params):
    toto = sp.local("toto", sp.build_lambda('???'), sp.TLambda(sp.TIntOrNat, sp.TIntOrNat))
    toto.drop()

  @sp.entry_point
  def h(self, params):
    self.data.fff = sp.some(sp.build_lambda('???'))

  @sp.entry_point
  def hh(self, params):
    self.data.value = self.data.fff.open_some().call(params)

  @sp.entry_point
  def i(self, params):
    ch1 = sp.local("ch1", sp.build_lambda('???'), sp.TLambda(sp.TIntOrNat, sp.TUnit))
    ch2 = sp.local("ch2", sp.build_lambda('???'), sp.TLambda(sp.TIntOrNat, sp.TInt))
    ch3 = sp.local("ch3", sp.build_lambda('???'), sp.TLambda(sp.TIntOrNat, sp.TBool))
    ch4 = sp.local("ch4", sp.build_lambda('???'), sp.TLambda(sp.TNat, sp.TNat))
    self.data.value = ch4.value.call(12)
    ch4.drop()
    ch3.drop()
    ch2.drop()
    ch1.drop()