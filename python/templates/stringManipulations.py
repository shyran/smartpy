# Strings and Bytes - Example for illustrative purposes only.

import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(s0 = sp.some("hello"),
                  b0 = sp.some(sp.bytes("0xAA")),
                  l0 = 0, l1 = 0)

    @sp.entry_point
    def concatenating(self, params):
        # Concatenating a list of strings or a list of bytes
        self.data.s0 = sp.some(sp.concat(params.s))
        self.data.b0 = sp.some(sp.concat([params.b0, params.b1, sp.concat(params.sb)]))

    @sp.entry_point
    def concatenating2(self, params):
        # Concatenating a two strings or two bytes
        self.data.s0 = sp.some(params.s1 + params.s2)
        self.data.b0 = sp.some(params.b1 + params.b2)

    @sp.entry_point
    def slicing(self, params):
        # Slicing a string or a byte (this returns an option)
        self.data.s0 = sp.slice(params.s, 2, 5)
        self.data.b0 = sp.slice(params.b, 1, 2)

        # Computing length with sp.len
        self.data.l0 = sp.len(params.s)
        with self.data.s0.match("Some") as arg:
            self.data.l0 += sp.len(arg)
        self.data.l1 = sp.len(params.b)

# Tests
@sp.add_test(name = "Welcome")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract()
    scenario += c1

    scenario += c1.slicing(s = "012345678901234567890123456789", b = sp.bytes("0xAA00BBCC"))
    scenario.verify_equal(c1.data.s0, sp.some("23456"))
    scenario.verify_equal(c1.data.b0, sp.some(sp.bytes("0x00BB")))
    scenario.verify_equal(c1.data.l0, 35)
    scenario.verify_equal(c1.data.l1, 4)
    scenario += c1.slicing(s = "01", b = sp.bytes("0xCC"))
    scenario.verify_equal(c1.data.s0, sp.none)
    # This one fails on sandbox because of missing type:
    scenario.verify_equal(c1.data.b0, sp.none)

    scenario += c1.concatenating( s  = ["01", "234", "567", "89"],
                                  sb = [sp.bytes('0x1234'), sp.bytes('0x'), sp.bytes('0x4567aabbCCDD')],
                                  b0 = sp.bytes("0x11"),
                                  b1 = sp.bytes("0x223344"))
    scenario.verify_equal(c1.data.s0, sp.some("0123456789"))
    scenario.verify_equal(c1.data.b0, sp.some(sp.bytes('0x1122334412344567AABBCCDD')))

    scenario += c1.concatenating2(s1 = "abc", s2 = "def", b1 = sp.bytes("0xaaaa"), b2 = sp.bytes("0xab"))

    scenario.show(c1.data)
    scenario.verify_equal(c1.data, sp.record(b0 = sp.some(sp.bytes("0xaaaaab")), l0 = 2, l1 = 1, s0 = sp.some('abcdef')))
