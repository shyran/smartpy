import smartpy as sp

class C(sp.Contract):
    def __init__(self): self.init(out = False)

    @sp.entry_point
    def ep(self, params): self.data.out = sp.now > sp.now.add_seconds(1)

@sp.add_test(name = "Timestamp")
def test():
    scenario  = sp.test_scenario()
    scenario += C()
