import smartpy as sp
import smartpy_michelson as mi

class MyContract(sp.Contract):
    def __init__(self):
        self.init(value = 0, s = '')

    @sp.entry_point
    def add(self, params):
        self.data.value = abs(mi.ADD(15, 16))

    @sp.entry_point
    def concat(self, params):
        concat = mi.operator("CONCAT", [sp.TList(sp.TString)], [sp.TString])
        self.data.s = concat(["a", "b", "c"])

    @sp.entry_point
    def seq(self, params):
        self.data.value = abs(mi.seq([mi.ADD(), mi.MUL(), mi.DUP(), mi.MUL()], 15, 16, 17))

@sp.add_test(name = "Inline Michelson")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract()
    scenario += c1
