# Collatz, calling other contracts - Example for illustrative purposes only.

import smartpy as sp

# Compute the length of the nth Collatz sequence
# (https://oeis.org/A006577) with on-chain continuations

def call(c, x):
    sp.transfer(x, sp.mutez(0), c)

class OnEven(sp.Contract):
    @sp.entry_point
    def run(self, params):
        call(params.k, params.x / 2)

class OnOdd(sp.Contract):
    @sp.entry_point
    def run(self, params):
        call(params.k, 3 * sp.nat(params.x) + 1)

class Collatz(sp.Contract):
    def __init__(self, onEven, onOdd):
        self.init(onEven  = onEven,
                  onOdd   = onOdd,
                  counter = 0)

    @sp.entry_point
    def run(self, x):
        tk    = sp.TRecord(k = sp.type_of(sp.self_entry_point()), x = sp.TNat)

        onEven = sp.contract(tk, self.data.onEven).open_some()
        onOdd  = sp.contract(tk, self.data.onOdd ).open_some()

        sp.if x > 1:
            self.data.counter += 1
            sp.if x % 2 == 0:
                call(onEven, sp.record(x = x, k = sp.self_entry_point()))
            sp.else:
                call(onOdd , sp.record(x = x, k = sp.self_entry_point()))

    @sp.entry_point
    def another_entry_point(self, params):
        pass

@sp.add_test(name = "Collatz")
def test():
    scenario = sp.test_scenario()
    scenario += OnEven()
    scenario += OnOdd()
    scenario += Collatz(onEven = sp.address("KT1GA7as5gMhU51cj9899cpXNDNLGCC2gFUC"),
                        onOdd  = sp.address("KT1LuGLEUp2xazHiEaDUAiKQYAXK26AQgynV"))
