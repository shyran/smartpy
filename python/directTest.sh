#! /bin/sh

set -e

BASEDIR=$(dirname "$0")
smpypy=`cd $BASEDIR; pwd`

$smpypy/smartpybasic.sh $smpypy/templates/tictactoe.py  "TicTacToe()"  /tmp/directTest
$smpypy/smartpybasic.sh $smpypy/templates/calculator.py "Calculator()" /tmp/directTest
